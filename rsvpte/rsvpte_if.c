/*
 *  Copyright (C) James R. Leu 2003
 *  jleu@mindspring.com
 *
 *  This software is covered under the LGPL, for more
 *  info check out http://www.gnu.org/copyleft/lgpl.html
 */

#include "rsvpte_struct.h"
#include "rsvpte_if.h"

#include "mpls_assert.h"
#include "mpls_mm_impl.h"
#include "mpls_socket_impl.h"
#include "mpls_timer_impl.h"
#include "mpls_ifmgr_impl.h"
#include "mpls_trace_impl.h"

uint32_t _rsvpte_if_next_index = 1;

rsvpte_if *rsvpte_if_create()
{
  rsvpte_if *i = (rsvpte_if *) mpls_malloc(sizeof(rsvpte_if));

  if (i) {
    memset(i, 0, sizeof(rsvpte_if));
    MPLS_REFCNT_INIT(i, 0);
    MPLS_LIST_ELEM_INIT(i, _global);
    i->label_space = -1;
    i->index = _rsvpte_if_get_next_index();
    i->oper_state = MPLS_OPER_DOWN;
    i->admin_state = MPLS_ADMIN_DISABLE;
  }
  return i;
}

void rsvpte_if_delete(rsvpte_if * i)
{
  LDP_PRINT(g->user_data,"if delete\n");
  MPLS_REFCNT_ASSERT(i, 0);
  mpls_free(i);
}

mpls_return_enum rsvpte_if_startup(rsvpte_global * g, rsvpte_if * i)
{
  LDP_ENTER(g->user_data, "rsvpte_if_startup");

  MPLS_ASSERT(i != NULL);

  i->oper_state = MPLS_OPER_UP;
  i->admin_state = MPLS_ADMIN_ENABLE;

  LDP_EXIT(g->user_data, "rsvpte_if_startup");

  return MPLS_SUCCESS;

  /*
   * when a interface update comes in, it will search the global 
   * list of interfaces, and start up the interface then
   */
  i->oper_state = MPLS_OPER_DOWN;

  LDP_EXIT(g->user_data, "rsvpte_if_startup-delayed");

  return MPLS_SUCCESS;
}

mpls_return_enum rsvpte_if_shutdown(rsvpte_global * g, rsvpte_if * i)
{
  MPLS_ASSERT(i != NULL);

  LDP_ENTER(g->user_data, "rsvpte_if_shutdown");

  i->oper_state = MPLS_OPER_DOWN;
  i->admin_state = MPLS_ADMIN_DISABLE;

  LDP_EXIT(g->user_data, "rsvpte_if_shutdown");

  return MPLS_SUCCESS;
}

uint32_t _rsvpte_if_get_next_index()
{
  uint32_t retval = _rsvpte_if_next_index;

  _rsvpte_if_next_index++;
  if (retval > _rsvpte_if_next_index) {
    _rsvpte_if_next_index = 1;
  }
  return retval;
}

mpls_bool rsvpte_if_is_active(rsvpte_if * i)
{
  MPLS_ASSERT(i);
  if (i->oper_state == MPLS_OPER_UP)
    return MPLS_BOOL_TRUE;

  return MPLS_BOOL_FALSE;
}

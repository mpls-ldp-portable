/*
 *  Copyright (C) James R. Leu 2003
 *  jleu@mindspring.com
 *
 *  This software is covered under the LGPL, for more
 *  info check out http://www.gnu.org/copyleft/lgpl.html
 */

#include "rsvpte_struct.h"
#include "rsvpte_cfg.h"
#include "rsvpte_global.h"
#include "rsvpte_if.h"
#include "mpls_ifmgr_impl.h"
#include "mpls_lock_impl.h"
#include "mpls_trace_impl.h"
#include "mpls_tree_impl.h"

mpls_cfg_handle rsvpte_cfg_open(mpls_instance_handle data)
{
  rsvpte_global *g = rsvpte_global_create(data);

  LDP_ENTER(data, "rsvpte_cfg_open");
  LDP_EXIT(data, "rsvpte_cfg_open");

  return (mpls_cfg_handle) g;
}

void rsvpte_cfg_close(mpls_cfg_handle g)
{
  LDP_ENTER((mpls_instance_handle) g->user_data, "rsvpte_cfg_close");
  rsvpte_global_delete(g);
  LDP_EXIT((mpls_instance_handle) g->user_data, "rsvpte_cfg_close");
}

/******************* GLOBAL **********************/

mpls_return_enum rsvpte_cfg_global_get(mpls_cfg_handle handle, rsvpte_global * g,
  uint32_t flag)
{
  rsvpte_global *global = (rsvpte_global *) handle;

  MPLS_ASSERT(global !=NULL);

  LDP_ENTER(global->user_data, "rsvpte_cfg_global_get");

  mpls_lock_get(global->global_lock); /* LOCK */

  if (flag & RSVPTE_GLOBAL_CFG_LSR_IDENTIFIER) {
    memcpy(&(g->lsr_identifier), &(global->lsr_identifier),
      sizeof(mpls_inet_addr));
  }
  if (flag & RSVPTE_GLOBAL_CFG_ADMIN_STATE) {
    g->admin_state = global->admin_state;
  }
#if MPLS_USE_LSR
  if (flag & RSVPTE_GLOBAL_CFG_LSR_HANDLE) {
    g->lsr_handle = global->lsr_handle;
  }
#endif

  mpls_lock_release(global->global_lock); /* UNLOCK */

  LDP_EXIT(global->user_data, "rsvpte_cfg_global_get");

  return MPLS_SUCCESS;
}

mpls_return_enum rsvpte_cfg_global_test(mpls_cfg_handle handle, rsvpte_global * g,
  uint32_t flag)
{
  rsvpte_global *global = (rsvpte_global *) handle;
  mpls_return_enum retval = MPLS_SUCCESS;

  MPLS_ASSERT(global !=NULL);

  LDP_ENTER(global->user_data, "rsvpte_cfg_global_test");

  mpls_lock_get(global->global_lock); /* LOCK */

  if (global->admin_state == MPLS_ADMIN_ENABLE && (flag & RSVPTE_GLOBAL_CFG_WHEN_DOWN))
    retval = MPLS_FAILURE;

  mpls_lock_release(global->global_lock); /* UNLOCK */

  LDP_EXIT(global->user_data, "rsvpte_cfg_global_test");

  return retval;
}

mpls_return_enum rsvpte_cfg_global_set(mpls_cfg_handle handle, rsvpte_global * g,
  uint32_t flag)
{
  rsvpte_global *global = (rsvpte_global *) handle;
  mpls_return_enum retval = MPLS_FAILURE;

  MPLS_ASSERT(global !=NULL);

  LDP_ENTER(global->user_data, "rsvpte_cfg_global_set");

  mpls_lock_get(global->global_lock); /* LOCK */

  if ((global->admin_state == MPLS_ADMIN_ENABLE && (flag & RSVPTE_GLOBAL_CFG_WHEN_DOWN)))
    goto rsvpte_cfg_global_set_end;

  if (flag & RSVPTE_GLOBAL_CFG_LSR_IDENTIFIER) {
    memcpy(&(global->lsr_identifier), &(g->lsr_identifier),
      sizeof(mpls_inet_addr));
  }
#if MPLS_USE_LSR
  if (flag & RSVPTE_GLOBAL_CFG_LSR_HANDLE) {
    global->lsr_handle = g->lsr_handle ;
  }
#endif
  if (flag & RSVPTE_GLOBAL_CFG_ADMIN_STATE) {
    if (global->admin_state == MPLS_ADMIN_ENABLE && g->admin_state == MPLS_ADMIN_DISABLE) {
      rsvpte_global_shutdown(global);
    } else if (global->admin_state == MPLS_ADMIN_DISABLE && g->admin_state ==
      MPLS_ADMIN_ENABLE) {
      rsvpte_global_startup(global);
    }
  }

  retval = MPLS_SUCCESS;

rsvpte_cfg_global_set_end:

  mpls_lock_release(global->global_lock); /* UNLOCK */

  LDP_EXIT(global->user_data, "rsvpte_cfg_global_set");

  return retval;
}

/******************* INTERFACE **********************/

mpls_return_enum rsvpte_cfg_if_get(mpls_cfg_handle handle, rsvpte_if * i, uint32_t flag)
{
  rsvpte_global *global = (rsvpte_global *) handle;
  rsvpte_if *iff = NULL;
  mpls_return_enum retval = MPLS_FAILURE;

  MPLS_ASSERT(global !=NULL && i != NULL);

  LDP_ENTER(global->user_data, "rsvpte_cfg_if_get");

  mpls_lock_get(global->global_lock); /* LOCK */

  if (rsvpte_global_find_if_index(global, i->index, &iff) != MPLS_SUCCESS)
      goto rsvpte_cfg_if_get_end;

  if (flag & RSVPTE_IF_CFG_LABEL_SPACE) {
    i->label_space = iff->label_space;
  }
  if (flag & RSVPTE_IF_CFG_ADMIN_STATE) {
    i->admin_state = iff->admin_state;
  }
  if (flag & RSVPTE_IF_CFG_OPER_STATE) {
    i->oper_state = iff->oper_state;
  }
  retval = MPLS_SUCCESS;

rsvpte_cfg_if_get_end:
  mpls_lock_release(global->global_lock); /* UNLOCK */

  LDP_EXIT(global->user_data, "rsvpte_cfg_if_get");

  return retval;
}

mpls_return_enum rsvpte_cfg_if_getnext(mpls_cfg_handle handle, rsvpte_if * i,
  uint32_t flag)
{
  rsvpte_global *g = (rsvpte_global *) handle;
  rsvpte_if *iff = NULL;
  mpls_return_enum r = MPLS_FAILURE;
  mpls_bool done = MPLS_BOOL_FALSE;
  int index;

  LDP_ENTER(g->user_data, "rsvpte_cfg_if_getnext");

  if (i->index == 0) {
    index = 1;
  } else {
    index = i->index + 1;
  }

  mpls_lock_get(g->global_lock); /* LOCK */
  while (done == MPLS_BOOL_FALSE) {
    switch ((r = rsvpte_global_find_if_index(g, index, &iff))) {
      case MPLS_SUCCESS:
      case MPLS_END_OF_LIST:
        done = MPLS_BOOL_TRUE;
        break;
      case MPLS_FAILURE:
        break;
      default:
        MPLS_ASSERT(0);
    }
    index++;
  }
  mpls_lock_release(g->global_lock); /* UNLOCK */

  if (r == MPLS_SUCCESS) {
    i->index = iff->index;
    LDP_EXIT(g->user_data, "rsvpte_cfg_if_getnext");
    return rsvpte_cfg_if_get(g, i, flag);
  }
  LDP_EXIT(g->user_data, "rsvpte_cfg_if_getnext");
  return r;
}

mpls_return_enum rsvpte_cfg_if_test(mpls_cfg_handle handle, rsvpte_if * i,
  uint32_t flag)
{
  rsvpte_global *global = (rsvpte_global *) handle;
  rsvpte_if *iff = NULL;
  mpls_return_enum retval = MPLS_FAILURE;

  MPLS_ASSERT(global !=NULL && i != NULL);

  LDP_ENTER(global->user_data, "rsvpte_cfg_if_test");

  mpls_lock_get(global->global_lock); /* LOCK */

  if (!(flag & RSVPTE_CFG_ADD)) {
    rsvpte_global_find_if_index(global, i->index, &iff);
  } else {
    retval = MPLS_SUCCESS;
    goto rsvpte_cfg_if_test_end;
  }

  if ((iff == NULL) ||
    ((rsvpte_if_is_active(iff) == MPLS_BOOL_TRUE) &&
(flag & RSVPTE_IF_CFG_WHEN_DOWN))) goto rsvpte_cfg_if_test_end;

  if (flag & RSVPTE_CFG_DEL) {
	  /*
    if (iff->entity != NULL)
      goto rsvpte_cfg_if_test_end;
      	   */
  }
  retval = MPLS_SUCCESS;

rsvpte_cfg_if_test_end:
  mpls_lock_release(global->global_lock); /* UNLOCK */

  LDP_EXIT(global->user_data, "rsvpte_cfg_if_test");

  return retval;
}

mpls_return_enum rsvpte_cfg_if_set(mpls_cfg_handle handle, rsvpte_if * i, uint32_t flag)
{
  rsvpte_global *global = (rsvpte_global*)handle;
  rsvpte_if *iff = NULL;
  mpls_return_enum retval = MPLS_FAILURE;

  MPLS_ASSERT(global !=NULL && i != NULL);

  LDP_ENTER(global->user_data, "rsvpte_cfg_if_set");

  mpls_lock_get(global->global_lock); /* LOCK */

  if (flag & RSVPTE_CFG_ADD) {
    if ((iff = rsvpte_if_create()) == NULL) {
      goto rsvpte_cfg_if_set_end;
    }
    if (mpls_if_handle_verify(global->ifmgr_handle, i->handle) == MPLS_BOOL_FALSE) {
      goto rsvpte_cfg_if_set_end;
    }
    iff->handle = i->handle;
    i->index = iff->index;
    _rsvpte_global_add_if(global, iff);
  } else {
    rsvpte_global_find_if_index(global, i->index, &iff);
  }

  if ((iff == NULL) ||
    ((rsvpte_if_is_active(iff) == MPLS_BOOL_TRUE) &&
(flag & RSVPTE_IF_CFG_WHEN_DOWN))) goto rsvpte_cfg_if_set_end;

  if (flag & RSVPTE_CFG_DEL) {
    _rsvpte_global_del_if(global, iff);

    retval = MPLS_SUCCESS;
    goto rsvpte_cfg_if_set_end;
  }
  if (flag & RSVPTE_IF_CFG_LABEL_SPACE) {
    iff->label_space = i->label_space;
  }
  if (flag & RSVPTE_IF_CFG_ADMIN_STATE) {
    if (iff->admin_state == MPLS_ADMIN_ENABLE && i->admin_state ==
      MPLS_ADMIN_DISABLE) {
      rsvpte_if_shutdown(global, iff);
    } else if (iff->admin_state == MPLS_ADMIN_DISABLE && i->admin_state ==
      MPLS_ADMIN_ENABLE) {
      rsvpte_if_startup(global, iff);
    }
  }

  retval = MPLS_SUCCESS;

rsvpte_cfg_if_set_end:
  mpls_lock_release(global->global_lock); /* UNLOCK */

  LDP_EXIT(global->user_data, "rsvpte_cfg_if_set");

  return retval;
}

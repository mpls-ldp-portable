/*
 *  Copyright (C) James R. Leu 2003
 *  jleu@mindspring.com
 *
 *  This software is covered under the LGPL, for more
 *  info check out http://www.gnu.org/copyleft/lgpl.html
 */

#ifndef RSVPTE_STRUCT_H
#define RSVPTE_STRUCT_H

#include "mpls_struct.h"
#include "mpls_list.h"
#include "mpls_refcnt.h"
#include "mpls_bitfield.h"

MPLS_LIST_ROOT(rsvpte_if_list, rsvpte_if);

typedef struct rsvpte_global {
    struct rsvpte_if_list iff;

    struct mpls_inet_addr lsr_identifier;
    mpls_admin_state_enum admin_state;

    mpls_lock_handle global_lock;
    mpls_instance_handle user_data;

    mpls_tree_handle addr_tree;

    mpls_socket_handle ra_socket;

    mpls_timer_mgr_handle timer_handle;
    mpls_socket_mgr_handle socket_handle;
    mpls_fib_handle fib_handle;
    mpls_ifmgr_handle ifmgr_handle;

#if MPLS_USE_LSR
    mpls_cfg_handle lsr_handle;
#else
    mpls_mpls_handle mpls_handle;
#endif
} rsvpte_global;

typedef struct rsvpte_if {
    MPLS_REFCNT_FIELD;
    MPLS_LIST_ELEM(rsvpte_if) _global;
    int label_space;
    uint32_t index;
    mpls_if_handle handle;

    mpls_oper_state_enum oper_state;
    mpls_admin_state_enum admin_state;
} rsvpte_if;

#define RSSPV_RSVP_ENC_OBJECTERROR -1
#define RSSPV_RSVP_DEC_OBJECTERROR -2

#define RSVP_IPV4_ADDR_LEN 4
#define RSVP_IPV6_ADDR_LEN 16

/**********************************************************************
    RSVP Objects

         Every object consists of one or more 32-bit words with a one-
         word header, with the following format:

                0             1              2             3
         +-------------+-------------+-------------+-------------+
         |       Length (bytes)      |  Class-Num  |   C-Type    |
         +-------------+-------------+-------------+-------------+
         |                                                       |
         //                  (Object contents)                   //
         |                                                       |
         +-------------+-------------+-------------+-------------+

 An object header has the following fields:

         Length

              A 16-bit field containing the total object length in
              bytes.  Must always be a multiple of 4, and at least 4.

         Class-Num

              Identifies the object class; values of this field are
              defined in Appendix A.  Each object class has a name,
              which is always capitalized in this document.  An RSVP
              implementation must recognize the following classes:

              NULL

                   A NULL object has a Class-Num of zero, and its C-Type
                   is ignored.  Its length must be at least 4, but can
                   be any multiple of 4.  A NULL object may appear
                   anywhere in a sequence of objects, and its contents
                   will be ignored by the receiver.

              SESSION

                   Contains the IP destination address (DestAddress),
                   the IP protocol id, and some form of generalized
                   destination port, to define a specific session for
                   the other objects that follow.  Required in every
                   RSVP message.

              RSVP_HOP

                   Carries the IP address of the RSVP-capable node that
                   sent this message and a logical outgoing interface
                   handle (LIH; see Section 3.3).  This document refers
                   to a RSVP_HOP object as a PHOP ("previous hop")
                   object for downstream messages or as a NHOP (" next
                   hop") object for upstream messages.

              TIME_VALUES

                   Contains the value for the refresh period R used by
                   the creator of the message; see Section 3.7.
                   Required in every Path and Resv message.

              STYLE

                   Defines the reservation style plus style-specific
                   information that is not in FLOWSPEC or FILTER_SPEC
                   objects.  Required in every Resv message.

              FLOWSPEC

                   Defines a desired QoS, in a Resv message.

              FILTER_SPEC

                   Defines a subset of session data packets that should
                   receive the desired QoS (specified by a FLOWSPEC
                   object), in a Resv message.

              SENDER_TEMPLATE

                   Contains a sender IP address and perhaps some
                   additional demultiplexing information to identify a
                   sender.  Required in a Path message.

              SENDER_TSPEC

                   Defines the traffic characteristics of a sender's
                   data flow.  Required in a Path message.

              ADSPEC

                   Carries OPWA data, in a Path message.

              ERROR_SPEC

                   Specifies an error in a PathErr, ResvErr, or a
                   confirmation in a ResvConf message.

              POLICY_DATA

                   Carries information that will allow a local policy
                   module to decide whether an associated reservation is
                   administratively permitted.  May appear in Path,
                   Resv, PathErr, or ResvErr message.

                   The use of POLICY_DATA objects is not fully specified
                   at this time; a future document will fill this gap.

              INTEGRITY

                   Carries cryptographic data to authenticate the
                   originating node and to verify the contents of this
                   RSVP message.  The use of the INTEGRITY object is
                   described in [Baker96].

              SCOPE

                   Carries an explicit list of sender hosts towards
                   which the information in the message is to be
                   forwarded.  May appear in a Resv, ResvErr, or
                   ResvTear message.  See Section 3.4.

              RESV_CONFIRM

                   Carries the IP address of a receiver that requested a
                   confirmation.  May appear in a Resv or ResvConf
                   message.

**********************************************************************/

#define RSVP_OBJECT_HDRSIZE 4

typedef struct rsvpObjectHeader_s {
    u_short length;
    u_char class;
    u_char cType;
} rsvpObjectHeader_t;


#define RSVP_SESSION_CLASS 1
#define RSVP_SESSION_CTYPE_IPV4 1
#define RSVP_SESSION_CTYPE_IPV6 2
#define RSVP_SESSION_CTYPE_TUNNEL4 7
#define RSVP_SESSION_CTYPE_TUNNEL6 8

/**********************************************************************
    RSVP SESSION Object 

     o IPv4/UDP SESSION object: Class = 1, C-Type = 1
 
    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +---------------+---------------+---------------+---------------+
   |                  IPv4 DestAddress (4 bytes)                   |
   +---------------+---------------+---------------+---------------+
   | Protocol Id   |     Flags     |            DstPort            |
   +---------------+---------------+---------------+---------------+

     o IPv6/UDP SESSION object: Class = 1, C-Type = 2

    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +---------------+---------------+---------------+---------------+
   |                                                               |
   +                                                               +
   |                                                               |
   +                   IPv6 DestAddress (16 bytes)                 +
   |                                                               |
   +                                                               +
   |                                                               |
   +---------------+---------------+---------------+---------------+
   |  Protocol Id  |     Flags     |           DstPort             |
   +---------------+---------------+---------------+---------------+

    DestAddress

           The IP unicast or multicast destination address of the
           session.  This field must be non-zero.

    Protocol Id

           The IP Protocol Identifier for the data flow.  This field
           must be non-zero.
    Flags

           0x01 = E_Police flag

                The E_Police flag is used in Path messages to determine
                the effective "edge" of the network, to control traffic
                policing.  If the sender host is not itself capable of
                traffic policing, it will set this bit on in Path
                messages it sends.  The first node whose RSVP is capable
                of traffic policing will do so (if appropriate to the
                service) and turn the flag off.

     DstPort

           The UDP/TCP destination port for the session.  Zero may be
           used to indicate `none'.

           Other SESSION C-Types could be defined in the future to
           support other demultiplexing conventions in the transport-
           layer or application layer.

   LSP_TUNNEL_IPv4 Session Object

   Class = SESSION, LSP_TUNNEL_IPv4 C-Type = 7

    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                   IPv4 tunnel end point address               |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |  MUST be zero                 |      Tunnel ID                |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                       Extended Tunnel ID                      |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

      IPv4 tunnel end point address

         IPv4 address of the egress node for the tunnel.

      Tunnel ID

         A 16-bit identifier used in the SESSION that remains constant
         over the life of the tunnel.

      Extended Tunnel ID

         A 32-bit identifier used in the SESSION that remains constant
         over the life of the tunnel.  Normally set to all zeros.
         Ingress nodes that wish to narrow the scope of a SESSION to the
         ingress-egress pair may place their IPv4 address here as a
         globally unique identifier.


   LSP_TUNNEL_IPv6 Session Object

   Class = SESSION, LSP_TUNNEL_IPv6 C_Type = 8

    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                                                               |
   +                                                               +
   |                   IPv6 tunnel end point address               |
   +                                                               +
   |                            (16 bytes)                         |
   +                                                               +
   |                                                               |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |  MUST be zero                 |      Tunnel ID                |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                                                               |
   +                                                               +
   |                       Extended Tunnel ID                      |
   +                                                               +
   |                            (16 bytes)                         |
   +                                                               +
   |                                                               |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

      IPv6 tunnel end point address

         IPv6 address of the egress node for the tunnel.

      Tunnel ID

         A 16-bit identifier used in the SESSION that remains constant
         over the life of the tunnel.

      Extended Tunnel ID

         A 16-byte identifier used in the SESSION that remains constant
         over the life of the tunnel.  Normally set to all zeros.
         Ingress nodes that wish to narrow the scope of a SESSION to the
         ingress-egress pair may place their IPv6 address here as a
         globally unique identifier.

**********************************************************************/

#define RSVP_SESSION4_SIZE 8

typedef struct rsvpSession4_s {
  u_int destAddr;
  u_char protocolId;
  u_char flags;
  u_short destPort;
} rsvpSession4_t;

#define RSVP_SESSION6_SIZE 20

typedef struct rsvpSession6_s {
  u_char destAddr[16];
  u_char protocolId;
  u_char flags;
  u_short destPort;
} rsvpSession6_t;

#define RSVP_SESSION_TUNNEL4_SIZE 12

typedef struct rsvpSessionTunnel4_s {
    u_int endPointAddress;
    u_short reserved;
    u_short tunnelId;
    u_int extentedTunnelId;
} rsvpSessionTunnel4_t;

#define RSVP_SESSION_TUNNEL6_SIZE 36

typedef struct rsvpSessionTunnel6_s {
    u_char endPointAddress[16];
    u_short reserved;
    u_short tunnelId;
    u_char extentedTunnelId[16];
} rsvpSessionTunnel6_t;

typedef struct rsvpSessionObject_s {
    struct rsvpObjectHeader_s hdr;
    union {
	struct rsvpSession4_s ipv4;
	struct rsvpSession6_s ipv6;
	struct rsvpSessionTunnel4_s tunnel4;
	struct rsvpSessionTunnel6_s tunnel6;
    } u;
} rsvpSessionObject_t;

#define RSVP_HOP_CLASS 3
#define RSVP_HOP_CTYPE_IPV4 1
#define RSVP_HOP_CTYPE_IPV6 2

/**********************************************************************
    RSVP HOP Object

     o    IPv4 RSVP_HOP object: Class = 3, C-Type = 1

    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1

   +---------------+---------------+---------------+---------------+
   |                 IPv4 Next/Previous Hop Address                |
   +---------------+---------------+---------------+---------------+
   |                     Logical Interface Handle                  |
   +---------------+---------------+---------------+---------------+

     o    IPv6 RSVP_HOP object: Class = 3, C-Type = 2

    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1

   +---------------+---------------+---------------+---------------+
   |                                                               |
   +                                                               +
   |                                                               |
   +                 IPv6 Next/Previous Hop Address                +
   |                                                               |
   +                                                               +
   |                                                               |
   +---------------+---------------+---------------+---------------+
   |                    Logical Interface Handle                   |
   +---------------+---------------+---------------+---------------+

      This object carries the IP address of the interface through which
      the last RSVP-knowledgeable hop forwarded this message.  The
      Logical Interface Handle (LIH) is used to distinguish logical
      outgoing interfaces, as discussed in Sections 3.3 and 3.9.  A node
      receiving an LIH in a Path message saves its value and returns it
      in the HOP objects of subsequent Resv messages sent to the node
      that originated the LIH.  The LIH should be identically zero if
      there is no logical interface handle.

**********************************************************************/

#define RSVP_HOP4_SIZE 8

typedef struct rsvpHop4_s {
  u_int hopAddr;
  u_int logicalIfHandle;
} rsvpHop4_t;

#define RSVP_HOP6_SIZE 20

typedef struct rsvpHop6_s {
  u_char hopAddr[16];
  u_int logicalIfHandle;
} rsvpHop6_t;

typedef struct rsvpHopObject_s {
    struct rsvpObjectHeader_s hdr;
    union {
	struct rsvpHop4_s ipv4;
	struct rsvpHop6_s ipv6;
    } u;
} rsvpHopObject_t;

#define RSVP_TIME_CLASS 5
#define RSVP_TIME_CTYPE 1

/**********************************************************************
    RSVP TIME Object

     o    TIME_VALUES Object: Class = 5, C-Type = 1

    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +---------------+---------------+---------------+---------------+
   |                       Refresh Period R                        |
   +---------------+---------------+---------------+---------------+

      Refresh Period

           The refresh timeout period R used to generate this message;
           in milliseconds.

**********************************************************************/

#define RSVP_TIME_SIZE 4

typedef struct rsvpTimeObject_s {
    struct rsvpObjectHeader_s hdr;
    u_int refresh;
} rsvpTimeObject_t;

#define RSVP_ERROR_SPEC_CLASS 6
#define RSVP_ERROR_SPEC_CTYPE_IPV4 1
#define RSVP_ERROR_SPEC_CTYPE_IPV6 2

/**********************************************************************
    RSVP ERROR_SPEC Object

     o    IPv4 ERROR_SPEC object: Class = 6, C-Type = 1

    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +---------------+---------------+---------------+---------------+
   |                IPv4 Error Node Address (4 bytes)              |
   +---------------+---------------+---------------+---------------+
   |     Flags     |   Error Code  |          Error Value          |
   +---------------+---------------+---------------+---------------+


     o    IPv6 ERROR_SPEC object: Class = 6, C-Type = 2

    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +---------------+---------------+---------------+---------------+
   |                                                               |
   +                                                               +
   |                                                               |
   +               IPv6 Error Node Address (16 bytes)              +
   |                                                               |
   +                                                               +
   |                                                               |
   +---------------+---------------+---------------+---------------+
   |     Flags     |   Error Code  |          Error Value          |
   +---------------+---------------+---------------+---------------+

      Error Node Address

           The IP address of the node in which the error was detected.

      Flags

           0x01 = InPlace

                This flag is used only for an ERROR_SPEC object in a
                ResvErr message.  If it on, this flag indicates that
                there was, and still is, a reservation in place at the
                failure point.

           0x02 = NotGuilty

                This flag is used only for an ERROR_SPEC object in a
                ResvErr message, and it is only set in the interface to
                the receiver application.  If it on, this flag indicates
                that the FLOWSPEC that failed was strictly greater than
                the FLOWSPEC requested by this receiver.

      Error Code

           A one-octet error description.

      Error Value

           A two-octet field containing additional information about the
                error.  Its contents depend upon the Error Type.

      The values for Error Code and Error Value are defined in Appendix
      B.

***********************************************************************/

typedef struct rsvpErrorSpec4_s {
  u_int hopAddr;
  u_char flag;
  u_char code;
  u_short value;
} rsvpErrorSpec4_t;

typedef struct rsvpErrorSpec6_s {
  u_char hopAddr[16];
  u_char flag;
  u_char code;
  u_short value;
} rsvpErrorSpec6_t;

typedef struct rsvpErrorSpecObject_s {
    struct rsvpObjectHeader_s hdr;
    union {
	struct rsvpErrorSpec4_s ipv4;
	struct rsvpErrorSpec6_s ipv6;
    } u;
} rsvpErrorSpecObject_t;

#define RSVP_SCOPE_LIST_CLASS 7
#define RSVP_SCOPE_LIST_CTYPE_IPV4 1
#define RSVP_SCOPE_LIST_CTYPE_IPV6 2

#define RSVP_MAX_SCOPE_LIST 16

/*********************************************************************** 

    RSVP SCOPE LIST Object

      This object contains a list of IP addresses, used for routing
      messages with wildcard scope without loops.  The addresses must be
      listed in ascending numerical order.

      o    IPv4 SCOPE List object: Class = 7, C-Type = 1

     0                   1                   2                   3
     0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
    +-------------+-------------+-------------+-------------+
    |                IPv4 Src Address (4 bytes)             |
    +-------------+-------------+-------------+-------------+
    //                                                      //
    +-------------+-------------+-------------+-------------+
    |                IPv4 Src Address (4 bytes)             |
    +-------------+-------------+-------------+-------------+


      o    IPv6  SCOPE list object: Class = 7, C-Type = 2

     0                   1                   2                   3
     0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
    +-------------+-------------+-------------+-------------+
    |                                                       |
    +                                                       +
    |                                                       |
    +                IPv6 Src Address (16 bytes)            +
    |                                                       |
    +                                                       +
    |                                                       |
    +-------------+-------------+-------------+-------------+
    //                                                      //
    +-------------+-------------+-------------+-------------+
    |                                                       |
    +                                                       +
    |                                                       |
    +                IPv6 Src Address (16 bytes)            +
    |                                                       |
    +                                                       +
    |                                                       |
    +-------------+-------------+-------------+-------------+

***********************************************************************/

typedef struct rsvpScopeList4_s {
  u_int addr[RSVP_MAX_SCOPE_LIST];
} rsvpScopeList4_t;

typedef struct rsvpScopeList6_s {
  u_char addr[RSVP_MAX_SCOPE_LIST][16];
} rsvpScopeList6_t;

typedef struct rsvpScopListObject_s {
    struct rsvpObjectHeader_s hdr;
    union {
	struct rsvpScopeList4_s ipv4;
	struct rsvpScopeList6_s ipv6;
    } u;
} rsvpScopListObject_t;
#define RSVP_STYLE_CLASS 8
#define RSVP_STYLE_CTYPE 1

/*********************************************************************** 

     STYLE class = 8.

      o    STYLE object: Class = 8, C-Type = 1

     0                   1                   2                   3
     0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
    +-----------------+---------------+---------------+-------------+
    |     Flags       |                 Option Vector               |
    +-----------------+---------------+---------------+-------------+



      Flags: 8 bits

           (None assigned yet)

      Option Vector: 24 bits

           A set of bit fields giving values for the reservation
           options.  If new options are added in the future,
           corresponding fields in the option vector will be assigned
           from the least-significant end.  If a node does not recognize
           a style ID, it may interpret as much of the option vector as
           it can, ignoring new fields that may have been defined.

           The option vector bits are assigned (from the left) as
           follows:

           19 bits: Reserved

           2 bits: Sharing control

                00b: Reserved

                01b: Distinct reservations

                10b: Shared reservations

                11b: Reserved

           3 bits: Sender selection control

                000b: Reserved

                001b: Wildcard

                010b: Explicit

                011b - 111b: Reserved

      The low order bits of the option vector are determined by the
      style, as follows:

              WF 10001b
              FF 01010b
              SE 10010b

***********************************************************************/

typedef struct rsvpStyleFlag_s {
  BITFIELDS_ASCENDING_4(u_int flag:8,	/* flags  */
    u_int reserved:19,			/* reserved */
    u_int shareControl:2,		/* sharing control */
    u_int senderSelectionControl:3); /* sender selection control */
} rsvpStyleFlag_t;

typedef struct rsvpStyleObject_s {
    struct rsvpObjectHeader_s hdr;

    union {
	struct rsvpStyleFlag_s flag;
	u_int mark;
    } flag;
} rsvpStyleObject_t;

#define RSVP_FILTER_SPEC_CLASS 10
#define RSVP_FILTER_SPEC_CTYPE_IPV4 1
#define RSVP_FILTER_SPEC_CTYPE_IPV6 2
#define RSVP_FILTER_SPEC_CTYPE_FLOW 3
#define RSVP_FILTER_SPEC4_SIZE 8
#define RSVP_FILTER_SPEC6_SIZE 20
#define RSVP_FILTER_SPEC_FLOW_SIZE 20

/*********************************************************************** 

 FILTER_SPEC class = 10.

      o    IPv4 FILTER_SPEC object: Class = 10, C-Type = 1

    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +---------------+---------------+---------------+---------------+
   |                   IPv4 SrcAddress (4 bytes)                   |
   +---------------+---------------+---------------+---------------+
   |     //////    |     //////    |            SrcPort            |
   +---------------+---------------+---------------+---------------+


      o    IPv6 FILTER_SPEC object: Class = 10, C-Type = 2

    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +---------------+---------------+---------------+---------------+
   |                                                               |
   +                                                               +
   |                                                               |
   +               IPv6 SrcAddress (16 bytes)                      +
   |                                                               |
   +                                                               +
   |                                                               |
   +---------------+---------------+---------------+---------------+
   |     //////    |     //////    |            SrcPort            |
   +---------------+---------------+---------------+---------------+


      o    IPv6 Flow-label FILTER_SPEC object: Class = 10, C-Type = 3

    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +---------------+---------------+---------------+---------------+
   |                                                               |
   +                                                               +
   |                                                               |
   +                   IPv6 SrcAddress (16 bytes)                  +
   |                                                               |
   +                                                               +
   |                                                               |
   +---------------+---------------+---------------+---------------+
   |    ///////    |            Flow Label (24 bits)               |
   +---------------+---------------+---------------+---------------+

      SrcAddress

           The IP source address for a sender host.  Must be non-zero.

      SrcPort

           The UDP/TCP source port for a sender, or zero to indicate
           `none'.

      Flow Label

           A 24-bit Flow Label, defined in IPv6.  This value may be used
           by the packet classifier to efficiently identify the packets
           belonging to a particular (sender->destination) data flow.

***********************************************************************/

typedef struct rsvpFilterSpec4_s {
  u_int sourceAddr;
  u_short reserved;
  u_short sourcePort;
} rsvpFilterSpec4_t;

typedef struct rsvpFilterSpec6_s {
  u_char sourceAddr[RSVP_IPV6_ADDR_LEN];
  u_short reserved;
  u_short sourcePort;
} rsvpFilterSpec6_t;

typedef struct rsvpFilterSpecFlags_s {
  BITFIELDS_ASCENDING_2(u_int reserved:8,
    u_int flowLabel:24);
} rsvpFilterSpecFlags_t;

typedef struct rsvpFilterSpecFlow6_s {
  u_char sourceAddr[RSVP_IPV6_ADDR_LEN];
  union {
    u_int mark;
    struct rsvpFilterSpecFlags_s flowLabel;
  } flag;
} rsvpFilterSpecFlow6_t;

typedef struct rsvpFilterSpecObject_s {
    struct rsvpObjectHeader_s hdr;
    union {
	struct rsvpFilterSpec4_s ipv4;
	struct rsvpFilterSpec6_s ipv6;
	struct rsvpFilterSpecFlow6_s flow;
    } u;
} rsvpFilterSpecObject_t;

#define RSVP_SENDER_TEMPLATE_CLASS 11
#define RSVP_SENDER_TEMPLATE_CTYPE_IPV4 1
#define RSVP_SENDER_TEMPLATE_CTYPE_IPV6 2
#define RSVP_SENDER_TEMPLATE_CTYPE_FLOW 3
#define RSVP_SENDER_TEMPLATE_CTYPE_TUNNEL4 7
#define RSVP_SENDER_TEMPLATE_CTYPE_TUNNEL6 8
#define RSVP_SENDER_TEMPLATE_TUNNEL4_SIZE 8
#define RSVP_SENDER_TEMPLATE_TUNNEL6_SIZE 20

/**********************************************************************
      SENDER_TEMPLATE Class

      SENDER_TEMPLATE class = 11.

      o    IPv4 SENDER_TEMPLATE object: Class = 11, C-Type = 1

           Definition same as IPv4/UDP FILTER_SPEC object.

      o    IPv6 SENDER_TEMPLATE object: Class = 11, C-Type = 2

           Definition same as IPv6/UDP FILTER_SPEC object.

      o    IPv6 Flow-label SENDER_TEMPLATE object: Class = 11, C-Type = 3


   LSP_TUNNEL_IPv4 Sender Template Object

   Class = SENDER_TEMPLATE, LSP_TUNNEL_IPv4 C-Type = 7

    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                   IPv4 tunnel sender address                  |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |  MUST be zero                 |            LSP ID             |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

      IPv4 tunnel sender address

         IPv4 address for a sender node

      LSP ID

         A 16-bit identifier used in the SENDER_TEMPLATE and the
         FILTER_SPEC that can be changed to allow a sender to share
         resources with itself.

   LSP_TUNNEL_IPv6 Sender Template Object

   Class = SENDER_TEMPLATE, LSP_TUNNEL_IPv6 C_Type = 8

    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                                                               |
   +                                                               +
   |                   IPv6 tunnel sender address                  |
   +                                                               +
   |                            (16 bytes)                         |
   +                                                               +
   |                                                               |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |  MUST be zero                 |            LSP ID             |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

      IPv6 tunnel sender address

         IPv6 address for a sender node

      LSP ID

         A 16-bit identifier used in the SENDER_TEMPLATE and the
         FILTER_SPEC that can be changed to allow a sender to share
         resources with itself.
**********************************************************************/

#define rsvpSenderTemplate4_s rsvpFilterSpec4_s
#define rsvpSenderTemplate4_t rsvpFilterSpec4_t

#define rsvpSenderTemplate6_s rsvpFilterSpec6_s
#define rsvpSenderTemplate6_t rsvpFilterSpec6_t

#define rsvpSenderTemplateFlow6_s rsvpFilterSpecFlow6_s
#define rsvpSenderTemplateFlow6_t rsvpFilterSpecFlow6_t

#define rsvpteEncodeSenderTemplate4 rsvpteEncodeFilterSpec4
#define rsvpteEncodeSenderTemplate6 rsvpteEncodeFilterSpec6
#define rsvpteEncodeSenderTemplateFlow rsvpteEncodeFilterSpecFlow

#define rsvpteDecodeSenderTemplate4 rsvpteDecodeFilterSpec4
#define rsvpteDecodeSenderTemplate6 rsvpteDecodeFilterSpec6
#define rsvpteDecodeSenderTemplateFlow rsvpteDecodeFilterSpecFlow

typedef struct rsvpSenderTemplateTunnel4_s {
    u_int sender;
    u_short reserved;
    u_short lspId;
} rsvpSenderTemplateTunnel4_t;

typedef struct rsvpSenderTemplateTunnel6_s {
    u_char sender[RSVP_IPV6_ADDR_LEN];
    u_short reserved;
    u_short lspId;
} rsvpSenderTemplateTunnel6_t;

typedef struct rsvpSenderTemplateObject_s {
    struct rsvpObjectHeader_s hdr;
    union {
	struct rsvpSenderTemplate4_s ipv4;
	struct rsvpSenderTemplate6_s ipv6;
	struct rsvpSenderTemplateFlow6_s flow;
	struct rsvpSenderTemplateTunnel4_s tunnel4;
	struct rsvpSenderTemplateTunnel6_s tunnel6;
    } u;
} rsvpSenderTemplateObject_t;

#define RSVP_SENDER_TSPEC_CLASS 12
#define RSVP_SENDER_TSPEC_CTYPE_INTSRV 2

/**********************************************************************
      SENDER_TSPEC Class

      SENDER_TSPEC class = 12.

      o    Intserv SENDER_TSPEC object: Class = 12, C-Type = 2

   The RSVP SENDER_TSPEC object carries information about a data
   source's generated traffic. The required RSVP SENDER_TSPEC object
   contains a global Token_Bucket_TSpec parameter (service_number 1,
   parameter 127, as defined in [RFC 2215]). This TSpec carries traffic
   information usable by either the Guaranteed or Controlled-Load QoS
   control services.

        31           24 23           16 15            8 7             0
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   1   | 0 (a) |    reserved           |             7 (b)             |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   2   |    1  (c)     |0| reserved    |             6 (d)             |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   3   |   127 (e)     |    0 (f)      |             5 (g)             |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   4   |  Token Bucket Rate [r] (32-bit IEEE floating point number)    |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   5   |  Token Bucket Size [b] (32-bit IEEE floating point number)    |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   6   |  Peak Data Rate [p] (32-bit IEEE floating point number)       |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   7   |  Minimum Policed Unit [m] (32-bit integer)                    |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   8   |  Maximum Packet Size [M]  (32-bit integer)                    |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

     (a) - Message format version number (0)
     (b) - Overall length (7 words not including header)
     (c) - Service header, service number 1 (default/global information)
     (d) - Length of service 1 data, 6 words not including header
     (e) - Parameter ID, parameter 127 (Token_Bucket_TSpec)
     (f) - Parameter 127 flags (none set)
     (g) - Parameter 127 length, 5 words not including header


   In this TSpec, the parameters [r] and [b] are set to reflect the
   sender's view of its generated traffic. The peak rate parameter [p]
   may be set to the sender's peak traffic generation rate (if known and
   controlled), the physical interface line rate (if known), or positive
   infinity (if no better value is available).  Positive infinity is
   represented as an IEEE single-precision floating-point number with an
   exponent of all ones (255) and a sign and mantissa of all zeros.  The
   format of IEEE floating-point numbers is further summarized in [RFC
   1832].

   The minimum policed unit parameter [m] should generally be set equal
   to the size of the smallest packet generated by the application. This
   packet size includes the application data and all protocol headers at
   or above the IP level (IP, TCP, UDP, RTP, etc.). The size given does
   not include any link-level headers, because these headers will change
   as the packet crosses different portions of the internetwork.


   The [m] parameter is used by nodes within the network to compute the
   maximum bandwidth overhead needed to carry a flow's packets over the
   particular link-level technology, based on the ratio of [m] to the
   link-level header size. This allows the correct amount of bandwidth
   to be allocated to the flow at each point in the net.  Note that
   smaller values of this parameter lead to increased overhead
   estimates, and thus increased likelyhood of a reservation request
   being rejected by the node. In some cases, an application
   transmitting a low percentage of very small packets may therefore
   choose to set the value of [m] larger than the actual minimum
   transmitted packet size. This will increase the likelyhood of the
   reservation succeeding, at the expense of policing packets of size
   less than [m] as if they were of size [m].

   Note that the an [m] value of zero is illegal. A value of zero would
   indicate that no data or IP headers are present, and would give an
   infinite amount of link-level overhead.

   The maximum packet size parameter [M] should be set to the size of
   the largest packet the application might wish to generate, as
   described in Section 2.3.2. This value must, by definition, be equal
   to or larger than the value of [m].


**********************************************************************/
typedef struct rsvpSenderTSpecObject_s {
    struct rsvpObjectHeader_s hdr;
} rsvpSenderTSpecObject_t;


#define RSVP_ADSPEC_CLASS 13
#define RSVP_ADSPEC_CTYPE_INTSRV 2
/**********************************************************************
      ADSPEC Class

      ADSPEC class = 13.

      o    Intserv ADSPEC object: Class = 13, C-Type = 2

   The basic ADSPEC format is shown below. The message header and the
   default general parameters fragment are always present. The fragments
   for Guaranteed or Controlled-Load service may be omitted if the
   service is not to be used by the RSVP session. Additional data
   fragments will be added if new services are defined.

       31           24 23            16 15            8 7             0
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       | 0 (a) |      reserved         |  Msg length - 1 (b)           |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |                                                               |
       |    Default General Parameters fragment (Service 1)  (c)       |
       |    (Always Present)                                           |
       |                                                               |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |                                                               |
       |    Guaranteed Service Fragment (Service 2)    (d)             |
       |    (Present if application might use Guaranteed Service)      |
       |                                                               |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |                                                               |
       |    Controlled-Load Service Fragment (Service 5)  (e)          |
       |    (Present if application might use Controlled-Load Service) |
       |                                                               |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

     (a) - Message format version number (0)
     (b) - Overall message length not including header word
     (c, d, e) - Data fragments

3.3.2. Default General Characterization Parameters ADSPEC data fragment

   All RSVP ADSPECs carry the general characterization parameters
   defined in [RFC 2215].  Values for global or default general
   parameters (values which apply to the all services or the path
   itself) are carried in the per-service data fragment for service
   number 1, as shown in the picture above.  This fragment is always
   present, and always first in the message.

       31            24 23           16 15            8 7             0
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   1   |    1  (c)     |x| reserved    |           8 (d)               |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   2   |    4 (e)      |    (f)        |           1 (g)               |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   3   |        IS hop cnt (32-bit unsigned integer)                   |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   4   |    6 (h)      |    (i)        |           1 (j)               |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   5   |  Path b/w estimate  (32-bit IEEE floating point number)       |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   6   |     8 (k)     |    (l)        |           1 (m)               |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   7   |        Minimum path latency (32-bit integer)                  |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   8   |     10 (n)    |      (o)      |           1 (p)               |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   9   |      Composed MTU (32-bit unsigned integer)                   |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

     (c) - Per-Service header, service number 1 (Default General
           Parameters)
     (d) - Global Break bit ([RFC 2215], Parameter 2) (marked x) and
           length of General Parameters data block.
     (e) - Parameter ID, parameter 4 (Number-of-IS-hops param from
           [RFC 2215])
     (f) - Parameter 4 flag byte
     (g) - Parameter 4 length, 1 word not including header
     (h) - Parameter ID, parameter 6 (Path-BW param from [RFC 2215])
     (i) - Parameter 6 flag byte
     (j) - Parameter 6 length, 1 word not including header
     (k) - Parameter ID, parameter 8 (minimum path latency from [RFC
           2215])
     (l) - Parameter 8 flag byte
     (m) - Parameter 8 length, 1 word not including header
     (n) - Parameter ID, parameter 10 (composed path MTU from [RFC 2215])
     (o) - Parameter 10 flag byte
     (p) - Parameter 10 length, 1 word not including header

**********************************************************************/
typedef struct rsvpAdSpecObject_s {
    struct rsvpObjectHeader_s hdr;
} rsvpAdSpecObject_t;

/**********************************************************************

3.2. RSVP FLOWSPEC Object

   The RSVP FLOWSPEC object carries information necessary to make
   reservation requests from the receiver(s) into the network. This
   includes an indication of which QoS control service is being
   requested, and the parameters needed for that service.

   The QoS control service requested is indicated by the service_number
   in the FLOWSPEC's per-service header.

3.2.1 FLOWSPEC object when requesting Controlled-Load service

   The format of an RSVP FLOWSPEC object originating at a receiver
   requesting Controlled-Load service is shown below. Each of the TSpec
   fields is represented using the preferred concrete representation
   specified in the 'Invocation Information' section of [RFC 2211]. The
   value of 5 in the per-service header (field (c), below) indicates
   that Controlled-Load service is being requested.

        31           24 23           16 15            8 7             0
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   1   | 0 (a) |    reserved           |             7 (b)             |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   2   |    5  (c)     |0| reserved    |             6 (d)             |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   3   |   127 (e)     |    0 (f)      |             5 (g)             |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   4   |  Token Bucket Rate [r] (32-bit IEEE floating point number)    |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   5   |  Token Bucket Size [b] (32-bit IEEE floating point number)    |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   6   |  Peak Data Rate [p] (32-bit IEEE floating point number)       |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   7   |  Minimum Policed Unit [m] (32-bit integer)                    |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   8   |  Maximum Packet Size [M]  (32-bit integer)                    |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

     (a) - Message format version number (0)
     (b) - Overall length (7 words not including header)
     (c) - Service header, service number 5 (Controlled-Load)
     (d) - Length of controlled-load data, 6 words not including
           per-service header
     (e) - Parameter ID, parameter 127 (Token Bucket TSpec)
     (f) - Parameter 127 flags (none set)
     (g) - Parameter 127 length, 5 words not including per-service
           header

   In this object, the TSpec parameters [r], [b], and [p] are set to
   reflect the traffic parameters of the receiver's desired reservation
   (the Reservation TSpec). The meaning of these fields is discussed
   fully in [RFC 2211]. Note that it is unlikely to make sense for the
   [p] term to be smaller than the [r] term.

   The maximum packet size parameter [M] should be set to the value of
   the smallest path MTU, which the receiver learns from information in
   arriving RSVP ADSPEC objects.  Alternatively, if the receiving
   application has built-in knowledge of the maximum packet size in use
   within the RSVP session, and this value is smaller than the smallest
   path MTU, [M] may be set to this value.  Note that requesting a value
   of [M] larger than the service modules along the data path can
   support will cause the reservation to fail. See section 2.3.2 for
   further discussion of the MTU value.

   The value of [m] can be chosen in several ways. Recall that when a
   resource reservation is installed at each intermediate node, the
   value used for [m] is the smaller of the receiver's request and the
   values in each sender's SENDER_TSPEC.

   If the application has a fixed, known minimum packet size, than that
   value should be used for [m]. This is the most desirable case.

   For a shared reservation style, the receiver may choose between two
   options, or pick some intermediate point between them.

      - if the receiver chooses a large value for [m], then the
      reservation will allocate less overhead for link-level headers.
      However, if a new sender with a smaller SENDER_TSPEC [m] joins the
      session later, an already-installed reservation may fail at that
      time.

      - if the receiver chooses a value of [m] equal to the smallest
      value which might be used by any sender, then the reservation will
      be forced to allocate more overhead for link-level headers.
      However it will not fail later if a new sender with a smaller
      SENDER_TSPEC [m] joins the session.

   For a FF reservation style, if no application-specific value is known
   the receiver should simply use the value of [m] arriving in each
   sender's SENDER_TSPEC for its reservation request to that sender.

**********************************************************************/
typedef struct rsvpFlowSpecObject_s {
    struct rsvpObjectHeader_s hdr;
} rsvpFlowSpecObject_t;

#define RSVP_POLICY_DATA_CLASS 14
#define RSVP_POLICY_DATA_CTYPE 1

/**********************************************************************
      POLICY_DATA Class

      POLICY_DATA class = 14.

      o    Type 1 POLICY_DATA object: Class = 14, C-Type = 1

           The contents of this object are for further study.

**********************************************************************/
typedef struct rsvpPolicyDataObject_s {
    struct rsvpObjectHeader_s hdr;
} rsvpPolicyDataObject_t;

#define RSVP_RESV_CONFIRM_CLASS 15
#define RSVP_RESV_CONFIRM_CTYPE_IPV4 1
#define RSVP_RESV_CONFIRM_CTYPE_IPV6 2

/**********************************************************************
      Resv_CONFIRM Class

      RESV_CONFIRM class = 15.

      o    IPv4 RESV_CONFIRM object: Class = 15, C-Type = 1

           +-------------+-------------+-------------+-------------+
           |            IPv4 Receiver Address (4 bytes)            |
           +-------------+-------------+-------------+-------------+


      o    IPv6 RESV_CONFIRM object: Class = 15, C-Type = 2

           +-------------+-------------+-------------+-------------+
           |                                                       |
           +                                                       +
           |                                                       |
           +            IPv6 Receiver Address (16 bytes)           +
           |                                                       |
           +                                                       +
           |                                                       |
           +-------------+-------------+-------------+-------------+
**********************************************************************/

typedef struct rsvpResvConfirmObject_s {
    struct rsvpObjectHeader_s hdr;
    union {
	u_int ipv4;
	u_char ipv6[16];
    } u;
} rsvpResvConfirmObject_t;


#define RSVP_LABEL_CLASS 16
#define RSVP_LABEL_CTYPE 1

/**********************************************************************

   LABEL class = 16, C_Type = 1

    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                           (top label)                         |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

   The contents of a LABEL is a single label, encoded in 4 octets.  Each
   generic MPLS label is an unsigned integer in the range 0 through
   1048575.  Generic MPLS labels and FR labels are encoded right aligned
   in 4 octets.  ATM labels are encoded with the VPI right justified in
   bits 0-15 and the VCI right justified in bits 16-31.

**********************************************************************/

typedef struct rsvpLabelObject_s {
    struct rsvpObjectHeader_s hdr;
    u_int label;
} rsvpLabelObject_t;

#define RSVP_LABEL_REQUEST_CLASS 19
#define RSVP_LABEL_REQUEST_CTYPE_GENERIC 1
#define RSVP_LABEL_REQUEST_CTYPE_ATM 2
#define RSVP_LABEL_REQUEST_CTYPE_FR 3

/**********************************************************************

   Label Request without Label Range Class = 19, C_Type = 1

    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |           Reserved            |             L3PID             |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

      Reserved

         This field is reserved.  It MUST be set to zero on transmission
         and MUST be ignored on receipt.

      L3PID

         an identifier of the layer 3 protocol using this path.
         Standard Ethertype values are used.


   Label Request with ATM Label Range Class = 19, C_Type = 2

    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |           Reserved            |             L3PID             |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |M| Res |    Minimum VPI        |      Minimum VCI              |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |  Res  |    Maximum VPI        |      Maximum VCI              |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

      Reserved (Res)

         This field is reserved.  It MUST be set to zero on transmission
         and MUST be ignored on receipt.

      L3PID

         an identifier of the layer 3 protocol using this path.
         Standard Ethertype values are used.

      M

         Setting this bit to one indicates that the node is capable of
         merging in the data plane

      Minimum VPI (12 bits)

         This 12 bit field specifies the lower bound of a block of
         Virtual Path Identifiers that is supported on the originating
         switch.  If the VPI is less than 12-bits it MUST be right
         justified in this field and preceding bits MUST be set to zero.

      Minimum VCI (16 bits)

         This 16 bit field specifies the lower bound of a block of
         Virtual Connection Identifiers that is supported on the
         originating switch.  If the VCI is less than 16-bits it MUST be
         right justified in this field and preceding bits MUST be set to
         zero.

      Maximum VPI (12 bits)

         This 12 bit field specifies the upper bound of a block of
         Virtual Path Identifiers that is supported on the originating
         switch.  If the VPI is less than 12-bits it MUST be right
         justified in this field and preceding bits MUST be set to zero.

      Maximum VCI (16 bits)

         This 16 bit field specifies the upper bound of a block of
         Virtual Connection Identifiers that is supported on the
         originating switch.  If the VCI is less than 16-bits it MUST be
         right justified in this field and preceding bits MUST be set to
         zero.


   Label Request with Frame Relay Label Range Class = 19, C_Type = 3

    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |           Reserved            |             L3PID             |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   | Reserved    |DLI|                     Minimum DLCI            |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   | Reserved        |                     Maximum DLCI            |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

      Reserved

         This field is reserved.  It MUST be set to zero on transmission
         and ignored on receipt.

      L3PID

         an identifier of the layer 3 protocol using this path.
         Standard Ethertype values are used.

      DLI

         DLCI Length Indicator.  The number of bits in the DLCI.  The
         following values are supported:

                   Len    DLCI bits

                    0        10
                    2        23

      Minimum DLCI

         This 23-bit field specifies the lower bound of a block of Data
         Link Connection Identifiers (DLCIs) that is supported on the
         originating switch.  The DLCI MUST be right justified in this
         field and unused bits MUST be set to 0.

      Maximum DLCI

         This 23-bit field specifies the upper bound of a block of Data
         Link Connection Identifiers (DLCIs) that is supported on the
         originating switch.  The DLCI MUST be right justified in this
         field and unused bits MUST be set to 0.
**********************************************************************/

#define RSVP_LABELREQ_GENERIC_SIZE 4

typedef struct rsvpLabelReqGeneric_s {
    u_short reserved;
    u_short l3pid;
} rsvpLabelReqGeneric_t;

#define RSVP_LABELREQ_ATM_SIZE 12

typedef struct rsvpLabelReqAtmMinFlag_s {
    BITFIELDS_ASCENDING_3(u_short M:1,
		u_short reserved:3,
		u_short vpi:12);
} rsvpLabelReqAtmMinFlag_t;

typedef struct rsvpLabelReqAtmMaxFlag_s {
    BITFIELDS_ASCENDING_2(u_short reserved:4,
		u_short vpi:12);
} rsvpLabelReqAtmMaxFlag_t;

typedef struct rsvpLabelReqAtm_s {
    u_short reserved;
    u_short l3pid;

    union {
	struct rsvpLabelReqAtmMinFlag_s flags;
	u_short mark;
    } minFlags;
    u_short minVci;

    union {
	struct rsvpLabelReqAtmMaxFlag_s flags;
	u_short mark;
    } maxFlags;
    u_short maxVci;
} rsvpLabelRequestAtm_t;

#define RSVP_LABELREQ_FR_SIZE 12

#define RSVP_LABEL_REQUEST_FR_LEN_10 0
#define RSVP_LABEL_REQUEST_FR_LEN_23 2

typedef struct rsvpLabelReqFrMinFlag_s {
    BITFIELDS_ASCENDING_3(u_int reserved:6,
		u_int dli:2,
		u_int min:12);
} rsvpLabelReqFrMinFlag_t;

typedef struct rsvpLabelReqFrMaxFlag_s {
    BITFIELDS_ASCENDING_2(u_int reserved:8,
		u_int max:24);
} rsvpLabelReqFrMaxFlag_t;

typedef struct rsvpLabelReqFr_s {
    u_short reserved;
    u_short l3pid;
    union {
	struct rsvpLabelReqFrMinFlag_s flags;
	u_int mark;
    } minFlags;
    union {
	struct rsvpLabelReqFrMaxFlag_s flags;
	u_int mark;
    } maxFlags;
} rsvpLabelRequestFr_t;

typedef struct rsvpLabelRequestObject_s {
    struct rsvpObjectHeader_s hdr;
    union {
	struct rsvpLabelReqGeneric_s generic;
	struct rsvpLabelReqAtm_s atm;
	struct rsvpLabelReqFr_s fr;
    } u;
} rsvpLabelRequestObject_t;


#define RSVP_EXPLICIT_ROUTE_CLASS 20
#define RSVP_EXPLICIT_ROUTE_CTYPE 1

#define RSVP_ERO_TYPE_IPV4 1
#define RSVP_ERO_TYPE_IPV6 2
#define RSVP_ERO_TYPE_ASN 32

#define RSVP_MAX_ERO 16

/**********************************************************************
   Explicit Route Object Class = 20, C_Type = 1

   Explicit routes are specified via the EXPLICIT_ROUTE object (ERO).
   The Explicit Route Class is 20.  Currently one C_Type is defined,
   Type 1 Explicit Route.  The EXPLICIT_ROUTE object has the following
   format:


    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                                                               |
   //                        (Subobjects)                          //
   |                                                               |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

   The contents of an EXPLICIT_ROUTE object are a series of variable-
   length data items called subobjects.  The subobjects are defined in
   section 4.3.3 below.

   If a Path message contains multiple EXPLICIT_ROUTE objects, only the
   first object is meaningful.  Subsequent EXPLICIT_ROUTE objects MAY be
   ignored and SHOULD NOT be propagated.

   Subobjects

   The contents of an EXPLICIT_ROUTE object are a series of variable-
   length data items called subobjects.  Each subobject has the form:

    0                   1
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-------------//----------------+
   |L|    Type     |     Length    | (Subobject contents)          |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-------------//----------------+

      L

         The L bit is an attribute of the subobject.  The L bit is set
         if the subobject represents a loose hop in the explicit route.
         If the bit is not set, the subobject represents a strict hop in
         the explicit route.

      Type

         The Type indicates the type of contents of the subobject.
         Currently defined values are:

                   1   IPv4 prefix
                   2   IPv6 prefix
                  32   Autonomous system number

      Length

         The Length contains the total length of the subobject in bytes,
         including the L, Type and Length fields.  The Length MUST be at
         least 4, and MUST be a multiple of 4

    Subobject 1:  IPv4 prefix

    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |L|    Type     |     Length    | IPv4 address (4 bytes)        |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   | IPv4 address (continued)      | Prefix Length |      Resvd    |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

      L

         The L bit is an attribute of the subobject.  The L bit is set
         if the subobject represents a loose hop in the explicit route.
         If the bit is not set, the subobject represents a strict hop in
         the explicit route.

      Type

         0x01  IPv4 address

      Length

         The Length contains the total length of the subobject in bytes,
         including the Type and Length fields.  The Length is always 8.

      IPv4 address

         An IPv4 address.  This address is treated as a prefix based on
         the prefix length value below.  Bits beyond the prefix are
         ignored on receipt and SHOULD be set to zero on transmission.

      Prefix length

         Length in bits of the IPv4 prefix

      Padding

         Zero on transmission.  Ignored on receipt.

   The contents of an IPv4 prefix subobject are a 4-octet IPv4 address,
   a 1-octet prefix length, and a 1-octet pad.  The abstract node
   represented by this subobject is the set of nodes that have an IP
   address which lies within this prefix.  Note that a prefix length of
   32 indicates a single IPv4 node.

   Subobject 2:  IPv6 Prefix

    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |L|    Type     |     Length    | IPv6 address (16 bytes)       |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   | IPv6 address (continued)                                      |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   | IPv6 address (continued)                                      |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   | IPv6 address (continued)                                      |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   | IPv6 address (continued)      | Prefix Length |      Resvd    |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

      L

         The L bit is an attribute of the subobject.  The L bit is set
         if the subobject represents a loose hop in the explicit route.
         If the bit is not set, the subobject represents a strict hop in
         the explicit route.

      Type

         0x02  IPv6 address

      Length

         The Length contains the total length of the subobject in bytes,
         including the Type and Length fields.  The Length is always 20.

      IPv6 address

         An IPv6 address.  This address is treated as a prefix based on
         the prefix length value below.  Bits beyond the prefix are
         ignored on receipt and SHOULD be set to zero on transmission.

      Prefix Length

         Length in bits of the IPv6 prefix.

      Padding

         Zero on transmission.  Ignored on receipt.

   The contents of an IPv6 prefix subobject are a 16-octet IPv6 address,
   a 1-octet prefix length, and a 1-octet pad.  The abstract node
   represented by this subobject is the set of nodes that have an IP
   address which lies within this prefix.  Note that a prefix length of
   128 indicates a single IPv6 node.

   Subobject 32:  Autonomous System Number

   The contents of an Autonomous System (AS) number subobject are a 2-
   octet AS number.  The abstract node represented by this subobject is
   the set of nodes belonging to the autonomous system.

   The length of the AS number subobject is 4 octets.
**********************************************************************/

typedef struct rsvpExplicitRouteFlag_s {
    BITFIELDS_ASCENDING_3(u_short L:1,
		u_short type:3,
		u_short len:4);
} rsvpExplicitRouteFlag_t;

#define RSVP_ER4_SIZE 8

typedef struct rsvpExplicitRoute4_s {
    union {
	struct rsvpExplicitRouteFlag_s flags;
	u_short mark;
    } flags;
    u_char address[RSVP_IPV4_ADDR_LEN];
    u_char prefixLen;
    u_char reserved;

    /* used only for easy access to contents of 'address' */
    u_int uIntAddress;
} rsvpExplicitRoute4_t;

#define RSVP_ER6_SIZE 8

typedef struct rsvpExplicitRoute6_s {
    union {
	struct rsvpExplicitRouteFlag_s flags;
	u_short mark;
    } flags;
    u_char address[RSVP_IPV6_ADDR_LEN];
    u_char prefixLen;
    u_char reserved;
} rsvpExplicitRoute6_s;

#define RSVP_ERASN_SIZE 4

typedef struct rsvpExplicitRouteAsn_s {
    union {
	struct rsvpExplicitRouteFlag_s flags;
	u_short mark;
    } flags;
    u_short asn;
} rsvpExplicitRouteAsn_t;

typedef struct rsvpExplicitRouteObject_s {
    struct rsvpObjectHeader_s hdr;
    union {
	struct rsvpExplicitRoute4_s ipv4;
	struct rsvpExplicitRoute6_s ipv6;
	struct rsvpExplicitRouteAsn_s asn;
    } ero[RSVP_MAX_ERO];
    int eroType[RSVP_MAX_ERO];
    int eroLength;
} rsvpExplicitRouteObject_t;

#define RSVP_RECORD_ROUTE_CLASS 21
#define RSVP_RECORD_ROUTE_CTYPE 1

#define RSVP_RECORD_ROUTE_TYPE_IPV4 1
#define RSVP_RECORD_ROUTE_TYPE_IPV6 2
#define RSVP_RECORD_ROUTE_TYPE_LABEL 3

#define RSVP_RECORD_ROUTE_FLAG_LOCOL_PROT_AVAIL 0x1
#define RSVP_RECORD_ROUTE_FLAG_LOCOL_PROT_INUSE 0x2

#define RSVP_RECORD_ROUTE4_SIZE 8
#define RSVP_RECORD_ROUTE6_SIZE 20
#define RSVP_RECORD_ROUTE_LABEL_SIZE 20

#define RSVP_MAX_RR 16

/**********************************************************************

   Record Route Object Class = 21, C_Type = 1

   Routes can be recorded via the RECORD_ROUTE object (RRO).
   Optionally, labels may also be recorded.  The Record Route Class is
   21.  Currently one C_Type is defined, Type 1 Record Route.  The
   RECORD_ROUTE object has the following format:


    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                                                               |
   //                        (Subobjects)                          //
   |                                                               |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

      Subobjects

         The contents of a RECORD_ROUTE object are a series of
         variable-length data items called subobjects.  The subobjects
         are defined in section 4.4.1 below.

   The RRO can be present in both RSVP Path and Resv messages.  If a
   Path message contains multiple RROs, only the first RRO is
   meaningful.  Subsequent RROs SHOULD be ignored and SHOULD NOT be
   propagated.  Similarly, if in a Resv message multiple RROs are
   encountered following a FILTER_SPEC before another FILTER_SPEC is
   encountered, only the first RRO is meaningful.  Subsequent RROs
   SHOULD be ignored and SHOULD NOT be propagated.



    Subobject 1: IPv4 address

    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |      Type     |     Length    | IPv4 address (4 bytes)        |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   | IPv4 address (continued)      | Prefix Length |      Flags    |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

      Type

         0x01  IPv4 address

      Length

         The Length contains the total length of the subobject in bytes,
         including the Type and Length fields.  The Length is always 8.

      IPv4 address

         A 32-bit unicast, host address.  Any network-reachable
         interface address is allowed here.  Illegal addresses, such as
         certain loopback addresses, SHOULD NOT be used.

      Prefix length

         32

      Flags

         0x01  Local protection available

               Indicates that the link downstream of this node is
               protected via a local repair mechanism.  This flag can
               only be set if the Local protection flag was set in the
               SESSION_ATTRIBUTE object of the corresponding Path
               message.

         0x02  Local protection in use

               Indicates that a local repair mechanism is in use to
               maintain this tunnel (usually in the face of an outage
               of the link it was previously routed over).


    Subobject 2: IPv6 address

    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |      Type     |     Length    | IPv6 address (16 bytes)       |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   | IPv6 address (continued)                                      |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   | IPv6 address (continued)                                      |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   | IPv6 address (continued)                                      |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   | IPv6 address (continued)      | Prefix Length |      Flags    |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

      Type

         0x02  IPv6 address

      Length

         The Length contains the total length of the subobject in bytes,
         including the Type and Length fields.  The Length is always 20.

      IPv6 address

         A 128-bit unicast host address.

      Prefix length

         128

      Flags

         0x01  Local protection available

               Indicates that the link downstream of this node is
               protected via a local repair mechanism.  This flag can
               only be set if the Local protection flag was set in the
               SESSION_ATTRIBUTE object of the corresponding Path
               message.


         0x02  Local protection in use

               Indicates that a local repair mechanism is in use to
               maintain this tunnel (usually in the face of an outage
               of the link it was previously routed over).

    Subobject 3, Label

    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |     Type      |     Length    |    Flags      |   C-Type      |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |       Contents of Label Object                                |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

      Type

         0x03  Label

      Length

         The Length contains the total length of the subobject in bytes,
         including the Type and Length fields.

      Flags

         0x01 = Global label
           This flag indicates that the label will be understood
           if received on any interface.

      C-Type

         The C-Type of the included Label Object.  Copied from the Label
         Object.

      Contents of Label Object

         The contents of the Label Object.  Copied from the Label Object
**********************************************************************/

typedef struct rsvpRecordRoute4_s {
    u_char type;
    u_char length;
    u_short addressMsb;
    u_short addressLsb;
    u_char prefixLen;
    u_char flags;
} rsvpRecordRoute4_t;

typedef struct rsvpRecordRoute6_s {
    u_char type;
    u_char length;
    u_short addressMsb;
    u_char addressMid[12];
    u_short addressLsb;
    u_char prefixLen;
    u_char flags;
} rsvpRecordRoute6_t;

typedef struct rsvpRecordRouteLabel_s {
    u_char type;
    u_char length;
    u_char flags;
    u_char cType;
    struct rsvpLabelObject_s label;
} rsvpRecordRouteLabel_t;

typedef struct rsvpRecordRouteObject_s {
    struct rsvpObjectHeader_s hdr;
    union {
	struct rsvpRecordRoute4_s ipv4;
	struct rsvpRecordRoute6_s ipv6;
	struct rsvpRecordRouteLabel_s label;
    } rr[RSVP_MAX_RR];
    int rrType[RSVP_MAX_RR];
    int rrLen;
} rsvpRecordRouteObject_t;





#define RSVP_SESSION_ATTRIB_CLASS 207
#define RSVP_SESSION_ATTRIB_CTYPE_LSP_TUNNEL 7
#define RSVP_SESSION_ATTRIB_CLASS_LSP_TUNNEL_RA 1

#define RSVP_MAX_SESSION_NAME 64

#define RSVP_SESSION_ATTRIB_FLAGS_LOCAL_PROTECT 0x1
#define RSVP_SESSION_ATTRIB_FLAGS_LABEL_RECORD 0x2
#define RSVP_SESSION_ATTRIB_FLAGS_SE_STYLE 0x4

#define RSVP_SESSION_ATTRIB_LSP_TUNNEL_BASE_SIZE 4
#define RSVP_SESSION_ATTRIB_LSP_TUNNEL_RA_BASE_SIZE 16

/**********************************************************************

   Session Attribute Object

   The Session Attribute Class is 207.  Two C_Types are defined,
   LSP_TUNNEL, C-Type = 7 and LSP_TUNNEL_RA, C-Type = 1.  The
   LSP_TUNNEL_RA C-Type includes all the same fields as the LSP_TUNNEL
   C-Type.  Additionally it carries resource affinity information.  The
   formats are as follows:

   Format without resource affinities

   SESSION_ATTRIBUTE class = 207, LSP_TUNNEL C-Type = 7

    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |   Setup Prio  | Holding Prio  |     Flags     |  Name Length  |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                                                               |
   //          Session Name      (NULL padded display string)      //
   |                                                               |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

      Setup Priority

         The priority of the session with respect to taking resources,
         in the range of 0 to 7.  The value 0 is the highest priority.
         The Setup Priority is used in deciding whether this session can
         preempt another session.

      Holding Priority

         The priority of the session with respect to holding resources,
         in the range of 0 to 7.  The value 0 is the highest priority.
         Holding Priority is used in deciding whether this session can
         be preempted by another session.

      Flags

         0x01  Local protection desired

               This flag permits transit routers to use a local repair
               mechanism which may result in violation of the explicit
               route object.  When a fault is detected on an adjacent
               downstream link or node, a transit router can reroute
               traffic for fast service restoration.

         0x02  Label recording desired

               This flag indicates that label information should be
               included when doing a route record.

         0x04  SE Style desired

               This flag indicates that the tunnel ingress node may
               choose to reroute this tunnel without tearing it down.
               A tunnel egress node SHOULD use the SE Style when
               responding with a Resv message.

      Name Length

         The length of the display string before padding, in bytes.

      Session Name

         A null padded string of characters.


    Format with resource affinities

    SESSION_ATTRIBUTE class = 207, LSP_TUNNEL_RA C-Type = 1

    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                         Exclude-any                           |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                         Include-any                           |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                         Include-all                           |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |   Setup Prio  | Holding Prio  |     Flags     |  Name Length  |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                                                               |
   //          Session Name      (NULL padded display string)      //
   |                                                               |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

      Exclude-any

         A 32-bit vector representing a set of attribute filters
         associated with a tunnel any of which renders a link
         unacceptable.

      Include-any

         A 32-bit vector representing a set of attribute filters
         associated with a tunnel any of which renders a link acceptable
         (with respect to this test).  A null set (all bits set to zero)
         automatically passes.

      Include-all

         A 32-bit vector representing a set of attribute filters
         associated with a tunnel all of which must be present for a
         link to be acceptable (with respect to this test).  A null set
         (all bits set to zero) automatically passes.

      Setup Priority

         The priority of the session with respect to taking resources,
         in the range of 0 to 7.  The value 0 is the highest priority.
         The Setup Priority is used in deciding whether this session can
         preempt another session.

      Holding Priority

         The priority of the session with respect to holding resources,
         in the range of 0 to 7.  The value 0 is the highest priority.
         Holding Priority is used in deciding whether this session can
         be preempted by another session.

      Flags

         0x01  Local protection desired

               This flag permits transit routers to use a local repair
               mechanism which may result in violation of the explicit
               route object.  When a fault is detected on an adjacent
               downstream link or node, a transit router can reroute
               traffic for fast service restoration.

         0x02  Label recording desired

               This flag indicates that label information should be
               included when doing a route record.

         0x04  SE Style desired

               This flag indicates that the tunnel ingress node may
               choose to reroute this tunnel without tearing it down.
               A tunnel egress node SHOULD use the SE Style when
               responding with a Resv message.

      Name Length

         The length of the display string before padding, in bytes.

      Session Name

         A null padded string of characters.
**********************************************************************/

typedef struct rsvpSessionAttribLspTunnel_s {
    u_char setupPrio;
    u_char holdPrio;
    u_char flags;
    u_char nameLen;
    u_char name[RSVP_MAX_SESSION_NAME];
} rsvpSessionAttribLspTunnel_t;

typedef struct rsvpSessionAttribLspTunnelRa_s {
    u_int excludeAny;
    u_int includeAny;
    u_int includeAll;
    u_char setupPrio;
    u_char holdPrio;
    u_char flags;
    u_char nameLen;
    u_char name[RSVP_MAX_SESSION_NAME];
} rsvpSessionAttribLspTunnelRa_t;

typedef struct rsvpSessionAttibObject_s {
    struct rsvpObjectHeader_s hdr;
    union {
	struct rsvpSessionAttribLspTunnel_s tunnel;
	struct rsvpSessionAttribLspTunnelRa_s tunnelRa;
    } u;
} rsvpSessionAttibObject_t;

/**********************************************************************

   HELLO REQUEST object

   Class = HELLO Class, C_Type = 1

    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                         Src_Instance                          |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                         Dst_Instance                          |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

   HELLO ACK object

   Class = HELLO Class, C_Type = 2

    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                         Src_Instance                          |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                         Dst_Instance                          |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

      Src_Instance: 32 bits

      a 32 bit value that represents the sender's instance.  The
      advertiser maintains a per neighbor representation/value.  This
      value MUST change when the sender is reset, when the node reboots,
      or when communication is lost to the neighboring node and
      otherwise remains the same.  This field MUST NOT be set to zero
      (0).

      Dst_Instance: 32 bits

      The most recently received Src_Instance value received from the
      neighbor.  This field MUST be set to zero (0) when no value has
      ever been seen from the neighbor.

**********************************************************************/

typedef struct rsvpHelloObject_s {
    struct rsvpObjectHeader_s hdr;
    u_int src;
    u_int dst;
} rsvpHelloObject_t;

/**********************************************************************
    RSVP Header

    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +---------------+---------------+---------------+---------------+
   | Vers  | Flags |    Msg Type   |         RSVP Checksum         |
   +---------------+---------------+---------------+---------------+
   |   Send_TTL    |  (Reserved)   |          RSVP Length          |
   +---------------+---------------+---------------+---------------+


         The fields in the common header are as follows:

         Vers: 4 bits

              Protocol version number.  This is version 1.

         Flags: 4 bits

              0x01-0x08: Reserved

                   No flag bits are defined yet.

         Msg Type: 8 bits

              1 = Path

              2 = Resv

              3 = PathErr

              4 = ResvErr

              5 = PathTear

              6 = ResvTear

              7 = ResvConf

         RSVP Checksum: 16 bits

              The one's complement of the one's complement sum of the
              message, with the checksum field replaced by zero for the
              purpose of computing the checksum.  An all-zero value
              means that no checksum was transmitted.

         Send_TTL: 8 bits

              The IP TTL value with which the message was sent.  See
              Section 3.8.

         RSVP Length: 16 bits

              The total length of this RSVP message in bytes, including
              the common header and the variable-length objects that
              follow.


**********************************************************************/

typedef struct rsvpHeaderFlag_s {
  BITFIELDS_ASCENDING_2(u_char version:4,
			u_char flags:4);
} rsvpHeaderFlag_s;

#define RSVP_HDRSIZE 8
#define RSVP_PDUMAXLEN 4096

typedef struct rsvpHeader_s {
  union {
    u_char mark;
    struct rsvpHeaderFlag_s flag;
  } flag;
  u_char messageType;
  u_short checksum;
  u_char ttl;
  u_char resserved;
  u_short length;
} rsvpHeader_t;

/**********************************************************************

 The format of a Path message is as follows:

	<Path Message> ::= <Common Header> [ <INTEGRITY> ]
                           <SESSION> <RSVP_HOP> <TIME_VALUES>
                           [ <EXPLICIT_ROUTE> ] <LABEL_REQUEST>
                           [ <SESSION_ATTRIBUTE> ]
                           [ <POLICY_DATA> ... ]
                           <sender descriptor>

	<sender descriptor> ::= <SENDER_TEMPLATE> <SENDER_TSPEC>
                                [ <ADSPEC> ] [ <RECORD_ROUTE> ]

**********************************************************************/

#define RSVP_PATH_MSG 1

typedef struct rsvpPathMsg_s {
    struct rsvpHeader_s header;
    struct rsvpSessionObject_s session;
    struct rsvpHopObject_s hop;
    struct rsvpTimeObject_s time;
    struct rsvpExplicitRouteObject_s ero;
    struct rsvpLabelRequestObject_s labelRequest;
    struct rsvpSessionAttibObject_s sessionAttrib;
    struct rsvpPolicyDataObject_s policyData;
    struct rsvpSenderTemplateObject_s senderTemplate;
    struct rsvpSenderTSpecObject_s senderTSpec;
    struct rsvpAdSpecObject_s adSpec;
    struct rsvpRecordRouteObject_s recordRoute;

    u_short sessionExists:1;
    u_short hopExists:1;
    u_short timeExists:1;
    u_short eroExists:1;
    u_short labelRequestExists:1;
    u_short sessionAttribExists:1;
    u_short policyDataExists:1;
    u_short senderTemplateExists:1;
    u_short senderTSpecExists:1;
    u_short adSpecExists:1;
    u_short recordRouteExists:1;

} rsvpPathMsg_t;

/**********************************************************************
 The Resv message format is as follows:

	<Resv Message> ::= <Common Header> [ <INTEGRITY> ]
                           <SESSION> <RSVP_HOP> <TIME_VALUES>
                           [ <RESV_CONFIRM> ]  [ <SCOPE> ]
                           [ <POLICY_DATA> ... ]
                           <STYLE> <flow descriptor list>

<flow descriptor list> ::= <FF flow descriptor list> | <SE flow descriptor>

<FF flow descriptor list> ::= <FLOWSPEC> <FILTER_SPEC> <LABEL>
                              [ <RECORD_ROUTE> ] | <FF flow descriptor list>
                              <FF flow descriptor>

<FF flow descriptor> ::= [ <FLOWSPEC> ] <FILTER_SPEC> <LABEL>
                         [ <RECORD_ROUTE> ]

<SE flow descriptor> ::= <FLOWSPEC> <SE filter spec list>

<SE filter spec list> ::= <SE filter spec> |
                          <SE filter spec list> <SE filter spec>

<SE filter spec> ::= <FILTER_SPEC> <LABEL> [ <RECORD_ROUTE> ]

      Note:  LABEL and RECORD_ROUTE (if present), are bound to the
             preceding FILTER_SPEC.  No more than one LABEL and/or
             RECORD_ROUTE may follow each FILTER_SPEC.

**********************************************************************/

#define RSVP_REV_MSG 2

typedef struct rsvpResvMsg_s {
    struct rsvpHeader_s header;
    struct rsvpSessionObject_s session;
    struct rsvpHopObject_s hop;
    struct rsvpTimeObject_s time;
    // struct rsvpResvConfirm_s resvConfirm;
    // struct rsvpScopeObject_s scope;
    struct rsvpPolicyDataObject_s policyData;
    struct rsvpStyleObject_s style;

    union {
	struct {
	    struct {
		// struct rsvpFlowSpecObject_s flowSpec;
		// struct rsvpFilterSpecObject_s filterSpec;
		struct rsvpLabelObject_s label;
		struct rsvpRecordRouteObject_s recordRoute;
		u_char flowSpecExists:1;
		u_char filterSpecExists:1;
		u_char labelExists:1;
		u_char recordRouteExits:1;
	    } ffEntry[16];
	    u_char numberFfEntry;
	} ff;
	struct {
	    // struct rsvpFlowSpecObject_s flowSpec;
	    struct {
		// struct rsvpFilterSpecObject_s filterSpec;
		struct rsvpLabelObject_s label;
		struct rsvpRecordRouteObject_s recordRoute;
		u_char filterSpecExists:1;
		u_char labelExists:1;
		u_char recordRouteExits:1;
	    } seEntry[16];
	    u_char numberSeEntry;
	} se;
    } flowDescr;

    u_short sessionExists:1;
    u_short hopExists:1;
    u_short timeExists:1;
    u_short eroExists:1;
    u_short resvConfirmExists:1;
    u_short scopeExists:1;
    u_short policyDataExists:1;
    u_short flowDescrExists:1;
    u_short senderTSpecExists:1;
    u_short adSpecExists:1;
    u_short recordRouteExists:1;

} rsvpResvMsg_t;
/**********************************************************************
 The Path Tear message format is as follows:

	<PathTear Message> ::= <Common Header> [ <INTEGRITY> ]
                               <SESSION> <RSVP_HOP>
                               [ <sender descriptor> ]

	<sender descriptor> ::= (see earlier definition)


**********************************************************************/
/**********************************************************************
 The Resv Tear message format is as follows:

	<ResvTear Message> ::= <Common Header> [<INTEGRITY>]
                               <SESSION> <RSVP_HOP> [ <SCOPE> ] <STYLE>
                               <flow descriptor list>

	<flow descriptor list> ::= (see earlier definition)

**********************************************************************/

/**********************************************************************

 The Path Error message format is as follows:

	<PathErr message> ::= <Common Header> [ <INTEGRITY> ]
                              <SESSION> <ERROR_SPEC> [ <POLICY_DATA> ...]
                              [ <sender descriptor> ]

	<sender descriptor> ::= (see earlier definition)

**********************************************************************/
/**********************************************************************

 The Resv Error message format is as follows:

	<ResvErr Message> ::= <Common Header> [ <INTEGRITY> ]
                              <SESSION> <RSVP_HOP> <ERROR_SPEC> [ <SCOPE> ]
                              [ <POLICY_DATA> ...] <STYLE>
                              [ <error flow descriptor> ]

                o    FF style:
                    <error flow descriptor> ::= <FF flow descriptor>

                o    SE style:
                    <error flow descriptor> ::= <SE flow descriptor>

**********************************************************************/
/**********************************************************************

 The Resv Conf message format is as follows:

	<ResvConf message> ::= <Common Header> [ <INTEGRITY> ]
                               <SESSION> <ERROR_SPEC> <RESV_CONFIRM>
                               <STYLE> <flow descriptor list>

	<flow descriptor list> ::= (see earlier definition)

**********************************************************************/

#endif /* RSVPTE_STRUCT_H */

/*
 *  Copyright (C) James R. Leu 2003
 *  jleu@mindspring.com
 *
 *  This software is covered under the LGPL, for more
 *  info check out http://www.gnu.org/copyleft/lgpl.html
 */

#ifndef _RSVPTE_IF_H_
#define _RSVPTE_IF_H_

#include "rsvpte_struct.h"

extern rsvpte_if *rsvpte_if_create();
extern void rsvpte_if_delete(rsvpte_if * i);
extern mpls_return_enum rsvpte_if_startup(rsvpte_global * g, rsvpte_if * i);
extern mpls_return_enum rsvpte_if_shutdown(rsvpte_global * g, rsvpte_if * i);
extern mpls_bool rsvpte_if_is_active(rsvpte_if * i);
extern uint32_t _rsvpte_if_get_next_index();

#endif

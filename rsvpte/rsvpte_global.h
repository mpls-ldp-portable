/*
 *  Copyright (C) James R. Leu 2002
 *  jleu@mindspring.com
 *
 *  This software is covered under the LGPL, for more
 *  info check out http://www.gnu.org/copyleft/lgpl.html
 */

#ifndef _RSVPTE_GLOBAL_H_
#define _RSVPTE_GLOBAL_H_

#include "rsvpte_struct.h"

extern rsvpte_global *rsvpte_global_create(mpls_instance_handle data);
extern mpls_return_enum rsvpte_global_delete(rsvpte_global * g);
extern mpls_return_enum rsvpte_global_startup(rsvpte_global * g);
extern mpls_return_enum rsvpte_global_shutdown(rsvpte_global * g);

extern rsvpte_if *rsvpte_global_find_if_handle(rsvpte_global * g,
    mpls_if_handle handle);

extern mpls_return_enum rsvpte_global_find_if_index(rsvpte_global * g,
    uint32_t index, rsvpte_if **);

extern void _rsvpte_global_add_if(rsvpte_global * g, rsvpte_if * i);
extern void _rsvpte_global_del_if(rsvpte_global * g, rsvpte_if * i);

#endif

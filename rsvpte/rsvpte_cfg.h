/*
 *  Copyright (C) James R. Leu 2003
 *  jleu@mindspring.com
 *
 *  This software is covered under the LGPL, for more
 *  info check out http://www.gnu.org/copyleft/lgpl.html
 */

#ifndef RSVPTE_CFG_H
#define RSVPTE_CFG_H

#include "rsvpte_struct.h"

#define RSVPTE_CFG_ADD				0x00000001
#define RSVPTE_CFG_DEL				0x10000000

#define RSVPTE_GLOBAL_CFG_ADMIN_STATE		0x00000002
#define RSVPTE_GLOBAL_CFG_LSR_IDENTIFIER	0x00000004
#define RSVPTE_GLOBAL_CFG_LSR_HANDLE		0x00000008

#define RSVPTE_GLOBAL_CFG_WHEN_DOWN		(RSVPTE_CFG_DEL|\
					RSVPTE_GLOBAL_CFG_LSR_IDENTIFIER|\
						RSVPTE_GLOBAL_CFG_LSR_HANDLE)

#define RSVPTE_IF_CFG_LABEL_SPACE		0x00000002
#define RSVPTE_IF_CFG_INDEX			0x00000004
#define RSVPTE_IF_CFG_ADMIN_STATE		0x00000008
#define RSVPTE_IF_CFG_OPER_STATE		0x00000010
#define RSVPTE_IF_CFG_LOCAL_SOURCE_ADDR		0x00000020
#define RSVPTE_IF_CFG_NAME			0x00000040

#define RSVPTE_IF_CFG_WHEN_DOWN			(RSVPTE_CFG_DEL|\
						RSVPTE_IF_CFG_LABEL_SPACE|\
						RSVPTE_IF_CFG_LOCAL_SOURCE_ADDR)

extern mpls_cfg_handle rsvpte_cfg_open(mpls_instance_handle data);
extern void rsvpte_cfg_close(mpls_cfg_handle handle);

extern mpls_return_enum rsvpte_cfg_global_get(mpls_cfg_handle handle,
    rsvpte_global * g, uint32_t flag);
extern mpls_return_enum rsvpte_cfg_global_set(mpls_cfg_handle handle,
    rsvpte_global * g, uint32_t flag);

extern mpls_return_enum rsvpte_cfg_if_get(mpls_cfg_handle handle,
    rsvpte_if * i, uint32_t flag);
extern mpls_return_enum rsvpte_cfg_if_getnext(mpls_cfg_handle handle,
    rsvpte_if * i, uint32_t flag);
extern mpls_return_enum rsvpte_cfg_if_test(mpls_cfg_handle handle,
    rsvpte_if * i, uint32_t flag);
extern mpls_return_enum rsvpte_cfg_if_set(mpls_cfg_handle handle,
    rsvpte_if * i, uint32_t flag);

#endif

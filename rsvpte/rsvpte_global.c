/*
 *  Copyright (C) James R. Leu 2003
 *  jleu@mindspring.com
 *
 *  This software is covered under the LGPL, for more
 *  info check out http://www.gnu.org/copyleft/lgpl.html
 */

#include "rsvpte_struct.h"
#include "rsvpte_global.h"
#include "rsvpte_if.h"

#include "mpls_socket_impl.h"
#include "mpls_timer_impl.h"
#include "mpls_ifmgr_impl.h"
#include "mpls_tree_impl.h"
#include "mpls_lock_impl.h"
#include "mpls_fib_impl.h"
#include "mpls_policy_impl.h"
#include "mpls_mm_impl.h"
#include "mpls_trace_impl.h"

#if MPLS_USE_LSR
#include "lsr_cfg.h"
#else
#include "mpls_mpls_impl.h"
#endif

void _rsvpte_global_fib_callback(mpls_cfg_handle handle, mpls_update_enum type,
  mpls_fec * dest)
{
  rsvpte_global *cfg = (rsvpte_global*)handle;

  LDP_ENTER(cfg->user_data, "_rsvpte_global_fib_callback");

  mpls_lock_get(cfg->global_lock);

  mpls_lock_release(cfg->global_lock);

  LDP_EXIT(cfg->user_data, "_rsvpte_global_fib_callback");
}

void _rsvpte_global_ifmgr_callback(mpls_cfg_handle handle, const mpls_update_enum type, mpls_inet_addr *addr)
{
  rsvpte_global *cfg = (rsvpte_global*)handle;

  LDP_ENTER(cfg->user_data, "_rsvpte_global_ifmgr_callback");

  mpls_lock_get(cfg->global_lock);

  mpls_lock_release(cfg->global_lock);

  LDP_EXIT(cfg->user_data, "_rsvpte_global_ifmgr_callback");
}

rsvpte_global *rsvpte_global_create(mpls_instance_handle data)
{
  rsvpte_global *g = (rsvpte_global *) mpls_malloc(sizeof(rsvpte_global));

  if (g) {
    memset(g, 0, sizeof(rsvpte_global));

    LDP_ENTER(g->user_data, "rsvpte_global_create");

    g->global_lock = mpls_lock_create("_rsvpte_global_lock_");
    mpls_lock_get(g->global_lock);

    MPLS_LIST_INIT(&g->iff, rsvpte_if);

    g->admin_state = MPLS_ADMIN_DISABLE;
    g->user_data = data;

    mpls_lock_release(g->global_lock);

    LDP_EXIT(g->user_data, "rsvpte_global_create");
  }

  return g;
}

mpls_return_enum rsvpte_global_startup(rsvpte_global * g)
{
  MPLS_ASSERT(g != NULL);

  LDP_ENTER(g->user_data, "rsvpte_global_startup");

  if (g->lsr_identifier.type == MPLS_FAMILY_NONE) {
/*
 * RSVPTE_TRACE_LOG(g->user_data, MPLS_TRACE_STATE_ALL, RSVPTE_TRACE_FLAG_ERROR,
 *    "rsvpte_global_startup: invalid LSRID\n");
 */
    goto rsvpte_global_startup_cleanup;
  }

  g->timer_handle = mpls_timer_open(g->user_data);
  if (mpls_timer_mgr_handle_verify(g->timer_handle) == MPLS_BOOL_FALSE) {
    goto rsvpte_global_startup_cleanup;
  }

  g->socket_handle = mpls_socket_mgr_open(g->user_data);
  if (mpls_socket_mgr_handle_verify(g->socket_handle) == MPLS_BOOL_FALSE) {
    goto rsvpte_global_startup_cleanup;
  }

  g->ifmgr_handle = mpls_ifmgr_open(g->user_data, g);
  if (mpls_ifmgr_handle_verify(g->ifmgr_handle) == MPLS_BOOL_FALSE) {
    goto rsvpte_global_startup_cleanup;
  }

  g->fib_handle = mpls_fib_open(g->user_data, g);
  if (mpls_fib_handle_verify(g->fib_handle) == MPLS_BOOL_FALSE) {
    goto rsvpte_global_startup_cleanup;
  }

#if MPLS_USE_LSR
  if (!g->lsr_handle) {
    goto rsvpte_global_startup_cleanup;
  }
#else
  g->mpls_handle = mpls_mpls_open(g->user_data);
  if (mpls_mpls_handle_verify(g->mpls_handle) == MPLS_BOOL_FALSE) {
    goto rsvpte_global_startup_cleanup;
  }
#endif

  g->ra_socket = mpls_socket_create_raw(g->socket_handle, IPPROTO_RSVP);
  if (mpls_socket_handle_verify(g->socket_handle, g->ra_socket) == MPLS_BOOL_FALSE) {
  /*
   *RSVPTE_TRACE_LOG(g->user_data, MPLS_TRACE_STATE_ALL,
   *  RSVPTE_TRACE_FLAG_DEBUG,
   *  "rsvpte_global_startup: error creating UDP socket\n");
   */
    goto rsvpte_global_startup_cleanup;
  }

  if (mpls_socket_options(g->socket_handle, g->ra_socket,
      MPLS_SOCKOP_NONBLOCK | MPLS_SOCKOP_REUSE |
      MPLS_SOCKOP_ROUTERALERT | MPLS_SOCKOP_HDRINCL) == MPLS_FAILURE) {
  /*
   *RSVPTE_TRACE_LOG(g->user_data, MPLS_TRACE_STATE_ALL,
   *  RSVPTE_TRACE_FLAG_DEBUG,
   *  "rsvpte_global_startup: error setting RAW socket options\n");
   */
    goto rsvpte_global_startup_cleanup;
  }
  mpls_socket_readlist_add(g->socket_handle, g->ra_socket, 0, MPLS_SOCKET_UDP_DATA);

  g->admin_state = MPLS_ADMIN_ENABLE;

  LDP_EXIT(g->user_data, "rsvpte_global_startup");
  return MPLS_SUCCESS;

rsvpte_global_startup_cleanup:
  rsvpte_global_shutdown(g);
  mpls_socket_close(g->socket_handle, g->ra_socket);
  g->ra_socket = 0;

  LDP_EXIT(g->user_data, "rsvpte_global_startup-error");

  return MPLS_FAILURE;
}

mpls_return_enum rsvpte_global_shutdown(rsvpte_global * g)
{
  MPLS_ASSERT(g);

  LDP_ENTER(g->user_data, "rsvpte_global_shutdown");

  g->admin_state = MPLS_ADMIN_DISABLE;

  mpls_socket_readlist_del(g->socket_handle, g->ra_socket);
  mpls_socket_close(g->socket_handle, g->ra_socket);

  mpls_lock_release(g->global_lock);
  mpls_timer_close(g->timer_handle);
  mpls_lock_get(g->global_lock);

  mpls_socket_mgr_close(g->socket_handle);
  mpls_ifmgr_close(g->ifmgr_handle);
  mpls_fib_close(g->fib_handle);

#if MPLS_USE_LSR
#else
  mpls_mpls_close(g->mpls_handle);
#endif

  LDP_EXIT(g->user_data, "rsvpte_global_shutdown");

  return MPLS_SUCCESS;
}

mpls_return_enum rsvpte_global_delete(rsvpte_global * g)
{
  if (g) {
    mpls_lock_delete(g->global_lock);
    LDP_PRINT(g->user_data, "global delete\n");
    mpls_free(g);
  }
  return MPLS_SUCCESS;
}

void _rsvpte_global_add_if(rsvpte_global * g, rsvpte_if * i)
{
  rsvpte_if *ip = NULL;

  MPLS_ASSERT(g && i);
  MPLS_REFCNT_HOLD(i);
  ip = MPLS_LIST_HEAD(&g->iff);
  while (ip != NULL) {
    if (ip->index > i->index) {
      MPLS_LIST_INSERT_BEFORE(&g->iff, ip, i, _global);
      return;
    }
    ip = MPLS_LIST_NEXT(&g->iff, ip, _global);
  }
  MPLS_LIST_ADD_TAIL(&g->iff, i, _global, rsvpte_if);
}

void _rsvpte_global_del_if(rsvpte_global * g, rsvpte_if * i)
{
  MPLS_ASSERT(g && i);
  MPLS_LIST_REMOVE(&g->iff, i, _global);
  MPLS_REFCNT_RELEASE(i, rsvpte_if_delete);
}

mpls_return_enum rsvpte_global_find_if_index(rsvpte_global * g, uint32_t index,
  rsvpte_if ** iff)
{
  rsvpte_if *i = NULL;

  if (g && index > 0) {

    /* because we sort our inserts by index, this lets us know
       if we've "walked" past the end of the list */

    i = MPLS_LIST_TAIL(&g->iff);
    if (i == NULL || i->index < index) {
      *iff = NULL;
      return MPLS_END_OF_LIST;
    }

    i = MPLS_LIST_HEAD(&g->iff);
    while (i != NULL) {
      if (i->index == index) {
        *iff = i;
        return MPLS_SUCCESS;
      }
      i = MPLS_LIST_NEXT(&g->iff, i, _global);
    }
  }
  *iff = NULL;
  return MPLS_FAILURE;
}

rsvpte_if *rsvpte_global_find_if_handle(rsvpte_global * g, mpls_if_handle handle)
{
  rsvpte_if *i = MPLS_LIST_HEAD(&g->iff);

  if (g) {
    while (i != NULL) {
      if (!mpls_if_handle_compare(i->handle, handle))
        return i;

      i = MPLS_LIST_NEXT(&g->iff, i, _global);
    }
  }
  return NULL;
}

/******************************************************************************
*                       Nortel Networks Software License                      *
*                                                                             *
* READ THE TERMS OF THIS LICENSE CAREFULLY.  BY USING, MODIFYING, OR          *
* DISTRIBUTING THIS SOFTWARE AND ANY ACCOMPANYING DOCUMENTATION (COLLECTIVELY,*
* "SOFTWARE") YOU ARE AGREEING TO ALL OF THE TERMS OF THIS LICENSE.           *
*                                                                             *
* 1.      Nortel Telecom Limited, on behalf of itself and its subsidiaries    *
* (collectively "Nortel Networks") grants to you a non-exclusive, perpetual,  *
* world-wide right to use, copy, modify, and distribute the Software at no    *
* charge.                                                                     *
*                                                                             *
* 2.      You may sublicense recipients of redistributed Software to use,     *
* copy, modify, and distribute the Software on substantially the same terms as*
* this License.  You may not impose any further restrictions on the           *
* recipient's exercise of the rights in the Software granted under this       *
* License.  Software distributed to other parties must be accompanied by a    *
* License containing a grant, disclaimer and limitation of liability          *
* substantially in the form of 3, 4, and 5 below provided that references to  *
* "Nortel Networks" may be changed to "Supplier".                             *
*                                                                             *
* 3.      Nortel Networks reserves the right to modify and release new        *
* versions of the Software from time to time which may include modifications  *
* made by third parties like you. Accordingly, you agree that you shall       *
* automatically grant a license to Nortel Networks to include, at its option, *
* in any new version of the Software any modifications to the Software made by*
* you and made available directly or indirectly to Nortel Networks.  Nortel   *
* Networks shall have the right to use, copy, modify, and distribute any such *
* modified Software on substantially the same terms as this License.          *
*                                                                             *
* 4.      THE SOFTWARE IS PROVIDED ON AN "AS IS" BASIS.  NORTEL NETWORKS AND  *
* ITS AGENTS AND SUPPLIERS DISCLAIM ALL REPRESENTATIONS, WARRANTIES AND       *
* CONDITIONS RELATING TO THE SOFTWARE, INCLUDING, BUT NOT LIMITED TO, IMPLIED *
* WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND         *
* NON-INFRINGEMENT OF THIRD-PARTY INTELLECTUAL PROPERTY RIGHTS.  NORTEL       *
* NETWORKS AND ITS AGENTS AND SUPPLIERS DO NOT WARRANT, GUARANTEE, OR MAKE ANY*
* REPRESENTATIONS REGARDING THE USE, OR THE RESULTS OF THE USE, OF THE        *
* SOFTWARE IN TERMS OR CORRECTNESS, ACCURACY, RELIABILITY, CURRENTNESS, OR    *
* OTHERWISE.                                                                  *
*                                                                             *
* 5.      NEITHER NORTEL NETWORKS NOR ANY OF ITS AGENTS OR SUPPLIERS SHALL BE *
* LIABLE FOR ANY DIRECT, INDIRECT, CONSEQUENTIAL, INCIDENTAL OR EXEMPLARY     *
* DAMAGES, OR ECONOMIC LOSSES (INCLUDING DAMAGES FOR LOSS OF BUSINESS PROFITS,*
* BUSINESS INTERRUPTION, LOSS OF BUSINESS INFORMATION AND THE LIKE), ARISING  *
* FROM THE SOFTWARE OR THIS LICENSE AGREEMENT, EVEN IF NORTEL NETWORKS OR SUCH*
* AGENT OR SUPPLIER HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES OR    *
* LOSSES, AND WHETHER ANY SUCH DAMAGE OR LOSS ARISES OUT OF CONTRACT, TORT, OR*
* OTHERWISE.                                                                  *
*                                                                             *
* 6.      This License shall be governed by the laws of the Province of       *
* Ontario, Canada.                                                            *
*******************************************************************************/

/******************************************************************************
 * This file contains the C implementation for encode/decode functions        * 
 * for the following types of messages: notification, hello, initialization,  *
 * keepAlive, address, address Withdraw, label Mapping, label Request, label  *
 * Withdraw and label Release. There are also encode/decode methods for all   * 
 * tlv types required by the previously enumerated messages.                  * 
 * Please remember that the pdu will always contain the header followed by 1  *
 * or more LDP messages. The file contains functions to encode/decode the LDP *
 * header as well.  							      * 
 * All the messages, header message and the tlvs are in conformity with the   * 
 * draft-ietf-mpls-ldp-04  (May 1999) and with draft-ietf-mpls-cr-ldp-01      *
 * (Jan 1999). 								      * 
 *								              *
 * Please note that the U bit in the message and the F bit in the tlv are     *
 * ignored in this version of the code.                                       *
 *								              *
 * Please note that the traffic parameters for traffic TLV have to be IEEE    *
 * single precision floating point numbers.                                   *
 *								              *
 * Please note that there might be a small chance for bit field manipulation  *
 * portability inconsistency. If such problems occure, the code requires      *
 * changes for a suitable bit-field manipulation. The code for encoding and   *
 * decoding makes the assumption that the compiler packs the bit fields in a  *
 * structure into adjacent bits of the same unit.                             * 
 *								              *
 * The usage of the encode/decode functions is described below.               * 
 *								              *
 * The encode functions take as arguments: a pointer to the structure which   *
 * implements msg/tlv, a nbuffer (where the encoding is done) and the buffer   *
 * length.							              *
 * If the encode is successfull, the function returns the total encoded       * 
 * length.								      *
 * If the encode fails, the function returns an error code.                   *
 * The encode functions for messages and message headers do not modify the    *
 * content of the struct which is to be encoded. All the other encode         *
 * functions will change the content of the structure. The pointer which      *
 * points to the beginning of the buffer is not changed.                      *
 *									      *
 * The decode functions take as arguments: a pointer to the structure which   *
 * is going to be populated after decoding, a pointer to a buffer and the     *
 * buffer length.							      *
 * If the decode is successful, the function returns the total decoded length *
 * If the decode fails, the function returns an error code. The decode        *
 * functions do not modify the pointer to the buffer which contains the data  *
 * to be decoded.							      *
 *									      *
 * Example on how to use the encode/decode functions for a keepAlive message: *
 *									      *
 *           Encode the keep alive message:                                   * 
 *           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^    				      *
 *           u_char buffer[500];	 				      *
 *           int returnCode; 						      *
 *           struct mplsLdpKeepAlMsg_s keepAliveMsg;            	      *
 *           keepAliveMsg.baseMsg.msgType   = MPLS_KEEPAL_MSGTYPE;            *
 *           keepAliveMsg.baseMsg.msgLength = MPLS_MSGIDFIXLEN;               *
 *           keepAliveMsg.baseMsg.msgId     = 123;		              *
 *           bzero(buffer, 500);                                  	      *
 *           returnCode = Mpls_encodeLdpKeepAliveMsg(&keepAliveMsg,           *
 *                                                   buffer,                  *
 *                                                   500);                    *
 *           if (returnCode < 0)                                              *
 *              check the error code				              *
 *           else                                                             *
 *              write(fd, buffer, returnCode);                                *
 *									      *
 *									      *
 *           Decode the keep alive meesage:                                   *
 *           ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^		                      *
 *           u_char buffer[500];					      *
 *           int returnCode;					              *
 *           struct mplsLdpKeepAlMsg_s keepAliveMsg;	            	      *
 *           read(fd, buffer, length);                                        *
 *           returnCode =  Mpls_decodeLdpKeepAliveMsg(&keepAliveMsg,          * 
 *                                                    buffer,                 *
 *                                                    500); 		      *
 *           if (returnCode < 0)	                                      *
 *              check the error code					      *
 *           else				 			      *
 *           { 								      *
 *              printKeepAliveMsg(&keepAliveMsg);	 	              *
 *           } 						                      *
 *								              *
 * An example on how to use the decode functions for the header and the       *
 * messages can be found in the main function.                                *
 *								              *
 * The code was tested for big endian and little endian for sparc5, linux     *
 * and i960.                                                                  *
 *								              *
 * In order to compile for little endian, the LITTLE_ENDIAN_BYTE_ORDER should *
 * be defined.								      *
 *								              *
 * At the end of this file there is an examples of a hex buffers and its      *
 * corresponding values.                                                      *
 *								              *
 *								              *
 * Version History                                                            *
 * Version          Date      Authors            Description                  *
 * ===========      ========  =========          ======================       *
 * mpls_encdec_01.c 99/03/15  Antonela Paraschiv draft-ietf-mpls-ldp-03 and   * 
 *                                               draft-ietf-mpls-cr-ldp-01    *
 *								              *
 * mpls_encdec_02.c 99/05/19  Antonela Paraschiv draft-ietf-mpls-ldp-04 and   * 
 *                                               draft-ietf-mpls-cr-ldp-01    *
 *								              *
 ******************************************************************************/


#define LITTLE_ENDIAN_BYTE_ORDER 1  /* <----- Please define this accordingly */
/* #define VXWORKS                  1 */
 
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef VXWORKS
   #include <in.h>         /* htons, htonl, ntohs, ntohl         */
   #include <types.h>      /* u_int, u_char, u_short, float etc. */
#else
   #include <netinet/in.h> /* htons, htonl, ntohs, ntohl         */
   #include <sys/types.h>  /* u_int, u_char, u_short, float etc. */
#endif VXWORKS 


#define MEM_COPY(X, Y, Z) memcpy(X, Y, Z)

/* macros to handle different byte orders (little endian or big endian) */
#ifdef LITTLE_ENDIAN_BYTE_ORDER 
   #define BITFIELDS_ASCENDING_2(X, Y)                Y; X;
   #define BITFIELDS_ASCENDING_3(X, Y, Z)             Z; Y; X;
   #define BITFIELDS_ASCENDING_4(X, Y, Z, W)          W; Z; Y; X;
   #define BITFIELDS_ASCENDING_7(X, Y, Z, W, U, A, B) B; A; U; W; Z; Y; X;
# else
   #define BITFIELDS_ASCENDING_2(X, Y)                X; Y;
   #define BITFIELDS_ASCENDING_3(X, Y, Z)             X; Y; Z;
   #define BITFIELDS_ASCENDING_4(X, Y, Z, W)          X; Y; Z; W;
   #define BITFIELDS_ASCENDING_7(X, Y, Z, W, U, A, B) X; Y; Z; W; U; A; B;
#endif LITTLE_ENDIAN_BYTE_ORDER 

/* macros used to decode the entire LDP; they declare local var for
   different type of messages */
#define MPLS_MSGSTRUCT( e )                 \
   mplsLdp ## e ## Msg_t test ## e ## Msg;  \

#define MPLS_MSGPARAM( e )                  \
   test ## e ## Msg                         \


#define DEBUG_INFO              1  /* set this to 0 if no debug info 
				      required                      */

/* debug macros */
#define DEBUG_CALL(X)           if (DEBUG_INFO) X;
#define PRINT_2(X, Y)           if (DEBUG_INFO) printf(X, Y)
#define PRINT_4(X, Y, Z, W)     if (DEBUG_INFO) printf(X, Y, Z, W)
#define PRINT_ERR(X)            if (DEBUG_INFO) printf(X)
#define PRINT_ERR_2(X, Y)       if (DEBUG_INFO) printf(X, Y)
#define PRINT_ERR_4(X, Y, Z, W) if (DEBUG_INFO) printf(X, Y, Z, W)


/*
 *    MESSAGE TYPE CONSTANS & TLV CONSTANTS
 */

#define MPLS_LDP_HDRSIZE         10      /* the size for mpls ldp hdr  */
#define MPLS_TLVFIXLEN           4       /* type + len                 */
#define MPLS_MSGIDFIXLEN         4       /* type + len                 */
#define MPLS_LDPIDLEN            6 
#define MPLS_PDUMAXLEN           4096     /* octets                    */
#define MPLS_VERSION             0x0001  
#define MPLS_IPV4ADDRFAMILYN     0x0100   /* rfc 1700 (network order)  */
#define MPLS_INIFINITE_TIMEVAL   0xfffff

/* for initialize message */
#define MPLS_INIT_MSGTYPE        0x0200  /* initialization msg          */
#define MPLS_CSP_TLVTYPE         0x0500  /* common params for init msg  */
#define MPLS_ASP_TLVTYPE         0x0501  /* atm session params          */
#define MPLS_FSP_TLVTYPE         0x0502  /* frame relay session params  */
#define MPLS_ASPFIXLEN           4       /* M + N + D + res             */
#define MPLS_FSPFIXLEN           4       /* M + N + res                 */
#define MPLS_CSPFIXLEN           14      /* protocolV + ... + ldp ids   */
#define MPLS_ATMLBLMAXLEN        10
#define MPLS_ATMLRGFIXLEN        8      
#define MPLS_FRLRGFIXLEN         8
#define MPLS_ASP_NOMERGE         0
#define MPLS_ASP_VPMERGE         1  
#define MPLS_ASP_VCMERGE         2  
#define MPLS_ASP_VPVCMERGE       3  
#define MPLS_FRLBLMAXLEN         10
#define MPLS_FRDLCI10BITS        0
#define MPLS_FRDLCI17BITS        1
#define MPLS_FRDLCI23BITS        2
 
/* for notification message */
#define MPLS_NOT_MSGTYPE         0x0001  /* notification msg            */
#define MPLS_NOT_ST_TLVTYPE      0x0300  /* status tlv for not msg      */
#define MPLS_NOT_ES_TLVTYPE      0x0301  /* extended status for not msg */
#define MPLS_NOT_RP_TLVTYPE      0x0302  /* returned PDU for not msg    */
#define MPLS_NOT_RM_TLVTYPE      0x0303  /* returned msg for not msg    */
#define MPLS_STATUSFIXLEN        10      /* status code + id + type     */
#define MPLS_EXSTATUSLEN         4 
#define MPLS_NOT_MAXSIZE         MPLS_PDUMAXLEN - MPLS_TLVFIXLEN - \
				 MPLS_MSGIDFIXLEN 
 
/* for hello message */
#define MPLS_HELLO_MSGTYPE       0x0100  /* hello msg                   */
#define MPLS_CHP_TLVTYPE         0x0400  /* Common Hello Param Tlv      */
#define MPLS_TRADR_TLVTYPE       0x0401  /* Transport Address Param Tlv */
#define MPLS_CSN_TLVTYPE         0x0402  /* Conf Seq Number Param Tlv   */
#define MPLS_CHPFIXLEN           4 
#define MPLS_CSNFIXLEN           4 
#define MPLS_TRADRFIXLEN         4 

/* for keep alive message */
#define MPLS_KEEPAL_MSGTYPE      0x0201  /* keep alive msg              */

/* for address messages */
#define MPLS_ADDR_MSGTYPE        0x0300  /* address msg                 */
#define MPLS_ADDRWITH_MSGTYPE    0x0301  /* address withdraw msg        */
#define MPLS_ADDRLIST_TLVTYPE    0x0101  /* addrss list tlv type        */
#define MPLS_IPv4LEN             4
#define MPLS_ADDFAMFIXLEN        2
#define MPLS_ADDLISTMAXLEN       MPLS_PDUMAXLEN - 2*MPLS_TLVFIXLEN - \
			         MPLS_MSGIDFIXLEN - MPLS_ADDFAMFIXLEN 
#define MPLS_MAXNUMBERADR        MPLS_ADDLISTMAXLEN / 4 

/* for label mapping message */
#define MPLS_LBLMAP_MSGTYPE      0x0400  /* label mapping msg           */
#define MPLS_FEC_TLVTYPE         0x0100  /* label mapping msg           */
#define MPLS_GENLBL_TLVTYPE      0x0200  /* generic label tlv           */
#define MPLS_ATMLBL_TLVTYPE      0x0201  /* atm label tlv               */
#define MPLS_FRLBL_TLVTYPE       0x0202  /* frame relay label tlv       */
#define MPLS_HOPCOUNT_TLVTYPE    0x0103  /* ho count tlv                */
#define MPLS_PATH_TLVTYPE        0x0104  /* path vector tlv             */
#define MPLS_REQMSGID_TLVTYPE    0x0600  /* lbl request msg id tlv      */
#define MPLS_WC_FEC              0x01    /* wildcard fec element        */
#define MPLS_PREFIX_FEC          0x02    /* prefix fec element          */
#define MPLS_HOSTADR_FEC         0x03    /* host addr fec element       */
#define MPLS_CRLSP_FEC           0x04    /* crlsp fec element           */
#define MPLS_MARTINIVC_FEC       0x80    /* Martini VC fec element      */
#define MPLS_FECMAXLEN           MPLS_PDUMAXLEN - 2*MPLS_TLVFIXLEN - \
			         MPLS_MSGIDFIXLEN
#define MPLS_LBLFIXLEN           4       /* v + vpi + vci + res         */
#define MPLS_HOPCOUNTFIXLEN      1       /* v + vpi + vci + res         */
#define MPLS_VCIDLEN             4       /* Martini VC id length        */
#define MPLS_VCIFPARAMMAXLEN     80      /* Martini VC Intf param max size */
#define MPLS_VCIFPARAM_HDR       2       /* size of TL in a VC Intf Param TLV */
#define MPLS_FEC_ELEMTYPELEN     1       
#define MPLS_FEC_PRELENLEN       1       
#define MPLS_FEC_ADRFAMLEN       2       
#define MPLS_FEC_CRLSPLEN        4       /* length of cr lsp fec        */
#define MPLS_FEC_MARTINILEN      8       /* length of the min martin fec*/
#define MPLS_MAXHOPSNUMBER       20      /* max # hops in path vector   */
#define MPLS_MAXNUMFECELEMENT    10      /* max # of fec elements       */

#define MPLS_VCIFPARAM_MTU       0x1
#define MPLS_VCIFPARAM_ATMCONCAT 0x2
#define MPLS_VCIFPARAM_INFO      0x3
#define MPLS_VCIFPARAM_CEMBYTES  0x4
#define MPLS_VCIFPARAM_CEMOPTION 0x5

/* for label request message */
#define MPLS_LBLREQ_MSGTYPE      0x0401  /* label request msg           */
#define MPLS_LBLMSGID_TLVTYPE    0x0601  /* lbl return msg id tlv       */

/* for label withdraw and release messages */
#define MPLS_LBLWITH_MSGTYPE     0x0402  /* label withdraw msg          */
#define MPLS_LBLREL_MSGTYPE      0x0403  /* label release msg           */

/* for ER tlvs */
#define MPLS_ER_TLVTYPE          0x0800  /* constraint routing tlv      */
#define MPLS_TRAFFIC_TLVTYPE     0x0810  /* traffic parameters tlv      */
#define MPLS_PDR_TLVTYPE         0x0811  /* traffic peak data rate tlv  */
#define MPLS_CDR_TLVTYPE         0x0812  /* committed data rate tlv     */
#define MPLS_CBT_TLVTYPE         0x0813  /* committed burst tolerance   */
#define MPLS_PREEMPT_TLVTYPE     0x0820  /* preemption tlv              */
#define MPLS_LSPID_TLVTYPE       0x0821  /* lspid tlv                   */
#define MPLS_RESCLASS_TLVTYPE    0x0822  /* resource class tlv          */
#define MPLS_PINNING_TLVTYPE     0x0823  /* route pinning tlv           */
#define MPLS_ERHOP_IPV4_TLVTYPE  0x801   /* explicit routing ipv4 tlv   */
#define MPLS_ERHOP_IPV6_TLVTYPE  0x802   /* explicit routing ipv6 tlv   */
#define MPLS_ERHOP_AS_TLVTYPE    0x803   /* explicit routing autonomous
				 	    system number tlv           */
#define MPLS_ERHOP_LSPID_TLVTYPE 0x804   /* explicit routing lspid tlv  */
#define MPLS_ERHOP_IPV4_FIXLEN   8       /* fix length in bytes         */
#define MPLS_ERHOP_IPV6_FIXLEN   20      /* fix length in bytes         */
#define MPLS_ERHOP_AS_FIXLEN     4       /* fix length in bytes         */
#define MPLS_ERHOP_LSPID_FIXLEN  8       /* fix length in bytes         */
#define MPLS_IPV6ADRLENGTH       16
#define MPLS_MAX_ER_HOPS         20      /* decent number of hops; 
				 	    change if required          */
#define MPLS_PREEMPTTLV_FIXLEN   4       /* setPrio + holdPrio + res    */
#define MPLS_LSPIDTLV_FIXLEN     8       /* res + crlspId + routerId    */
#define MPLS_TRAFFICPARAMLENGTH  4       /* traffic parameters length   */

/* for label abort request message */
#define MPLS_LBLABORT_MSGTYPE 0x0404     /* label abort request msg     */


/*
 * Error codes
 */

#define MPLS_ENC_BUFFTOOSMALL    -1
#define MPLS_DEC_BUFFTOOSMALL    -2
#define MPLS_ENC_TLVERROR        -3
#define MPLS_DEC_TLVERROR        -4
#define MPLS_ENC_ATMLBLERROR     -5
#define MPLS_DEC_ATMLBLERROR     -6
#define MPLS_ENC_BASEMSGERROR    -7
#define MPLS_DEC_BASEMSGERROR    -8
#define MPLS_ENC_CSPERROR        -9
#define MPLS_DEC_CSPERROR        -10
#define MPLS_ENC_ASPERROR        -11
#define MPLS_DEC_ASPERROR        -12
#define MPLS_ENC_FSPERROR        -13
#define MPLS_DEC_FSPERROR        -14
#define MPLS_ENC_STATUSERROR     -16
#define MPLS_DEC_STATUSERROR     -17
#define MPLS_ENC_EXSTATERROR     -18
#define MPLS_DEC_EXSTATERROR     -19
#define MPLS_ENC_RETPDUERROR     -20
#define MPLS_DEC_RETPDUERROR     -21
#define MPLS_ENC_RETMSGERROR     -22
#define MPLS_DEC_RETMSGERROR     -23
#define MPLS_PDU_LENGTH_ERROR    -24
#define MPLS_ENC_CHPERROR        -25
#define MPLS_DEC_CHPERROR        -26
#define MPLS_ENC_CSNERROR        -27
#define MPLS_DEC_CSNERROR        -28
#define MPLS_ENC_TRADRERROR      -29
#define MPLS_DEC_TRADRERROR      -30
#define MPLS_ENC_ADRLISTERROR    -31
#define MPLS_DEC_ADRLISTERROR    -32
#define MPLS_WC_FECERROR         -33
#define MPLS_PATHVECTORERROR     -34
#define MPLS_ENC_FECERROR        -35
#define MPLS_DEC_FECERROR        -36
#define MPLS_ENC_GENLBLERROR     -37
#define MPLS_DEC_GENLBLERROR     -38
#define MPLS_ENC_MAPATMERROR     -39
#define MPLS_DEC_MAPATMERROR     -40
#define MPLS_ENC_FRLBLERROR      -41
#define MPLS_DEC_FRLBLERROR      -42
#define MPLS_ENC_COSERROR        -43
#define MPLS_DEC_COSERROR        -44
#define MPLS_ENC_HOPCOUNTERROR   -45
#define MPLS_DEC_HOPCOUNTERROR   -46
#define MPLS_ENC_PATHVECERROR    -47
#define MPLS_DEC_PATHVECERROR    -48
#define MPLS_ENC_LBLMSGIDERROR   -49
#define MPLS_DEC_LBLMSGIDERROR   -50
#define MPLS_ENC_HDRTLVERROR     -51
#define MPLS_DEC_HDRTLVERROR     -52
#define MPLS_ENC_FECELEMERROR    -53
#define MPLS_DEC_FECELEMERROR    -54
#define MPLS_ENC_FSPLBLERROR     -55
#define MPLS_DEC_FSPLBLERROR     -56
#define MPLS_ENC_ERHOPERROR      -57
#define MPLS_DEC_ERHOPERROR      -58
#define MPLS_ENC_ERTLVERROR      -59
#define MPLS_DEC_ERTLVERROR      -60
#define MPLS_ENC_ERHOPLENERROR   -61
#define MPLS_DEC_ERHOPLENERROR   -62
#define MPLS_TLVTYPEERROR        -63
#define MPLS_MSGTYPEERROR        -64
#define MPLS_FECERROR            -65
#define MPLS_ENC_TRAFFICERROR    -66
#define MPLS_DEC_TRAFFICERROR    -67
#define MPLS_ENC_LSPIDERROR      -68 
#define MPLS_DEC_LSPIDERROR      -69 
#define MPLS_ENC_RESCLSERROR     -70 
#define MPLS_DEC_RESCLSERROR     -71 
#define MPLS_ENC_PREEMPTERROR    -72 
#define MPLS_DEC_PREEMPTERROR    -73 
#define MPLS_ENC_PINNINGERROR    -74
#define MPLS_DEC_PINNINGERROR    -75
#define MPLS_FLOATTYPEERROR      -76
#define MPLS_FECTLVERROR         -77
#define MPLS_IPV4LENGTHERROR     -78
#define MPLS_ER_HOPSNUMERROR     -79


/**********************************************************************
   LDP header 
 
    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |  Version                      |         PDU Length            |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                         LDP Identifier                        |
   +                               +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                               |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
**********************************************************************/

   typedef struct mplsLdpHeader_s
   {
      u_short protocolVersion;
      u_short pduLength;      /* length excluding the version and length */
      u_int   lsrAddress;     /* IP address assigned to LSR              */
      u_short labelSpace;     /* within LSR                              */
 
   } mplsLdpHeader_t;
 

/**********************************************************************
   LDP Messages (All LDP messages have the following format:)

    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |U|   Message Type              |      Message Length           |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                     Message ID                                |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                                                               |
   +                                                               +
   |                     Mandatory Parameters                      |
   +                                                               +
   |                                                               |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                                                               |
   +                                                               +
   |                     Optional Parameters                       |
   +                                                               +
   |                                                               |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
Note: the U flag is ignored for now. There is not check for its value.
**********************************************************************/

   typedef struct mplsLdpMsgFlag_s
   {
      BITFIELDS_ASCENDING_2( u_short uBit   :1,
	                     u_short msgType:15 )
   } mplsLdpMsgFlag_t;

   typedef struct mplsLdpMsg_s
   {
     union {  struct mplsLdpMsgFlag_s flags;
              u_short                 mark;
           } flags;

     u_short   msgLength; /* msgId + mandatory param + optional param */
     u_int     msgId;     /* used to identify the notification msg    */
 
   } mplsLdpMsg_t;



/**********************************************************************
   Type-Length-Value Encoding

    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |U|F|        Type               |            Length             |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                                                               |
   |                             Value                             |
   ~                                                               ~
   |                                                               |
   |                               +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                               |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
Note: the decode functions for tlv do not check the values for
      F flag. They check only the value of the U flag; if
      it is set will ignore the tlv and keep processing the message;
      otherwise will ignore the message and return error. Please note 
      that the unknown tlv which is skipped will not be stored anywhere.
**********************************************************************/

   typedef struct mplsLdpTlvFlag_s
   {
      BITFIELDS_ASCENDING_3( u_short uBit:1,
	                     u_short fBit:1,
	                     u_short type:14 )
   } mplsLdpTlvFlag_t;

   typedef struct mplsLdpTlv_s
   {
      union {  struct mplsLdpTlvFlag_s flags;
               u_short                 mark;
            } flags;

      u_short length;   /* length of the value field */
 
   } mplsLdpTlv_t;

 


/**********************************************************************
  Common Session Parameters TLV

        0                   1                   2                   3
        0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |U|F| Common Sess Parms (0x0500)|      Length                   |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       | Protocol Version              |      Keep Alive Time          |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |A|D| Reserved  |     PVLim     |      Max PDU Length           |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |                 Receiver LDP Identifer                        |
       +                               +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |                               |
       -+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-++
***********************************************************************/

   typedef struct mplsLdpCspFlag_s
   {
      BITFIELDS_ASCENDING_4( u_short lad:1,  /* 1 = downstream on demand  */
                             u_short ld :1,  /* loop detection            */
                             u_short res:6,  /* reserved                  */
                             u_short pvl:8   /* path vec limit            */
             )	
   } mplsLdpCspFlag_t;

   typedef struct mplsLdpCspTlv_s
   {
      struct mplsLdpTlv_s baseTlv;
      u_short             protocolVersion;
      u_short             holdTime;        /* proposed keep alive interval */
 
      union {  struct mplsLdpCspFlag_s flags;
               u_short                 mark;
            } flags;
 
      u_short         maxPduLen;
      u_int           rcvLsrAddress;
      u_short         rcvLsId;
 
   } mplsLdpCspTlv_t;




/*********************************************************************** 
   ATM Label Range Component

     0                   1                   2                   3
     0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     |  Res  |    Minimum VPI        |      Minimum VCI              |
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     |  Res  |    Maximum VPI        |      Maximum VCI              |
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
***********************************************************************/

   typedef struct mplsLdpAtmLblRngFlag_s
   {
      BITFIELDS_ASCENDING_3( u_int res1  :4,  /* reserved : 0 on transmision */
                             u_int minVpi:12, /* if <12 bits right justified */
                             u_int minVci:16  /* if <16 bits right justified */
                           )
      BITFIELDS_ASCENDING_3( u_int res2  :4,  /* reserved : 0 on transmision */
                             u_int maxVpi:12, /* if <12 bits right justified */
                             u_int maxVci:16  /* if <16 bits right justified */
                           )
   } mplsLdpAtmLblRngFlag_t;

   typedef struct mplsLdpAtmLblRng_s
   {
      union {
               struct mplsLdpAtmLblRngFlag_s flags;
               u_int                      mark[2];
            } flags;
   } mplsLdpAtmLblRng_t;



/*********************************************************************** 
 Flags for ATM Session Parameters TLV and 
	   Frame Relay Session Parameters TLV

 Note: both types of session parameters have the same type of flags;
       use then the same struct
***********************************************************************/

   typedef struct mplsLdpSPFlag_s
   {
      BITFIELDS_ASCENDING_4( u_int mergeType:2, /* merge typ            */
                             u_int numLblRng:4, /* # of label range com */
                             u_int dir      :1, /* 0 => bidirectional   */
                             u_int res      :25 
                           )
   } mplsLdpSPFlag_t;


/*********************************************************************** 
   ATM Session Parameters TLV

  0                   1                   2                   3
  0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  |U|F|   ATM Sess Parms (0x0501) |      Length                   |
  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  | M |   N   |D|                        Reserved                 |
  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  |                 ATM Label Range Component 1                   |
  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  |                                                               |
  ~                                                               ~
  |                                                               |
  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  |                 ATM Label Range Component N                   |
  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
***********************************************************************/

   typedef struct mplsLdpAspTlv_s
   {
      struct mplsLdpTlv_s baseTlv;
      union {  struct mplsLdpSPFlag_s flags;
               u_int                  mark;
            } flags;
      struct mplsLdpAtmLblRng_s lblRngList[MPLS_ATMLBLMAXLEN];

   } mplsLdpAspTlv_t;




/***********************************************************************
   Frame Relay Label Range Component

    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    | Reserved    |Len|                     Minimum DLCI            |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    | Reserved        |                     Maximum DLCI            |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
***********************************************************************/

   typedef struct mplsLdpFrFlag_s
   {
      BITFIELDS_ASCENDING_3( u_int res_min:7,
                             u_int len    :2,
                             u_int minDlci:23 )
      BITFIELDS_ASCENDING_2( u_int res_max:9,
                             u_int maxDlci:23 )
   } mplsLdpFrFlag_t;
 
   typedef struct mplsLdpFrLblRng_s
   {
      union {
               struct mplsLdpFrFlag_s flags;
               u_int                  mark[2];
            } flags;

   } mplsLdpFrLblRng_t;



/**********************************************************************
   Frame Relay Session Parameters TLV

     0                   1                   2                   3
     0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     |U|F|   FR Sess Parms (0x0502)  |      Length                   |
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     | M |   N   |D|                        Reserved                 |
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     |             Frame Relay Label Range Component 1               |
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     |                                                               |
     ~                                                               ~
     |                                                               |
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     |             Frame Relay Label Range Component N               |
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
***********************************************************************/

   typedef struct mplsLdpFspTlv_s
   {
      struct mplsLdpTlv_s baseTlv;
      union {
               struct mplsLdpSPFlag_s flags;
               u_int                  mark;
            } flags;
      struct mplsLdpFrLblRng_s lblRngList[MPLS_FRLBLMAXLEN];
 
   } mplsLdpFspTlv_t;




/***********************************************************************
   Initialization Message Encoding

    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |U|   Initialization (0x0200)   |      Message Length           |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                     Message ID                                |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                     Common Session Parameters TLV             |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                     Optional Parameters                       |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
***********************************************************************/

   typedef struct mplsLdpInitMsg_s
   {
      struct mplsLdpMsg_s    baseMsg;
      struct mplsLdpCspTlv_s csp;
      struct mplsLdpAspTlv_s asp;
      struct mplsLdpFspTlv_s fsp;
      u_char                 cspExists:1;
      u_char                 aspExists:1;
      u_char                 fspExists:1;
 
   } mplsLdpInitMsg_t;



/***********************************************************************
   Status Code Encoding

      0                   1                   2                   3
      0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     |E|F|                 Status Data                               |
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
***********************************************************************/
   typedef struct mplsLdpStautsFlag_s
   {
      BITFIELDS_ASCENDING_3( u_int error  :1, /* E bit */
	                     u_int forward:1, /* F bit */
	                     u_int status :30 )
   } mplsLdpStautsFlag_t;



/***********************************************************************
   Status (TLV) Encoding

    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |U|F| Status (0x0300)           |      Length                   |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                     Status Code                               |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                     Message ID                                |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |      Message Type             |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
***********************************************************************/

   typedef struct mplsLdpStatusTlv_s
   {
      struct mplsLdpTlv_s baseTlv;
      union {
	      struct mplsLdpStautsFlag_s flags;
	      u_int                      mark;
            } flags;
      u_int               msgId;
      u_short             msgType;

   } mplsLdpStatusTlv_t;



/***********************************************************************
   Extended Status (TLV) Encoding
***********************************************************************/

   typedef struct mplsLdpExStatusTlv_s
   {
      struct mplsLdpTlv_s    baseTlv;
      u_int                  value;   /* additional info for status */

   } mplsLdpExStatusTlv_t;



/***********************************************************************
   Returned PDU (TLV) Encoding
***********************************************************************/

   typedef struct mplsLdpRetPduTlv_s
   {
      struct mplsLdpTlv_s    baseTlv;
      struct mplsLdpHeader_s headerTlv;
      u_char                 data[MPLS_NOT_MAXSIZE];

   } mplsLdpRetPduTlv_t;



/***********************************************************************
   Returned MSG (TLV) Encoding
***********************************************************************/

   typedef struct mplsLdpRetMsgTlv_s 
   {
      struct mplsLdpTlv_s baseTlv;
      u_short             msgType;
      u_short             msgLength;
      u_char              data[MPLS_NOT_MAXSIZE];

   } mplsLdpRetMsgTlv_t;



/***********************************************************************
   LSPID Tlv encoding

      0                   1                   2                   3
      0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     |U|F|      LSPID-TLV  (0x0821)  |      Length                   |
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     |       Reserved                |      Local CRLSP ID           |
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     |                       Ingress LSR Router ID                   |
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
***********************************************************************/

   typedef struct mplsLdpLspIdTlv_s
   {
      struct mplsLdpTlv_s baseTlv;
      u_short             res;
      u_short             localCrlspId;
      u_int               routerId;     /* ingress lsr router id */

   } mplsLdpLspIdTlv_t;


/***********************************************************************
   Notification Message Encoding

    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |U|   Notification (0x0001)     |      Message Length           |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                     Message ID                                |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                     Status (TLV)                              |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                     Optional Parameters                       |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                     LSPID TLV (optional for CR-LDP)           |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
***********************************************************************/

   typedef struct mplsLdpNotifMsg_s
   {
      struct mplsLdpMsg_s         baseMsg;
      struct mplsLdpStatusTlv_s   status;
      struct mplsLdpExStatusTlv_s exStatus;    /* extended status tlv */
      struct mplsLdpRetPduTlv_s   retPdu;      /* returned PDU tlv    */
      struct mplsLdpRetMsgTlv_s   retMsg;      /* returned MSG tlv    */
      struct mplsLdpLspIdTlv_s    lspidTlv;    /* lspid tlv           */

      u_char                      statusTlvExists:1;
      u_char                      exStatusTlvExists:1;
      u_char                      retPduTlvExists:1;
      u_char                      retMsgTlvExists:1;
      u_char                      lspidTlvExists;
 
   } mplsLdpNotifMsg_t;


/***********************************************************************
   Common Hello Parameters Tlv encoding
 
      0                   1                   2                   3
      0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     |U|F| Common Hello Parms(0x0400)|      Length                   |
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     |      Hold Time                |T|R| Reserved                  |
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
***********************************************************************/
   typedef struct mplsLdpChpFlag_s
   {
     BITFIELDS_ASCENDING_3( u_short target :1, /* T bit */
                            u_short request:1, /* R bit */
                            u_short res    :14 )
   } mplsLdpChpFlag_t;
 
   typedef struct mplsLdpChpTlv_s 
   {
      struct mplsLdpTlv_s baseTlv;
      u_short             holdTime;
      union {  struct mplsLdpChpFlag_s flags;
               u_short                 mark;
            } flags;

   } mplsLdpChpTlv_t;



/***********************************************************************
   Transport Address (TLV) Encoding
***********************************************************************/

   typedef struct mplsLdpTrAdrTlv_s
   {
      struct mplsLdpTlv_s baseTlv;
      u_int               address;

   } mplsLdpTrAdrTlv_t;



/***********************************************************************
   Configuration Sequence Number (TLV) Encoding
***********************************************************************/

   typedef struct mplsLdpCsnTlv_s 
   {
      struct mplsLdpTlv_s baseTlv;
      u_int               seqNumber;

   } mplsLdpCsnTlv_t;



/***********************************************************************
     Hello message encoding 
 
    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |U|   Hello (0x0100)            |      Message Length           |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                     Message ID                                |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                     Common Hello Parameters TLV               |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                     Optional Parameters                       |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
***********************************************************************/

   typedef struct mplsLdpHelloMsg_s
   {
      struct mplsLdpMsg_s         baseMsg;
      struct mplsLdpChpTlv_s      chp;         /* common hello param tlv  */
      struct mplsLdpTrAdrTlv_s    trAdr;       /* transport address tlv   */
      struct mplsLdpCsnTlv_s      csn;         /* configuration seq # tlv */
      u_char                      chpTlvExists:1;
      u_char                      trAdrTlvExists:1;
      u_char                      csnTlvExists:1;
 
   } mplsLdpHelloMsg_t;


/***********************************************************************
   KeepAlive Message encoding
 
    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |U|   KeepAlive (0x0201)        |      Message Length           |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                     Message ID                                |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                     Optional Parameters                       |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

Note: there are no optional param defined for keep alive.
***********************************************************************/

   typedef struct mplsLdpKeepAlMsg_s
   {
      struct mplsLdpMsg_s baseMsg;

   } mplsLdpKeepAlMsg_t ;



/***********************************************************************
   Address List TLV encoding

    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |U|F| Address List (0x0101)     |      Length                   |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |     Address Family            |                               |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+                               |
   |                                                               |
   |                        Addresses                              |
   ~                                                               ~
   |                                                               |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
***********************************************************************/

   typedef struct mplsLdpAdrTlv_s
   {
      struct mplsLdpTlv_s baseTlv;
      u_short             addrFamily;
      u_int               address[MPLS_MAXNUMBERADR];

   } mplsLdpAdrTlv_t;




/***********************************************************************
   Address (0x0300) / Address Withdraw(0x0301)  message encoding

    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |U|   Address                   |      Message Length           |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                     Message ID                                |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                                                               |
   |                     Address List TLV                          |
   |                                                               |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                     Optional Parameters                       |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

Note: there are no optional param defined for address message.
***********************************************************************/

   typedef struct mplsLdpAdrMsg_s
   {
      struct mplsLdpMsg_s    baseMsg;
      struct mplsLdpAdrTlv_s addressList;
      u_char                 adrListTlvExists:1;

   } mplsLdpAdrMsg_t;



/***********************************************************************
   Wildcard FEC Element encoding
***********************************************************************/

   typedef struct mplsLdpWildFec_s
   {
      u_char type;

   } mplsLdpWildFec_t;



/***********************************************************************
   Prefix FEC Element encoding

      0                   1                   2                   3
      0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     |  Prefix (2)   |     Address Family            |     PreLen    |
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     |                     Prefix                                    |
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

   Host Address FEC Element encoding

      0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     | Host Addr (3) |     Address Family            | Host Addr Len |
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     |                     Host Addr                                 |
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

Note: the code handles prefixes and host addresses whose length is 
      less or equal to 4 bytes.
***********************************************************************/

   typedef struct mplsLdpAddressFec_s
   {
      u_char  type;
      u_short addressFam;
      u_char  preLen;      /* prefix FEC: length of the adr prefix (in bits)
			      or host adr FEC: length of the host address (in 
			      bytes) */
      u_int   address;

   } mplsLdpAddressFec_t;


/***********************************************************************
   CRLSP FEC Element encoding

      0                   1                   2                   3
      0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     | CR-LSP (4)    |          Reserved                             |
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
***********************************************************************/

   typedef struct mplsLdpCrlspFec_s
   {
      u_char  type;
      u_char  res1;  /* reserved */
      u_short res2;  /* reserved */

   } mplsLdpCrlspFec_t;


/***********************************************************************
   Martini VC FEC Element encoding

    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   | VC tlv(128)   |C|         VC Type             |VC info Length |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                      Group ID                                 |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                        VC ID                                  |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                       Interface parameters                    |
   |                              "                                |
   |                              "                                |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

   Interface Parameter encoding

    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |  Parameter ID |    Length     |    Variable Length Value      |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                         Variable Length Value                 |
   |                             "                                 |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

***********************************************************************/

   

   typedef struct mplsLdpMartiniInterfaceParams_s
   {
      u_char id;
      u_char len;
      u_char value[MPLS_VCIFPARAMMAXLEN];
   } mplsLdpMartiniInterfaceParams_t;

   typedef struct mplsLdMartiniVcFlag_s
   {
      BITFIELDS_ASCENDING_4( u_int type:8,
                             u_int control:1, /* C bit */
                             u_int vcType:15, /* VC type */
                             u_int vcInfoLen:8);
   } mplsLdMartiniVcFlag_t;

   typedef struct mplsLdpMartiniVcFec_s
   {
      union { struct mplsLdMartiniVcFlag_s flags;
              u_int                        mark;
            } flags;
      u_int   groupId;
      u_int   vcId;

      struct mplsLdpMartiniInterfaceParams_s vcIfParams[5];
      int vcIfParamsLen;

   } mplsLdpMartiniVcFec_t;


/***********************************************************************
   FEC Tlv encoding

    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |U|F| FEC (0x0100)              |      Length                   |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                        FEC Element 1                          |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                                                               |
   ~                                                               ~
   |                                                               |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                        FEC Element n                          |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
***********************************************************************/
   
   typedef union mplsFecElement_u 
   {
      struct mplsLdpAddressFec_s addressEl;  /* prefix | host adr */
      struct mplsLdpWildFec_s    wildcardEl; /* for wilcard fec   */
      struct mplsLdpCrlspFec_s   crlspEl;    /* CRLSP fec elem    */
      struct mplsLdpMartiniVcFec_s   martiniVcEl;    /* Martini VC fec elem */

   } mplsFecElement_t;
    
   typedef struct mplsLdpFecTlv_s 
   {
      struct mplsLdpTlv_s     baseTlv;
      union mplsFecElement_u  fecElArray[MPLS_MAXNUMFECELEMENT];
      u_short                 fecElemTypes[MPLS_MAXNUMFECELEMENT];
      u_char                  wcElemExists:1; 
      u_short                 numberFecElements;

   } mplsLdpFecTlv_t;


/***********************************************************************
   Generic Label Tlv encoding

    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |U|F| Generic Label (0x0200)    |      Length                   |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |     Label                                                     |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
***********************************************************************/

   typedef struct mplsLdpGenLblTlv_s
   {
      struct mplsLdpTlv_s baseTlv;
      u_int               label;   /* 20-bit number in 4 octet field */

   } mplsLdpGenLblTlv_t;


/***********************************************************************
   Atm Label Tlv encoding

    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |U|F| ATM Label (0x0201)        |         Length                |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |Res| V |          VPI          |         VCI                   |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
***********************************************************************/

   typedef struct mplsLdpAtmLblFlag_s 
   {
      BITFIELDS_ASCENDING_3( u_short res:2,
	                     u_short v  :2,
	                     u_short vpi:12 )
   } mplsLdpAtmLblFlag_t;

   typedef struct mplsLdpAtmLblTlv_s
   {
      struct mplsLdpTlv_s baseTlv;

      union {  struct mplsLdpAtmLblFlag_s flags;
               u_short                    mark;
            } flags;

      u_short             vci;

   } mplsLdpAtmLblTlv_t;



/***********************************************************************
   Frame Relay Label Tlv encoding

    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |U|F| Frame Relay Label (0x0202)|       Length                  |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   | Reserved    |Len|                     DLCI                    |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
***********************************************************************/

   typedef struct mplsLdpFrLblFlag_s
   {
      BITFIELDS_ASCENDING_3( u_int res :7,
	                     u_int len :2,
	                     u_int dlci:23 )

   } mplsLdpFrLblFlag_t;

   typedef struct mplsLdpFrLblTlv_s
   {
      struct mplsLdpTlv_s baseTlv;

      union {  struct mplsLdpFrLblFlag_s flags;
               u_int                     mark;
            } flags;

   } mplsLdpFrLblTlv_t;



/***********************************************************************
   Hop Count Tlv encoding

    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |U|F| Hop Count (0x0103)        |      Length                   |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |     HC Value  |
   +-+-+-+-+-+-+-+-+
***********************************************************************/

   typedef struct mplsLdpHopTlv_s
   {
      struct mplsLdpTlv_s baseTlv;
      u_char              hcValue; /* hop count value */

   } mplsLdpHopTlv_t;



/***********************************************************************
   Path Vector Tlv encoding

    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |U|F| Path Vector (0x0104)      |        Length                 |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                            LSR Id 1                           |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                                                               |
   ~                                                               ~
   |                                                               |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                            LSR Id n                           |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
***********************************************************************/

   typedef struct mplsLdpPathTlv_s 
   {
      struct mplsLdpTlv_s baseTlv;
      u_int               lsrId[MPLS_MAXHOPSNUMBER];

   } mplsLdpPathTlv_t;



/***********************************************************************
   Lbl request message id Tlv encoding
***********************************************************************/

   typedef struct mplsLdpLblMsgIdTlv_s
   {
      struct mplsLdpTlv_s baseTlv;
      u_int  msgId;

   } mplsLdpLblMsgIdTlv_t;



/***********************************************************************
   Preemption Tlv encoding

      0                   1                   2                   3
      0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     |U|F| Preemption-TLV  (0x0820)  |      Length                   |
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     |  SetPrio      | HoldPrio      |      Reserved                 |
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
***********************************************************************/

   typedef struct mplsLdpPreemptTlv_s
   {
      struct mplsLdpTlv_s baseTlv;
      u_char              setPrio;  /* 0 => most important path */
      u_char              holdPrio; /* 0 => most important path */
      u_short             res;

   } mplsLdpPreemptTlv_t;



/***********************************************************************
   Resource class Tlv encoding

      0                   1                   2                   3
      0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     |U|F|      ResCls-TLV  (0x0822) |      Length                   |
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     |                             RsCls                             |
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
***********************************************************************/

   typedef struct mplsLdpResClsTlv_s
   {
      struct mplsLdpTlv_s baseTlv;
      u_int               rsCls;   /* resource class bit mask */

   } mplsLdpResClsTlv_t;



/***********************************************************************
   Lbl return message id Tlv encoding
***********************************************************************/

   typedef struct mplsLdpRetMsgIdTlv_s
   {
      struct mplsLdpTlv_s baseTlv;

   } mplsLdpLblRetMsgIdTlv_t;



/***********************************************************************
   ER flag structure which is common to IPV4 and IPV6 ER TLV
***********************************************************************/

   typedef struct mplsLdpErIPFlag_s 
   {
      BITFIELDS_ASCENDING_3( u_int l     :1 , /* 0 => loose hop */
	                     u_int res   :23, 
	                     u_int preLen:8 )
   } mplsLdpErIPFlag_t;

/***********************************************************************
   ER flag structure which is common to AS and LSPID ER TLV
***********************************************************************/
   typedef struct mplsLdpErFlag_s
   {
      BITFIELDS_ASCENDING_2( u_short l     :1 , /* 0 => loose hop */
	                     u_short res   :15 )
   } mplsLdpErFlag_t;


/***********************************************************************
   Explicit Routing IPv4 Tlv encoding

      0                   1                   2                   3
      0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     |U|F|         0x801             |      Length                   |
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     |L|      Reserved                               |    PreLen     |
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     |                    IPv4 Address (4 bytes)                     |
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
***********************************************************************/

   typedef struct mplsLdpErIpv4_s
   {
      struct mplsLdpTlv_s baseTlv;
      union {  struct mplsLdpErIPFlag_s flags;
               u_int                    mark;
            } flags;
      u_int   address;

   } mplsLdpErIpv4_t;



/***********************************************************************
   Explicit Routing IPv6 Tlv encoding

      0                   1                   2                   3
      0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     |U|F|          0x802            |      Length                   |
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     |L|             Reserved                        |    PreLen     |
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     |                  IPV6 address                                 |
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     |                  IPV6 address (continued)                     |
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     |                  IPV6 address (continued)                     |
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     |                  IPV6 address (continued)                     |
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
***********************************************************************/

   typedef struct mplsLdpErIpv6_s
   {
      struct mplsLdpTlv_s baseTlv;
      union {  struct mplsLdpErIPFlag_s flags;
               u_int                    mark;
            } flags;
      u_char address[MPLS_IPV6ADRLENGTH];

   } mplsLdpErIpv6_t;



/***********************************************************************
   Explicit Routing Autonomous systen number Tlv encoding

      0                   1                   2                   3
      0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     |U|F|          0x803            |      Length                   |
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     |L|          Reserved           |                AS Number      |
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
***********************************************************************/

   typedef struct mplsLdpErAs_s
   {
      struct mplsLdpTlv_s baseTlv;
      union {  struct mplsLdpErFlag_s flags;
               u_short                mark;
            } flags;
      u_short asNumber;

   } mplsLdpErAs_t;



/***********************************************************************
   Explicit Routing LSPID Tlv encoding

      0                   1                   2                   3
      0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     |U|F|          0x804            |      Length                   |
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     |L|          Reserved           |               Local LSPID     |
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     |                       Ingress LSR Router ID                   |
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
***********************************************************************/

   typedef struct mplsLdpErLspId_s 
   {
      struct mplsLdpTlv_s baseTlv;
      union {  struct mplsLdpErFlag_s flags;
               u_short                mark;
            } flags;
      u_short lspid;
      u_int   routerId;

   } mplsLdpErLspId_t;



/***********************************************************************
   Constraint Routing Tlv encoding

      0                   1                   2                   3
      0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     |U|F|         ER-TLV  (0x0800)  |      Length                   |
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     |                          ER-Hop TLV 1                         |
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     |                          ER-Hop TLV 2                         |
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     ~                          ............                         ~
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     |                          ER-Hop TLV n                         |
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
***********************************************************************/

   typedef union mplsLdpErHop_u 
   {
      struct mplsLdpErIpv4_s  erIpv4; 
      struct mplsLdpErIpv6_s  erIpv6;
      struct mplsLdpErAs_s    erAs;
      struct mplsLdpErLspId_s erLspId;

   } mplsLdpErHop_t;

   typedef struct mplsLdpErTlv_s
   {
      struct mplsLdpTlv_s baseTlv;
      union mplsLdpErHop_u erHopArray[MPLS_MAX_ER_HOPS];
      u_short              erHopTypes[MPLS_MAX_ER_HOPS]; /* need to know the 
							    types when handle
							    the union */
      u_short              numberErHops;

   } mplsLdpErTlv_t;



/***********************************************************************
   Traffic parameters TLV

      0                   1                   2                   3
     0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     |U|F| Traf. Param. TLV  (0x0810)|      Length                   |
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     |     Flags     |    Frequency  |     Reserved  |    Weight     |
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     |                    Peak Data Rate (PDR)                       |
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     |                    Peak Burst Size (PBS)                      |
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     |                    Committed Data Rate (CDR)                  |
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     |                    Committed Burst Size (CBS)                 |
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     |                    Excess Burst Size (EBS)                    |
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

    Flag field:
     +--+--+--+--+--+--+--+--+
     | Res |F6|F5|F4|F3|F2|F1|
     +--+--+--+--+--+--+--+--+
***********************************************************************/

   typedef struct mplsLdpTrafficFlag_s
   {
      BITFIELDS_ASCENDING_7( u_char res:2,
	                     u_char f6Bit:1,
	                     u_char f5Bit:1,
	                     u_char f4Bit:1,
	                     u_char f3Bit:1,
	                     u_char f2Bit:1,
	                     u_char f1Bit:1 )
   } mplsLdpTrafficFlag_t;
  
   typedef struct mplsLdpTrafficTlv_s
   {
      struct mplsLdpTlv_s baseTlv;
      union {  struct mplsLdpTrafficFlag_s flags;
               u_char                      mark;
            } flags;
      u_char              freq; 
      u_char              res; 
      u_char              weight; 
      union { float pdr;
	      u_int mark;
            } pdr;
      union { float pbs;
	      u_int mark;
            } pbs;
      union { float cdr;
	      u_int mark;
            } cdr;
      union { float cbs;
	      u_int mark;
            } cbs;
      union { float ebs;
	      u_int mark;
            } ebs;

   } mplsLdpTrafficTlv_t;
 


/***********************************************************************
   Route pinning TLV

      0                   1                   2                   3
      0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     |U|F|          0x823            |      Length                   |
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     |P|                        Reserved                             |
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
***********************************************************************/

   typedef struct mplsLdpPinningTlvFlag_s
   {
      BITFIELDS_ASCENDING_2( u_int pBit:1, /* 1 => route pinning requested */
	                     u_int res:31 )
   } mplsLdpPinningTlvFlag_t;

   typedef struct mplsLdpPinningTlv_s
   {
      struct mplsLdpTlv_s                     baseTlv;
      union {  struct mplsLdpPinningTlvFlag_s flags;
               u_int                          mark;
            } flags;
   } mplsLdpPinningTlv_t;



/***********************************************************************
   Label Mapping Message encoding
 
   0                   1                   2                   3
   0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |U|   Label Mapping (0x0400)   |      Message Length            |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                     Message ID                                |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                     FEC TLV                                   |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                     Label TLV                                 |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |              Label Request Message ID TLV  (mandatory)        |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                     LSPID TLV            (CR-LDP, mandatory)  |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                     Traffic  TLV         (CR-LDP, optional)   |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
***********************************************************************/

   typedef struct mplsLdpLblMapMsg_s
   {
      struct mplsLdpMsg_s       baseMsg;

      /* FEC tlv */
      struct mplsLdpFecTlv_s    fecTlv;

      /* Label TLV */
      struct mplsLdpGenLblTlv_s   genLblTlv;   /* generic label tlv */
      struct mplsLdpAtmLblTlv_s   atmLblTlv;   /* atm label tlv     */
      struct mplsLdpFrLblTlv_s    frLblTlv;    /* fr label tlv      */

      /* Optional parameters */ 
      struct mplsLdpHopTlv_s      hopCountTlv; /* hop count tlv     */
      struct mplsLdpPathTlv_s     pathVecTlv;  /* path vector tlv   */
      struct mplsLdpLblMsgIdTlv_s lblMsgIdTlv; /* lbl msg id tlv    */
      struct mplsLdpLspIdTlv_s    lspidTlv;    /* lspid tlv         */
      struct mplsLdpTrafficTlv_s  trafficTlv;  /* traffic tlv       */

      u_char                      fecTlvExists:1;
      u_char                      genLblTlvExists:1;
      u_char                      atmLblTlvExists:1;
      u_char                      frLblTlvExists:1;
      u_char                      hopCountTlvExists:1;
      u_char                      pathVecTlvExists:1;
      u_char                      lblMsgIdTlvExists:1;
      u_char                      lspidTlvExists:1;
      u_char                      trafficTlvExists:1;

   } mplsLdpLblMapMsg_t;



/***********************************************************************
   Label Request Message encoding

   0                   1                   2                   3
   0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |U|   Label Request (0x0401)   |      Message Length            |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                     Message ID                                |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                     FEC TLV                                   |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                     Return Message ID TLV  (mandatory)        |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                     LSPID TLV            (CR-LDP, mandatory)  |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                     ER-TLV               (CR-LDP, optional)   |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                     Traffic  TLV         (CR-LDP, optional)   |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                     Pinning TLV          (CR-LDP, optional)   |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                     Resource Class TLV (CR-LDP, optional)     |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                     Pre-emption  TLV     (CR-LDP, optional)   |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
***********************************************************************/

   typedef struct mplsLdpLblReqMsg_s
   {
      struct mplsLdpMsg_s               baseMsg;

      /* FEC tlv */
      struct mplsLdpFecTlv_s            fecTlv;

      /* Optional parameters */ 
      struct mplsLdpHopTlv_s            hopCountTlv; /* hop count tlv     */
      struct mplsLdpPathTlv_s           pathVecTlv;  /* path vector tlv   */

      /* Optional parameters for CR */
      struct mplsLdpRetMsgIdTlv_s       lblMsgIdTlv; /* lbl msg id tlv    */
      struct mplsLdpErTlv_s             erTlv;       /* constraint rtg tlv*/
      struct mplsLdpTrafficTlv_s        trafficTlv;  /* traffic tlv       */
      struct mplsLdpLspIdTlv_s          lspidTlv;    /* lspid tlv         */
      struct mplsLdpPinningTlv_s        pinningTlv;  /* pinning tlv       */
      struct mplsLdpResClsTlv_s         resClassTlv; /* resource class tlv*/
      struct mplsLdpPreemptTlv_s        preemptTlv;  /* peemtion tlv      */

      u_char                            fecTlvExists:1;
      u_char                            hopCountTlvExists:1;
      u_char                            pathVecTlvExists:1;
      u_char                            lblMsgIdTlvExists:1;
      u_char                            erTlvExists:1;
      u_char                            trafficTlvExists:1;
      u_char                            lspidTlvExists:1;
      u_char                            pinningTlvExists:1;
      u_char                            recClassTlvExists:1;
      u_char                            preemptTlvExists:1;

   } mplsLdpLblReqMsg_t;

 

/***********************************************************************

   Label Withdraw Message encoding

    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |U|   Label Withdraw (0x0402)   |      Message Length           |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                     Message ID                                |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                     FEC TLV                                   |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                     Label TLV (optional)                      |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                     LSPID TLV (optional for CR-LDP)           |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+



   Label Release Message encoding

    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |U|   Label Release (0x0403)   |      Message Length            |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                     Message ID                                |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                     FEC TLV                                   |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                     Label TLV (optional)                      |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                     LSPID TLV (optional for CR-LDP)           |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 
Note: the Label Withdraw Message encoding and the Label Release Message enc
      look very much the same. I will create only one type of struct for 
      both message types.
      The Label Withdraw Message and Label Release Message can optionally
      carry LSPID TLV.
***********************************************************************/

   typedef struct mplsLdpLbl_W_R_Msg_s
   {
      struct mplsLdpMsg_s       baseMsg;

      /* FEC tlv */
      struct mplsLdpFecTlv_s    fecTlv;

      /* Label TLV */
      struct mplsLdpGenLblTlv_s   genLblTlv;   /* generic label tlv */
      struct mplsLdpAtmLblTlv_s   atmLblTlv;   /* atm label tlv     */
      struct mplsLdpFrLblTlv_s    frLblTlv;    /* fr label tlv      */
      struct mplsLdpLspIdTlv_s    lspidTlv;    /* lspid tlv         */


      u_char                      fecTlvExists:1;
      u_char                      genLblTlvExists:1;
      u_char                      atmLblTlvExists:1;
      u_char                      frLblTlvExists:1;
      u_char                      lspidTlvExists:1;

   } mplsLdpLbl_W_R_Msg_t;



/***********************************************************************
   Label Abort Request Message encoding
 
   0                   1                   2                   3
   0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |U|   Label Abort Req (0x0404) |      Message Length            |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                     Message ID                                |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                     FEC TLV                                   |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |              Label Request Message ID TLV                     |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
***********************************************************************/

   typedef struct mplsLdpLblAbortMsg_s
   {
      struct mplsLdpMsg_s         baseMsg;

      struct mplsLdpFecTlv_s      fecTlv;      /* fec tlv        */
      struct mplsLdpLblMsgIdTlv_s lblMsgIdTlv; /* lbl msg id tlv */

      u_char                      fecTlvExists:1;
      u_char                      lblMsgIdTlvExists:1;

   } mplsLdpLblAbortMsg_t;



/***********************************************************************




/*
 *          Function declarations
 *
 *  Note: Encode functions return the length of the data which was encoded. 
 *        The first argument (which is a pointer to the structure which
 *        contains the data to be encoded) is not modified in the encode functions
 *        which encode the messages and message headers. All the other encode 
 *        fuctions modify the content of the structures to be encoded (tlvs, 
 *        message parameters, etc). 
 *
 *	  Decode functions for tlv return the length of the value. 
 */

int Mpls_encodeLdpMsgHeader      (mplsLdpHeader_t         *, u_char *, int);
int Mpls_decodeLdpMsgHeader      (mplsLdpHeader_t         *, u_char *, int);
int Mpls_encodeLdpAtmLblRng      (mplsLdpAtmLblRng_t      *, u_char *, int);
int Mpls_decodeLdpAtmLblRng      (mplsLdpAtmLblRng_t      *, u_char *, int);
int Mpls_encodeLdpAsp            (mplsLdpAspTlv_t         *, u_char *, int);
int Mpls_decodeLdpAsp            (mplsLdpAspTlv_t         *, u_char *, int);
int Mpls_encodeLdpTlv            (mplsLdpTlv_t            *, u_char *, int);
int Mpls_decodeLdpTlv            (mplsLdpTlv_t            *, u_char *, int);
int Mpls_encodeLdpInitMsg        (mplsLdpInitMsg_t        *, u_char *, int);
int Mpls_decodeLdpInitMsg        (mplsLdpInitMsg_t        *, u_char *, int);
int Mpls_encodeLdpCsp            (mplsLdpCspTlv_t         *, u_char *, int);
int Mpls_decodeLdpCsp            (mplsLdpCspTlv_t         *, u_char *, int);
int Mpls_encodeLdpBaseMsg        (mplsLdpMsg_t            *, u_char *, int);
int Mpls_decodeLdpBaseMsg        (mplsLdpMsg_t            *, u_char *, int);
int Mpls_encodeLdpFrLblRng       (mplsLdpFrLblRng_t       *, u_char *, int);
int Mpls_decodeLdpFrLblRng       (mplsLdpFrLblRng_t       *, u_char *, int);
int Mpls_encodeLdpFsp            (mplsLdpFspTlv_t         *, u_char *, int);
int Mpls_decodeLdpFsp            (mplsLdpFspTlv_t         *, u_char *, int);
int Mpls_encodeLdpNotMsg         (mplsLdpNotifMsg_t       *, u_char *, int);
int Mpls_decodeLdpNotMsg         (mplsLdpNotifMsg_t       *, u_char *, int);
int Mpls_encodeLdpStatus         (mplsLdpStatusTlv_t      *, u_char *, int);
int Mpls_decodeLdpStatus         (mplsLdpStatusTlv_t      *, u_char *, int);
int Mpls_encodeLdpExStatus       (mplsLdpExStatusTlv_t    *, u_char *, int);
int Mpls_decodeLdpExStatus       (mplsLdpExStatusTlv_t    *, u_char *, int);
int Mpls_encodeLdpRetPdu         (mplsLdpRetPduTlv_t      *, u_char *, int);
int Mpls_decodeLdpRetPdu         (mplsLdpRetPduTlv_t      *, u_char *, int, u_short);
int Mpls_encodeLdpRetMsg         (mplsLdpRetMsgTlv_t      *, u_char *, int);
int Mpls_decodeLdpRetMsg         (mplsLdpRetMsgTlv_t      *, u_char *, int, u_short);
int Mpls_encodeLdpHelloMsg       (mplsLdpHelloMsg_t       *, u_char *, int);
int Mpls_decodeLdpHelloMsg       (mplsLdpHelloMsg_t       *, u_char *, int);
int Mpls_encodeLdpChp            (mplsLdpChpTlv_t         *, u_char *, int);
int Mpls_decodeLdpChp            (mplsLdpChpTlv_t         *, u_char *, int);
int Mpls_encodeLdpCsn            (mplsLdpCsnTlv_t         *, u_char *, int);
int Mpls_decodeLdpCsn            (mplsLdpCsnTlv_t         *, u_char *, int);
int Mpls_encodeLdpTrAdr          (mplsLdpTrAdrTlv_t       *, u_char *, int);
int Mpls_decodeLdpTrAdr          (mplsLdpTrAdrTlv_t       *, u_char *, int);
int Mpls_encodeLdpKeepAliveMsg   (mplsLdpKeepAlMsg_t      *, u_char *, int);
int Mpls_decodeLdpKeepAliveMsg   (mplsLdpKeepAlMsg_t      *, u_char *, int);
int Mpls_encodeLdpAdrTlv         (mplsLdpAdrTlv_t         *, u_char *, int);
int Mpls_decodeLdpAdrTlv         (mplsLdpAdrTlv_t         *, u_char *, int, u_short);
int Mpls_encodeLdpAdrMsg         (mplsLdpAdrMsg_t         *, u_char *, int);
int Mpls_decodeLdpAdrMsg         (mplsLdpAdrMsg_t         *, u_char *, int);
int Mpls_encodeLdpFecTlv         (mplsLdpFecTlv_t         *, u_char *, int);
int Mpls_decodeLdpFecTlv         (mplsLdpFecTlv_t         *, u_char *, int, u_short);
int Mpls_encodeLdpGenLblTlv      (mplsLdpGenLblTlv_t      *, u_char *, int);
int Mpls_decodeLdpGenLblTlv      (mplsLdpGenLblTlv_t      *, u_char *, int);
int Mpls_encodeLdpAtmLblTlv      (mplsLdpAtmLblTlv_t      *, u_char *, int);
int Mpls_decodeLdpAtmLblTlv      (mplsLdpAtmLblTlv_t      *, u_char *, int);
int Mpls_encodeLdpFrLblTlv       (mplsLdpFrLblTlv_t       *, u_char *, int);
int Mpls_decodeLdpFrLblTlv       (mplsLdpFrLblTlv_t       *, u_char *, int);
int Mpls_encodeLdpHopTlv         (mplsLdpHopTlv_t         *, u_char *, int);
int Mpls_decodeLdpHopTlv         (mplsLdpHopTlv_t         *, u_char *, int);
int Mpls_encodeLdpLblMsgIdTlv    (mplsLdpLblMsgIdTlv_t    *, u_char *, int);
int Mpls_decodeLdpLblMsgIdTlv    (mplsLdpLblMsgIdTlv_t    *, u_char *, int);
int Mpls_encodeLdpPathVectorTlv  (mplsLdpPathTlv_t        *, u_char *, int);
int Mpls_decodeLdpPathVectorTlv  (mplsLdpPathTlv_t        *, u_char *, int, u_short);
int Mpls_encodeLdpLblMapMsg      (mplsLdpLblMapMsg_t      *, u_char *, int);
int Mpls_decodeLdpLblMapMsg      (mplsLdpLblMapMsg_t      *, u_char *, int);
int Mpls_encodeLdpFecAdrEl       (mplsFecElement_t        *, u_char *, int ,u_char);
int Mpls_decodeLdpFecAdrEl       (mplsFecElement_t        *, u_char *, int ,u_char);
int Mpls_encodeLdpLblRetMsgIdTlv (mplsLdpLblRetMsgIdTlv_t *, u_char *, int);
int Mpls_decodeLdpLblRetMsgIdTlv (mplsLdpLblRetMsgIdTlv_t *, u_char *, int);
int Mpls_encodeLdpLbl_W_R_Msg    (mplsLdpLbl_W_R_Msg_t    *, u_char *, int);
int Mpls_decodeLdpLbl_W_R_Msg    (mplsLdpLbl_W_R_Msg_t    *, u_char *, int);
int Mpls_encodeLdpERTlv          (mplsLdpErTlv_t          *, u_char *, int);
int Mpls_decodeLdpERTlv          (mplsLdpErTlv_t          *, u_char *, int, u_short);
int Mpls_encodeLdpErHop          (mplsLdpErHop_t          *, u_char *, int, u_short);
int Mpls_decodeLdpErHop          (mplsLdpErHop_t          *, u_char *, int, u_short *);
int Mpls_encodeLdpTrafficTlv     (mplsLdpTrafficTlv_t     *, u_char *, int);
int Mpls_decodeLdpTrafficTlv     (mplsLdpTrafficTlv_t     *, u_char *, int, u_short);
int Mpls_encodeLdpLblReqMsg      (mplsLdpLblReqMsg_t      *, u_char *, int);
int Mpls_decodeLdpLblReqMsg      (mplsLdpLblReqMsg_t      *, u_char *, int);
int Mpls_encodeLdpPreemptTlv     (mplsLdpPreemptTlv_t     *, u_char *, int);
int Mpls_decodeLdpPreemptTlv     (mplsLdpPreemptTlv_t     *, u_char *, int);
int Mpls_encodeLdpLspIdTlv       (mplsLdpLspIdTlv_t       *, u_char *, int);
int Mpls_decodeLdpLspIdTlv       (mplsLdpLspIdTlv_t       *, u_char *, int);
int Mpls_encodeLdpResClsTlv      (mplsLdpResClsTlv_t      *, u_char *, int);
int Mpls_decodeLdpResClsTlv      (mplsLdpResClsTlv_t      *, u_char *, int);
int Mpls_encodeLdpPinningTlv     (mplsLdpPinningTlv_t     *, u_char *, int);
int Mpls_decodeLdpPinningTlv     (mplsLdpPinningTlv_t     *, u_char *, int);
int Mpls_encodeLdpLblAbortMsg    (mplsLdpLblAbortMsg_t    *, u_char *, int);
int Mpls_decodeLdpLblAbortMsg    (mplsLdpLblAbortMsg_t    *, u_char *, int);



 

/*
 *   DEBUG function declarations
 */

void printTlv             (mplsLdpTlv_t          *);
void printHeader          (mplsLdpHeader_t       *);
void printCspFlags        (mplsLdpCspFlag_t      *);
void printCspFlagsPerByte (u_short               *);
void printCspTlv          (mplsLdpCspTlv_t       *);
void printAspFlags        (mplsLdpSPFlag_t      *);
void printAspFlagsPerByte (u_int                 *);
void printAspTlv          (mplsLdpAspTlv_t       *);
void printFspFlags        (mplsLdpSPFlag_t      *);
void printFspTlv          (mplsLdpFspTlv_t       *);
void printInitMsg         (mplsLdpInitMsg_t      *);
void printRetMsgTlv       (mplsLdpRetMsgTlv_t    *);
void printRetPduTlv       (mplsLdpRetPduTlv_t    *);
void printExStatusTlv     (mplsLdpExStatusTlv_t  *);
void printStatusTlv       (mplsLdpStatusTlv_t    *);
void printNotMsg          (mplsLdpNotifMsg_t     *);
void printCsnTlv          (mplsLdpCsnTlv_t       *);
void printTrAdrTlv        (mplsLdpTrAdrTlv_t     *);
void printChpTlv          (mplsLdpChpTlv_t       *);
void printHelloMsg        (mplsLdpHelloMsg_t     *);
void printKeepAliveMsg    (mplsLdpKeepAlMsg_t    *);
void printAdrListTlv      (mplsLdpAdrTlv_t       *);
void printAddressMsg      (mplsLdpAdrMsg_t       *);
void printFecListTlv      (mplsLdpFecTlv_t       *);
void printLblMsgIdTlv     (mplsLdpLblMsgIdTlv_t  *);
void printPathVecTlv      (mplsLdpPathTlv_t      *);
void printHopTlv          (mplsLdpHopTlv_t       *);
void printFrLblTlv        (mplsLdpFrLblTlv_t     *);
void printAtmLblTlv       (mplsLdpAtmLblTlv_t    *);
void printGenLblTlv       (mplsLdpGenLblTlv_t    *);
void printLlbMapMsg       (mplsLdpLblMapMsg_t    *);
void printErFlags         (mplsLdpErFlag_t       *);
void printErIPFlags       (mplsLdpErIPFlag_t     *);
void printErTlv           (mplsLdpErTlv_t        *);
void printTrafficTlv      (mplsLdpTrafficTlv_t   *);
void printLlbReqMsg       (mplsLdpLblReqMsg_t    *);
void printAtmLabel        (mplsLdpAtmLblRng_t    *, int);
void printFspLabel        (mplsLdpFrLblRng_t     *, int);
void printErHop           (mplsLdpErHop_t        *, u_short);
void printLbl_W_R_Msg     (mplsLdpLbl_W_R_Msg_t  *);
void printPreemptTlv      (mplsLdpPreemptTlv_t   *);
void printLspIdTlv        (mplsLdpLspIdTlv_t     *);
void printResClsTlv       (mplsLdpResClsTlv_t    *);
void printPinningTlv      (mplsLdpPinningTlv_t   *);
void printLlbAbortMsg     (mplsLdpLblAbortMsg_t  *);

int  converAsciiToHex     (u_char *, int, u_char *);
int  converHexToAscii     (u_char *, int, u_char *);



int main(void)
{
   mplsLdpHeader_t    testHeaderDecode;
   u_char             buff[10000];
   u_char             buffHex[20002];
   u_char           * startPtr;
   u_char             currentChar;
   u_short            totalSize;
   u_short            type = 0;
   int                encodedSize, sizeBuf;
   int                continueRead;
   int                n    = 0;

   bzero(buffHex, 600);
   bzero(buff, 600);

   sizeBuf = sizeof(buff);

   /* the buffer should not contain blanks or invalid hex caracters */
   printf("Please provide the buffer (in hex). The buffer should end with 'q'!\n");
   continueRead = 1;
   do{
      scanf("%c", &currentChar);
      if ( currentChar == 'q')
      {
	 continueRead = 0;
      }
      buffHex[n++] = currentChar;
   } while( continueRead);

   PRINT_2("n = %d\n", n-1);

   n = converHexToAscii(buffHex, n-1, buff);
   /*converAsciiToHex(buffHex, n, buff);*/
   PRINT_2("read %d chars \n", n);

   startPtr = buff; 

   encodedSize = Mpls_decodeLdpMsgHeader( &testHeaderDecode,
					  startPtr, 
					  n);
   if (encodedSize < 0)
   {
      printf("Failed while decoding HEADER\n");
      return 0;
   }
   DEBUG_CALL(printHeader(&testHeaderDecode));
   startPtr += encodedSize; 

   totalSize = 0;
   if (testHeaderDecode.pduLength > sizeof(buff))
   {
      PRINT_ERR("Buffer too small. Decoding failed\n");
      return 0;
   }
   while(totalSize < (u_short)(testHeaderDecode.pduLength-6))
   {
      /* found the message type */
      MEM_COPY((u_char*)&type, startPtr, 2);
      type = ntohs(type) & 0x7fff;   /* ignore the U bit for now */
      PRINT_2("Found type %x\n", type);

      switch(type)
      {
	 case MPLS_INIT_MSGTYPE:
	 {
            MPLS_MSGSTRUCT(Init);
            encodedSize = Mpls_decodeLdpInitMsg( &MPLS_MSGPARAM(Init), 
      				                 startPtr,
                                                 n-MPLS_LDP_HDRSIZE-totalSize);
            PRINT_2("decodedSize for Init msg = %d\n", encodedSize);
            if (encodedSize < 0)
            {
               return 0;
            }
            DEBUG_CALL(printInitMsg(&MPLS_MSGPARAM(Init)));
	    totalSize += encodedSize;
	    startPtr  += encodedSize;
	    break;
	 }
	 case MPLS_NOT_MSGTYPE:
	 {
            MPLS_MSGSTRUCT(Notif);
            encodedSize = Mpls_decodeLdpNotMsg( &MPLS_MSGPARAM(Notif), 
      				                startPtr,
                                                n-MPLS_LDP_HDRSIZE-totalSize);
	    PRINT_2("decodedSize for Notif msg = %d\n", encodedSize);
            if (encodedSize < 0)
            {
               return 0;
            }
	    DEBUG_CALL(printNotMsg(&MPLS_MSGPARAM(Notif)));
	    totalSize += encodedSize;
	    startPtr  += encodedSize;
	    break;
	 }
	 case MPLS_KEEPAL_MSGTYPE:
	 {
            MPLS_MSGSTRUCT(KeepAl);
            encodedSize = Mpls_decodeLdpKeepAliveMsg(&MPLS_MSGPARAM(KeepAl), 
      				                     startPtr,
                                                     n-MPLS_LDP_HDRSIZE-totalSize);
            PRINT_2("decodedSize for KeepAlive msg = %d\n", encodedSize);
            if (encodedSize < 0)
            {
               return 0;
            }
            DEBUG_CALL(printKeepAliveMsg(&MPLS_MSGPARAM(KeepAl)));
	    totalSize += encodedSize;
	    startPtr  += encodedSize;
	    break;
	 }
	 case MPLS_HELLO_MSGTYPE:
	 {
            MPLS_MSGSTRUCT(Hello);
            encodedSize = Mpls_decodeLdpHelloMsg( &MPLS_MSGPARAM(Hello), 
      				                  startPtr,
                                                  n-MPLS_LDP_HDRSIZE-totalSize);
            PRINT_2("decodedSize for Hello msg = %d\n", encodedSize);
            if (encodedSize < 0)
            {
               return 0;
            }
            DEBUG_CALL(printHelloMsg(&MPLS_MSGPARAM(Hello)));
	    totalSize += encodedSize;
	    startPtr  += encodedSize;
	    break;
	 }
	 case MPLS_LBLREQ_MSGTYPE:
	 {
            MPLS_MSGSTRUCT(LblReq);
            encodedSize = Mpls_decodeLdpLblReqMsg( &MPLS_MSGPARAM(LblReq), 
      				                   startPtr,
                                                   n-MPLS_LDP_HDRSIZE-totalSize);
            PRINT_2("decodedSize for Req msg = %d\n", encodedSize);
            if (encodedSize < 0)
            {
               return 0;
            }
            DEBUG_CALL(printLlbReqMsg(&MPLS_MSGPARAM(LblReq)));
	    totalSize += encodedSize;
	    startPtr  += encodedSize;
	    break;
	 }
	 case MPLS_LBLMAP_MSGTYPE:
	 {
            MPLS_MSGSTRUCT(LblMap);
            encodedSize = Mpls_decodeLdpLblMapMsg( &MPLS_MSGPARAM(LblMap), 
      				                   startPtr,
                                                   n-MPLS_LDP_HDRSIZE-totalSize);
            PRINT_2("decodedSize for Map msg = %d\n", encodedSize);
            if (encodedSize < 0)
            {
               return 0;
            }
            DEBUG_CALL(printLlbMapMsg(&MPLS_MSGPARAM(LblMap)));
	    totalSize += encodedSize;
	    startPtr  += encodedSize;
	    break;
	 }
	 case MPLS_ADDR_MSGTYPE:
	 case MPLS_ADDRWITH_MSGTYPE:
	 {
            MPLS_MSGSTRUCT(Adr);
            encodedSize = Mpls_decodeLdpAdrMsg( &MPLS_MSGPARAM(Adr), 
      				                startPtr,
                                                n-MPLS_LDP_HDRSIZE-totalSize);
            PRINT_2("decodedSize for Adr msg = %d\n", encodedSize);
            if (encodedSize < 0)
            {
               return 0;
            }
            DEBUG_CALL(printAddressMsg(&MPLS_MSGPARAM(Adr)));
	    totalSize += encodedSize;
	    startPtr  += encodedSize;
	    break;
	 }
	 case MPLS_LBLWITH_MSGTYPE:
	 case MPLS_LBLREL_MSGTYPE:
	 {
            MPLS_MSGSTRUCT(Lbl_W_R_);
            encodedSize = Mpls_decodeLdpLbl_W_R_Msg( &MPLS_MSGPARAM(Lbl_W_R_), 
      				                     startPtr,
                                                     n-MPLS_LDP_HDRSIZE-totalSize);
            PRINT_2("decodedSize for Lbl Release/Mapping msg = %d\n", encodedSize);
            if (encodedSize < 0)
            {
               return 0;
            }
            DEBUG_CALL(printLbl_W_R_Msg(&MPLS_MSGPARAM(Lbl_W_R_)));
	    totalSize += encodedSize;
	    startPtr  += encodedSize;
	    break;
	 }
	 case MPLS_LBLABORT_MSGTYPE:
	 {
            MPLS_MSGSTRUCT(LblAbort);
            encodedSize = Mpls_decodeLdpLblAbortMsg( &MPLS_MSGPARAM(LblAbort), 
      				                     startPtr,
                                                     n-MPLS_LDP_HDRSIZE-totalSize);
            PRINT_2("decodedSize for Abort msg = %d\n", encodedSize);
            if (encodedSize < 0)
            {
               return 0;
            }
            DEBUG_CALL(printLlbAbortMsg(&MPLS_MSGPARAM(LblAbort)));
	    totalSize += encodedSize;
	    startPtr  += encodedSize;
	    break;
	 }
	 default:
	 {
            PRINT_ERR_2("Unknown message type = %x\n", type);
            return 0;
	 }
      } /* switch */
   } /* while */

   return 1;
}




/*
 *      Encode-decode for Ldp Msg Header 
 */

/* 
 * Encode:
 */
int Mpls_encodeLdpMsgHeader
(
   mplsLdpHeader_t * header, 
   u_char          * buff, 
   int               bufSize
)
{
   mplsLdpHeader_t headerCopy;  

   if (MPLS_LDP_HDRSIZE > bufSize)
   {
      /* not enough room for header */
      return MPLS_ENC_BUFFTOOSMALL; 
   }

   headerCopy                 = *header;
   headerCopy.protocolVersion = htons(headerCopy.protocolVersion);
   headerCopy.pduLength       = htons(headerCopy.pduLength);
   headerCopy.lsrAddress      = htonl(headerCopy.lsrAddress);
   headerCopy.labelSpace      = htons(headerCopy.labelSpace);

   MEM_COPY(buff, (u_char *)&headerCopy, MPLS_LDP_HDRSIZE);

   return MPLS_LDP_HDRSIZE;

} /* End : Mpls_encodeLdpMsgHeader */

/* 
 * Decode: 
 */
int Mpls_decodeLdpMsgHeader
(
   mplsLdpHeader_t * header, 
   u_char          * buff, 
   int               bufSize
)
{
   if (MPLS_LDP_HDRSIZE > bufSize)
   {
      return MPLS_DEC_BUFFTOOSMALL; 
   }

   MEM_COPY((u_char *)header, buff, MPLS_LDP_HDRSIZE);

   header->protocolVersion = ntohs(header->protocolVersion);
   header->pduLength       = ntohs(header->pduLength);
   header->lsrAddress      = ntohl(header->lsrAddress);
   header->labelSpace      = ntohs(header->labelSpace);

   /* check if the length is over the max length */
   if (header->pduLength > MPLS_PDUMAXLEN)
   { 
      return MPLS_PDU_LENGTH_ERROR;
   }

   return MPLS_LDP_HDRSIZE;

} /* End: Mpls_decodeLdpMsgHeader */




/*
 *      Encode-decode for Ldp Base Message
 */

/* 
 * Encode:
 */
int Mpls_encodeLdpBaseMsg
(
   mplsLdpMsg_t * ldpMsg, 
   u_char       * buff, 
   int            bufSize
)
{
   if (MPLS_MSGIDFIXLEN + MPLS_TLVFIXLEN > bufSize)
   {
      /* not enough room for header */
      return MPLS_ENC_BUFFTOOSMALL; 
   }

   ldpMsg->flags.mark = htons(ldpMsg->flags.mark);
   ldpMsg->msgLength  = htons(ldpMsg->msgLength);
   ldpMsg->msgId      = htonl(ldpMsg->msgId);

   MEM_COPY(buff, (u_char *)ldpMsg, MPLS_MSGIDFIXLEN + MPLS_TLVFIXLEN);

   return (MPLS_MSGIDFIXLEN + MPLS_TLVFIXLEN);

} /* End : Mpls_encodeLdpBaseMsg*/

/* 
 * Decode: 
 */
int Mpls_decodeLdpBaseMsg 
(
   mplsLdpMsg_t * ldpMsg, 
   u_char       * buff, 
   int            bufSize
)
{
   if (MPLS_MSGIDFIXLEN + MPLS_TLVFIXLEN > bufSize)
   {
      return MPLS_DEC_BUFFTOOSMALL; 
   }

   MEM_COPY((u_char *)ldpMsg, buff, MPLS_MSGIDFIXLEN + MPLS_TLVFIXLEN);

   ldpMsg->flags.mark = ntohs(ldpMsg->flags.mark);
   ldpMsg->msgLength  = ntohs(ldpMsg->msgLength);
   ldpMsg->msgId      = ntohl(ldpMsg->msgId);

   return MPLS_MSGIDFIXLEN + MPLS_TLVFIXLEN;

} /* End: Mpls_decodeLdpBaseMsg */


/*
 *      Encode-decode for ATM Label Range Component
 */

/* 
 * encode: 
 */ 
int Mpls_encodeLdpAtmLblRng
(
   mplsLdpAtmLblRng_t * atmLbl, 
   u_char             * buff, 
   int                  bufSize
)
{
   if (MPLS_ATMLRGFIXLEN > bufSize)
   {
      /* not enough room for label */
      return MPLS_ENC_BUFFTOOSMALL; 
   }

   atmLbl->flags.flags.res1 = 0;
   atmLbl->flags.flags.res2 = 0;
   atmLbl->flags.mark[0] = htonl(atmLbl->flags.mark[0]);
   atmLbl->flags.mark[1] = htonl(atmLbl->flags.mark[1]);

   MEM_COPY(buff, (u_char *)atmLbl, MPLS_ATMLRGFIXLEN);

   return MPLS_ATMLRGFIXLEN;
   
} /* End Mpls_encodeLdpAtmLblRng */


/* 
 * decode: 
 */ 
int Mpls_decodeLdpAtmLblRng
(
   mplsLdpAtmLblRng_t * atmLbl, 
   u_char             * buff, 
   int                  bufSize
)
{
   if (MPLS_ATMLRGFIXLEN > bufSize)
   {
      PRINT_ERR("failed decoding the Atm Lbl Rng\n");
      return MPLS_DEC_BUFFTOOSMALL; 
   }

   MEM_COPY((u_char *)atmLbl, buff, MPLS_ATMLRGFIXLEN);

   atmLbl->flags.mark[0]    = ntohl(atmLbl->flags.mark[0]);
   atmLbl->flags.mark[1]    = ntohl(atmLbl->flags.mark[1]);

   return MPLS_ATMLRGFIXLEN;

} /* End Mpls_decodeLdpAtmLblRng */





/*
 *      Encode-decode for ATM Session Parameters 
 */

/* 
 * encode: 
 */ 
int Mpls_encodeLdpAsp
(
   mplsLdpAspTlv_t * atmAsp, 
   u_char          * buff, 
   int               bufSize
)
{
   u_int           encodedSize = 0;
   u_short         totalSize   = 0;
   u_char *        tempBuf     = buff; /* no change for the buff ptr */
   u_int           i, numLblRng;

   /* get the size of the atmAsp to be encoded and check it against
      the buffer size */

   if (MPLS_TLVFIXLEN + (int)(atmAsp->baseTlv.length)> bufSize)
   {
      /* not enough room */
      return MPLS_ENC_BUFFTOOSMALL; 
   }

   /* 
    *  encode for tlv 
    */
   encodedSize = Mpls_encodeLdpTlv( &(atmAsp->baseTlv),
                                    tempBuf, 
				    bufSize);
   if (encodedSize < 0)
   {
      return MPLS_ENC_TLVERROR;
   }
   tempBuf   += encodedSize; 
   totalSize += encodedSize;

   /* 
    *  encode for M + N + D + res 
    */
   numLblRng                = atmAsp->flags.flags.numLblRng;
   atmAsp->flags.flags.res  = 0;
   atmAsp->flags.mark       = htonl(atmAsp->flags.mark);

   MEM_COPY( tempBuf, 
	     (u_char*)&(atmAsp->flags.mark),
             MPLS_ASPFIXLEN );
   tempBuf   += MPLS_ASPFIXLEN; 
   totalSize += MPLS_ASPFIXLEN;

   /* 
    *  encode for ATM labels 
    */
   for (i = 0; i < numLblRng; i++)
   {
      encodedSize = Mpls_encodeLdpAtmLblRng( &(atmAsp->lblRngList[i]),
		                             tempBuf,
		                             bufSize-totalSize);
      if (encodedSize < 0)
      {
	 return MPLS_ENC_ATMLBLERROR;
      }
      tempBuf   += encodedSize;
      totalSize += encodedSize;
   }

   return totalSize; 
   
} /* End Mpls_encodeLdpAsp */


/* 
 * decode: 
 */ 
int Mpls_decodeLdpAsp
(
   mplsLdpAspTlv_t * atmAsp, 
   u_char          * buff, 
   int               bufSize
)
{
   int      decodedSize = 0;
   u_short  totalSize = 0;
   u_char * tempBuf     = buff; /* no change for the buff ptr */
   u_int    i;

   if (MPLS_ASPFIXLEN > bufSize)
   {
      /* the buffer does not contain even the required field*/
      PRINT_ERR("failed in decoding LdpAsp\n");
      return MPLS_DEC_BUFFTOOSMALL; 
   }

   /* 
    *  decode for M + N + D + res 
    */
   MEM_COPY((u_char *)&(atmAsp->flags.mark), tempBuf, MPLS_ASPFIXLEN);
   tempBuf   += MPLS_ASPFIXLEN;
   totalSize += MPLS_ASPFIXLEN;

   atmAsp->flags.mark = ntohl(atmAsp->flags.mark);

   /*
    *  decode for ATM labels 
    */
   for (i = 0; i < atmAsp->flags.flags.numLblRng; i++)
   {
      decodedSize = Mpls_decodeLdpAtmLblRng( &(atmAsp->lblRngList[i]),
		                             tempBuf,
			                     bufSize - totalSize);
      if (decodedSize < 0)
      {
         PRINT_ERR_2("failed in decoding LdpAtmLabel[%d] for LdpAsp\n", i);
	 return MPLS_DEC_ATMLBLERROR;
      }
      tempBuf   += decodedSize;
      totalSize += decodedSize;
   }

   return  totalSize; 

} /* End Mpls_decodeLdpAsp */



/*
 *      Encode-decode for TLV
 */

/* 
 * encode: 
 */ 
int Mpls_encodeLdpTlv 
(
   mplsLdpTlv_t * tlv, 
   u_char       * buff, 
   int            bufSize
)
{
   if (MPLS_TLVFIXLEN > bufSize)
   {
      /* not enough room for label */
      return MPLS_ENC_BUFFTOOSMALL; 
   }
   
   tlv->flags.mark = htons(tlv->flags.mark); 
   tlv->length     = htons(tlv->length); 

   MEM_COPY(buff, (u_char *)tlv, MPLS_TLVFIXLEN);

   return MPLS_TLVFIXLEN;

} /* End: Mpls_encodeLdpTlv */


/* 
 * decode: 
 */ 
int Mpls_decodeLdpTlv 
(
   mplsLdpTlv_t * tlv, 
   u_char       * buff, 
   int            bufSize
)
{
   if (MPLS_TLVFIXLEN > bufSize)
   {
      PRINT_ERR("Failed decoding TLV\n");
      return MPLS_DEC_BUFFTOOSMALL; 
   }

   MEM_COPY((u_char *)tlv, buff, MPLS_TLVFIXLEN);

   tlv->flags.mark = ntohs(tlv->flags.mark); 
   tlv->length     = ntohs(tlv->length); 

   return MPLS_TLVFIXLEN;

} /* End: Mpls_decodeLdpTlv */




/*
 *      Encode-decode for CSP (common session param)
 */

/* 
 * encode: 
 */ 
int Mpls_encodeLdpCsp
(
   mplsLdpCspTlv_t  * csp, 
   u_char           * buff, 
   int                bufSize
)
{
   u_int           encodedSize = 0;
   u_char *        tempBuf     = buff; /* no change for the buff ptr */
   u_char *        cspPtr;

   if (MPLS_CSPFIXLEN + MPLS_TLVFIXLEN > bufSize)
   {
      /* not enough room */
      return MPLS_ENC_BUFFTOOSMALL; 
   }

   cspPtr  = (u_char *)csp;

   /* 
    *  encode for tlv 
    */
   encodedSize = Mpls_encodeLdpTlv( &(csp->baseTlv),
                                    tempBuf, 
				    bufSize); 
   if (encodedSize < 0)
   {
      PRINT_ERR("failed encoding the tlv in CSP\n");
      return MPLS_ENC_TLVERROR;
   }
   tempBuf += encodedSize; 
   cspPtr  += encodedSize;

   /* 
    *  encode for the rest of the Csp 
    */
   csp->protocolVersion = htons(csp->protocolVersion);
   csp->holdTime        = htons(csp->holdTime);
   csp->flags.mark      = htons(csp->flags.mark);
   csp->maxPduLen       = htons(csp->maxPduLen);
   csp->rcvLsrAddress   = htonl(csp->rcvLsrAddress);
   csp->rcvLsId         = htons(csp->rcvLsId);
   
   MEM_COPY(tempBuf, 
            cspPtr, 
            MPLS_CSPFIXLEN);

   return (MPLS_CSPFIXLEN + MPLS_TLVFIXLEN);

} /* End: Mpls_encodeLdpCsp*/


/* 
 * decode: 
 */ 
int Mpls_decodeLdpCsp
(
   mplsLdpCspTlv_t  * csp, 
   u_char           * buff, 
   int                bufSize
)
{
   u_char * cspPtr;

   if (MPLS_CSPFIXLEN > bufSize)
   {
      /* not enough data for Csp */
      PRINT_ERR("failed decoding LdpCsp\n");
      return MPLS_DEC_BUFFTOOSMALL; 
   }

   cspPtr  = (u_char *)csp;
   cspPtr  += MPLS_TLVFIXLEN; /* we want to point to the flags since the
				 tlv was decoded before we reach here */

   /* 
    *  decode for the rest of the Csp 
    */
   MEM_COPY( cspPtr, 
	     buff, 
             MPLS_CSPFIXLEN);

   csp->protocolVersion = ntohs(csp->protocolVersion);
   csp->holdTime        = ntohs(csp->holdTime);
   csp->flags.mark      = ntohs(csp->flags.mark);
   csp->maxPduLen       = ntohs(csp->maxPduLen);
   csp->rcvLsrAddress   = ntohl(csp->rcvLsrAddress);
   csp->rcvLsId         = ntohs(csp->rcvLsId);
   
   return MPLS_CSPFIXLEN;

} /* Mpls_decodeLdpCsp*/



/*
 *      Encode-decode for Fr Session Parameters
 */

/* 
 * encode
 */ 
int Mpls_encodeLdpFsp
(
   mplsLdpFspTlv_t * frFsp, 
   u_char          * buff, 
   int               bufSize
)
{
   u_int    encodedSize = 0;
   u_short  totalSize   = 0;
   u_char * tempBuf     = buff; /* no change for the buff ptr */
   u_int    i, numLblRng;

   /* get the size of the frAsp to be encoded and check it against
      the buffer size */

   if (MPLS_TLVFIXLEN + (int)(frFsp->baseTlv.length)> bufSize)
   {
      /* not enough room */
      return MPLS_ENC_BUFFTOOSMALL; 
   }

   /* 
    *  encode for tlv 
    */
   encodedSize = Mpls_encodeLdpTlv( &(frFsp->baseTlv),
                                    tempBuf, 
				    bufSize);
   if (encodedSize < 0)
   {
      return MPLS_ENC_TLVERROR;
   }
   tempBuf   += encodedSize; 
   totalSize += encodedSize;

   /* 
    *  encode for M + N + dir + res 
    */
   numLblRng              = frFsp->flags.flags.numLblRng;
   frFsp->flags.flags.res = 0;
   frFsp->flags.mark      = htonl(frFsp->flags.mark);

   MEM_COPY( tempBuf, 
	     (u_char*)&(frFsp->flags.mark),
             MPLS_FSPFIXLEN );
   tempBuf   += MPLS_FSPFIXLEN; 
   totalSize += MPLS_FSPFIXLEN;

   /* 
    *  encode for FR labels 
    */
   for (i = 0; i < numLblRng; i++)
   {
      encodedSize = Mpls_encodeLdpFrLblRng( &(frFsp->lblRngList[i]),
		                            tempBuf,
		                            bufSize-totalSize);
      if (encodedSize < 0)
      {
	 return MPLS_ENC_FSPLBLERROR;
      }
      tempBuf   += encodedSize;
      totalSize += encodedSize;
   }

   return totalSize;

} /* End: Mpls_encodeLdpFsp */


/* 
 * decode
 */ 
int Mpls_decodeLdpFsp
(
   mplsLdpFspTlv_t * frFsp, 
   u_char          * buff, 
   int               bufSize
)
{ 
   int      decodedSize = 0;
   u_short  totalSize   = 0;
   u_char * tempBuf     = buff; /* no change for the buff ptr */
   u_int    i;

   if (MPLS_FSPFIXLEN > bufSize)
   {
      /* the buffer does not contain even the required field*/
      PRINT_ERR("failed in decoding LdpFsp\n");
      return MPLS_DEC_BUFFTOOSMALL; 
   }

   /* 
    *  decode for M + N + res 
    */
   MEM_COPY((u_char *)&(frFsp->flags.mark), tempBuf, MPLS_FSPFIXLEN);
   tempBuf   += MPLS_FSPFIXLEN;
   totalSize += MPLS_FSPFIXLEN;

   frFsp->flags.mark = ntohl(frFsp->flags.mark);

   /*
    *  decode for FR labels 
    */
   for (i = 0; i < frFsp->flags.flags.numLblRng; i++)
   {
      decodedSize = Mpls_decodeLdpFrLblRng( &(frFsp->lblRngList[i]),
		                            tempBuf,
			                    bufSize - totalSize);
      if (decodedSize < 0)
      {
         PRINT_ERR_2("failed in decoding LdpFrLabel[%d] for LdpFsp\n", i);
	 return MPLS_DEC_FSPLBLERROR;
      }
      tempBuf   += decodedSize;
      totalSize += decodedSize;
   }

   return  totalSize; 

} /* End: Mpls_decodeLdpFsp */



/*
 *      Encode-decode for INIT msg 
 */

/* 
 * encode for init message 
 */ 
int Mpls_encodeLdpInitMsg
(
   mplsLdpInitMsg_t * initMsg, 
   u_char           * buff, 
   int                bufSize
)
{
   mplsLdpInitMsg_t initMsgCopy;
   u_int            encodedSize, totalSize;
   u_char *         tempBuf = buff; /* no change for the buff ptr */

   initMsgCopy = *initMsg;
   totalSize   = 0;

   /* check the length of the messageId + mandatory param +
      optional param */
   if ((int)(initMsgCopy.baseMsg.msgLength) + MPLS_TLVFIXLEN > bufSize) 
   {
      PRINT_ERR("failed to encode the init msg: BUFFER TOO SMALL\n");
      return MPLS_ENC_BUFFTOOSMALL;
   }

   /*
    *  encode the base part of the pdu message
    */
   encodedSize = Mpls_encodeLdpBaseMsg( &(initMsgCopy.baseMsg), 
				        tempBuf, 
				        bufSize);
   if (encodedSize < 0)
   {
      return MPLS_ENC_BASEMSGERROR;
   }
   PRINT_2("Encode BaseMsg for init on %d bytes\n", encodedSize);
   tempBuf   += encodedSize;
   totalSize += encodedSize;

   /*
    *  encode the csp if any 
    */
   if (initMsgCopy.cspExists)
   {
      encodedSize = Mpls_encodeLdpCsp( &(initMsgCopy.csp), 
				       tempBuf, 
				       bufSize-totalSize);
      if (encodedSize < 0)
      {
	 return MPLS_ENC_CSPERROR;
      }
      PRINT_2("Encoded for CSP %d bytes\n", encodedSize);
      tempBuf   += encodedSize;
      totalSize += encodedSize;
   }

   /*
    *  encode the asp if any 
    */
   if (initMsgCopy.aspExists)
   {

      encodedSize = Mpls_encodeLdpAsp( &(initMsgCopy.asp), 
				       tempBuf, 
				       bufSize-totalSize);
      if (encodedSize < 0)
      {
	 return MPLS_ENC_ASPERROR;
      }
      PRINT_2("Encoded for ASP %d bytes\n", encodedSize);
      tempBuf   += encodedSize;
      totalSize += encodedSize;
   }

   /*
    *  encode the fsp if any 
    */
   if (initMsgCopy.fspExists)
   {
      encodedSize = Mpls_encodeLdpFsp( &(initMsgCopy.fsp), 
				       tempBuf, 
				       bufSize-totalSize);
      if (encodedSize < 0)
      {
	 return MPLS_ENC_FSPERROR;
      }
      tempBuf   += encodedSize;
      totalSize += encodedSize;
   }
   
   return totalSize;

} /* End: Mpls_encodeLdpInitMsg */


/* 
 * decode for init message 
 */ 
int Mpls_decodeLdpInitMsg
(
   mplsLdpInitMsg_t * initMsg, 
   u_char           * buff, 
   int                bufSize
)
{
   int          decodedSize    = 0;
   u_int        totalSize      = 0;
   u_int        stopLength     = 0;
   u_char *     tempBuf        = buff; /* no change for the buff ptr */
   u_int        totalSizeParam = 0;
   mplsLdpTlv_t tlvTemp;

   /*
    *  decode the base part of the pdu message
    */
   bzero(initMsg, sizeof(mplsLdpInitMsg_t));
   decodedSize = Mpls_decodeLdpBaseMsg( &(initMsg->baseMsg), 
				        tempBuf, 
				        bufSize);
   if (decodedSize < 0)
   {
      return MPLS_DEC_BASEMSGERROR;
   }
   PRINT_2("Decode BaseMsg for init on %d bytes\n", decodedSize);

   if (initMsg->baseMsg.flags.flags.msgType != MPLS_INIT_MSGTYPE)
   {
      PRINT_ERR_2("Not the right message type; expected init and got %x\n",
		   initMsg->baseMsg.flags.flags.msgType);
      return MPLS_MSGTYPEERROR;
   }
   tempBuf   += decodedSize;
   totalSize += decodedSize;

   if (bufSize-totalSize <= 0)
   {
      /* nothing left for decoding */
      PRINT_ERR("Init msg does not have anything beside base msg\n");
      return totalSize;
   }

   PRINT_4("bufSize = %d,  totalSize = %d, initMsg->baseMsg.msgLength = %d\n",
	   bufSize, totalSize, initMsg->baseMsg.msgLength);

   /* Have to check the baseMsg.msgLength to know when to finish.
    * We finsh when the totalSizeParam is >= to the base message length - the
    * message id length (4) 
    */
    
   stopLength = initMsg->baseMsg.msgLength - MPLS_MSGIDFIXLEN;
   while (stopLength > totalSizeParam)
   {
      /*
       *  decode the tlv to check what's next
       */
      decodedSize = Mpls_decodeLdpTlv(&tlvTemp, tempBuf, bufSize-totalSize);
      if (decodedSize < 0)
      {
         /* something wrong */
         PRINT_ERR("INIT msg decode failed for tlv\n");
         return MPLS_DEC_TLVERROR;
      }

      tempBuf        += decodedSize;
      totalSize      += decodedSize;
      totalSizeParam += decodedSize;

      switch (tlvTemp.flags.flags.type)
      {
      case MPLS_CSP_TLVTYPE:
	   {
              decodedSize = Mpls_decodeLdpCsp( &(initMsg->csp), 
				               tempBuf, 
				               bufSize-totalSize);
              if (decodedSize < 0)
              {
		 PRINT_ERR("Failure when decoding Csp from init msg\n");
	         return MPLS_DEC_CSPERROR;
              }
	      PRINT_2("Decoded for CSP %d bytes\n", decodedSize);
              tempBuf        += decodedSize;
              totalSize      += decodedSize;
              totalSizeParam += decodedSize;
	      initMsg->cspExists   = 1;
	      initMsg->csp.baseTlv = tlvTemp;
	      DEBUG_CALL(printCspTlv(&(initMsg->csp)));
	      break;
	   }
      case MPLS_ASP_TLVTYPE:
	   {
              decodedSize = Mpls_decodeLdpAsp( &(initMsg->asp), 
				               tempBuf, 
				               bufSize-totalSize);
              if (decodedSize < 0)
              {
		 PRINT_ERR("Failure when decoding Asp from init msg\n");
	         return MPLS_DEC_ASPERROR;
              }
	      PRINT_2("Decoded for ASP %d bytes\n", decodedSize);
              tempBuf        += decodedSize;
              totalSize      += decodedSize;
              totalSizeParam += decodedSize;
	      initMsg->aspExists   = 1;
	      initMsg->asp.baseTlv = tlvTemp;
	      DEBUG_CALL(printAspTlv(&(initMsg->asp)));
	      break;
	   }
      case MPLS_FSP_TLVTYPE:
	   {
              decodedSize = Mpls_decodeLdpFsp( &(initMsg->fsp), 
				               tempBuf, 
				               bufSize-totalSize);
              if (decodedSize < 0)
              {
		 PRINT_ERR("Failure when decoding Fsp from init msg\n");
	         return MPLS_DEC_FSPERROR;
              }
              tempBuf        += decodedSize;
              totalSize      += decodedSize;
              totalSizeParam += decodedSize;
	      initMsg->fspExists   = 1;
	      initMsg->fsp.baseTlv = tlvTemp;
	      DEBUG_CALL(printFspTlv(&(initMsg->fsp)));
	      break;
	   }
      default:
	   {
	      PRINT_ERR_2("Found wrong tlv type while decoding init msg (%d)\n",
	             tlvTemp.flags.flags.type);
	      if (tlvTemp.flags.flags.uBit == 1)
	      {
		 /* ignore the Tlv and continue processing */
                 tempBuf        += tlvTemp.length;
                 totalSize      += tlvTemp.length;
                 totalSizeParam += tlvTemp.length;
                 break;
	      }
	      else
	      {
		 /* drop the message; return error */
		 return MPLS_TLVTYPEERROR;
	      }
	   }
      } /* switch type */

   } /* while */

   PRINT_2("totalsize for Mpls_decodeLdpInitMsg is %d\n", totalSize);

   return totalSize;

} /* End: Mpls_decodeLdpInitMsg */



/*
 *      Encode-decode for Fr Label Range 
 */

/* 
 * encode
 */ 
int Mpls_encodeLdpFrLblRng 
(
   mplsLdpFrLblRng_t * frLabel, 
   u_char            * buff, 
   int                 bufSize
)
{
   if (MPLS_FRLRGFIXLEN > bufSize)
   {
      /* not enough room for label */
      return MPLS_ENC_BUFFTOOSMALL; 
   }

   frLabel->flags.flags.res_min = 0;
   frLabel->flags.flags.res_max = 0;
   frLabel->flags.mark[0] = htonl(frLabel->flags.mark[0]);
   frLabel->flags.mark[1] = htonl(frLabel->flags.mark[1]);

   MEM_COPY(buff, (u_char *)frLabel, MPLS_FRLRGFIXLEN);

   return MPLS_FRLRGFIXLEN;

} /* End: Mpls_encodeLdpFrLblRng */



/* 
 * decode
 */ 
int Mpls_decodeLdpFrLblRng 
(
   mplsLdpFrLblRng_t * frLabel, 
   u_char            * buff, 
   int                 bufSize
)
{
   if (MPLS_FRLRGFIXLEN > bufSize)
   {
      PRINT_ERR("failed decoding the Fr Lbl Rng\n");
      return MPLS_DEC_BUFFTOOSMALL; 
   }

   MEM_COPY((u_char *)frLabel, buff, MPLS_FRLRGFIXLEN);

   frLabel->flags.mark[0] = ntohl(frLabel->flags.mark[0]);
   frLabel->flags.mark[1] = ntohl(frLabel->flags.mark[1]);

   return MPLS_FRLRGFIXLEN;

} /* End: Mpls_decodeLdpFrLblRng */




/*
 *      Encode-decode for NOTIFICATION msg 
 */

/* 
 * encode for notification message 
 */ 
int Mpls_encodeLdpNotMsg
(
   mplsLdpNotifMsg_t * notMsg, 
   u_char            * buff, 
   int                 bufSize
)
{
   mplsLdpNotifMsg_t notMsgCopy;
   u_int             encodedSize = 0;
   u_int             totalSize   = 0;
   u_char *          tempBuf     = buff; /* no change for the buff ptr */

   /* check the length of the messageId + mandatory param +
      optional param */
   if ((int)(notMsg->baseMsg.msgLength) + MPLS_TLVFIXLEN > bufSize) 
   {
      PRINT_ERR("failed to encode the not msg: BUFFER TOO SMALL\n");
      return MPLS_ENC_BUFFTOOSMALL;
   }

   notMsgCopy = *notMsg;

   /*
    *  encode the base part of the pdu message
    */
   encodedSize = Mpls_encodeLdpBaseMsg( &(notMsgCopy.baseMsg), 
				        tempBuf, 
				        bufSize);
   if (encodedSize < 0)
   {
      return MPLS_ENC_BASEMSGERROR;
   }
   PRINT_2("Encode BaseMsg for not on %d bytes\n", encodedSize);

   tempBuf   += encodedSize;
   totalSize += encodedSize;

   if (notMsgCopy.statusTlvExists)
   {
      encodedSize = Mpls_encodeLdpStatus( &(notMsgCopy.status), 
				          tempBuf, 
				          bufSize-totalSize);
      if (encodedSize < 0)
      {
	 return MPLS_ENC_STATUSERROR;
      }
      PRINT_2("Encoded for STATUS %d bytes\n", encodedSize);
      tempBuf   += encodedSize;
      totalSize += encodedSize;
   }
   if (notMsgCopy.exStatusTlvExists)
   {
      encodedSize = Mpls_encodeLdpExStatus( &(notMsgCopy.exStatus), 
				            tempBuf, 
				            bufSize-totalSize);
      if (encodedSize < 0)
      {
	 return MPLS_ENC_EXSTATERROR;
      }
      PRINT_2("Encoded for EXTENDED STATUS %d bytes\n", encodedSize);
      tempBuf   += encodedSize;
      totalSize += encodedSize;
   }
   if (notMsgCopy.retPduTlvExists)
   {
      encodedSize = Mpls_encodeLdpRetPdu( &(notMsgCopy.retPdu), 
				          tempBuf, 
				          bufSize-totalSize);
      if (encodedSize < 0)
      {
	 return MPLS_ENC_RETPDUERROR;
      }
      PRINT_2("Encoded for RET PDU %d bytes\n", encodedSize);
      tempBuf   += encodedSize;
      totalSize += encodedSize;
   }
   if (notMsgCopy.retMsgTlvExists)
   {
      encodedSize = Mpls_encodeLdpRetMsg( &(notMsgCopy.retMsg), 
				            tempBuf, 
				            bufSize-totalSize);
      if (encodedSize < 0)
      {
	 return MPLS_ENC_RETMSGERROR;
      }
      PRINT_2("Encoded for RET MSG %d bytes\n", encodedSize);
      tempBuf   += encodedSize;
      totalSize += encodedSize;
   }
   if (notMsgCopy.lspidTlvExists)
   {
      encodedSize = Mpls_encodeLdpLspIdTlv( &(notMsgCopy.lspidTlv), 
				             tempBuf, 
				             bufSize-totalSize);
      if (encodedSize < 0)
      {
         return MPLS_ENC_LSPIDERROR;
      }
      PRINT_2("Encoded for LSPID Tlv %d bytes\n", encodedSize);
      tempBuf   += encodedSize;
      totalSize += encodedSize;
   }

   return totalSize;

} /* End: Mpls_encodeLdpNotMsg */


/* 
 * decode for notification message 
 */ 
int Mpls_decodeLdpNotMsg
(
   mplsLdpNotifMsg_t * notMsg, 
   u_char            * buff, 
   int                 bufSize
)
{
   u_int           decodedSize    = 0;
   u_int           totalSize      = 0;
   u_int           stopLength     = 0;
   u_int           totalSizeParam = 0;
   u_char *        tempBuf        = buff; /* no change for the buff ptr */
   mplsLdpTlv_t    tlvTemp;

   /*
    *  decode the base part of the pdu message
    */
   bzero(notMsg, sizeof(mplsLdpNotifMsg_t));
   decodedSize = Mpls_decodeLdpBaseMsg( &(notMsg->baseMsg), 
				        tempBuf, 
				        bufSize);
   if (decodedSize < 0)
   {
      return MPLS_DEC_BASEMSGERROR;
   }
   PRINT_2("Decode BaseMsg for not on %d bytes\n", decodedSize);

   if (notMsg->baseMsg.flags.flags.msgType != MPLS_NOT_MSGTYPE)
   {
      PRINT_ERR_2("Not the right message type; expected not and got %x\n",
		   notMsg->baseMsg.flags.flags.msgType);
      return MPLS_MSGTYPEERROR;
   }

   tempBuf   += decodedSize;
   totalSize += decodedSize;

   if (bufSize-totalSize <= 0)
   {
      /* nothing left for decoding */
      PRINT_ERR("Not msg does not have anything beside base msg\n");
      return totalSize;
   }

   PRINT_4("bufSize = %d,  totalSize = %d, notMsg->baseMsg.msgLength = %d\n",
	   bufSize, totalSize, notMsg->baseMsg.msgLength);

   /* Have to check the baseMsg.msgLength to know when to finish.
    * We finsh when the totalSizeParam is >= to the base message length - the
    * message id length (4) 
    */
    
   stopLength = notMsg->baseMsg.msgLength - MPLS_MSGIDFIXLEN;
   while (stopLength > totalSizeParam)
   {
      /*
       *  decode the tlv to check what's next
       */
      bzero(&tlvTemp, MPLS_TLVFIXLEN);
      decodedSize = Mpls_decodeLdpTlv(&tlvTemp, tempBuf, bufSize-totalSize);
      if (decodedSize < 0)
      {
         /* something wrong */
         PRINT_ERR("NOT msg decode failed for tlv\n");
         return MPLS_DEC_TLVERROR;
      }

      tempBuf        += decodedSize;
      totalSize      += decodedSize;
      totalSizeParam += decodedSize;

      switch (tlvTemp.flags.flags.type)
      {
         case MPLS_NOT_ST_TLVTYPE:
         {
            decodedSize = Mpls_decodeLdpStatus( &(notMsg->status), 
				                tempBuf, 
				                bufSize-totalSize);
            if (decodedSize < 0)
            {
	       PRINT_ERR("Failure when decoding Status from not msg\n");
	       return MPLS_DEC_STATUSERROR;
            }
	    PRINT_2("Decoded for STATUS %d bytes\n", decodedSize);
            tempBuf        += decodedSize;
            totalSize      += decodedSize;
            totalSizeParam += decodedSize;

	    notMsg->statusTlvExists = 1;
	    notMsg->status.baseTlv  = tlvTemp;
	    DEBUG_CALL(printStatusTlv(&(notMsg->status)));
	    break;
         }
         case MPLS_NOT_ES_TLVTYPE:
	 {
            decodedSize = Mpls_decodeLdpExStatus( &(notMsg->exStatus), 
				                  tempBuf, 
				                  bufSize-totalSize);
            if (decodedSize < 0)
            {
	       PRINT_ERR("Failure when decoding Extended Status from not msg\n");
	       return MPLS_DEC_EXSTATERROR;
            }
	    PRINT_2("Decoded for EX_STATUS %d bytes\n", decodedSize);
            tempBuf        += decodedSize;
            totalSize      += decodedSize;
            totalSizeParam += decodedSize;

	    notMsg->exStatusTlvExists = 1;
	    notMsg->exStatus.baseTlv  = tlvTemp;
	    DEBUG_CALL(printExStatusTlv(&(notMsg->exStatus)));
	    break;
	 }
         case MPLS_NOT_RP_TLVTYPE:
	 {
            decodedSize = Mpls_decodeLdpRetPdu( &(notMsg->retPdu), 
				                tempBuf, 
				                bufSize-totalSize,
						tlvTemp.length);
            if (decodedSize < 0)
            {
	       PRINT_ERR("Failure when decoding Returned PDU from not msg\n");
	       return MPLS_DEC_RETPDUERROR;
            }
	    PRINT_2("Decoded for RET_PDU %d bytes\n", decodedSize);
            tempBuf        += decodedSize;
            totalSize      += decodedSize;
            totalSizeParam += decodedSize;

	    notMsg->retPduTlvExists = 1;
	    notMsg->retPdu.baseTlv  = tlvTemp;
	    DEBUG_CALL(printRetPduTlv(&(notMsg->retPdu)));
	    break;
	 }
         case MPLS_NOT_RM_TLVTYPE:
	 {
            decodedSize = Mpls_decodeLdpRetMsg( &(notMsg->retMsg), 
				                tempBuf, 
				                bufSize-totalSize,
						tlvTemp.length);
            if (decodedSize < 0)
            {
	       PRINT_ERR("Failure when decoding Returned MSG from not msg\n");
	       return MPLS_DEC_RETMSGERROR;
            }
	    PRINT_2("Decoded for RET_MSG %d bytes\n", decodedSize);
            tempBuf        += decodedSize;
            totalSize      += decodedSize;
            totalSizeParam += decodedSize;

	    notMsg->retMsgTlvExists = 1;
	    notMsg->retMsg.baseTlv  = tlvTemp;
	    DEBUG_CALL(printRetMsgTlv(&(notMsg->retMsg)));
	    break;
	 }
         case MPLS_LSPID_TLVTYPE:
	 {
            decodedSize = Mpls_decodeLdpLspIdTlv( &(notMsg->lspidTlv), 
				                    tempBuf, 
				                    bufSize-totalSize);
            if (decodedSize < 0)
            {
	       PRINT_ERR("Failure when dec LSPID tlv from Not msg\n");
	       return MPLS_DEC_LSPIDERROR;
            }
	    PRINT_2("Decoded for lspid tlv %d bytes\n", decodedSize);
            tempBuf        += decodedSize;
            totalSize      += decodedSize;
            totalSizeParam += decodedSize;

	    notMsg->lspidTlvExists   = 1;
	    notMsg->lspidTlv.baseTlv = tlvTemp;
	    DEBUG_CALL(printLspIdTlv(&(notMsg->lspidTlv)));
	    break;
	 }
         default:
	 {
	      PRINT_ERR_2("Found wrong tlv type while decoding not msg (%d)\n",
	             tlvTemp.flags.flags.type);
	      if (tlvTemp.flags.flags.uBit == 1)
	      {
		 /* ignore the Tlv and continue processing */
                 tempBuf        += tlvTemp.length;
                 totalSize      += tlvTemp.length;
                 totalSizeParam += tlvTemp.length;
                 break;
	      }
	      else
	      {
		 /* drop the message; return error */
		 return MPLS_TLVTYPEERROR;
	      }
	   }
      } /* switch type */

   } /* while */

   PRINT_2("totalsize for Mpls_decodeLdpNotMsg is %d\n", totalSize);

   return totalSize;

} /* End: Mpls_decodeLdpNotMsg */



/*
 *      Encode-decode for Status TLV 
 */

/* 
 * encode:
 */ 
int Mpls_encodeLdpStatus 
(
   mplsLdpStatusTlv_t * status, 
   u_char             * buff, 
   int                  bufSize
) 
{
   u_int    encodedSize = 0;
   u_char * tempBuf     = buff; /* no change for the buff ptr */
   u_char * statusPtr;

   if (MPLS_STATUSFIXLEN + MPLS_TLVFIXLEN > bufSize)
   {
      /* not enough room */
      return MPLS_ENC_BUFFTOOSMALL; 
   }

   /* 
    *  encode for tlv 
    */
   encodedSize = Mpls_encodeLdpTlv( &(status->baseTlv),
                                    tempBuf, 
				    bufSize); 
   if (encodedSize < 0)
   {
      PRINT_ERR("failed encoding the tlv in STATUS\n");
      return MPLS_ENC_TLVERROR;
   }

   statusPtr  = (u_char *)status;
   tempBuf    += encodedSize; 
   statusPtr  += encodedSize;

   /* 
    *  encode for the rest of the  Status
    */
   status->flags.mark = htonl(status->flags.mark);
   status->msgId      = htonl(status->msgId);
   status->msgType    = htons(status->msgType);

   MEM_COPY(tempBuf, 
            statusPtr, 
            MPLS_STATUSFIXLEN);

   return (MPLS_STATUSFIXLEN + MPLS_TLVFIXLEN);

} /* End: Mpls_encodeLdpStatus */


/* 
 * decode:
 */ 
int Mpls_decodeLdpStatus 
(
   mplsLdpStatusTlv_t * status, 
   u_char             * buff, 
   int                  bufSize
)
{
   u_char * statusPtr;

   if (MPLS_STATUSFIXLEN> bufSize)
   {
      /* not enough data for Status*/
      PRINT_ERR("failed decoding Status\n");
      return MPLS_DEC_BUFFTOOSMALL; 
   }

   statusPtr  = (u_char *)status;
   statusPtr  += MPLS_TLVFIXLEN; /* we want to point to the flags since the
				 tlv was decoded before we reach here */

   /* 
    *  decode for the rest of the Status
    */
   MEM_COPY( statusPtr, 
	     buff, 
             MPLS_STATUSFIXLEN);

   status->flags.mark = ntohl(status->flags.mark);
   status->msgId      = ntohl(status->msgId);
   status->msgType    = ntohs(status->msgType);
   
   return MPLS_STATUSFIXLEN;

} /* End: Mpls_decodeLdpStatus */


/*
 *      Encode-decode for Extended Status TLV 
 */


/* 
 * encode:
 */ 
int Mpls_encodeLdpExStatus 
(
   mplsLdpExStatusTlv_t * status, 
   u_char               * buff, 
   int                    bufSize
)
{
   u_int    encodedSize = 0;
   u_char * tempBuf     = buff; /* no change for the buff ptr */

   if (MPLS_EXSTATUSLEN + MPLS_TLVFIXLEN > bufSize)
   {
      /* not enough room */
      return MPLS_ENC_BUFFTOOSMALL; 
   }

   /* 
    *  encode for tlv 
    */
   encodedSize = Mpls_encodeLdpTlv( &(status->baseTlv),
                                    tempBuf, 
				    bufSize); 
   if (encodedSize < 0)
   {
      PRINT_ERR("failed encoding the tlv in EX_STATUS\n");
      return MPLS_ENC_TLVERROR;
   }
   tempBuf   += encodedSize; 

   status->value = htonl(status->value);

   MEM_COPY(tempBuf, 
	    (u_char *)&(status->value),
            sizeof(u_int));

   return (MPLS_EXSTATUSLEN + MPLS_TLVFIXLEN);

} /* End: Mpls_encodeLdpExStatus */


/* 
 * decode:
 */ 
int Mpls_decodeLdpExStatus 
(
   mplsLdpExStatusTlv_t * status, 
   u_char               * buff, 
   int                    bufSize
)
{
   if (MPLS_EXSTATUSLEN > bufSize)
   {
      /* not enough data for ExStatus*/
      PRINT_ERR("failed decoding ExStatus\n");
      return MPLS_DEC_BUFFTOOSMALL; 
   }

   /* 
    *  decode for the rest of the Status
    */
   MEM_COPY( &(status->value), 
	     buff, 
             MPLS_EXSTATUSLEN);

   status->value = ntohl(status->value);

   return MPLS_EXSTATUSLEN;

} /* End: Mpls_decodeLdpExStatus */




/*
 *      Encode-decode for Return PDU TLV
 */

/* 
 * encode:
 */ 
int Mpls_encodeLdpRetPdu 
(
   mplsLdpRetPduTlv_t   * retPdu, 
   u_char               * buff, 
   int                    bufSize
)
{
   u_int    encodedSize = 0;
   u_char * tempBuf     = buff; /* no change for the buff ptr */
   u_short  tempLength;         /* to store the tlv length for
				   later use */

   if ( MPLS_TLVFIXLEN + (int)(retPdu->baseTlv.length) > bufSize )
   {
      /* not enough room */
      return MPLS_ENC_BUFFTOOSMALL; 
   }

   tempLength = retPdu->baseTlv.length;
   /* 
    *  encode for tlv 
    */
   encodedSize = Mpls_encodeLdpTlv( &(retPdu->baseTlv),
                                    tempBuf, 
				    bufSize); 
   if (encodedSize < 0)
   {
      PRINT_ERR("failed encoding the tlv in RET_PDU\n");
      return MPLS_ENC_TLVERROR;
   }
   tempBuf += encodedSize; 

   /* 
    *  encode the data of the ret pdu
    */

   encodedSize = Mpls_encodeLdpMsgHeader( &(retPdu->headerTlv),
					  tempBuf,
					  bufSize-encodedSize);
   if (encodedSize < 0)
   {
      PRINT_ERR("failed encoding the header Tlv in RET_PDU\n");
      return MPLS_ENC_HDRTLVERROR;
   }
   tempBuf += encodedSize; 

   MEM_COPY(tempBuf, 
            retPdu->data, 
            tempLength);

   return (MPLS_TLVFIXLEN + tempLength);

} /* End: Mpls_encodeLdpRetPdu */


/* 
 * decode:
 */ 
int Mpls_decodeLdpRetPdu 
(
   mplsLdpRetPduTlv_t   * retPdu, 
   u_char               * buff, 
   int                    bufSize,
   u_short                tlvLength
)
{
   u_char * tempBuf = buff; /* no change for the buff ptr */
   u_int    decodedSize;

   if ((int)tlvLength > bufSize)
   {
      /* not enough data for ExStatus*/
      PRINT_ERR("failed decoding Ret pdu\n");
      return MPLS_DEC_BUFFTOOSMALL; 
   }

   /* 
    *  decode data for ret pdu
    */
   decodedSize = Mpls_decodeLdpMsgHeader( &(retPdu->headerTlv),
					  tempBuf,
					  bufSize);
   if (decodedSize < 0)
   {
      PRINT_ERR("failed decoding the header Tlv in RET_PDU\n");
      return MPLS_DEC_HDRTLVERROR;
   }
   tempBuf += decodedSize; 

   MEM_COPY( retPdu->data, 
	     tempBuf, 
             tlvLength);

   return tlvLength;

} /* End: Mpls_decodeLdpRetPdu */



/*
 *      Encode-decode for Return Msg TLV 
 */

/* 
 * encode:
 */ 
int Mpls_encodeLdpRetMsg 
(
   mplsLdpRetMsgTlv_t   * retMsg, 
   u_char               * buff, 
   int                    bufSize
)
{
   u_char * retMsgPtr;
   u_int    encodedSize = 0;
   u_char * tempBuf     = buff; /* no change for the buff ptr */
   u_short  tempLength;         /* to store the tlv length for
				   later use */

   if ( MPLS_TLVFIXLEN + (int)(retMsg->baseTlv.length) > bufSize )
   {
      /* not enough room */
      return MPLS_ENC_BUFFTOOSMALL; 
   }

   tempLength = retMsg->baseTlv.length;
   /* 
    *  encode for tlv 
    */
   encodedSize = Mpls_encodeLdpTlv( &(retMsg->baseTlv),
                                    tempBuf, 
				    bufSize); 
   if (encodedSize < 0)
   {
      PRINT_ERR("failed encoding the tlv in RET_MSG\n");
      return MPLS_ENC_TLVERROR;
   }

   retMsgPtr = (u_char *)retMsg;
   tempBuf   += encodedSize; 
   retMsgPtr += encodedSize;

   /* 
    *  encode the data of the ret pdu
    */

   retMsg->msgType   = htons(retMsg->msgType);
   retMsg->msgLength = htons(retMsg->msgLength);

   MEM_COPY(tempBuf, 
            retMsgPtr, 
            tempLength);

   return (MPLS_TLVFIXLEN + tempLength);

} /* End: Mpls_encodeLdpRetMsg */


/* 
 * decode:
 */ 
int Mpls_decodeLdpRetMsg 
(
   mplsLdpRetMsgTlv_t  * retMsg, 
   u_char              * buff, 
   int                   bufSize,
   u_short               tlvLength
)
{
   u_char * tempBuf     = buff; /* no change for the buff ptr */
   u_char * retMsgPtr;

   if ((int)tlvLength > bufSize)
   {
      /* not enough data for ExStatus*/
      PRINT_ERR("failed decoding Ret msg\n");
      return MPLS_DEC_BUFFTOOSMALL; 
   }
   retMsgPtr = (u_char *)retMsg;
   retMsgPtr += MPLS_TLVFIXLEN;

   /* 
    *  decode data for ret msg 
    */
   MEM_COPY( retMsgPtr, 
	     tempBuf, 
             tlvLength);

   retMsg->msgType   = ntohs(retMsg->msgType);
   retMsg->msgLength = ntohs(retMsg->msgLength);

   return tlvLength;

} /* End: Mpls_decodeLdpRetMsg */


/*
 *      Encode-decode for HELLO msg 
 */

/* 
 * encode for HELLO message 
 */ 
int Mpls_encodeLdpHelloMsg
(
   mplsLdpHelloMsg_t * helloMsg, 
   u_char            * buff, 
   int                 bufSize
)
{
   mplsLdpHelloMsg_t helloMsgCopy;
   u_int             encodedSize = 0;
   u_int             totalSize   = 0; 
   u_char *          tempBuf     = buff; /* no change for the buff ptr */

   /* check the length of the messageId + mandatory param +
      optional param */
   if ((int)(helloMsg->baseMsg.msgLength) + MPLS_TLVFIXLEN > bufSize) 
   {
      PRINT_ERR("failed to encode the hello msg: BUFFER TOO SMALL\n");
      return MPLS_ENC_BUFFTOOSMALL;
   }

   helloMsgCopy = *helloMsg;

   /*
    *  encode the base part of the pdu message
    */
   encodedSize = Mpls_encodeLdpBaseMsg( &(helloMsgCopy.baseMsg), 
				        tempBuf, 
				        bufSize);
   if (encodedSize < 0)
   {
      return MPLS_ENC_BASEMSGERROR;
   }
   PRINT_2("Encode BaseMsg for hello on %d bytes\n", encodedSize);
   tempBuf   += encodedSize;
   totalSize += encodedSize;

   
   /*
    *  encode the status tlv if any 
    */
   if (helloMsgCopy.chpTlvExists)
   {
      encodedSize = Mpls_encodeLdpChp( &(helloMsgCopy.chp), 
				         tempBuf, 
				         bufSize-totalSize);
      if (encodedSize < 0)
      {
	 return MPLS_ENC_CHPERROR;
      }
      PRINT_2("Encoded for CHP %d bytes\n", encodedSize);
      tempBuf   += encodedSize;
      totalSize += encodedSize;
   }
   if (helloMsgCopy.trAdrTlvExists)
   {
      encodedSize = Mpls_encodeLdpTrAdr( &(helloMsgCopy.trAdr), 
				         tempBuf, 
				         bufSize-totalSize);
      if (encodedSize < 0)
      {
	 return MPLS_ENC_TRADRERROR;
      }
      PRINT_2("Encoded for TR ADDR %d bytes\n", encodedSize);
      tempBuf   += encodedSize;
      totalSize += encodedSize;
   }
   if (helloMsgCopy.csnTlvExists)
   {
      encodedSize = Mpls_encodeLdpCsn( &(helloMsgCopy.csn), 
				         tempBuf, 
				         bufSize-totalSize);
      if (encodedSize < 0)
      {
	 return MPLS_ENC_CSNERROR;
      }
      PRINT_2("Encoded for CSN %d bytes\n", encodedSize);
      tempBuf   += encodedSize;
      totalSize += encodedSize;
   }

   return totalSize;

} /* End: Mpls_encodeLdpHelloMsg */


/* 
 * decode for HELLO message 
 */ 
int Mpls_decodeLdpHelloMsg
(
   mplsLdpHelloMsg_t * helloMsg, 
   u_char            * buff, 
   int                 bufSize
)
{
   u_int        decodedSize = 0;
   u_int        totalSize   = 0;
   u_int        stopLength  = 0;
   u_int        totalSizeParam = 0;
   u_char *     tempBuf     = buff; /* no change for the buff ptr */
   mplsLdpTlv_t tlvTemp;

   /*
    *  decode the base part of the pdu message
    */
   bzero(helloMsg, sizeof(mplsLdpHelloMsg_t));
   decodedSize = Mpls_decodeLdpBaseMsg( &(helloMsg->baseMsg), 
				        tempBuf, 
				        bufSize);
   if (decodedSize < 0)
   {
      return MPLS_DEC_BASEMSGERROR;
   }
   PRINT_2("Decode BaseMsg for hello on %d bytes\n", decodedSize);

   if (helloMsg->baseMsg.flags.flags.msgType != MPLS_HELLO_MSGTYPE)
   {
      PRINT_ERR_2("Not the right message type; expected hello and got %x\n",
		   helloMsg->baseMsg.flags.flags.msgType);
      return MPLS_MSGTYPEERROR;
   }

   tempBuf   += decodedSize;
   totalSize += decodedSize;

   if (bufSize-totalSize <= 0)
   {
      /* nothing left for decoding */
      PRINT_ERR("Hello msg does not have anything beside base msg\n");
      return totalSize;
   }

   PRINT_4("bufSize = %d,  totalSize = %d, helloMsg->baseMsg.msgLength = %d\n",
	   bufSize, totalSize, helloMsg->baseMsg.msgLength);

   /* Have to check the baseMsg.msgLength to know when to finish.
    * We finsh when the totalSizeParam is >= to the base message length - the
    * message id length (4) 
    */
    
   stopLength = helloMsg->baseMsg.msgLength - MPLS_MSGIDFIXLEN;
   while (stopLength > totalSizeParam)
   {
      /*
       *  decode the tlv to check what's next
       */
      bzero(&tlvTemp, MPLS_TLVFIXLEN);
      decodedSize = Mpls_decodeLdpTlv(&tlvTemp, tempBuf, bufSize-totalSize);
      if (decodedSize < 0)
      {
         /* something wrong */
         PRINT_ERR("NOT msg decode failed for tlv\n");
         return MPLS_DEC_TLVERROR;
      }

      tempBuf        += decodedSize;
      totalSize      += decodedSize;
      totalSizeParam += decodedSize;

      switch (tlvTemp.flags.flags.type)
      {
         case MPLS_CHP_TLVTYPE:
         {
              decodedSize = Mpls_decodeLdpChp( &(helloMsg->chp), 
				               tempBuf, 
				               bufSize-totalSize);
              if (decodedSize < 0)
              {
		 PRINT_ERR("Failure when decoding Chp from hello msg\n");
	         return MPLS_DEC_CHPERROR;
              }
	      PRINT_2("Decoded for CHP %d bytes\n", decodedSize);
              tempBuf        += decodedSize;
              totalSize      += decodedSize;
              totalSizeParam += decodedSize;

	      helloMsg->chpTlvExists = 1;
	      helloMsg->chp.baseTlv  = tlvTemp;
	      DEBUG_CALL(printChpTlv(&(helloMsg->chp)));
	      break;
	 }
         case MPLS_TRADR_TLVTYPE:
	 {
            decodedSize = Mpls_decodeLdpTrAdr( &(helloMsg->trAdr), 
				               tempBuf, 
				               bufSize-totalSize);
            if (decodedSize < 0)
            {
	       PRINT_ERR("Failure when decoding TrAdr from hello msg\n");
	       return MPLS_DEC_TRADRERROR;
            }
	    PRINT_2("Decoded for TrAdr %d bytes\n", decodedSize);
            tempBuf        += decodedSize;
            totalSize      += decodedSize;
            totalSizeParam += decodedSize;

	    helloMsg->trAdrTlvExists = 1;
	    helloMsg->trAdr.baseTlv  = tlvTemp;
	    DEBUG_CALL(printTrAdrTlv(&(helloMsg->trAdr)));
	    break;
	 }
         case MPLS_CSN_TLVTYPE:
	 {
            decodedSize = Mpls_decodeLdpCsn( &(helloMsg->csn), 
				             tempBuf, 
				             bufSize-totalSize);
            if (decodedSize < 0)
            {
	       PRINT_ERR("Failure when decoding Csn from hello msg\n");
	       return MPLS_DEC_CSNERROR;
            }
	    PRINT_2("Decoded for CSN %d bytes\n", decodedSize);
            tempBuf        += decodedSize;
            totalSize      += decodedSize;
            totalSizeParam += decodedSize;

	    helloMsg->csnTlvExists = 1;
	    helloMsg->csn.baseTlv  = tlvTemp;
	    DEBUG_CALL(printCsnTlv(&(helloMsg->csn)));
	    break;
	 }
         default:
	 {
	    PRINT_ERR_2("Found wrong tlv type while decoding hello msg (%d)\n",
	             tlvTemp.flags.flags.type);
	    if (tlvTemp.flags.flags.uBit == 1)
	    {
	       /* ignore the Tlv and continue processing */
               tempBuf        += tlvTemp.length;
               totalSize      += tlvTemp.length;
               totalSizeParam += tlvTemp.length;
               break;
	    }
	    else
	    {
	       /* drop the message; return error */
	       return MPLS_TLVTYPEERROR;
	    }
	 }
      } /* switch type */

   } /* while */

   PRINT_2("totalsize for Mpls_decodeLdpHelloMsg is %d\n", totalSize);

   return totalSize;

} /* End: Mpls_decodeLdpHelloMsg */




/* 
 * Encode for Common Hello Parameters TLV
 */ 

/*
 *  encode
 */
int Mpls_encodeLdpChp 
(
   mplsLdpChpTlv_t * chp, 
   u_char          * buff, 
   int               bufSize
)
{
   int      encodedSize = 0;
   u_char * tempBuf     = buff; /* no change for the buff ptr */
   u_char * chpPtr;

   /* get the size of the chp to be encoded and check it against
      the buffer size */

   if (MPLS_TLVFIXLEN + MPLS_CHPFIXLEN > bufSize)
   {
      /* not enough room */
      return MPLS_ENC_BUFFTOOSMALL; 
   }

   /* 
    *  encode for tlv 
    */
   encodedSize = Mpls_encodeLdpTlv( &(chp->baseTlv),
                                    tempBuf, 
				    MPLS_TLVFIXLEN);
   if (encodedSize < 0)
   {
      return MPLS_ENC_TLVERROR;
   }

   chpPtr  = (u_char *)chp;
   tempBuf += encodedSize; 
   chpPtr  += encodedSize;

   /* 
    *  encode for hold time + T +  R + res 
    */
   chp->flags.flags.res = 0; 
   chp->flags.mark      = htons(chp->flags.mark);
   chp->holdTime        = htons(chp->holdTime);

   MEM_COPY( tempBuf, 
	     chpPtr,
             MPLS_CHPFIXLEN);

   return (MPLS_TLVFIXLEN + MPLS_CHPFIXLEN);

} /* End: Mpls_encodeLdpChp */


/* 
 * decode
 */ 
int Mpls_decodeLdpChp 
(
   mplsLdpChpTlv_t * chp, 
   u_char          * buff, 
   int               bufSize
)
{
   u_char * chpPtr;
 
   if (MPLS_CHPFIXLEN > bufSize)
   {
      /* not enough data for Chp */
      PRINT_ERR("failed decoding hello Chp\n");
      return MPLS_DEC_BUFFTOOSMALL;
   }
 
   chpPtr = (u_char *)chp;
   chpPtr += MPLS_TLVFIXLEN; /* we want to point to the flags since the
                                 tlv was decoded before we reach here */
 
   /*
    *  decode for the rest of the Chp
    */
   MEM_COPY( chpPtr,
             buff,
             MPLS_CHPFIXLEN);
 
   chp->holdTime   = ntohs(chp->holdTime);
   chp->flags.mark = ntohs(chp->flags.mark);
 
   return MPLS_CHPFIXLEN;

} /* End: Mpls_decodeLdpChp */


/* 
 * Encode for Configuration Sequence Number TLV
 */ 

/*
 *  encode
 */
int Mpls_encodeLdpCsn 
(
   mplsLdpCsnTlv_t * csn, 
   u_char          * buff, 
   int               bufSize
)
{
   int      encodedSize = 0;
   u_char * tempBuf     = buff; /* no change for the buff ptr */
 
   if (MPLS_CSNFIXLEN + MPLS_TLVFIXLEN > bufSize)
   {
      /* not enough room */
      return MPLS_ENC_BUFFTOOSMALL;
   }
 
   /*
    *  encode for tlv
    */
   encodedSize = Mpls_encodeLdpTlv( &(csn->baseTlv),
                                    tempBuf,
                                    bufSize);
   if (encodedSize < 0)
   {
      PRINT_ERR("failed encoding the tlv in hello Csn\n");
      return MPLS_ENC_TLVERROR;
   }
   tempBuf += encodedSize;
 
   csn->seqNumber = htonl(csn->seqNumber);
 
   MEM_COPY(tempBuf,
            (u_char *)&(csn->seqNumber),
            MPLS_CSNFIXLEN);
 
   return (MPLS_CSNFIXLEN + MPLS_TLVFIXLEN);

} /* End: Mpls_encodeLdpCsn */

/* 
 * decode
 */ 
int Mpls_decodeLdpCsn 
(
   mplsLdpCsnTlv_t * csn, 
   u_char          * buff, 
   int               bufSize
)
{
   u_char * tempBuf = buff; /* no change for the buff ptr */
 
   if (MPLS_CSNFIXLEN > bufSize)
   {
      /* not enough data for csn data*/
      PRINT_ERR("failed decoding hello Csn\n");
      return MPLS_DEC_BUFFTOOSMALL;
   }
 
   /*
    *  decode for the rest of the Csn 
    */
   MEM_COPY( &(csn->seqNumber),
             tempBuf,
             MPLS_CSNFIXLEN);
 
   csn->seqNumber = ntohl(csn->seqNumber);
 
   return MPLS_CSNFIXLEN;

} /* End: Mpls_decodeLdpCsn */


/* 
 * Encode for Transport Address TLV 
 */ 

/*
 *  encode
 */
int Mpls_encodeLdpTrAdr 
(
   mplsLdpTrAdrTlv_t * trAdr, 
   u_char            * buff, 
   int                 bufSize
)
{
   int      encodedSize = 0;
   u_char * tempBuf     = buff; /* no change for the buff ptr */
 
   if (MPLS_TRADRFIXLEN + MPLS_TLVFIXLEN > bufSize)
   {
      /* not enough room */
      return MPLS_ENC_BUFFTOOSMALL;
   }
 
   /*
    *  encode for tlv
    */
   encodedSize = Mpls_encodeLdpTlv( &(trAdr->baseTlv),
                                    tempBuf,
                                    bufSize);
   if (encodedSize < 0)
   {
      PRINT_ERR("failed encoding the tlv in hello TrAdr\n");
      return MPLS_ENC_TLVERROR;
   }
   tempBuf += encodedSize;
 
   trAdr->address = htonl(trAdr->address);
 
   MEM_COPY(tempBuf,
            (u_char *)&(trAdr->address),
            MPLS_TRADRFIXLEN);
 
   return (MPLS_TRADRFIXLEN + MPLS_TLVFIXLEN);

} /* End: Mpls_encodeLdpTrAdr */

/* 
 * decode 
 */ 
int Mpls_decodeLdpTrAdr 
(
   mplsLdpTrAdrTlv_t * trAdr, 
   u_char            * buff, 
   int                 bufSize
)
{
   u_char * tempBuf     = buff; /* no change for the buff ptr */
 
   if (MPLS_TRADRFIXLEN > bufSize)
   {
      /* not enough data for csn data*/
      PRINT_ERR("failed decoding hello TrAdr\n");
      return MPLS_DEC_BUFFTOOSMALL;
   }
 
   /*
    *  decode for the rest of the TrAdr 
    */
   MEM_COPY( &(trAdr->address),
             tempBuf,
             MPLS_TRADRFIXLEN);
 
   trAdr->address = ntohl(trAdr->address);
 
   return MPLS_TRADRFIXLEN;

} /* End: Mpls_decodeLdpTrAdr*/


/* 
 * Encode for KeepAlive Message
 */ 

/*
 *  encode
 */
int Mpls_encodeLdpKeepAliveMsg 
(
   mplsLdpKeepAlMsg_t *  keepAlive, 
   u_char             * buff, 
   int                  bufSize
)
{
   mplsLdpKeepAlMsg_t keepAliveCopy;
   u_int              encodedSize = 0;

   if (MPLS_MSGIDFIXLEN + MPLS_TLVFIXLEN > bufSize) 
   {
      PRINT_ERR("failed to encode the keep alive msg: BUFFER TOO SMALL\n");
      return MPLS_ENC_BUFFTOOSMALL;
   }

   keepAliveCopy = *keepAlive;

   encodedSize = Mpls_encodeLdpBaseMsg( &(keepAliveCopy.baseMsg), 
				        buff, 
				        bufSize);
   if (encodedSize < 0)
   {
      return MPLS_ENC_BASEMSGERROR;
   }
   PRINT_2("Encode BaseMsg for keep alive on %d bytes\n", encodedSize);

   return (MPLS_MSGIDFIXLEN + MPLS_TLVFIXLEN);

} /* End: Mpls_encodeLdpKeepAliveMsg */


/*
 *  decode
 */
int Mpls_decodeLdpKeepAliveMsg 
(
   mplsLdpKeepAlMsg_t * keepAlive, 
   u_char             * buff, 
   int                  bufSize
)
{
   int decodedSize = 0;

   bzero(keepAlive, MPLS_MSGIDFIXLEN + MPLS_TLVFIXLEN);
   decodedSize = Mpls_decodeLdpBaseMsg( &(keepAlive->baseMsg), 
				        buff, 
				        bufSize);
   if (decodedSize < 0)
   {
      return MPLS_DEC_BASEMSGERROR;
   }
   PRINT_2("Decode BaseMsg for keep alive on %d bytes\n", decodedSize);

   if (keepAlive->baseMsg.flags.flags.msgType != MPLS_KEEPAL_MSGTYPE)
   {
      PRINT_ERR_2("Not the right message type; expected keep alive and got %x\n",
		   keepAlive->baseMsg.flags.flags.msgType);
      return MPLS_MSGTYPEERROR;
   }

   return (MPLS_MSGIDFIXLEN + MPLS_TLVFIXLEN);

} /* End: Mpls_decodeLdpKeepAliveMsg */



/* 
 * Encode for Address List TLV 
 */ 

/*
 *  encode
 */
int Mpls_encodeLdpAdrTlv 
(
   mplsLdpAdrTlv_t * adrList, 
   u_char          * buff, 
   int               bufSize
)
{
   int      i, numberAdr; 
   int      encodedSize = 0;
   u_char * tempBuf     = buff; /* no change for the buff ptr */
   u_short  tempLength;         /* to store the tlv length for
				   later use */
 
   if (MPLS_TLVFIXLEN + (int)(adrList->baseTlv.length) > bufSize)
   {
      /* not enough room */
      return MPLS_ENC_BUFFTOOSMALL;
   }
 
   tempLength = adrList->baseTlv.length;
   /*
    *  encode for tlv
    */
   encodedSize = Mpls_encodeLdpTlv( &(adrList->baseTlv),
                                    tempBuf,
                                    bufSize);
   if (encodedSize < 0)
   {
      PRINT_ERR("failed encoding the tlv in AdrList\n");
      return MPLS_ENC_TLVERROR;
   }

   tempBuf += encodedSize;
 
   adrList->addrFamily = htons(adrList->addrFamily);
 
   numberAdr = (tempLength - sizeof(u_short)) / sizeof(u_int);
   for (i = 0; i < numberAdr; i++)
   {
      adrList->address[i] = htonl(adrList->address[i]);
   }

   MEM_COPY(tempBuf,
            (u_char *)&adrList->addrFamily,
            sizeof(u_short));

   tempBuf += sizeof(u_short);

   MEM_COPY(tempBuf,
            (u_char *)&adrList->address,
            tempLength - sizeof(u_short));
 
   return (tempLength + MPLS_TLVFIXLEN);

} /* End: Mpls_encodeLdpAdrTlv */

/*
 *  decode
 *
 *  Note: the tlvLength is used to specify what is the length of the 
 *        encoding in the AdrTlv.
 */
int Mpls_decodeLdpAdrTlv 
(
   mplsLdpAdrTlv_t * adrList, 
   u_char          * buff, 
   int             bufSize,
   u_short         tlvLength
)
{
   u_char * tempBuf = buff; /* no change for the buff ptr */
   int      i, numberAdr; 
 
   if ((int)tlvLength > bufSize)
   {
      /* not enough data for Adr list tlv*/
      PRINT_ERR("failed decoding AddrList tlv\n");
      return MPLS_DEC_BUFFTOOSMALL;
   }
 
   /*
    *  decode for the addressFamily and addresses of the address list
    */
   MEM_COPY( (u_char *)&adrList->addrFamily,
             tempBuf,
             sizeof(u_short));
   tempBuf += sizeof(u_short);

   adrList->addrFamily = ntohs(adrList->addrFamily);

   MEM_COPY( (u_char *)&adrList->address,
             tempBuf,
             tlvLength - sizeof(u_short));

   numberAdr = (tlvLength - sizeof(u_short)) / sizeof(u_int);
   for (i = 0; i < numberAdr; i++)
   {
      adrList->address[i] = ntohl(adrList->address[i]);
   }

   return tlvLength;

} /* End: Mpls_decodeLdpAdrTlv */



/* 
 * Encode for Address / Address Withdraw messages
 */ 

/*
 *  encode
 */
int Mpls_encodeLdpAdrMsg 
(
   mplsLdpAdrMsg_t * addrMsg, 
   u_char          * buff, 
   int               bufSize
)
{
   mplsLdpAdrMsg_t addrMsgCopy;
   int             encodedSize = 0;
   u_int           totalSize   = 0; 
   u_char *        tempBuf     = buff; /* no change for the buff ptr */

   /* check the length of the messageId + param */
   if ((int)(addrMsg->baseMsg.msgLength) + MPLS_TLVFIXLEN > bufSize) 
   {
      PRINT_ERR("failed to encode the address msg: BUFFER TOO SMALL\n");
      return MPLS_ENC_BUFFTOOSMALL;
   }

   addrMsgCopy  = *addrMsg;

   /*
    *  encode the base part of the pdu message
    */
   encodedSize = Mpls_encodeLdpBaseMsg( &(addrMsgCopy.baseMsg), 
				        tempBuf, 
				        bufSize);
   if (encodedSize < 0)
   {
      return MPLS_ENC_BASEMSGERROR;
   }
   PRINT_2("Encode BaseMsg for address on %d bytes\n", encodedSize);
   tempBuf   += encodedSize;
   totalSize += encodedSize;

   /*
    *  encode the address list tlv if any
    */
   if (addrMsg->adrListTlvExists)
   {
      encodedSize = Mpls_encodeLdpAdrTlv( &(addrMsgCopy.addressList), 
				          tempBuf, 
				          bufSize-totalSize);
      if (encodedSize < 0)
      {
         return MPLS_ENC_ADRLISTERROR;
      }
      PRINT_2("Encoded for AddressList Tlv %d bytes\n", encodedSize);
   }

   return (addrMsg->baseMsg.msgLength + MPLS_TLVFIXLEN);
   
} /* End: */

/*
 *  decode
 */
int Mpls_decodeLdpAdrMsg 
(
   mplsLdpAdrMsg_t * addrMsg, 
   u_char          * buff, 
   int               bufSize
)
{
   int          decodedSize    = 0;
   u_int        totalSize      = 0;
   u_int        stopLength     = 0;
   u_int        totalSizeParam = 0;
   u_char *     tempBuf        = buff; /* no change for the buff ptr */
   mplsLdpTlv_t tlvTemp;

   /*
    *  decode the base part of the pdu message
    */
   bzero(addrMsg, sizeof(mplsLdpAdrMsg_t));
   decodedSize = Mpls_decodeLdpBaseMsg( &(addrMsg->baseMsg), 
				        tempBuf, 
				        bufSize);
   if (decodedSize < 0)
   {
      return MPLS_DEC_BASEMSGERROR;
   }
   PRINT_2("Decode BaseMsg for address msg on %d bytes\n", decodedSize);

   if ((addrMsg->baseMsg.flags.flags.msgType != MPLS_ADDR_MSGTYPE) && 
       (addrMsg->baseMsg.flags.flags.msgType != MPLS_ADDRWITH_MSGTYPE))
   {
      PRINT_ERR_2("Not the right message type; expected adr and got %x\n",
		   addrMsg->baseMsg.flags.flags.msgType);
      return MPLS_MSGTYPEERROR;
   }

   tempBuf   += decodedSize;
   totalSize += decodedSize;

   if (bufSize-totalSize <= 0)
   {
      /* nothing left for decoding */
      PRINT_ERR("Adr msg does not have anything beside base msg\n");
      return totalSize;
   }

   PRINT_4("bufSize = %d,  totalSize = %d, addrMsg->baseMsg.msgLength = %d\n",
	   bufSize, totalSize, addrMsg->baseMsg.msgLength);

   /* Have to check the baseMsg.msgLength to know when to finish.
    * We finsh when the totalSizeParam is >= to the base message length - the
    * message id length (4) 
    */
    
   stopLength = addrMsg->baseMsg.msgLength - MPLS_MSGIDFIXLEN;
   while (stopLength > totalSizeParam)
   {
      /*
       *  decode the tlv to check what's next
       */
      bzero(&tlvTemp, MPLS_TLVFIXLEN);
      decodedSize = Mpls_decodeLdpTlv(&tlvTemp, tempBuf, bufSize-totalSize);
      if (decodedSize < 0)
      {
         /* something wrong */
         PRINT_ERR("ADR msg decode failed for tlv\n");
         return MPLS_DEC_TLVERROR;
      }

      tempBuf        += decodedSize;
      totalSize      += decodedSize;
      totalSizeParam += decodedSize;

      switch (tlvTemp.flags.flags.type)
      {
         case MPLS_ADDRLIST_TLVTYPE:
         {
            decodedSize = Mpls_decodeLdpAdrTlv( &(addrMsg->addressList), 
				                tempBuf, 
				                bufSize-totalSize,
						tlvTemp.length);
            if (decodedSize < 0)
            {
	       PRINT_ERR("Failure when decoding AdrList tlv from adr msg\n");
	       return MPLS_DEC_ADRLISTERROR;
            }
	    PRINT_2("Decoded for ADRLIST %d bytes\n", decodedSize);
            tempBuf        += decodedSize;
            totalSize      += decodedSize;
            totalSizeParam += decodedSize;

	    addrMsg->adrListTlvExists    = 1;
	    addrMsg->addressList.baseTlv = tlvTemp;
	    DEBUG_CALL(printAdrListTlv(&(addrMsg->addressList)));
	    break;
	}
        default:
	{
	   PRINT_ERR_2("Found wrong tlv type while decoding adr msg (%x)\n",
	             tlvTemp.flags.flags.type);
	   if (tlvTemp.flags.flags.uBit == 1)
	   {
	      /* ignore the Tlv and continue processing */
              tempBuf        += tlvTemp.length;
              totalSize      += tlvTemp.length;
              totalSizeParam += tlvTemp.length;
              break;
	   }
	   else
	   {
	      /* drop the message; return error */
	      return MPLS_TLVTYPEERROR;
	   }
	}
      } /* switch type */

   } /* while */

   PRINT_2("totalsize for Mpls_decodeLdpAdrMsg is %d\n", totalSize);

   return totalSize;
   
} /* End: Mpls_decodeLdpAdrMsg*/



/* 
 * Encode for FEC ELEMENT 
 */ 

/*
 *  encode
 */
int Mpls_encodeLdpFecAdrEl 
(
   mplsFecElement_t * fecAdrEl,
   u_char           * buff, 
   int                bufSize,
   u_char             type
)
{
   int      encodedSize = 0;
   u_char * tempBuf     = buff;
 
   switch (type)
   {
      case MPLS_WC_FEC:
      {
	 if (MPLS_FEC_ELEMTYPELEN > bufSize)
	 {
	    return MPLS_ENC_BUFFTOOSMALL;
	 }
         *buff  = fecAdrEl->wildcardEl.type;
	 encodedSize = MPLS_FEC_ELEMTYPELEN;
	 break;
      }
      case MPLS_PREFIX_FEC:
      {
         fecAdrEl->addressEl.addressFam = htons(fecAdrEl->addressEl.addressFam);
         fecAdrEl->addressEl.address    = htonl(fecAdrEl->addressEl.address);

	 encodedSize = MPLS_FEC_ADRFAMLEN + MPLS_FEC_ELEMTYPELEN +
		       MPLS_FEC_PRELENLEN + 
		       (int)(fecAdrEl->addressEl.preLen / 8) +
		       ((int)(fecAdrEl->addressEl.preLen % 8) > 0 ? 1 : 0);

	 if (encodedSize > bufSize)
	 {
	    return MPLS_ENC_BUFFTOOSMALL;
	 }
         *tempBuf = fecAdrEl->addressEl.type;
         tempBuf++; /* for MPLS_FEC_ELEMTYPELEN */

         MEM_COPY( tempBuf,
                   (u_char*)&(fecAdrEl->addressEl.addressFam),
                   MPLS_FEC_ADRFAMLEN);
	 tempBuf += MPLS_FEC_ADRFAMLEN;

         *tempBuf = fecAdrEl->addressEl.preLen;
         tempBuf++; /* for MPLS_FEC_PRELENLEN */

         MEM_COPY( tempBuf,
                   (u_char*)&(fecAdrEl->addressEl.address),
                   (int)(fecAdrEl->addressEl.preLen / 8) +
		   ((int)(fecAdrEl->addressEl.preLen % 8) > 0 ? 1 : 0));
	 break;
      }
      case MPLS_HOSTADR_FEC:
      {
         fecAdrEl->addressEl.addressFam = htons(fecAdrEl->addressEl.addressFam);
         fecAdrEl->addressEl.address    = htonl(fecAdrEl->addressEl.address);

	 encodedSize = MPLS_FEC_ADRFAMLEN + MPLS_FEC_ELEMTYPELEN +
		       MPLS_FEC_PRELENLEN + fecAdrEl->addressEl.preLen;

	 if (encodedSize > bufSize)
	 {
	    return MPLS_ENC_BUFFTOOSMALL;
	 }
         *tempBuf = fecAdrEl->addressEl.type;
         tempBuf++; /* for MPLS_FEC_ELEMTYPELEN */

         MEM_COPY( tempBuf,
                   (u_char*)&(fecAdrEl->addressEl.addressFam),
                   MPLS_FEC_ADRFAMLEN);
	 tempBuf += MPLS_FEC_ADRFAMLEN;

         *tempBuf = fecAdrEl->addressEl.preLen;
         tempBuf++; /* for MPLS_FEC_PRELENLEN */

         MEM_COPY( tempBuf,
                   (u_char*)&(fecAdrEl->addressEl.address),
                   fecAdrEl->addressEl.preLen);
	 break;
      }
      case MPLS_CRLSP_FEC:
      {
	 if (MPLS_FEC_CRLSPLEN > bufSize)
	 {
	    return MPLS_ENC_BUFFTOOSMALL;
	 }
	 fecAdrEl->crlspEl.res1 = 0;
	 fecAdrEl->crlspEl.res2 = 0;
         MEM_COPY( tempBuf,
                   (u_char*)&(fecAdrEl->crlspEl),
                   MPLS_FEC_CRLSPLEN);
	 encodedSize = MPLS_FEC_CRLSPLEN;
	 break;
      }
      case MPLS_MARTINIVC_FEC:
      {
         u_char *vcHead = tempBuf;
         int encoded = 0;
         int cur = 0;

         if (MPLS_FEC_MARTINILEN > bufSize)
         {
            return MPLS_ENC_BUFFTOOSMALL;
         }

         /* skip past the first MPLS_FEC_MARTINILEN bytes, we need
          * to know the vcInfoLen before we write that info,
          * 'encoded' will keep track of the vcInfoLen */

         encodedSize = MPLS_FEC_MARTINILEN;
         tempBuf += MPLS_FEC_MARTINILEN;

         if (fecAdrEl->martiniVcEl.vcId)
         {

            fecAdrEl->martiniVcEl.vcId = htonl(fecAdrEl->martiniVcEl.vcId);

            MEM_COPY( tempBuf,
                      (u_char*)&(fecAdrEl->martiniVcEl.vcId),
                      MPLS_VCIDLEN);

            tempBuf += MPLS_VCIDLEN;
            encoded += MPLS_VCIDLEN;

            for (cur = 0; cur < fecAdrEl->martiniVcEl.vcIfParamsLen; cur++)
            {
               int valueLen = 0;
               /*
                * This is dumb, the length in vcIfParam includes the type
                * and the length, so a length of 4 means 2 bytes of value
                */

               *tempBuf = fecAdrEl->martiniVcEl.vcIfParams[cur].id;
               tempBuf++;
               *tempBuf = fecAdrEl->martiniVcEl.vcIfParams[cur].len;
               tempBuf++;

               valueLen = fecAdrEl->martiniVcEl.vcIfParams[cur].len;
               if (valueLen < MPLS_VCIFPARAM_HDR)
               {
                  /* because of this encoding we MUST have
                   * atleast a length of MPLS_VCIFPARAM_HDR */
                  return MPLS_ENC_FECERROR;
               }
               valueLen -= MPLS_VCIFPARAM_HDR;

               if (valueLen)
               {
                  MEM_COPY( tempBuf,
                            (u_char*)&(fecAdrEl->martiniVcEl.
                            vcIfParams[cur].value),
                            valueLen);
                  tempBuf += valueLen;
               }

               encoded += fecAdrEl->martiniVcEl.vcIfParams[cur].len;
            }
         }
         encodedSize += encoded;

         fecAdrEl->martiniVcEl.flags.flags.vcInfoLen = encoded;
         fecAdrEl->martiniVcEl.flags.mark =
            htonl(fecAdrEl->martiniVcEl.flags.mark);
         fecAdrEl->martiniVcEl.groupId =
            htonl(fecAdrEl->martiniVcEl.groupId);

         MEM_COPY( vcHead,
                   (u_char*)&(fecAdrEl->martiniVcEl),
                   MPLS_FEC_MARTINILEN);

         break;

      }
      default:
      {
	 PRINT_ERR_2("Found wrong FEC type while encoding FEC elem (%d)\n",
	             type);
	 return MPLS_ENC_FECELEMERROR;
	 break;
      }
   } /* end: switch */

   return encodedSize;
   
} /* End: Mpls_encodeLdpFecAdrEl*/


/*
 *  decode
 */
int Mpls_decodeLdpFecAdrEl 
(
   mplsFecElement_t * fecAdrEl,
   u_char           * buff, 
   int                bufSize,
   u_char             type
)
{
   int      decodedSize = 0;
   u_char * tempBuff    = buff;
   u_int    preLen      = 0;

   switch (type)
   {
      case MPLS_WC_FEC:
      {
	 fecAdrEl->wildcardEl.type = *buff;
	 decodedSize = MPLS_FEC_ELEMTYPELEN; 
	 break;
      }
      case MPLS_PREFIX_FEC:
      {
	 decodedSize = MPLS_FEC_ADRFAMLEN + MPLS_FEC_ELEMTYPELEN +
		       MPLS_FEC_PRELENLEN; 
	 if (decodedSize > bufSize)
	 {
	    return MPLS_DEC_BUFFTOOSMALL;
	 }
         fecAdrEl->addressEl.type = *tempBuff;
         tempBuff++; /* for MPLS_FEC_ELEMTYPELEN */

         MEM_COPY( (u_char*)&(fecAdrEl->addressEl.addressFam),
		   tempBuff,
                   MPLS_FEC_ADRFAMLEN);
         tempBuff += MPLS_FEC_ADRFAMLEN;

         fecAdrEl->addressEl.preLen = *tempBuff;
         tempBuff++; /* for MPLS_FEC_PRELENLEN */

         fecAdrEl->addressEl.addressFam = ntohs(fecAdrEl->addressEl.addressFam);

	 /* now we get the prefix; we need to use the preLen which was
	    decoded from buff */

         preLen = (int)(fecAdrEl->addressEl.preLen / 8) +
		  ((int)(fecAdrEl->addressEl.preLen % 8) > 0 ? 1 : 0);

	 if (fecAdrEl->addressEl.preLen > sizeof(u_int) * 8)
	 {
	    /* error - the length cannot exeed 32 bits */
	    /* skip the FEC and return error code */
            /* fill in the preLen field to the number of bytes for this
               fec; we need it to know how much to skip from the buffer 
               when we do the decoding for the following fec element */
            fecAdrEl->addressEl.preLen = preLen + decodedSize;
            return MPLS_FECERROR;
	 }
	 if ((int)preLen > bufSize - decodedSize)
	 {
	    return MPLS_DEC_BUFFTOOSMALL;
	 }
         MEM_COPY( (u_char *)&(fecAdrEl->addressEl.address),
		   tempBuff,
                   preLen);

         fecAdrEl->addressEl.address = ntohl(fecAdrEl->addressEl.address);
	 decodedSize += preLen;
	 break;
      }
      case MPLS_HOSTADR_FEC:
      {
	 decodedSize = MPLS_FEC_ADRFAMLEN + MPLS_FEC_ELEMTYPELEN +
		       MPLS_FEC_PRELENLEN; 
	 if (decodedSize > bufSize)
	 {
	    return MPLS_DEC_BUFFTOOSMALL;
	 }
	 fecAdrEl->addressEl.type = *tempBuff;
	 tempBuff++; /* for MPLS_FEC_ELEMTYPELEN */

         MEM_COPY( (u_char*)&(fecAdrEl->addressEl.addressFam),
		   tempBuff,
                   MPLS_FEC_ADRFAMLEN);
         tempBuff += MPLS_FEC_ADRFAMLEN;

	 fecAdrEl->addressEl.preLen = *tempBuff;
	 tempBuff++; /* for MPLS_FEC_PRELENLEN */

         fecAdrEl->addressEl.addressFam = ntohs(fecAdrEl->addressEl.addressFam);

	 /* now we get the host address; we need to use the preLen which was
	    decoded from buff */
         
	 preLen = fecAdrEl->addressEl.preLen;
	 if (fecAdrEl->addressEl.preLen > sizeof(u_int))
	 {
	    /* error - the length cannot exeed 32 bits */
	    /* skip the FEC and return error code */
            /* fill in the preLen field to the number of bytes for this
               fec; we need it to know how much to skip from the buffer 
               when we do the decoding for the following fec element */
            fecAdrEl->addressEl.preLen = preLen + decodedSize;
            return MPLS_FECERROR;
	 }
	 if ((int)preLen > bufSize - decodedSize)
	 {
	    return MPLS_DEC_BUFFTOOSMALL;
	 }
         MEM_COPY( (u_char *)&(fecAdrEl->addressEl.address),
		   tempBuff,
                   preLen);

         fecAdrEl->addressEl.address = ntohl(fecAdrEl->addressEl.address);
	 decodedSize += preLen;
	 break;
      }
      case MPLS_CRLSP_FEC:
      {
	 if (MPLS_FEC_CRLSPLEN > bufSize)
	 {
	    return MPLS_DEC_BUFFTOOSMALL;
	 }
         MEM_COPY( (u_char*)&(fecAdrEl->crlspEl),
		   tempBuff,
                   MPLS_FEC_CRLSPLEN);
	 decodedSize = MPLS_FEC_CRLSPLEN;
	 break;
      }
      case MPLS_MARTINIVC_FEC:
      {
         int decoded = 0;
         int cur = 0;

         if (MPLS_FEC_MARTINILEN > bufSize)
         {
            return MPLS_DEC_BUFFTOOSMALL;
         }
         MEM_COPY( (u_char*)&(fecAdrEl->martiniVcEl),
                   tempBuff,
                   MPLS_FEC_MARTINILEN);

         decodedSize = MPLS_FEC_MARTINILEN;
         tempBuff += MPLS_FEC_MARTINILEN;

         fecAdrEl->martiniVcEl.flags.mark =
            ntohl(fecAdrEl->martiniVcEl.flags.mark);
         fecAdrEl->martiniVcEl.groupId =
            ntohl(fecAdrEl->martiniVcEl.groupId);

         if (fecAdrEl->martiniVcEl.flags.flags.vcInfoLen)
         {
            MEM_COPY( (u_char*)&(fecAdrEl->martiniVcEl.vcId),
                      tempBuff, MPLS_VCIDLEN);
            tempBuff += MPLS_VCIDLEN;
            decoded += MPLS_VCIDLEN;

            fecAdrEl->martiniVcEl.vcId = ntohl(fecAdrEl->martiniVcEl.vcId);

            while (fecAdrEl->martiniVcEl.flags.flags.vcInfoLen > decoded)
            {
               int valueLen = 0;
               /*
                * This is dumb, the length in vcIfParam includes the type
                * and the length, so a length of 4 means 2 bytes of value
                */

               fecAdrEl->martiniVcEl.vcIfParams[cur].id = *tempBuff++;
               fecAdrEl->martiniVcEl.vcIfParams[cur].len = *tempBuff++;

               valueLen = fecAdrEl->martiniVcEl.vcIfParams[cur].len;
               if (valueLen < MPLS_VCIFPARAM_HDR)
               {
                  /* because of this encoding we MUST have
                   * atleast a length of MPLS_VCIFPARAM_HDR */
                  return MPLS_DEC_FECERROR;
               }
               valueLen -= MPLS_VCIFPARAM_HDR;

               if (valueLen)
               {
                  /* impl limitation, in the future this might be bigger */
                  // assert(valueLen <= MPLS_VCIFPARAMMAXLEN)

                  MEM_COPY( (u_char*)&(fecAdrEl->martiniVcEl.
                            vcIfParams[cur].value),
                            tempBuff,
                            valueLen);
                  tempBuff += valueLen;
               }

               decoded += fecAdrEl->martiniVcEl.vcIfParams[cur].len;
              
               cur++;
               /* impl limitation, there are only 5 types right now */
               // assert (cur <= 5);
            }
         }
         fecAdrEl->martiniVcEl.vcIfParamsLen = cur;
         decodedSize += decoded;
         break;
      }
      default:
      {
	 PRINT_ERR_2("Found wrong FEC type while decoding FEC elem (%d)\n",
	             type);
	 return MPLS_DEC_FECELEMERROR;
	 break;
      }
   } /* end: switch */

   return decodedSize;
   
} /* End: Mpls_decodeLdpFecAdrEl*/




/* 
 * Encode for FEC TLV 
 */ 

/*
 *  encode
 */
int Mpls_encodeLdpFecTlv 
(
   mplsLdpFecTlv_t * fecTlv, 
   u_char          * buff, 
   int               bufSize
)
{
   u_char * tempBuf     = buff; /* no change for the buff ptr */
   u_short  i;
   int      encodedSize = 0;
   u_int    fecElSize   = 0;    /* used to compute the sum of
					  all fec elements */
 
   if ((int)fecTlv->baseTlv.length + MPLS_TLVFIXLEN > bufSize)
   {
      /* not enough room */
      return MPLS_ENC_BUFFTOOSMALL;
   }
   
   /* check how many fec elements we have */
   if (fecTlv->numberFecElements > MPLS_MAXNUMFECELEMENT)
   {
      /* too many fec elem; need to increase MPLS_MAXNUMFECELEMENT */
      PRINT_ERR("Too many fec elem\n");
      return MPLS_FECTLVERROR;
   }

   for (i = 0; i < fecTlv->numberFecElements; i++)
   {
      if ( (fecTlv->fecElemTypes[i] == MPLS_WC_FEC) && 
	   (fecTlv->numberFecElements != 1) )
      {
	 return MPLS_WC_FECERROR;
      }
   }

   /*
    *  encode for tlv
    */
   encodedSize = Mpls_encodeLdpTlv( &(fecTlv->baseTlv),
                                    tempBuf,
                                    bufSize);
   if (encodedSize < 0)
   {
      PRINT_ERR("failed encoding the tlv in FEC tlv\n");
      return MPLS_ENC_TLVERROR;
   }
   tempBuf   += encodedSize;
   fecElSize += encodedSize;
 
   /* encode now the FEC elements; check if wc exists; if it is there
      then it should be the only element */

   for (i = 0; i < fecTlv->numberFecElements; i++)
   {
      encodedSize = Mpls_encodeLdpFecAdrEl(&(fecTlv->fecElArray[i]),
					   tempBuf,
					   bufSize - fecElSize,
					   fecTlv->fecElemTypes[i]);
      if(encodedSize < 0)
      {
	 return MPLS_ENC_FECELEMERROR;
      }
      tempBuf   += encodedSize;
      fecElSize += encodedSize;
   }

   return fecElSize; 

} /* End: Mpls_encodeLdpFecTlv */



/*
 *  decode
 */
int Mpls_decodeLdpFecTlv 
(
   mplsLdpFecTlv_t * fecTlv, 
   u_char          * buff, 
   int               bufSize,
   u_short           tlvLength
)
{
   u_char * tempBuf     = buff; /* no change for the buff ptr */
   int      decodedSize = 0;
   u_int    fecElSize   = 0;    /* used to compute the sum of
					  all fec elements */
   u_short  i = 0;
   u_char   type;
 
   if ((int)tlvLength > bufSize)
   {
      /* not enough data for Fec elements tlv*/
      PRINT_ERR("failed decoding FEC elements tlv\n");
      return MPLS_DEC_BUFFTOOSMALL;
   }
 
   /*
    *  decode for the FEC elements; check also that if we have a wc element,
    *  it is the only element encoded in the FEC;
    */
   type = *tempBuf; /* first thing after the TLV base should be the type
		       of the fec element */

   fecTlv->numberFecElements = 0;

   while (tlvLength > fecElSize)
   {

      /* check how many fec elements we have */
      if (fecTlv->numberFecElements > (u_short)(MPLS_MAXNUMFECELEMENT-1))
      {
         /* too many fec elem; need to increase MPLS_MAXNUMFECELEMENT */
         PRINT_ERR("Too many fec elem\n");
         return MPLS_FECTLVERROR;
      }

      decodedSize = Mpls_decodeLdpFecAdrEl(&(fecTlv->fecElArray[i]),
					   tempBuf,
					   bufSize - fecElSize,
					   type);
      if((decodedSize < 0) && (decodedSize != MPLS_FECERROR))
      {
	 return MPLS_DEC_FECELEMERROR;
      }
      else 
      {
         /* if the element had wrong preLen value, just skip it */
         if (decodedSize != MPLS_FECERROR)
         {
            fecTlv->fecElemTypes[i] = type;
            fecTlv->numberFecElements++;
            i++;

            tempBuf   += decodedSize;
            fecElSize += decodedSize;
         }
	 else
	 {
	    /* the preLen was filled with the total length
	       of the fec element to be skipped */
            tempBuf   += fecTlv->fecElArray[i].addressEl.preLen;
            fecElSize += fecTlv->fecElArray[i].addressEl.preLen;
	 }
      }

      /* get the type of the next element */
      type = *tempBuf;

   } /* end while */

   for (i = 0; i < fecTlv->numberFecElements; i++)
   {
      if ( (fecTlv->fecElemTypes[i] == MPLS_WC_FEC) && 
	   (fecTlv->numberFecElements != 1) )
      {
	 return MPLS_WC_FECERROR;
      }
   }

   return fecElSize; /* fecElSize should be equal to tlvLength */
   
} /* End: Mpls_decodeLdpFecTlv */



/* 
 * Encode for Generic label TLV 
 */ 

/*
 *  encode
 */
int Mpls_encodeLdpGenLblTlv 
(
   mplsLdpGenLblTlv_t * genLbl, 
   u_char             * buff, 
   int                  bufSize
)
{
   int      encodedSize = 0;
   u_char * tempBuf     = buff; /* no change for the buff ptr */
 
   if (MPLS_TLVFIXLEN + (int)(genLbl->baseTlv.length) > bufSize)
   {
      /* not enough room */
      return MPLS_ENC_BUFFTOOSMALL;
   }
 
   /*
    *  encode for tlv
    */
   encodedSize = Mpls_encodeLdpTlv( &(genLbl->baseTlv),
                                    tempBuf,
                                    bufSize);
   if (encodedSize < 0)
   {
      PRINT_ERR("failed encoding the tlv in Generic Label\n");
      return MPLS_ENC_TLVERROR;
   }
   tempBuf += encodedSize;
 
   genLbl->label = htonl(genLbl->label);
 
   MEM_COPY(tempBuf,
            (u_char*)&(genLbl->label),
            MPLS_LBLFIXLEN);

   return (MPLS_TLVFIXLEN + MPLS_LBLFIXLEN);
 
} /* End: Mpls_encodeLdpGenLblTlv */


/*
 *  decode
 */
int Mpls_decodeLdpGenLblTlv 
(
   mplsLdpGenLblTlv_t * genLbl, 
   u_char             * buff, 
   int                  bufSize
)
{
   if (MPLS_LBLFIXLEN > bufSize)
   {
      /* not enough data for generic label tlv*/
      PRINT_ERR("failed decoding Generic tlv\n");
      return MPLS_DEC_BUFFTOOSMALL;
   }
 
   /*
    *  decode the label
    */
   MEM_COPY( (u_char*)&(genLbl->label),
             buff,
             MPLS_LBLFIXLEN);
 
   genLbl->label = ntohl(genLbl->label);

   return MPLS_LBLFIXLEN;

} /* End: Mpls_decodeLdpGenLblTlv */



/* 
 * Encode for ATM Label TLV 
 */ 

/*
 *  encode
 */
int Mpls_encodeLdpAtmLblTlv 
(
   mplsLdpAtmLblTlv_t * atmLblTlv, 
   u_char             * buff, 
   int                  bufSize
)
{
   int      encodedSize = 0;
   u_char * tempBuf     = buff; /* no change for the buff ptr */
   u_char * atmLblPtr;

   if (MPLS_TLVFIXLEN + MPLS_LBLFIXLEN > bufSize)
   {
      /* not enough room */
      return MPLS_ENC_BUFFTOOSMALL; 
   }

   atmLblPtr = (u_char*)atmLblTlv;

   /* 
    *  encode for tlv 
    */
   encodedSize = Mpls_encodeLdpTlv( &(atmLblTlv->baseTlv),
                                    tempBuf, 
				    MPLS_TLVFIXLEN);
   if (encodedSize < 0)
   {
      return MPLS_ENC_TLVERROR;
   }
   tempBuf   += encodedSize; 
   atmLblPtr += encodedSize;

   /* 
    *  encode for flags
    */
   atmLblTlv->flags.flags.res = 0;
   atmLblTlv->flags.mark      = htons(atmLblTlv->flags.mark);
   atmLblTlv->vci             = htons(atmLblTlv->vci);

   MEM_COPY( tempBuf, 
	     atmLblPtr,
             MPLS_LBLFIXLEN);

   return (MPLS_LBLFIXLEN + MPLS_TLVFIXLEN); 

} /* End: Mpls_encodeLdpAtmLblTlv */


/*
 *  decode
 */
int Mpls_decodeLdpAtmLblTlv 
(
   mplsLdpAtmLblTlv_t * atmLblTlv, 
   u_char             * buff, 
   int                  bufSize
)
{
   u_char * atmLblTlvPtr;
 
   if (MPLS_LBLFIXLEN> bufSize)
   {
      /* not enough data for AtmLabel*/
      PRINT_ERR("failed decoding atm label tlv\n");
      return MPLS_DEC_BUFFTOOSMALL;
   }
   
   atmLblTlvPtr = (u_char *)atmLblTlv;
   atmLblTlvPtr += MPLS_TLVFIXLEN; /* to point after the Tlv which was
				      decoded before we reach here */
   /*
    *  decode for the rest of the AtmLblTlv 
    */
   MEM_COPY( atmLblTlvPtr,
             buff,
             MPLS_LBLFIXLEN);
 
   atmLblTlv->flags.mark = ntohs(atmLblTlv->flags.mark);
   atmLblTlv->vci        = ntohs(atmLblTlv->vci);

   return MPLS_LBLFIXLEN;

} /* End: Mpls_decodeLdpAtmLblTlv */


/* 
 * Encode for FR Label TLV 
 */ 

/*
 *  encode
 */
int Mpls_encodeLdpFrLblTlv 
(
   mplsLdpFrLblTlv_t * frLblTlv, 
   u_char            * buff, 
   int                 bufSize
)
{
   int      encodedSize = 0;
   u_char * tempBuf     = buff; /* no change for the buff ptr */

   if (MPLS_TLVFIXLEN + MPLS_LBLFIXLEN > bufSize)
   {
      /* not enough room */
      return MPLS_ENC_BUFFTOOSMALL; 
   }

   /* 
    *  encode for tlv 
    */
   encodedSize = Mpls_encodeLdpTlv( &(frLblTlv->baseTlv),
                                    tempBuf, 
				    MPLS_TLVFIXLEN);
   if (encodedSize < 0)
   {
      return MPLS_ENC_TLVERROR;
   }
   tempBuf += encodedSize; 

   /* 
    *  encode for flags
    */
   frLblTlv->flags.mark = htonl(frLblTlv->flags.mark);

   MEM_COPY( tempBuf, 
	     (u_char*)&(frLblTlv->flags.mark),
             MPLS_LBLFIXLEN);

   return (MPLS_LBLFIXLEN + MPLS_TLVFIXLEN); 

} /* End: Mpls_encodeLdpFrLblTlv */


/*
 *  decode
 */
int Mpls_decodeLdpFrLblTlv 
(
   mplsLdpFrLblTlv_t * frLblTlv, 
   u_char            * buff, 
   int               bufSize
)
{
   u_char * tempBuf = buff; /* no change for the buff ptr */
 
   if (MPLS_LBLFIXLEN> bufSize)
   {
      /* not enough data for FrLabel */
      PRINT_ERR("failed decoding fr label tlv\n");
      return MPLS_DEC_BUFFTOOSMALL;
   }
   
   /*
    *  decode for the rest of the FrLblTlv 
    */
   MEM_COPY( (u_char*)&(frLblTlv->flags.mark),
             tempBuf,
             MPLS_LBLFIXLEN);

   frLblTlv->flags.mark = ntohl(frLblTlv->flags.mark); 

   return MPLS_LBLFIXLEN;

} /* End: Mpls_decodeLdpFrLblTlv */



/* 
 * Encode for Hop Count TLV 
 */ 

/*
 *  encode
 */
int Mpls_encodeLdpHopTlv 
(
   mplsLdpHopTlv_t * hopCountTlv, 
   u_char          * buff, 
   int               bufSize
)
{
   int      encodedSize = 0;
   u_char * tempBuf     = buff; /* no change for the buff ptr */

   if (MPLS_TLVFIXLEN + MPLS_HOPCOUNTFIXLEN > bufSize)
   {
      /* not enough room */
      return MPLS_ENC_BUFFTOOSMALL; 
   }

   /* 
    *  encode for tlv 
    */
   encodedSize = Mpls_encodeLdpTlv( &(hopCountTlv->baseTlv),
                                    tempBuf, 
				    MPLS_TLVFIXLEN);
   if (encodedSize < 0)
   {
      return MPLS_ENC_TLVERROR;
   }
   tempBuf += encodedSize; 

   /* 
    *  encode for hop count value 
    */
   *tempBuf = hopCountTlv->hcValue;

   return (MPLS_HOPCOUNTFIXLEN + MPLS_TLVFIXLEN); 

} /* End: Mpls_encodeLdpFrLblTlv */


/*
 *  decode
 */
int Mpls_decodeLdpHopTlv 
(
   mplsLdpHopTlv_t * hopCountTlv, 
   u_char          * buff, 
   int               bufSize
)
{
   if (MPLS_HOPCOUNTFIXLEN > bufSize)
   {
      /* not enough data for hop count value*/
      PRINT_ERR("failed decoding hop count tlv\n");
      return MPLS_DEC_BUFFTOOSMALL;
   }
   
   /*
    *  decode for the hop count value
    */
   hopCountTlv->hcValue = *buff;

   return MPLS_HOPCOUNTFIXLEN;

} /* End: Mpls_decodeLdpHopTlv */




/* 
 * Encode for Lbl Msg Id TLV 
 */ 

/*
 *  encode
 */
int Mpls_encodeLdpLblMsgIdTlv 
(
   mplsLdpLblMsgIdTlv_t * lblMsgIdTlv, 
   u_char               * buff, 
   int                    bufSize
)
{
   int      encodedSize = 0;
   u_char * tempBuf     = buff; /* no change for the buff ptr */

   if (MPLS_TLVFIXLEN + MPLS_LBLFIXLEN > bufSize)
   {
      /* not enough room */
      return MPLS_ENC_BUFFTOOSMALL; 
   }

   /* 
    *  encode for tlv 
    */
   encodedSize = Mpls_encodeLdpTlv( &(lblMsgIdTlv->baseTlv),
                                    tempBuf, 
				    MPLS_TLVFIXLEN);
   if (encodedSize < 0)
   {
      return MPLS_ENC_TLVERROR;
   }
   tempBuf += encodedSize; 

   /* 
    *  encode for msg id 
    */

   lblMsgIdTlv->msgId = htonl(lblMsgIdTlv->msgId);

   MEM_COPY( tempBuf, 
	     (u_char*)&(lblMsgIdTlv->msgId),
             MPLS_LBLFIXLEN);

   return (MPLS_LBLFIXLEN + MPLS_TLVFIXLEN); 

} /* End: Mpls_encodeLdpFrLblTlv */


/*
 *  decode
 */
int Mpls_decodeLdpLblMsgIdTlv 
(
   mplsLdpLblMsgIdTlv_t * lblMsgIdTlv, 
   u_char               * buff, 
   int                    bufSize
)
{
   if (MPLS_LBLFIXLEN> bufSize)
   {
      /* not enough data for msg id tlv*/
      PRINT_ERR("failed decoding lbl msg id tlv\n");
      return MPLS_DEC_BUFFTOOSMALL;
   }
   
   /*
    *  decode for the rest of the LblMsgId Tlv 
    */
   MEM_COPY( (u_char*)&(lblMsgIdTlv->msgId),
             buff,
             MPLS_LBLFIXLEN);

   lblMsgIdTlv->msgId = ntohl(lblMsgIdTlv->msgId);

   return MPLS_LBLFIXLEN;

} /* End: Mpls_decodeLdpLblMsgIdTlv */



/* 
 * Encode for Path Vector TLV 
 */ 

/*
 *  encode
 */
int Mpls_encodeLdpPathVectorTlv 
(
   mplsLdpPathTlv_t * pathVectorTlv,
   u_char           * buff, 
   int                bufSize
)
{
   u_char * pathVectorTlvPtr;
   u_char * tempBuf     = buff; /* no change for the buff ptr */
   int      encodedSize = 0;
   u_int    i, numLsrIds;
   u_short  tempLength;         /* to store the tlv length for
				   later use */

   if (MPLS_TLVFIXLEN + (int)(pathVectorTlv->baseTlv.length)> bufSize)
   {
      /* not enough room */
      return MPLS_ENC_BUFFTOOSMALL; 
   }

   pathVectorTlvPtr  = (u_char*)pathVectorTlv;
   tempLength        = pathVectorTlv->baseTlv.length;
   /* 
    *  encode for tlv 
    */
   encodedSize = Mpls_encodeLdpTlv( &(pathVectorTlv->baseTlv),
                                    tempBuf, 
				    MPLS_TLVFIXLEN);
   if (encodedSize < 0)
   {
      return MPLS_ENC_TLVERROR;
   }
   tempBuf          += encodedSize; 
   pathVectorTlvPtr += encodedSize;

   /* 
    *  encode for labels
    */

   if (tempLength % MPLS_LBLFIXLEN != 0)
   {
      return MPLS_PATHVECTORERROR;
   }

   numLsrIds = tempLength / MPLS_LBLFIXLEN;
   if (numLsrIds > MPLS_MAXHOPSNUMBER)
   {
      /* too many lsrIds; need to increase MPLS_MAXHOPSNUMBER */
      PRINT_ERR_2("Too many lsr ids (%d)\n", numLsrIds);
      return MPLS_PATHVECTORERROR;
   }

   for (i = 0; i < numLsrIds; i++)
   {
      pathVectorTlv->lsrId[i] = htonl(pathVectorTlv->lsrId[i]);
   }

   MEM_COPY( tempBuf, 
	     pathVectorTlvPtr,
             tempLength);

   return (tempLength + MPLS_TLVFIXLEN); 

} /* End: Mpls_encodeLdpPathVectorTlv */


/*
 *  decode
 */
int Mpls_decodeLdpPathVectorTlv 
(
   mplsLdpPathTlv_t * pathVectorTlv,
   u_char           * buff, 
   int                bufSize,
   u_short            tlvLength
)
{
   u_char * tempBuf     = buff; /* no change for the buff ptr */
   u_int    i, numLsrIds;
 
   if (MPLS_LBLFIXLEN> bufSize)
   {
      /* not enough data for msg id tlv*/
      PRINT_ERR("failed decoding lbl msg id tlv\n");
      return MPLS_DEC_BUFFTOOSMALL;
   }
   
   if (tlvLength % MPLS_LBLFIXLEN != 0)
   {
      PRINT_ERR_2("Wrong length for Path vector tlv (%d)\n", tlvLength);
      return MPLS_PATHVECTORERROR;
   }

   numLsrIds = tlvLength / MPLS_LBLFIXLEN;
   if (numLsrIds > MPLS_MAXHOPSNUMBER)
   {
      /* too many lsrIds; need to increase MPLS_MAXHOPSNUMBER */
      PRINT_ERR_2("Too many lsr ids (%d)\n", numLsrIds);
      return MPLS_PATHVECTORERROR;
   }

   /*
    *  decode for the rest of the LblMsgId Tlv 
    */
   MEM_COPY( (u_char*)(pathVectorTlv->lsrId),
             tempBuf,
             tlvLength);

   for (i = 0; i < numLsrIds; i++)
   {
      pathVectorTlv->lsrId[i] = ntohl(pathVectorTlv->lsrId[i]);
   }

   return tlvLength;

} /* End: Mpls_decodeLdpPathVectorTlv */



/* 
 * Encode for Label Mapping Message
 */ 

/*
 *  encode
 */
int Mpls_encodeLdpLblMapMsg 
(
   mplsLdpLblMapMsg_t * lblMapMsg, 
   u_char             * buff, 
   int                  bufSize
)
{
   mplsLdpLblMapMsg_t lblMapMsgCopy;
   int                encodedSize = 0;
   u_int              totalSize   = 0;
   u_char *           tempBuf     = buff; /* no change for the buff ptr */

   /* check the length of the messageId + param */
   if ((int)(lblMapMsg->baseMsg.msgLength) + MPLS_TLVFIXLEN > bufSize) 
   {
      PRINT_ERR("failed to encode the lbl mapping msg: BUFFER TOO SMALL\n");
      return MPLS_ENC_BUFFTOOSMALL;
   }

   lblMapMsgCopy = *lblMapMsg;

   /*
    *  encode the base part of the pdu message
    */
   encodedSize = Mpls_encodeLdpBaseMsg( &(lblMapMsgCopy.baseMsg), 
				        tempBuf, 
				        bufSize);
   if (encodedSize < 0)
   {
      return MPLS_ENC_BASEMSGERROR;
   }
   PRINT_2("Encode BaseMsg for label mapping on %d bytes\n", encodedSize);
   tempBuf   += encodedSize;
   totalSize += encodedSize;

   /*
    *  encode the tlv if any
    */
   if (lblMapMsgCopy.fecTlvExists)
   {
      encodedSize = Mpls_encodeLdpFecTlv( &(lblMapMsgCopy.fecTlv), 
				          tempBuf, 
				          bufSize-totalSize);
      if (encodedSize < 0)
      {
         return MPLS_ENC_FECERROR;
      }
      PRINT_2("Encoded for FEC Tlv %d bytes\n", encodedSize);
      tempBuf   += encodedSize;
      totalSize += encodedSize;
   }

   if (lblMapMsgCopy.genLblTlvExists)
   {
      encodedSize = Mpls_encodeLdpGenLblTlv( &(lblMapMsgCopy.genLblTlv), 
				             tempBuf, 
				             bufSize-totalSize);
      if (encodedSize < 0)
      {
         return MPLS_ENC_GENLBLERROR;
      }
      PRINT_2("Encoded for Generic Label Tlv %d bytes\n", encodedSize);
      tempBuf   += encodedSize;
      totalSize += encodedSize;
   }
   if (lblMapMsgCopy.atmLblTlvExists)
   {
      encodedSize = Mpls_encodeLdpAtmLblTlv( &(lblMapMsgCopy.atmLblTlv), 
				             tempBuf, 
				             bufSize-totalSize);
      if (encodedSize < 0)
      {
         return MPLS_ENC_MAPATMERROR;
      }
      PRINT_2("Encoded for Atm Label Tlv %d bytes\n", encodedSize);
      tempBuf   += encodedSize;
      totalSize += encodedSize;
   }
   if (lblMapMsgCopy.frLblTlvExists)
   {
      encodedSize = Mpls_encodeLdpFrLblTlv( &(lblMapMsgCopy.frLblTlv), 
				             tempBuf, 
				             bufSize-totalSize);
      if (encodedSize < 0)
      {
         return MPLS_ENC_FRLBLERROR;
      }
      PRINT_2("Encoded for Fr Label Tlv %d bytes\n", encodedSize);
      tempBuf   += encodedSize;
      totalSize += encodedSize;
   }
   if (lblMapMsgCopy.hopCountTlvExists)
   {
      encodedSize = Mpls_encodeLdpHopTlv( &(lblMapMsgCopy.hopCountTlv), 
				          tempBuf, 
				          bufSize-totalSize);
      if (encodedSize < 0)
      {
         return MPLS_ENC_HOPCOUNTERROR;
      }
      PRINT_2("Encoded for Hop Count Tlv %d bytes\n", encodedSize);
      tempBuf   += encodedSize;
      totalSize += encodedSize;
   }
   if (lblMapMsgCopy.pathVecTlvExists)
   {
      encodedSize = Mpls_encodeLdpPathVectorTlv( &(lblMapMsgCopy.pathVecTlv), 
				                 tempBuf, 
				                 bufSize-totalSize);
      if (encodedSize < 0)
      {
         return MPLS_ENC_PATHVECERROR;
      }
      PRINT_2("Encoded for Path Vector Tlv %d bytes\n", encodedSize);
      tempBuf   += encodedSize;
      totalSize += encodedSize;
   }
   if (lblMapMsgCopy.lblMsgIdTlvExists)
   {
      encodedSize = Mpls_encodeLdpLblMsgIdTlv( &(lblMapMsgCopy.lblMsgIdTlv), 
				               tempBuf, 
				               bufSize-totalSize);
      if (encodedSize < 0)
      {
         return MPLS_ENC_LBLMSGIDERROR;
      }
      PRINT_2("Encoded for lbl request msg id Tlv %d bytes\n", encodedSize);
      tempBuf   += encodedSize;
      totalSize += encodedSize;
   }
   if (lblMapMsgCopy.trafficTlvExists)
   {
      encodedSize = Mpls_encodeLdpTrafficTlv( &(lblMapMsgCopy.trafficTlv), 
				              tempBuf, 
				              bufSize-totalSize);
      if (encodedSize < 0)
      {
         return MPLS_ENC_TRAFFICERROR;
      }
      PRINT_2("Encoded for Traffic Tlv %d bytes\n", encodedSize);
      tempBuf   += encodedSize;
      totalSize += encodedSize;
   }
   if (lblMapMsgCopy.lspidTlvExists)
   {
      encodedSize = Mpls_encodeLdpLspIdTlv( &(lblMapMsgCopy.lspidTlv), 
				             tempBuf, 
				             bufSize-totalSize);
      if (encodedSize < 0)
      {
         return MPLS_ENC_LSPIDERROR;
      }
      PRINT_2("Encoded for LSPID Tlv %d bytes\n", encodedSize);
      tempBuf   += encodedSize;
      totalSize += encodedSize;
   }

   return totalSize;

} /* End: Mpls_encodeLdpLblMapMsg */


/*
 *  decode
 */
int Mpls_decodeLdpLblMapMsg 
(
   mplsLdpLblMapMsg_t * lblMapMsg, 
   u_char             * buff, 
   int                  bufSize
)
{
   int          decodedSize    = 0;
   u_int        totalSize      = 0;
   u_int        stopLength     = 0;
   u_int        totalSizeParam = 0;
   u_char     * tempBuf        = buff; /* no change for the buff ptr */
   mplsLdpTlv_t tlvTemp;

   /*
    *  decode the base part of the pdu message
    */
   bzero(lblMapMsg, sizeof(mplsLdpLblMapMsg_t));
   decodedSize = Mpls_decodeLdpBaseMsg( &(lblMapMsg->baseMsg), 
				        tempBuf, 
				        bufSize);
   if (decodedSize < 0)
   {
      return MPLS_DEC_BASEMSGERROR;
   }
   PRINT_2("Decode BaseMsg for Lbl Mapping on %d bytes\n", decodedSize);

   if (lblMapMsg->baseMsg.flags.flags.msgType != MPLS_LBLMAP_MSGTYPE)
   {
      PRINT_ERR_2("Not the right message type; expected lbl map and got %x\n",
		   lblMapMsg->baseMsg.flags.flags.msgType);
      return MPLS_MSGTYPEERROR;
   }

   tempBuf   += decodedSize;
   totalSize += decodedSize;

   if (bufSize-totalSize <= 0)
   {
      /* nothing left for decoding */
      PRINT_ERR("Lbl Mapping msg does not have anything beside base msg\n");
      return totalSize;
   }

   PRINT_4("bufSize = %d,  totalSize = %d, lblMapMsg->baseMsg.msgLength = %d\n",
	   bufSize, totalSize, lblMapMsg->baseMsg.msgLength);

   /* Have to check the baseMsg.msgLength to know when to finish.
    * We finsh when the totalSizeParam is >= to the base message length - the
    * message id length (4) 
    */
    
   stopLength = lblMapMsg->baseMsg.msgLength - MPLS_MSGIDFIXLEN;
   while (stopLength > totalSizeParam)
   {
      /*
       *  decode the tlv to check what's next
       */
      bzero(&tlvTemp, MPLS_TLVFIXLEN);
      decodedSize = Mpls_decodeLdpTlv(&tlvTemp, tempBuf, bufSize-totalSize);
      if (decodedSize < 0)
      {
         /* something wrong */
         PRINT_ERR("Label Mapping msg decode failed for tlv\n");
         return MPLS_DEC_TLVERROR;
      }

      tempBuf        += decodedSize;
      totalSize      += decodedSize;
      totalSizeParam += decodedSize;

      switch (tlvTemp.flags.flags.type)
      {
         case MPLS_FEC_TLVTYPE:
	 {
            decodedSize = Mpls_decodeLdpFecTlv( &(lblMapMsg->fecTlv), 
				                tempBuf, 
				                bufSize-totalSize,
						tlvTemp.length);
            if (decodedSize < 0)
            {
	       PRINT_ERR("Failure when decoding FEC tlv from LblMap msg\n");
	       return MPLS_DEC_FECERROR;
            }
	    PRINT_2("Decoded for FEC %d bytes\n", decodedSize);
            tempBuf        += decodedSize;
            totalSize      += decodedSize;
            totalSizeParam += decodedSize;

	    lblMapMsg->fecTlvExists   = 1;
	    lblMapMsg->fecTlv.baseTlv = tlvTemp;
	    DEBUG_CALL(printFecListTlv(&(lblMapMsg->fecTlv)));
	    break;
	 }
         case MPLS_GENLBL_TLVTYPE:
	 {
            decodedSize = Mpls_decodeLdpGenLblTlv( &(lblMapMsg->genLblTlv), 
				                   tempBuf, 
				                   bufSize-totalSize);
            if (decodedSize < 0)
            {
	       PRINT_ERR("Failure when dec GEN Lbl tlv from LblMap msg\n");
	       return MPLS_DEC_GENLBLERROR;
            }
	    PRINT_2("Decoded for Gen Lbl %d bytes\n", decodedSize);
            tempBuf        += decodedSize;
            totalSize      += decodedSize;
            totalSizeParam += decodedSize;

	    lblMapMsg->genLblTlvExists   = 1;
	    lblMapMsg->genLblTlv.baseTlv = tlvTemp;
	    DEBUG_CALL(printGenLblTlv(&(lblMapMsg->genLblTlv)));
	    break;
	 }
         case MPLS_ATMLBL_TLVTYPE:
	 {
            decodedSize = Mpls_decodeLdpAtmLblTlv( &(lblMapMsg->atmLblTlv), 
				                   tempBuf, 
				                   bufSize-totalSize);
            if (decodedSize < 0)
            {
	       PRINT_ERR("Failure when dec ATM Lbl tlv from LblMap msg\n");
	       return MPLS_DEC_MAPATMERROR;
            }
	    PRINT_2("Decoded for Atm Lbl %d bytes\n", decodedSize);
            tempBuf        += decodedSize;
            totalSize      += decodedSize;
            totalSizeParam += decodedSize;

	    lblMapMsg->atmLblTlvExists   = 1;
	    lblMapMsg->atmLblTlv.baseTlv = tlvTemp;
	    DEBUG_CALL(printAtmLblTlv(&(lblMapMsg->atmLblTlv)));
	    break;
	 }
         case MPLS_FRLBL_TLVTYPE:
	 {
            decodedSize = Mpls_decodeLdpFrLblTlv( &(lblMapMsg->frLblTlv), 
				                  tempBuf, 
				                  bufSize-totalSize);
            if (decodedSize < 0)
            {
	       PRINT_ERR("Failure when dec FR Lbl tlv from LblMap msg\n");
	       return MPLS_DEC_FRLBLERROR;
            }
	    PRINT_2("Decoded for Fr Lbl %d bytes\n", decodedSize);
            tempBuf        += decodedSize;
            totalSize      += decodedSize;
            totalSizeParam += decodedSize;

	    lblMapMsg->frLblTlvExists   = 1;
	    lblMapMsg->frLblTlv.baseTlv = tlvTemp;
	    DEBUG_CALL(printFrLblTlv(&(lblMapMsg->frLblTlv)));
	    break;
	 }
         case MPLS_HOPCOUNT_TLVTYPE:
	 {
            decodedSize = Mpls_decodeLdpHopTlv( &(lblMapMsg->hopCountTlv), 
				                tempBuf, 
				                bufSize-totalSize);
            if (decodedSize < 0)
            {
	       PRINT_ERR("Failure when dec HopCount tlv from LblMap msg\n");
	       return MPLS_DEC_HOPCOUNTERROR;
            }
	    PRINT_2("Decoded for HopCount %d bytes\n", decodedSize);
            tempBuf        += decodedSize;
            totalSize      += decodedSize;
            totalSizeParam += decodedSize;

	    lblMapMsg->hopCountTlvExists   = 1;
	    lblMapMsg->hopCountTlv.baseTlv = tlvTemp;
	    DEBUG_CALL(printHopTlv(&(lblMapMsg->hopCountTlv)));
	    break;
	 }
         case MPLS_PATH_TLVTYPE:
	 {
            decodedSize = Mpls_decodeLdpPathVectorTlv(&(lblMapMsg->pathVecTlv), 
				                      tempBuf, 
				                      bufSize-totalSize,
						      tlvTemp.length);
            if (decodedSize < 0)
            {
	       PRINT_ERR("Failure when dec Path Vec tlv from LblMap msg\n");
	       return MPLS_DEC_PATHVECERROR;
            }
	    PRINT_2("Decoded for PATH VECTOR %d bytes\n", decodedSize);
            tempBuf        += decodedSize;
            totalSize      += decodedSize;
            totalSizeParam += decodedSize;

	    lblMapMsg->pathVecTlvExists   = 1;
	    lblMapMsg->pathVecTlv.baseTlv = tlvTemp;
	    DEBUG_CALL(printPathVecTlv(&(lblMapMsg->pathVecTlv)));
	    break;
	 }
         case MPLS_REQMSGID_TLVTYPE:
	 {
            decodedSize = Mpls_decodeLdpLblMsgIdTlv( &(lblMapMsg->lblMsgIdTlv), 
				                     tempBuf, 
				                     bufSize-totalSize);
            if (decodedSize < 0)
            {
	       PRINT_ERR("Failure when dec LblMsgId tlv from LblMap msg\n");
	       return MPLS_DEC_LBLMSGIDERROR;
            }
	    PRINT_2("Decoded for LblMsgId %d bytes\n", decodedSize);
            tempBuf        += decodedSize;
            totalSize      += decodedSize;
            totalSizeParam += decodedSize;

	    lblMapMsg->lblMsgIdTlvExists   = 1;
	    lblMapMsg->lblMsgIdTlv.baseTlv = tlvTemp;
	    DEBUG_CALL(printLblMsgIdTlv(&(lblMapMsg->lblMsgIdTlv)));
	    break;
	 }
         case MPLS_TRAFFIC_TLVTYPE:
	 {
            decodedSize = Mpls_decodeLdpTrafficTlv( &(lblMapMsg->trafficTlv), 
				                    tempBuf, 
				                    bufSize-totalSize,
						    tlvTemp.length);
            if (decodedSize < 0)
            {
	       PRINT_ERR("Failure when dec Traffic tlv from LblMap msg\n");
	       return MPLS_DEC_TRAFFICERROR;
            }
	    PRINT_2("Decoded for Traffic %d bytes\n", decodedSize);
            tempBuf        += decodedSize;
            totalSize      += decodedSize;
            totalSizeParam += decodedSize;

	    lblMapMsg->trafficTlvExists   = 1;
	    lblMapMsg->trafficTlv.baseTlv = tlvTemp;
	    DEBUG_CALL(printTrafficTlv(&(lblMapMsg->trafficTlv)));
	    break;
	 }
         case MPLS_LSPID_TLVTYPE:
	 {
            decodedSize = Mpls_decodeLdpLspIdTlv( &(lblMapMsg->lspidTlv), 
				                    tempBuf, 
				                    bufSize-totalSize);
            if (decodedSize < 0)
            {
	       PRINT_ERR("Failure when dec LSPID tlv from LblMap msg\n");
	       return MPLS_DEC_LSPIDERROR;
            }
	    PRINT_2("Decoded for lspid tlv %d bytes\n", decodedSize);
            tempBuf        += decodedSize;
            totalSize      += decodedSize;
            totalSizeParam += decodedSize;

	    lblMapMsg->lspidTlvExists   = 1;
	    lblMapMsg->lspidTlv.baseTlv = tlvTemp;
	    DEBUG_CALL(printLspIdTlv(&(lblMapMsg->lspidTlv)));
	    break;
	 }
         default:
	 {
	    PRINT_ERR_2("Found wrong tlv type while decoding lbl map msg (%x)\n",
	             tlvTemp.flags.flags.type);
	    if (tlvTemp.flags.flags.uBit == 1)
	    {
	       /* ignore the Tlv and continue processing */
               tempBuf        += tlvTemp.length;
               totalSize      += tlvTemp.length;
               totalSizeParam += tlvTemp.length;
               break;
	    }
	    else
	    {
	       /* drop the message; return error */
	       return MPLS_TLVTYPEERROR;
	    }
	 }
      } /* switch type */

   } /* while */

   PRINT_2("totalsize for Mpls_decodeLdpLblMapMsg is %d\n", totalSize);

   return totalSize;
   
} /* End: Mpls_decodeLdpLblMapMsg */



/* 
 * Encode for Retrun MessageId TLV 
 */ 

/*
 *  encode
 */
int Mpls_encodeLdpLblRetMsgIdTlv 
(
   mplsLdpLblRetMsgIdTlv_t * lblMsgIdTlv, 
   u_char                  * buff, 
   int                       bufSize
)
{
   /* 
    *  encode for tlv 
    */
   if( Mpls_encodeLdpTlv( &(lblMsgIdTlv->baseTlv),
                             buff, 
			     MPLS_TLVFIXLEN) < 0)
   {
      return MPLS_ENC_TLVERROR;
   }

   return MPLS_TLVFIXLEN;

} /* End: Mpls_encodeLdpLblRetMsgIdTlv */

/*
 *  decode
 */
int Mpls_decodeLdpLblRetMsgIdTlv 
(
   mplsLdpLblRetMsgIdTlv_t * lblMsgIdTlv, 
   u_char                  * buff, 
   int                       bufSize
)
{ 
  /* this function does not need to do anything */
  return 0;
} /* End: Mpls_decodeLdpLblRetMsgIdTlv */




/* 
 * Encode for Label Request Message
 */ 

/*
 *  encode
 */
int Mpls_encodeLdpLblReqMsg 
(
   mplsLdpLblReqMsg_t * lblReqMsg, 
   u_char             * buff, 
   int                  bufSize
)
{
   mplsLdpLblReqMsg_t lblReqMsgCopy;
   int                encodedSize;
   u_int              totalSize = 0;
   u_char *           tempBuf   = buff; /* no change for the buff ptr */

   /* check the length of the messageId + param */
   if ((int)(lblReqMsg->baseMsg.msgLength) + MPLS_TLVFIXLEN > bufSize) 
   {
      PRINT_ERR("failed to encode the lbl request msg: BUFFER TOO SMALL\n");
      return MPLS_ENC_BUFFTOOSMALL;
   }

   lblReqMsgCopy = *lblReqMsg;

   /*
    *  encode the base part of the pdu message
    */
   encodedSize = Mpls_encodeLdpBaseMsg( &(lblReqMsgCopy.baseMsg), 
				        tempBuf, 
				        bufSize);
   if (encodedSize < 0)
   {
      return MPLS_ENC_BASEMSGERROR;
   }
   PRINT_2("Encode BaseMsg for label request on %d bytes\n", encodedSize);
   tempBuf   += encodedSize;
   totalSize += encodedSize;

   /*
    *  encode the tlv if any
    */
   if (lblReqMsgCopy.fecTlvExists)
   {
      encodedSize = Mpls_encodeLdpFecTlv( &(lblReqMsgCopy.fecTlv), 
				          tempBuf, 
				          bufSize-totalSize);
      if (encodedSize < 0)
      {
         return MPLS_ENC_FECERROR;
      }
      PRINT_2("Encoded for FEC Tlv %d bytes\n", encodedSize);
      tempBuf   += encodedSize;
      totalSize += encodedSize;
   }
   if (lblReqMsgCopy.hopCountTlvExists)
   {
      encodedSize = Mpls_encodeLdpHopTlv( &(lblReqMsgCopy.hopCountTlv), 
				          tempBuf, 
				          bufSize-totalSize);
      if (encodedSize < 0)
      {
         return MPLS_ENC_HOPCOUNTERROR;
      }
      PRINT_2("Encoded for Hop Count Tlv %d bytes\n", encodedSize);
      tempBuf   += encodedSize;
      totalSize += encodedSize;
   }
   if (lblReqMsgCopy.pathVecTlvExists)
   {
      encodedSize = Mpls_encodeLdpPathVectorTlv( &(lblReqMsgCopy.pathVecTlv), 
				                 tempBuf, 
				                 bufSize-totalSize);
      if (encodedSize < 0)
      {
         return MPLS_ENC_PATHVECERROR;
      }
      PRINT_2("Encoded for Hop Count Tlv %d bytes\n", encodedSize);
      tempBuf   += encodedSize;
      totalSize += encodedSize;
   }
   if (lblReqMsgCopy.lblMsgIdTlvExists)
   {
      encodedSize = Mpls_encodeLdpLblRetMsgIdTlv( &(lblReqMsgCopy.lblMsgIdTlv), 
				                  tempBuf, 
				                  bufSize-totalSize);
      if (encodedSize < 0)
      {
         return MPLS_ENC_LBLMSGIDERROR;
      }
      PRINT_2("Encoded for Hop Count Tlv %d bytes\n", encodedSize);
      tempBuf   += encodedSize;
      totalSize += encodedSize;
   }
   if (lblReqMsgCopy.erTlvExists)
   {
      encodedSize = Mpls_encodeLdpERTlv( &(lblReqMsgCopy.erTlv), 
				         tempBuf, 
				         bufSize-totalSize);
      if (encodedSize < 0)
      {
         return MPLS_ENC_ERTLVERROR;
      }
      PRINT_2("Encoded for CR Tlv %d bytes\n", encodedSize);
      tempBuf   += encodedSize;
      totalSize += encodedSize;
   }
   if (lblReqMsgCopy.trafficTlvExists)
   {
      encodedSize = Mpls_encodeLdpTrafficTlv( &(lblReqMsgCopy.trafficTlv), 
				              tempBuf, 
				              bufSize-totalSize);
      if (encodedSize < 0)
      {
         return MPLS_ENC_TRAFFICERROR;
      }
      PRINT_2("Encoded for Traffic Tlv %d bytes\n", encodedSize);
      tempBuf   += encodedSize;
      totalSize += encodedSize;
   }
   if (lblReqMsgCopy.lspidTlvExists)
   {
      encodedSize = Mpls_encodeLdpLspIdTlv( &(lblReqMsgCopy.lspidTlv), 
				             tempBuf, 
				             bufSize-totalSize);
      if (encodedSize < 0)
      {
         return MPLS_ENC_LSPIDERROR;
      }
      PRINT_2("Encoded for LSPID Tlv %d bytes\n", encodedSize);
      tempBuf   += encodedSize;
      totalSize += encodedSize;
   }
   if (lblReqMsgCopy.pinningTlvExists)
   {
      encodedSize = Mpls_encodeLdpPinningTlv( &(lblReqMsgCopy.pinningTlv), 
				              tempBuf, 
				              bufSize-totalSize);
      if (encodedSize < 0)
      {
         return MPLS_ENC_PINNINGERROR;
      }
      PRINT_2("Encoded for Pinning Tlv %d bytes\n", encodedSize);
      tempBuf   += encodedSize;
      totalSize += encodedSize;
   }
   if (lblReqMsgCopy.recClassTlvExists)
   {
      encodedSize = Mpls_encodeLdpResClsTlv( &(lblReqMsgCopy.resClassTlv), 
				             tempBuf, 
				             bufSize-totalSize);
      if (encodedSize < 0)
      {
         return MPLS_ENC_RESCLSERROR;
      }
      PRINT_2("Encoded for Resource class Tlv %d bytes\n", encodedSize);
      tempBuf   += encodedSize;
      totalSize += encodedSize;
   }
   if (lblReqMsgCopy.preemptTlvExists)
   {
      encodedSize = Mpls_encodeLdpPreemptTlv( &(lblReqMsgCopy.preemptTlv), 
				              tempBuf, 
				              bufSize-totalSize);
      if (encodedSize < 0)
      {
         return MPLS_ENC_PREEMPTERROR;
      }
      PRINT_2("Encoded for Preempt Tlv %d bytes\n", encodedSize);
      tempBuf   += encodedSize;
      totalSize += encodedSize;
   }

   return totalSize;

} /* End: Mpls_encodeLdpLblReqMsg */

/*
 *  decode
 */
int Mpls_decodeLdpLblReqMsg 
(
   mplsLdpLblReqMsg_t * lblReqMsg, 
   u_char             * buff, 
   int                  bufSize
)
{
   int          decodedSize    = 0;
   u_int        totalSize      = 0;
   u_int        stopLength     = 0;
   u_int        totalSizeParam = 0;
   u_char *     tempBuf        = buff; /* no change for the buff ptr */
   mplsLdpTlv_t tlvTemp;

   /*
    *  decode the base part of the pdu message
    */
   bzero(lblReqMsg, sizeof(mplsLdpLblReqMsg_t));
   decodedSize = Mpls_decodeLdpBaseMsg( &(lblReqMsg->baseMsg), 
				        tempBuf, 
				        bufSize);
   if (decodedSize < 0)
   {
      return MPLS_DEC_BASEMSGERROR;
   }
   PRINT_2("Decode BaseMsg for Lbl Request on %d bytes\n", decodedSize);

   if (lblReqMsg->baseMsg.flags.flags.msgType != MPLS_LBLREQ_MSGTYPE)
   {
      PRINT_ERR_2("Not the right message type; expected lbl req and got %x\n",
		   lblReqMsg->baseMsg.flags.flags.msgType);
      return MPLS_MSGTYPEERROR;
   }

   tempBuf   += decodedSize;
   totalSize += decodedSize;

   if (bufSize-totalSize <= 0)
   {
      /* nothing left for decoding */
      PRINT_ERR("Lbl Request msg does not have anything beside base msg\n");
      return totalSize;
   }

   PRINT_4("bufSize = %d,  totalSize = %d, lblReqMsg->baseMsg.msgLength = %d\n",
	   bufSize, totalSize, lblReqMsg->baseMsg.msgLength);

   /* Have to check the baseMsg.msgLength to know when to finish.
    * We finsh when the totalSizeParam is >= to the base message length - the
    * message id length (4) 
    */
    
   stopLength = lblReqMsg->baseMsg.msgLength - MPLS_MSGIDFIXLEN;
   while (stopLength > totalSizeParam)
   {
      /*
       *  decode the tlv to check what's next
       */
      bzero(&tlvTemp, MPLS_TLVFIXLEN);
      decodedSize = Mpls_decodeLdpTlv(&tlvTemp, tempBuf, bufSize-totalSize);
      if (decodedSize < 0)
      {
         /* something wrong */
         PRINT_ERR("Label Request msg decode failed for tlv\n");
         return MPLS_DEC_TLVERROR;
      }

      tempBuf        += decodedSize;
      totalSize      += decodedSize;
      totalSizeParam += decodedSize;

      switch (tlvTemp.flags.flags.type)
      {
         case MPLS_FEC_TLVTYPE:
	 {
            decodedSize = Mpls_decodeLdpFecTlv( &(lblReqMsg->fecTlv), 
				                tempBuf, 
				                bufSize-totalSize,
						tlvTemp.length);
            if (decodedSize < 0)
            {
	       PRINT_ERR("Failure when decoding FEC tlv from LblReq msg\n");
	       return MPLS_DEC_FECERROR;
            }
	    PRINT_2("Decoded for FEC %d bytes\n", decodedSize);
            tempBuf        += decodedSize;
            totalSize      += decodedSize;
            totalSizeParam += decodedSize;

	    lblReqMsg->fecTlvExists   = 1;
	    lblReqMsg->fecTlv.baseTlv = tlvTemp;
	    DEBUG_CALL(printFecListTlv(&(lblReqMsg->fecTlv)));
	    break;
	 }
         case MPLS_HOPCOUNT_TLVTYPE:
	 {
            decodedSize = Mpls_decodeLdpHopTlv( &(lblReqMsg->hopCountTlv), 
				                tempBuf, 
				                bufSize-totalSize);
            if (decodedSize < 0)
            {
	       PRINT_ERR("Failure when dec HopCount tlv from LblReq msg\n");
	       return MPLS_DEC_HOPCOUNTERROR;
            }
	    PRINT_2("Decoded for HopCount %d bytes\n", decodedSize);
            tempBuf        += decodedSize;
            totalSize      += decodedSize;
            totalSizeParam += decodedSize;

	    lblReqMsg->hopCountTlvExists   = 1;
	    lblReqMsg->hopCountTlv.baseTlv = tlvTemp;
	    DEBUG_CALL(printHopTlv(&(lblReqMsg->hopCountTlv)));
	    break;
	 }
         case MPLS_PATH_TLVTYPE:
	 {
            decodedSize = Mpls_decodeLdpPathVectorTlv(&(lblReqMsg->pathVecTlv), 
				                      tempBuf, 
				                      bufSize-totalSize,
						      tlvTemp.length);
            if (decodedSize < 0)
            {
	       PRINT_ERR("Failure when dec Path Vec tlv from LblReq msg\n");
	       return MPLS_DEC_PATHVECERROR;
            }
	    PRINT_2("Decoded for PATH VECTOR %d bytes\n", decodedSize);
            tempBuf        += decodedSize;
            totalSize      += decodedSize;
            totalSizeParam += decodedSize;

	    lblReqMsg->pathVecTlvExists   = 1;
	    lblReqMsg->pathVecTlv.baseTlv = tlvTemp;
	    DEBUG_CALL(printPathVecTlv(&(lblReqMsg->pathVecTlv)));
	    break;
	 }
         case MPLS_LBLMSGID_TLVTYPE:
	 {
	    lblReqMsg->lblMsgIdTlvExists   = 1;
	    lblReqMsg->lblMsgIdTlv.baseTlv = tlvTemp;
	    DEBUG_CALL(printTlv(&(lblReqMsg->lblMsgIdTlv.baseTlv)));
	    break;
	 }
         case MPLS_ER_TLVTYPE:
	   {
            decodedSize = Mpls_decodeLdpERTlv( &(lblReqMsg->erTlv), 
				               tempBuf, 
				               bufSize-totalSize,
					       tlvTemp.length);
            if (decodedSize < 0)
            {
	       PRINT_ERR("Failure when dec CR tlv from LblReq msg\n");
	       return MPLS_DEC_ERTLVERROR;
            }
	    PRINT_2("Decoded for CR %d bytes\n", decodedSize);
            tempBuf        += decodedSize;
            totalSize      += decodedSize;
            totalSizeParam += decodedSize;

	    lblReqMsg->erTlvExists   = 1;
	    lblReqMsg->erTlv.baseTlv = tlvTemp;
	    DEBUG_CALL(printErTlv(&(lblReqMsg->erTlv)));
	    break;
	 }
         case MPLS_TRAFFIC_TLVTYPE:
	 {
            decodedSize = Mpls_decodeLdpTrafficTlv( &(lblReqMsg->trafficTlv), 
				                    tempBuf, 
				                    bufSize-totalSize,
						    tlvTemp.length);
            if (decodedSize < 0)
            {
	       PRINT_ERR("Failure when dec Traffic tlv from LblReq msg\n");
	       return MPLS_DEC_TRAFFICERROR;
            }
	    PRINT_2("Decoded for Traffic %d bytes\n", decodedSize);
            tempBuf        += decodedSize;
            totalSize      += decodedSize;
            totalSizeParam += decodedSize;

	    lblReqMsg->trafficTlvExists   = 1;
	    lblReqMsg->trafficTlv.baseTlv = tlvTemp;
	    DEBUG_CALL(printTrafficTlv(&(lblReqMsg->trafficTlv)));
	    break;
	 }
         case MPLS_LSPID_TLVTYPE:
	 {
            decodedSize = Mpls_decodeLdpLspIdTlv( &(lblReqMsg->lspidTlv), 
				                    tempBuf, 
				                    bufSize-totalSize);
            if (decodedSize < 0)
            {
	       PRINT_ERR("Failure when dec LSPID tlv from LblReq msg\n");
	       return MPLS_DEC_LSPIDERROR;
            }
	    PRINT_2("Decoded for lspid tlv %d bytes\n", decodedSize);
            tempBuf        += decodedSize;
            totalSize      += decodedSize;
            totalSizeParam += decodedSize;

	    lblReqMsg->lspidTlvExists   = 1;
	    lblReqMsg->lspidTlv.baseTlv = tlvTemp;
	    DEBUG_CALL(printLspIdTlv(&(lblReqMsg->lspidTlv)));
	    break;
	 }
         case MPLS_PINNING_TLVTYPE:
	 {
            decodedSize = Mpls_decodeLdpPinningTlv( &(lblReqMsg->pinningTlv), 
				                    tempBuf, 
				                    bufSize-totalSize);
            if (decodedSize < 0)
            {
	       PRINT_ERR("Failure when dec Pinning tlv from LblReq msg\n");
	       return MPLS_DEC_PINNINGERROR;
            }
	    PRINT_2("Decoded for pining tlv %d bytes\n", decodedSize);
            tempBuf        += decodedSize;
            totalSize      += decodedSize;
            totalSizeParam += decodedSize;

	    lblReqMsg->pinningTlvExists   = 1;
	    lblReqMsg->pinningTlv.baseTlv = tlvTemp;
	    DEBUG_CALL(printPinningTlv(&(lblReqMsg->pinningTlv)));
	    break;
	 }
         case MPLS_RESCLASS_TLVTYPE:
	 {
            decodedSize = Mpls_decodeLdpResClsTlv( &(lblReqMsg->resClassTlv), 
				                    tempBuf, 
				                    bufSize-totalSize);
            if (decodedSize < 0)
            {
	       PRINT_ERR("Failure when dec ResClass tlv from LblReq msg\n");
	       return MPLS_DEC_RESCLSERROR;
            }
	    PRINT_2("Decoded for %d bytes\n", decodedSize);
            tempBuf        += decodedSize;
            totalSize      += decodedSize;
            totalSizeParam += decodedSize;

	    lblReqMsg->recClassTlvExists   = 1;
	    lblReqMsg->resClassTlv.baseTlv = tlvTemp;
	    DEBUG_CALL(printResClsTlv(&(lblReqMsg->resClassTlv)));
	    break;
	 }
         case MPLS_PREEMPT_TLVTYPE:
	 {
            decodedSize = Mpls_decodeLdpPreemptTlv( &(lblReqMsg->preemptTlv), 
				                    tempBuf, 
				                    bufSize-totalSize);
            if (decodedSize < 0)
            {
	       PRINT_ERR("Failure when dec preempt tlv from LblReq msg\n");
	       return MPLS_DEC_PREEMPTERROR;
            }
	    PRINT_2("Decoded for preempt tlv %d bytes\n", decodedSize);
            tempBuf        += decodedSize;
            totalSize      += decodedSize;
            totalSizeParam += decodedSize;

	    lblReqMsg->preemptTlvExists   = 1;
	    lblReqMsg->preemptTlv.baseTlv = tlvTemp;
	    DEBUG_CALL(printPreemptTlv(&(lblReqMsg->preemptTlv)));
	    break;
	 }
         default:
	 {
	    PRINT_ERR_2("Found wrong type while decoding lbl req msg (%x)\n",
	             tlvTemp.flags.flags.type);
	    if (tlvTemp.flags.flags.uBit == 1)
	    {
	       /* ignore the Tlv and continue processing */
               tempBuf        += tlvTemp.length;
               totalSize      += tlvTemp.length;
               totalSizeParam += tlvTemp.length;
               break;
	    }
	    else
	    {
	       /* drop the message; return error */
	       return MPLS_TLVTYPEERROR;
	    }
	 }
      } /* switch type */

   } /* while */

   PRINT_2("totalsize for Mpls_decodeLdpLblReqMsg is %d\n", totalSize);
   return totalSize;

} /*End: Mpls_decodeLdpLblReqMsg */



/* 
 * Encode for Label Withdraw and Label Release Message
 */ 

/*
 *  encode
 */
int Mpls_encodeLdpLbl_W_R_Msg 
(
   mplsLdpLbl_W_R_Msg_t * lbl_W_R_Msg,
   u_char               * buff, 
   int                    bufSize
)
{
   mplsLdpLbl_W_R_Msg_t lbl_W_R_MsgCopy;
   int                  encodedSize;
   u_int                totalSize = 0;
   u_char *             tempBuf   = buff; /* no change for the buff ptr */

   /* check the length of the messageId + param */
   if ((int)(lbl_W_R_Msg->baseMsg.msgLength) + MPLS_TLVFIXLEN > bufSize) 
   {
      PRINT_ERR("failed to encode the lbl mapping msg: BUFFER TOO SMALL\n");
      return MPLS_ENC_BUFFTOOSMALL;
   }

   lbl_W_R_MsgCopy = *lbl_W_R_Msg;

   /*
    *  encode the base part of the pdu message
    */
   encodedSize = Mpls_encodeLdpBaseMsg( &(lbl_W_R_MsgCopy.baseMsg), 
				        tempBuf, 
				        bufSize);
   if (encodedSize < 0)
   {
      return MPLS_ENC_BASEMSGERROR;
   }
   PRINT_2("Encode BaseMsg for label withdraw on %d bytes\n", encodedSize);
   tempBuf   += encodedSize;
   totalSize += encodedSize;

   /*
    *  encode the tlv if any
    */
   if (lbl_W_R_MsgCopy.fecTlvExists)
   {
      encodedSize = Mpls_encodeLdpFecTlv( &(lbl_W_R_MsgCopy.fecTlv), 
				          tempBuf, 
				          bufSize-totalSize);
      if (encodedSize < 0)
      {
         return MPLS_ENC_FECERROR;
      }
      PRINT_2("Encoded for FEC Tlv %d bytes\n", encodedSize);
      tempBuf   += encodedSize;
      totalSize += encodedSize;
   }

   if (lbl_W_R_MsgCopy.genLblTlvExists)
   {
      encodedSize = Mpls_encodeLdpGenLblTlv( &(lbl_W_R_MsgCopy.genLblTlv), 
				             tempBuf, 
				             bufSize-totalSize);
      if (encodedSize < 0)
      {
         return MPLS_ENC_GENLBLERROR;
      }
      PRINT_2("Encoded for Generic Label Tlv %d bytes\n", encodedSize);
      tempBuf   += encodedSize;
      totalSize += encodedSize;
   }
   if (lbl_W_R_MsgCopy.atmLblTlvExists)
   {
      encodedSize = Mpls_encodeLdpAtmLblTlv( &(lbl_W_R_MsgCopy.atmLblTlv), 
				             tempBuf, 
				             bufSize-totalSize);
      if (encodedSize < 0)
      {
         return MPLS_ENC_MAPATMERROR;
      }
      PRINT_2("Encoded for Atm Label Tlv %d bytes\n", encodedSize);
      tempBuf   += encodedSize;
      totalSize += encodedSize;
   }
   if (lbl_W_R_MsgCopy.frLblTlvExists)
   {
      encodedSize = Mpls_encodeLdpFrLblTlv( &(lbl_W_R_MsgCopy.frLblTlv), 
				             tempBuf, 
				             bufSize-totalSize);
      if (encodedSize < 0)
      {
         return MPLS_ENC_FRLBLERROR;
      }
      PRINT_2("Encoded for Fr Label Tlv %d bytes\n", encodedSize);
      tempBuf   += encodedSize;
      totalSize += encodedSize;
   }
   if (lbl_W_R_MsgCopy.lspidTlvExists)
   {
      encodedSize = Mpls_encodeLdpLspIdTlv( &(lbl_W_R_MsgCopy.lspidTlv), 
				             tempBuf, 
				             bufSize-totalSize);
      if (encodedSize < 0)
      {
         return MPLS_ENC_LSPIDERROR;
      }
      PRINT_2("Encoded for LSPID Tlv %d bytes\n", encodedSize);
      tempBuf   += encodedSize;
      totalSize += encodedSize;
   }

   return totalSize;

} /* End: Mpls_encodeLdpLbl_W_R_Msg */


/*
 *  decode
 */
int Mpls_decodeLdpLbl_W_R_Msg 
(
   mplsLdpLbl_W_R_Msg_t * lbl_W_R_Msg,
   u_char               * buff, 
   int                    bufSize
)
{
   int          decodedSize;
   u_int        totalSize      = 0;
   u_int        stopLength     = 0;
   u_int        totalSizeParam = 0;
   u_char *     tempBuf        = buff; /* no change for the buff ptr */
   mplsLdpTlv_t tlvTemp;

   /*
    *  decode the base part of the pdu message
    */
   bzero(lbl_W_R_Msg, sizeof(mplsLdpLbl_W_R_Msg_t));
   decodedSize = Mpls_decodeLdpBaseMsg( &(lbl_W_R_Msg->baseMsg), 
				        tempBuf, 
				        bufSize);
   if (decodedSize < 0)
   {
      return MPLS_DEC_BASEMSGERROR;
   }
   PRINT_2("Decode BaseMsg for Lbl Withdraw on %d bytes\n", decodedSize);

   if ((lbl_W_R_Msg->baseMsg.flags.flags.msgType != MPLS_LBLWITH_MSGTYPE) && 
       (lbl_W_R_Msg->baseMsg.flags.flags.msgType != MPLS_LBLREL_MSGTYPE))
   {
      PRINT_ERR_2("Not the right message type; expected lbl W_R and got %x\n",
		   lbl_W_R_Msg->baseMsg.flags.flags.msgType);
      return MPLS_MSGTYPEERROR;
   }

   tempBuf   += decodedSize;
   totalSize += decodedSize;

   if (bufSize-totalSize <= 0)
   {
      /* nothing left for decoding */
      PRINT_ERR("Lbl Withdraw msg does not have anything beside base msg\n");
      return totalSize;
   }

   PRINT_4("bufSize = %d,  totalSize = %d, lbl_W_R_Msg->baseMsg.msgLength = %d\n",
	   bufSize, totalSize, lbl_W_R_Msg->baseMsg.msgLength);

   /* Have to check the baseMsg.msgLength to know when to finish.
    * We finsh when the totalSizeParam is >= to the base message length - the
    * message id length (4) 
    */
    
   stopLength = lbl_W_R_Msg->baseMsg.msgLength - MPLS_MSGIDFIXLEN;
   while (stopLength > totalSizeParam)
   {
      /*
       *  decode the tlv to check what's next
       */
      bzero(&tlvTemp, MPLS_TLVFIXLEN);
      decodedSize = Mpls_decodeLdpTlv(&tlvTemp, tempBuf, bufSize-totalSize);
      if (decodedSize < 0)
      {
         /* something wrong */
         PRINT_ERR("Label Mapping msg decode failed for tlv\n");
         return MPLS_DEC_TLVERROR;
      }

      tempBuf        += decodedSize;
      totalSize      += decodedSize;
      totalSizeParam += decodedSize;

      switch (tlvTemp.flags.flags.type)
      {
         case MPLS_FEC_TLVTYPE:
	 {
            decodedSize = Mpls_decodeLdpFecTlv( &(lbl_W_R_Msg->fecTlv), 
				                tempBuf, 
				                bufSize-totalSize,
						tlvTemp.length);
            if (decodedSize < 0)
            {
	       PRINT_ERR("Failure when decoding FEC tlv from LblWithdr msg\n");
	       return MPLS_DEC_FECERROR;
            }
	    PRINT_2("Decoded for FEC %d bytes\n", decodedSize);
            tempBuf        += decodedSize;
            totalSize      += decodedSize;
            totalSizeParam += decodedSize;

	    lbl_W_R_Msg->fecTlvExists   = 1;
	    lbl_W_R_Msg->fecTlv.baseTlv = tlvTemp;
	    DEBUG_CALL(printFecListTlv(&(lbl_W_R_Msg->fecTlv)));
	    break;
	 }
         case MPLS_GENLBL_TLVTYPE:
	 {
            decodedSize = Mpls_decodeLdpGenLblTlv( &(lbl_W_R_Msg->genLblTlv), 
				                   tempBuf, 
				                   bufSize-totalSize);
            if (decodedSize < 0)
            {
	       PRINT_ERR("Failure when dec GEN Lbl tlv from LblWithdr msg\n");
	       return MPLS_DEC_GENLBLERROR;
            }
	    PRINT_2("Decoded for Gen Lbl %d bytes\n", decodedSize);
            tempBuf        += decodedSize;
            totalSize      += decodedSize;
            totalSizeParam += decodedSize;

	    lbl_W_R_Msg->genLblTlvExists   = 1;
	    lbl_W_R_Msg->genLblTlv.baseTlv = tlvTemp;
	    DEBUG_CALL(printGenLblTlv(&(lbl_W_R_Msg->genLblTlv)));
	    break;
	 }
         case MPLS_ATMLBL_TLVTYPE:
	 {
            decodedSize = Mpls_decodeLdpAtmLblTlv( &(lbl_W_R_Msg->atmLblTlv), 
				                   tempBuf, 
				                   bufSize-totalSize);
            if (decodedSize < 0)
            {
	       PRINT_ERR("Failure when dec ATM Lbl tlv from LblWithdr msg\n");
	       return MPLS_DEC_MAPATMERROR;
            }
	    PRINT_2("Decoded for Atm Lbl %d bytes\n", decodedSize);
            tempBuf        += decodedSize;
            totalSize      += decodedSize;
            totalSizeParam += decodedSize;

	    lbl_W_R_Msg->atmLblTlvExists   = 1;
	    lbl_W_R_Msg->atmLblTlv.baseTlv = tlvTemp;
	    DEBUG_CALL(printAtmLblTlv(&(lbl_W_R_Msg->atmLblTlv)));
	    break;
	 }
         case MPLS_FRLBL_TLVTYPE:
	 {
            decodedSize = Mpls_decodeLdpFrLblTlv( &(lbl_W_R_Msg->frLblTlv), 
				                  tempBuf, 
				                  bufSize-totalSize);
            if (decodedSize < 0)
            {
	       PRINT_ERR("Failure when dec FR Lbl tlv from LblWithdr msg\n");
	       return MPLS_DEC_FRLBLERROR;
            }
	    PRINT_2("Decoded for Fr Lbl %d bytes\n", decodedSize);
            tempBuf        += decodedSize;
            totalSize      += decodedSize;
            totalSizeParam += decodedSize;

	    lbl_W_R_Msg->frLblTlvExists   = 1;
	    lbl_W_R_Msg->frLblTlv.baseTlv = tlvTemp;
	    DEBUG_CALL(printFrLblTlv(&(lbl_W_R_Msg->frLblTlv)));
	    break;
	 }
         case MPLS_LSPID_TLVTYPE:
	 {
            decodedSize = Mpls_decodeLdpLspIdTlv( &(lbl_W_R_Msg->lspidTlv), 
				                    tempBuf, 
				                    bufSize-totalSize);
            if (decodedSize < 0)
            {
	       PRINT_ERR("Failure when dec LSPID tlv from LblW_R msg\n");
	       return MPLS_DEC_LSPIDERROR;
            }
	    PRINT_2("Decoded for lspid tlv %d bytes\n", decodedSize);
            tempBuf        += decodedSize;
            totalSize      += decodedSize;
            totalSizeParam += decodedSize;

	    lbl_W_R_Msg->lspidTlvExists   = 1;
	    lbl_W_R_Msg->lspidTlv.baseTlv = tlvTemp;
	    DEBUG_CALL(printLspIdTlv(&(lbl_W_R_Msg->lspidTlv)));
	    break;
	 }
         default:
	 {
	    PRINT_ERR_2("Found wrong tlv type while decoding lbl withdr msg (%x)\n",
	             tlvTemp.flags.flags.type);
	    if (tlvTemp.flags.flags.uBit == 1)
	    {
	       /* ignore the Tlv and continue processing */
               tempBuf        += tlvTemp.length;
               totalSize      += tlvTemp.length;
               totalSizeParam += tlvTemp.length;
               break;
	    }
	    else
	    {
	       /* drop the message; return error */
	       return MPLS_TLVTYPEERROR;
	    }
	 }
      } /* switch type */

   } /* while */

   PRINT_2("totalsize for Mpls_decodeLdpLblWithdrawMsgIdTlv is %d\n",
	       totalSize);

   return totalSize;
   
} /* End: Mpls_decodeLdpLbl_W_R_Msg */



/* 
 * Encode for CR Tlv 
 */ 

/*
 *  encode
 */
int Mpls_encodeLdpERTlv 
(
   mplsLdpErTlv_t * erTlv, 
   u_char         * buff, 
   int              bufSize
)
{
   int      encodedSize = 0;
   u_int    totalSize   = 0;
   u_char * tempBuf     = buff; /* no change for the buff ptr */
   u_int    i;

   if (MPLS_TLVFIXLEN + (int)(erTlv->baseTlv.length)> bufSize)
   {
      /* not enough room */
      return MPLS_ENC_BUFFTOOSMALL; 
   }

   /* 
    *  encode for tlv 
    */
   encodedSize = Mpls_encodeLdpTlv( &(erTlv->baseTlv),
                                    tempBuf, 
				    MPLS_TLVFIXLEN);
   if (encodedSize < 0)
   {
      return MPLS_ENC_TLVERROR;
   }
   tempBuf   += encodedSize; 
   totalSize += encodedSize;

   if (erTlv->numberErHops > MPLS_MAX_ER_HOPS)
   {
      PRINT_ERR("MPLS_MAX_ER_HOPS is too small. Increase it if nec\n");
      return MPLS_ER_HOPSNUMERROR;
   }

   /* 
    *  encode for ER hops 
    */
   for (i = 0; i <erTlv->numberErHops ; i++)
   {
      encodedSize = Mpls_encodeLdpErHop( &(erTlv->erHopArray[i]), 
                                         tempBuf,
				         bufSize - totalSize,
			                 erTlv->erHopTypes[i]);
      if (encodedSize < 0)
      {
         return MPLS_ENC_ERHOPERROR;
      }
      tempBuf   += encodedSize; 
      totalSize += encodedSize;
   }

   return totalSize;

} /* End: Mpls_encodeLdpERTlv */


/*
 *  decode
 */
int Mpls_decodeLdpERTlv 
(
   mplsLdpErTlv_t * erTlv, 
   u_char         * buff, 
   int              bufSize,
   u_short          tlvLength
)
{
   u_char * tempBuf     = buff; /* no change for the buff ptr */
   u_char * erTlvPtr;
   u_int    i = 0;
   int      decodedSize = 0;
   u_int    erHopSize   = 0;    /* used to compute the sum of
			           all er hop elements + flags*/
   u_short  type;               /* filled in by Mpls_decodeLdpErHop
				   with the type of the ER hop 
				   decoded */
 
   if ((int)tlvLength > bufSize)
   {
      /* not enough data for Fec elements tlv*/
      PRINT_ERR("failed decoding CR tlv \n");
      return MPLS_DEC_BUFFTOOSMALL;
   }
 
   erTlvPtr = (u_char *)erTlv;
   erTlvPtr += MPLS_TLVFIXLEN; /* we want to point to the flags since the
                                 tlv was decoded before we reach here */
 
   while (tlvLength > erHopSize)
   {
      if (erTlv->numberErHops > (u_short)(MPLS_MAX_ER_HOPS - 1))
      {
         PRINT_ERR("MPLS_MAX_ER_HOPS is too small. Increase it if nec\n");
         return MPLS_ER_HOPSNUMERROR;
      }

      decodedSize = Mpls_decodeLdpErHop( &(erTlv->erHopArray[i]),
					 tempBuf,
					 bufSize - erHopSize,
					 &type);
      if(decodedSize < 0)
      {
	 return MPLS_DEC_ERHOPERROR;
      }

      erTlv->erHopTypes[i] = type;
      erTlv->numberErHops++;
      i++;

      tempBuf   += decodedSize;
      erHopSize += decodedSize;

   } /* end while */
   
   return erHopSize;

} /* End: Mpls_decodeLdpERTlv */



/* 
 * Encode for ER Hop 
 */ 

/*
 *  encode
 */
int Mpls_encodeLdpErHop
(
   mplsLdpErHop_t * erHop, 
   u_char         * buff, 
   int              bufSize,
   u_short          type
)
{
   int      encodedSize = 0;
   u_char * tempBuff    = buff;
   u_char * startPtr;
 
   switch (type)
   {
      case MPLS_ERHOP_IPV4_TLVTYPE:
      {
	 if (MPLS_ERHOP_IPV4_FIXLEN + MPLS_TLVFIXLEN > bufSize)
	 {
	    return MPLS_ENC_BUFFTOOSMALL;
	 }

         /* check how much is the preLen; should be between 0-32 */
         if (erHop->erIpv4.flags.flags.preLen > 32)
         {
            return MPLS_IPV4LENGTHERROR;
         }

	 encodedSize = Mpls_encodeLdpTlv( &(erHop->erIpv4.baseTlv),
					  tempBuff, bufSize);
         if (encodedSize < 0)
	 {
	    return MPLS_ENC_TLVERROR;
	 }
	 tempBuff  += encodedSize;
	 startPtr  = (u_char *)&(erHop->erIpv4); 
	 startPtr  += encodedSize;

	 erHop->erIpv4.flags.flags.res  = 0;
	 erHop->erIpv4.flags.mark = htonl(erHop->erIpv4.flags.mark);
	 erHop->erIpv4.address    = htonl(erHop->erIpv4.address);

         MEM_COPY( tempBuff,
		   startPtr,
                   MPLS_ERHOP_IPV4_FIXLEN);
	 encodedSize += MPLS_ERHOP_IPV4_FIXLEN;
	 break;
      }
      case MPLS_ERHOP_IPV6_TLVTYPE:
      {
	 if (MPLS_ERHOP_IPV6_FIXLEN + MPLS_TLVFIXLEN > bufSize)
	 {
	    return MPLS_ENC_BUFFTOOSMALL;
	 }
	 encodedSize = Mpls_encodeLdpTlv( &(erHop->erIpv6.baseTlv),
					  tempBuff, bufSize);
         if (encodedSize < 0)
	 {
	    return MPLS_ENC_TLVERROR;
	 }
	 tempBuff  += encodedSize;
	 startPtr  = (u_char *)&(erHop->erIpv6); 
	 startPtr  += encodedSize;

	 erHop->erIpv6.flags.flags.res  = 0;
	 erHop->erIpv6.flags.mark = htonl(erHop->erIpv6.flags.mark);

         MEM_COPY( tempBuff,
		   startPtr,
                   MPLS_ERHOP_IPV6_FIXLEN);

	 encodedSize += MPLS_ERHOP_IPV6_FIXLEN;
	 break;
      }
      case MPLS_ERHOP_AS_TLVTYPE:
      {
	 if (MPLS_ERHOP_AS_FIXLEN + MPLS_TLVFIXLEN > bufSize)
	 {
	    return MPLS_ENC_BUFFTOOSMALL;
	 }
	 encodedSize = Mpls_encodeLdpTlv( &(erHop->erAs.baseTlv),
					  tempBuff, bufSize);
         if (encodedSize < 0)
	 {
	    return MPLS_ENC_TLVERROR;
	 }
	 tempBuff  += encodedSize;
	 startPtr  = (u_char *)&(erHop->erAs); 
	 startPtr  += encodedSize;

	 erHop->erAs.flags.flags.res  = 0;
	 erHop->erAs.flags.mark = htons(erHop->erAs.flags.mark);
	 erHop->erAs.asNumber   = htons(erHop->erAs.asNumber);

         MEM_COPY( tempBuff,
		   startPtr,
                   MPLS_ERHOP_AS_FIXLEN);

	 encodedSize += MPLS_ERHOP_AS_FIXLEN;
	 break;
      }
      case MPLS_ERHOP_LSPID_TLVTYPE:
      {
	 if (MPLS_ERHOP_LSPID_FIXLEN + MPLS_TLVFIXLEN > bufSize)
	 {
	    return MPLS_ENC_BUFFTOOSMALL;
	 }
	 encodedSize = Mpls_encodeLdpTlv( &(erHop->erLspId.baseTlv),
					  tempBuff, bufSize);
         if (encodedSize < 0)
	 {
	    return MPLS_ENC_TLVERROR;
	 }
	 tempBuff  += encodedSize;
	 startPtr  = (u_char *)&(erHop->erLspId); 
	 startPtr  += encodedSize;

	 erHop->erLspId.flags.flags.res  = 0;
	 erHop->erLspId.flags.mark = htons(erHop->erLspId.flags.mark);
	 erHop->erLspId.lspid      = htons(erHop->erLspId.lspid);
	 erHop->erLspId.routerId   = htonl(erHop->erLspId.routerId);

         MEM_COPY( tempBuff,
		   startPtr,
                   MPLS_ERHOP_LSPID_FIXLEN);

	 encodedSize += MPLS_ERHOP_LSPID_FIXLEN;
	 break;
      }
      default:
      {
	 PRINT_ERR_2("Found wrong ER hop type while encoding FEC elem (%d)\n",
	             type);
	 return MPLS_ENC_ERHOPERROR;
	 break;
      }
   }  /* end: switch */

   return encodedSize;
   
} /* End: Mpls_encodeLdpErHop */

/*
 *  decode
 */
 int Mpls_decodeLdpErHop
(
   mplsLdpErHop_t * erHop, 
   u_char         * buff, 
   int              bufSize,
   u_short        * type
)
{
   int           decodedSize = 0;
   u_char       *tempBuf     = buff;
   u_char       *startPtr;
   mplsLdpTlv_t  tlvTemp;

   /*
    *  decode the tlv to check what is the type of the ER hop
    */
   decodedSize = Mpls_decodeLdpTlv(&tlvTemp, tempBuf, bufSize);
   if (decodedSize < 0)
   {
      /* something wrong */
      PRINT_ERR("ErHop decode failed for tlv\n");
      return MPLS_DEC_TLVERROR;
   }
   tempBuf += decodedSize;
 
   switch (tlvTemp.flags.flags.type)
   {
      case MPLS_ERHOP_IPV4_TLVTYPE:
      {
	 if (MPLS_ERHOP_IPV4_FIXLEN > bufSize - MPLS_TLVFIXLEN)
	 {
	    return MPLS_DEC_BUFFTOOSMALL;
	 }
         startPtr = (u_char *)&(erHop->erIpv4);
	 startPtr += decodedSize; /* skip the tlv */

         MEM_COPY( startPtr,
		   tempBuf,
                   MPLS_ERHOP_IPV4_FIXLEN);
	 erHop->erIpv4.flags.mark = ntohl(erHop->erIpv4.flags.mark);
	 erHop->erIpv4.address    = ntohl(erHop->erIpv4.address);
	 erHop->erIpv4.baseTlv    = tlvTemp;

         /* check how much is the preLen; should be between 0-32 */
         if (erHop->erIpv4.flags.flags.preLen > 32)
         {
            return MPLS_IPV4LENGTHERROR;
         }

	 decodedSize += MPLS_ERHOP_IPV4_FIXLEN;
	 break;
      }
      case MPLS_ERHOP_IPV6_TLVTYPE:
      {
	 if (MPLS_ERHOP_IPV6_FIXLEN > bufSize - MPLS_TLVFIXLEN)
	 {
	    return MPLS_DEC_BUFFTOOSMALL;
	 }
         startPtr = (u_char *)&(erHop->erIpv6);
	 startPtr += decodedSize; /* skip the tlv */

         MEM_COPY( startPtr,
		   tempBuf,
                   MPLS_ERHOP_IPV6_FIXLEN);
	 erHop->erIpv6.flags.mark = ntohl(erHop->erIpv6.flags.mark);
	 erHop->erIpv6.baseTlv    = tlvTemp;

	 decodedSize += MPLS_ERHOP_IPV6_FIXLEN;
	 break;
      }
      case MPLS_ERHOP_AS_TLVTYPE:
      {
	 if (MPLS_ERHOP_AS_FIXLEN > bufSize - MPLS_TLVFIXLEN)
	 {
	    return MPLS_DEC_BUFFTOOSMALL;
	 }
         startPtr = (u_char *)&(erHop->erAs);
	 startPtr += decodedSize; /* skip the tlv */

         MEM_COPY( startPtr,
		   tempBuf,
                   MPLS_ERHOP_AS_FIXLEN);
	 erHop->erAs.flags.mark = ntohs(erHop->erAs.flags.mark);
	 erHop->erAs.asNumber   = ntohs(erHop->erAs.asNumber);
	 erHop->erAs.baseTlv    = tlvTemp;

	 decodedSize += MPLS_ERHOP_AS_FIXLEN;
	 break;
      }
      case MPLS_ERHOP_LSPID_TLVTYPE:
      {
	 if (MPLS_ERHOP_LSPID_FIXLEN > bufSize - MPLS_TLVFIXLEN)
	 {
	    return MPLS_DEC_BUFFTOOSMALL;
	 }
         startPtr = (u_char *)&(erHop->erLspId);
	 startPtr += decodedSize; /* skip the tlv */

         MEM_COPY( startPtr,
		   tempBuf,
                   MPLS_ERHOP_LSPID_FIXLEN);
	 erHop->erLspId.flags.mark = ntohs(erHop->erLspId.flags.mark);
	 erHop->erLspId.lspid      = ntohs(erHop->erLspId.lspid);
	 erHop->erLspId.routerId   = ntohl(erHop->erLspId.routerId);
	 erHop->erLspId.baseTlv    = tlvTemp;

	 decodedSize += MPLS_ERHOP_LSPID_FIXLEN;
	 break;
      }
      default:
      {
	 PRINT_ERR_2("Found wrong ER hop type while decoding ER (%d)\n",
	             *type);
	 return MPLS_DEC_ERHOPERROR;
	 break;
      }
   } /* end: switch */

   *type = tlvTemp.flags.flags.type;
   return decodedSize;
   
} /* End: Mpls_decodeLdpErHop */



/* 
 * Encode for Traffic Tlv 
 */ 

/*
 *  encode
 */
int Mpls_encodeLdpTrafficTlv
(
   mplsLdpTrafficTlv_t * trafficTlv, 
   u_char              * buff, 
   int                   bufSize
)
{
   int      encodedSize = 0;
   u_char * tempBuf     = buff; /* no change for the buff ptr */
   u_char * trafficTlvPtr;
   u_short  tempLength;         /* to store the tlv length for
				   later use */

   if (MPLS_TLVFIXLEN + (int)(trafficTlv->baseTlv.length)> bufSize)
   {
      /* not enough room */
      return MPLS_ENC_BUFFTOOSMALL; 
   }

   tempLength = trafficTlv->baseTlv.length;
   /* 
    *  encode for tlv 
    */
   encodedSize = Mpls_encodeLdpTlv( &(trafficTlv->baseTlv),
                                    tempBuf, 
				    MPLS_TLVFIXLEN);
   if (encodedSize < 0)
   {
      return MPLS_ENC_TLVERROR;
   }
   tempBuf += encodedSize;
   trafficTlvPtr = (u_char *)trafficTlv;
   trafficTlvPtr += encodedSize;

   /*
    *   encode Traffic flags + Frequency + Reserved + Weight
    */
   encodedSize = sizeof(u_char) * 4;
   MEM_COPY( tempBuf,
	     trafficTlvPtr,
	     encodedSize);
   tempBuf       += encodedSize;
   trafficTlvPtr += encodedSize;

   /*
    *   encode for Traffic parameters 
    */
   if ( (MPLS_TRAFFICPARAMLENGTH != sizeof(float)) ||
        (sizeof(float) != sizeof(u_int)) )
   {
      PRINT_ERR_2("There is not compatibility for float type (%d)\n",
		   (int)sizeof(float));
      return MPLS_FLOATTYPEERROR;
   }

   trafficTlv->pdr.mark = htonl(trafficTlv->pdr.mark);
   trafficTlv->pbs.mark = htonl(trafficTlv->pbs.mark);
   trafficTlv->cdr.mark = htonl(trafficTlv->cdr.mark);
   trafficTlv->cbs.mark = htonl(trafficTlv->cbs.mark);
   trafficTlv->ebs.mark = htonl(trafficTlv->ebs.mark);

   MEM_COPY( tempBuf,
	     trafficTlvPtr,
	     MPLS_TRAFFICPARAMLENGTH * 5);

   return (MPLS_TLVFIXLEN + tempLength);

} /* End: Mpls_encodeLdpTrafficTlv */

/*
 *  decode
 */
int Mpls_decodeLdpTrafficTlv
(
   mplsLdpTrafficTlv_t * trafficTlv, 
   u_char              * buff, 
   int                   bufSize,
   u_short               tlvLength
)
{
   u_char *     tempBuf     = buff; /* no change for the buff ptr */
   int          decodedSize = 0;
   u_char *     trafficTlvPtr;
 
   if ((int)tlvLength > bufSize)
   {
      /* not enough data for Fec elements tlv*/
      PRINT_ERR("failed decoding Traffic tlv \n");
      return MPLS_DEC_BUFFTOOSMALL;
   }
   trafficTlvPtr = (u_char *)trafficTlv;
   trafficTlvPtr += MPLS_TLVFIXLEN;

   /*
    *   decode Traffic flags + Frequency + Reserved + Weight
    */
   decodedSize = sizeof(u_char) * 4;
   MEM_COPY( trafficTlvPtr,
	     tempBuf,
	     decodedSize);
   tempBuf       += decodedSize;
   trafficTlvPtr += decodedSize;

   /* 
    * decode the traffic parameters
    */
   if (MPLS_TRAFFICPARAMLENGTH != sizeof(float))
   {
      PRINT_ERR_2("There is not compatibility for float type (%d)\n",
		   decodedSize);
      return MPLS_FLOATTYPEERROR;
   }
   MEM_COPY( trafficTlvPtr,
	     tempBuf,
	     MPLS_TRAFFICPARAMLENGTH * 5);

   trafficTlv->pdr.mark = ntohl(trafficTlv->pdr.mark);
   trafficTlv->pbs.mark = ntohl(trafficTlv->pbs.mark);
   trafficTlv->cdr.mark = ntohl(trafficTlv->cdr.mark);
   trafficTlv->cbs.mark = ntohl(trafficTlv->cbs.mark);
   trafficTlv->ebs.mark = ntohl(trafficTlv->ebs.mark);

   return tlvLength;
 
} /* End: Mpls_decodeLdpTrafficTlv */



/* 
 * Encode for Preempt Tlv 
 */ 

/*
 *  encode
 */
int Mpls_encodeLdpPreemptTlv 
(
   mplsLdpPreemptTlv_t * preemptTlv, 
   u_char              * buff, 
   int                   bufSize
)
{
   int      encodedSize = 0;
   u_char * tempBuf     = buff; /* no change for the buff ptr */
   u_char * preemptTlvPtr;

   if (MPLS_TLVFIXLEN + MPLS_PREEMPTTLV_FIXLEN > bufSize)
   {
      /* not enough room */
      return MPLS_ENC_BUFFTOOSMALL; 
   }

   /* 
    *  encode for tlv 
    */
   encodedSize = Mpls_encodeLdpTlv( &(preemptTlv->baseTlv),
                                    tempBuf, 
				    MPLS_TLVFIXLEN);
   if (encodedSize < 0)
   {
      return MPLS_ENC_TLVERROR;
   }
   tempBuf += encodedSize;

   preemptTlv->res = 0;
   preemptTlvPtr   = (u_char*)preemptTlv;
   preemptTlvPtr   += encodedSize;

   MEM_COPY( tempBuf,
	     preemptTlvPtr,
	     MPLS_PREEMPTTLV_FIXLEN);

   return (MPLS_TLVFIXLEN + MPLS_PREEMPTTLV_FIXLEN);

} /* End: Mpls_encodeLdpPreemptTlv */


/*
 *  decode
 */
int Mpls_decodeLdpPreemptTlv 
(
   mplsLdpPreemptTlv_t * preemptTlv, 
   u_char              * buff, 
   int                   bufSize
)
{
   u_char * preemptTlvPtr;

   if (MPLS_PREEMPTTLV_FIXLEN > bufSize)
   {
      PRINT_ERR("failed decoding preempt tlv\n");
      return MPLS_DEC_BUFFTOOSMALL;
   }
   preemptTlvPtr = (u_char *)preemptTlv;
   preemptTlvPtr += MPLS_TLVFIXLEN;
 
   MEM_COPY( preemptTlvPtr,
	     buff,
             MPLS_PREEMPTTLV_FIXLEN);

   return MPLS_PREEMPTTLV_FIXLEN;

} /* End: Mpls_decodeLdpPreemptTlv */



/* 
 * Encode for LSPID Tlv 
 */ 

/*
 *  encode
 */
int Mpls_encodeLdpLspIdTlv 
(
   mplsLdpLspIdTlv_t * lspIdTlv, 
   u_char            * buff, 
   int                 bufSize
)
{
   int      encodedSize = 0;
   u_char * tempBuf     = buff; /* no change for the buff ptr */
   u_char * lspIdTlvPtr;

   if (MPLS_TLVFIXLEN + MPLS_LSPIDTLV_FIXLEN > bufSize)
   {
      /* not enough room */
      return MPLS_ENC_BUFFTOOSMALL; 
   }

   /* 
    *  encode for tlv 
    */
   encodedSize = Mpls_encodeLdpTlv( &(lspIdTlv->baseTlv),
                                    tempBuf, 
				    MPLS_TLVFIXLEN);
   if (encodedSize < 0)
   {
      return MPLS_ENC_TLVERROR;
   }
   tempBuf   += encodedSize;

   lspIdTlvPtr   = (u_char*)lspIdTlv;
   lspIdTlvPtr   += encodedSize;

   lspIdTlv->res          = 0;
   lspIdTlv->localCrlspId = htons(lspIdTlv->localCrlspId);
   lspIdTlv->routerId     = htonl(lspIdTlv->routerId);

   MEM_COPY( tempBuf,
	     lspIdTlvPtr,
	     MPLS_LSPIDTLV_FIXLEN);

   return (MPLS_TLVFIXLEN + MPLS_LSPIDTLV_FIXLEN);

} /* End: Mpls_encodeLdpLspIdTlv */


/*
 *  decode
 */
int Mpls_decodeLdpLspIdTlv 
(
   mplsLdpLspIdTlv_t * lspIdTlv, 
   u_char            * buff, 
   int                 bufSize
)
{
   u_char * lspIdTlvPtr;

   if (MPLS_PREEMPTTLV_FIXLEN > bufSize)
   {
      PRINT_ERR("failed decoding LspId\n");
      return MPLS_DEC_BUFFTOOSMALL;
   }
   lspIdTlvPtr = (u_char*)lspIdTlv;
   lspIdTlvPtr += MPLS_TLVFIXLEN;
 
   MEM_COPY( lspIdTlvPtr,
	     buff,
             MPLS_LSPIDTLV_FIXLEN);

   lspIdTlv->localCrlspId = ntohs(lspIdTlv->localCrlspId);
   lspIdTlv->routerId     = ntohl(lspIdTlv->routerId);

   return MPLS_LSPIDTLV_FIXLEN;

} /* End:  Mpls_decodeLdpLspIdTlv*/



/* 
 * Encode for Resource Class Tlv 
 */ 

/*
 *  encode
 */
int Mpls_encodeLdpResClsTlv 
(
   mplsLdpResClsTlv_t * resClsTlv, 
   u_char             * buff, 
   int                  bufSize
)
{
   int      encodedSize = 0;
   u_char * tempBuf     = buff; /* no change for the buff ptr */

   if (MPLS_TLVFIXLEN + (int)sizeof(u_int) > bufSize)
   {
      /* not enough room */
      return MPLS_ENC_BUFFTOOSMALL; 
   }

   /* 
    *  encode for tlv 
    */
   encodedSize = Mpls_encodeLdpTlv( &(resClsTlv->baseTlv),
                                    tempBuf, 
				    MPLS_TLVFIXLEN);
   if (encodedSize < 0)
   {
      return MPLS_ENC_TLVERROR;
   }
   tempBuf   += encodedSize;

   resClsTlv->rsCls = htonl(resClsTlv->rsCls);

   MEM_COPY( tempBuf,
	     (u_char *)&(resClsTlv->rsCls),
	     sizeof(u_int));

   return (MPLS_TLVFIXLEN + sizeof(u_int));

} /* End: Mpls_encodeLdpResClsTlv */


/*
 *  decode
 */
int Mpls_decodeLdpResClsTlv 
(
   mplsLdpResClsTlv_t * resClsTlv, 
   u_char             * buff, 
   int                  bufSize
)
{
   if ((int)sizeof(u_int) > bufSize)
   {
      PRINT_ERR("failed decoding resClass tlv\n");
      return MPLS_DEC_BUFFTOOSMALL;
   }
 
   MEM_COPY( (u_char *)&(resClsTlv->rsCls),
	     buff,
             sizeof(u_int));
   resClsTlv->rsCls = ntohl(resClsTlv->rsCls);

   return sizeof(u_int);

} /* End: Mpls_decodeLdpResClsTlv */



/* 
 * Encode for Route Pinning Tlv 
 */ 

/*
 *  encode
 */
int Mpls_encodeLdpPinningTlv 
(
   mplsLdpPinningTlv_t * pinningTlv, 
   u_char              * buff, 
   int                   bufSize
)
{
   int      encodedSize = 0;
   u_char * tempBuf     = buff; /* no change for the buff ptr */

   if (MPLS_TLVFIXLEN + (int)sizeof(u_int) > bufSize)
   {
      /* not enough room */
      return MPLS_ENC_BUFFTOOSMALL; 
   }

   /* 
    *  encode for tlv 
    */
   encodedSize = Mpls_encodeLdpTlv( &(pinningTlv->baseTlv),
                                    tempBuf, 
				    MPLS_TLVFIXLEN);
   if (encodedSize < 0)
   {
      return MPLS_ENC_TLVERROR;
   }
   tempBuf += encodedSize;

   pinningTlv->flags.flags.res = 0;
   pinningTlv->flags.mark      = htonl(pinningTlv->flags.mark);

   MEM_COPY( tempBuf,
	     (u_char *)&(pinningTlv->flags.mark),
	     sizeof(u_int));

   return (MPLS_TLVFIXLEN + sizeof(u_int));

} /* End: Mpls_encodeLdpPinningTlv*/


/*
 *  decode
 */
int Mpls_decodeLdpPinningTlv 
(
   mplsLdpPinningTlv_t * pinningTlv, 
   u_char              * buff, 
   int                   bufSize
)
{
   if ((int)sizeof(u_int) > bufSize)
   {
      PRINT_ERR("failed decoding route pinning tlv\n");
      return MPLS_DEC_BUFFTOOSMALL;
   }
 
   MEM_COPY( (u_char *)&(pinningTlv->flags.mark),
	     buff,
             sizeof(u_int));
   pinningTlv->flags.mark = ntohl(pinningTlv->flags.mark);

   return sizeof(u_int);

} /* End: Mpls_decodeLdpPinningTlv */


/* 
 * Label Abort Request Message
 */ 

/*
 *  encode
 */
int Mpls_encodeLdpLblAbortMsg 
(
   mplsLdpLblAbortMsg_t * lblAbortMsg, 
   u_char               * buff, 
   int                  bufSize
)
{
   mplsLdpLblAbortMsg_t lblAbortMsgCopy;
   int                  encodedSize = 0;
   u_int                totalSize   = 0;
   u_char *             tempBuf     = buff; /* no change for the buff ptr */

   /* check the length of the messageId + param */
   if ((int)(lblAbortMsg->baseMsg.msgLength) + MPLS_TLVFIXLEN > bufSize) 
   {
      PRINT_ERR("failed to encode the lbl abort request msg: BUFFER TOO SMALL\n");
      return MPLS_ENC_BUFFTOOSMALL;
   }

   lblAbortMsgCopy = *lblAbortMsg;

   /*
    *  encode the base part of the pdu message
    */
   encodedSize = Mpls_encodeLdpBaseMsg( &(lblAbortMsgCopy.baseMsg), 
				        tempBuf, 
				        bufSize);
   if (encodedSize < 0)
   {
      return MPLS_ENC_BASEMSGERROR;
   }
   PRINT_2("Encode BaseMsg for label abort request msg on %d bytes\n", encodedSize);
   tempBuf   += encodedSize;
   totalSize += encodedSize;

   /*
    *  encode the tlv if any
    */
   if (lblAbortMsgCopy.fecTlvExists)
   {
      encodedSize = Mpls_encodeLdpFecTlv( &(lblAbortMsgCopy.fecTlv), 
				          tempBuf, 
				          bufSize-totalSize);
      if (encodedSize < 0)
      {
         return MPLS_ENC_FECERROR;
      }
      PRINT_2("Encoded for FEC Tlv %d bytes\n", encodedSize);
      tempBuf   += encodedSize;
      totalSize += encodedSize;
   }
   if (lblAbortMsgCopy.lblMsgIdTlvExists)
   {
      encodedSize = Mpls_encodeLdpLblMsgIdTlv( &(lblAbortMsgCopy.lblMsgIdTlv), 
				               tempBuf, 
				               bufSize-totalSize);
      if (encodedSize < 0)
      {
         return MPLS_ENC_LBLMSGIDERROR;
      }
      PRINT_2("Encoded for lbl request msg id Tlv %d bytes\n", encodedSize);
      tempBuf   += encodedSize;
      totalSize += encodedSize;
   }

   return totalSize;

} /* End: Mpls_encodeLdpLblAbortMsg */


/*
 *  decode
 */
int Mpls_decodeLdpLblAbortMsg 
(
   mplsLdpLblAbortMsg_t * lblAbortMsg, 
   u_char               * buff, 
   int                  bufSize
)
{
   int          decodedSize    = 0;
   u_int        totalSize      = 0;
   u_int        stopLength     = 0;
   u_int        totalSizeParam = 0;
   u_char     * tempBuf        = buff; /* no change for the buff ptr */
   mplsLdpTlv_t tlvTemp;

   /*
    *  decode the base part of the pdu message
    */
   bzero(lblAbortMsg, sizeof(mplsLdpLblAbortMsg_t));
   decodedSize = Mpls_decodeLdpBaseMsg( &(lblAbortMsg->baseMsg), 
				        tempBuf, 
				        bufSize);
   if (decodedSize < 0)
   {
      return MPLS_DEC_BASEMSGERROR;
   }
   PRINT_2("Decode BaseMsg for Lbl Abort Request Msg on %d bytes\n", decodedSize);

   if (lblAbortMsg->baseMsg.flags.flags.msgType != MPLS_LBLABORT_MSGTYPE)
   {
      PRINT_ERR_2("Not the right message type; expected lbl abort and got %x\n",
		   lblAbortMsg->baseMsg.flags.flags.msgType);
      return MPLS_MSGTYPEERROR;
   }

   tempBuf   += decodedSize;
   totalSize += decodedSize;

   if (bufSize-totalSize <= 0)
   {
      /* nothing left for decoding */
      PRINT_ERR("Lbl Abort msg does not have anything beside base msg\n");
      return totalSize;
   }

   PRINT_4("bufSize = %d,  totalSize = %d, lblAbortMsg->baseMsg.msgLength = %d\n",
	   bufSize, totalSize, lblAbortMsg->baseMsg.msgLength);

   /* Have to check the baseMsg.msgLength to know when to finish.
    * We finsh when the totalSizeParam is >= to the base message length - the
    * message id length (4) 
    */
    
   stopLength = lblAbortMsg->baseMsg.msgLength - MPLS_MSGIDFIXLEN;
   while (stopLength > totalSizeParam)
   {
      /*
       *  decode the tlv to check what's next
       */
      bzero(&tlvTemp, MPLS_TLVFIXLEN);
      decodedSize = Mpls_decodeLdpTlv(&tlvTemp, tempBuf, bufSize-totalSize);
      if (decodedSize < 0)
      {
         /* something wrong */
         PRINT_ERR("Label Abort msg decode failed for tlv\n");
         return MPLS_DEC_TLVERROR;
      }

      tempBuf        += decodedSize;
      totalSize      += decodedSize;
      totalSizeParam += decodedSize;

      switch (tlvTemp.flags.flags.type)
      {
         case MPLS_FEC_TLVTYPE:
	 {
            decodedSize = Mpls_decodeLdpFecTlv( &(lblAbortMsg->fecTlv), 
				                tempBuf, 
				                bufSize-totalSize,
						tlvTemp.length);
            if (decodedSize < 0)
            {
	       PRINT_ERR("Failure when decoding FEC tlv from LblAbort msg\n");
	       return MPLS_DEC_FECERROR;
            }
	    PRINT_2("Decoded for FEC %d bytes\n", decodedSize);
            tempBuf        += decodedSize;
            totalSize      += decodedSize;
            totalSizeParam += decodedSize;

	    lblAbortMsg->fecTlvExists   = 1;
	    lblAbortMsg->fecTlv.baseTlv = tlvTemp;
	    DEBUG_CALL(printFecListTlv(&(lblAbortMsg->fecTlv)));
	    break;
	 }
         case MPLS_REQMSGID_TLVTYPE:
	 {
            decodedSize = Mpls_decodeLdpLblMsgIdTlv( &(lblAbortMsg->lblMsgIdTlv), 
				                     tempBuf, 
				                     bufSize-totalSize);
            if (decodedSize < 0)
            {
	       PRINT_ERR("Failure when dec LblMsgId tlv from LblAbort msg\n");
	       return MPLS_DEC_LBLMSGIDERROR;
            }
	    PRINT_2("Decoded for LblMsgId %d bytes\n", decodedSize);
            tempBuf        += decodedSize;
            totalSize      += decodedSize;
            totalSizeParam += decodedSize;

	    lblAbortMsg->lblMsgIdTlvExists   = 1;
	    lblAbortMsg->lblMsgIdTlv.baseTlv = tlvTemp;
	    DEBUG_CALL(printLblMsgIdTlv(&(lblAbortMsg->lblMsgIdTlv)));
	    break;
	 }
         default:
	 {
	    PRINT_ERR_2("Found wrong tlv type while decoding lbl abort msg (%x)\n",
	             tlvTemp.flags.flags.type);
	    if (tlvTemp.flags.flags.uBit == 1)
	    {
	       /* ignore the Tlv and continue processing */
               tempBuf        += tlvTemp.length;
               totalSize      += tlvTemp.length;
               totalSizeParam += tlvTemp.length;
               break;
	    }
	    else
	    {
	       /* drop the message; return error */
	       return MPLS_TLVTYPEERROR;
	    }
	 }
      } /* switch type */

   } /* while */

   PRINT_2("totalsize for Mpls_decodeLdpLblAbortMsg is %d\n", totalSize);

   return totalSize;
   
} /* End: Mpls_decodeLdpLblAbortMsg */


/*
 *   DEBUG functions 
 */
void printTlv(mplsLdpTlv_t *tlv)
{
   printf("\t Tlv:\n");
   printf("\t BaseTlv: uBit = %d\n \t\t  fBit = %d\n \t\t  type = %x\n \t\t  length = %d\n",
	    tlv->flags.flags.uBit, 
	    tlv->flags.flags.fBit,
	    tlv->flags.flags.type, 
	    tlv->length);
}

void printHeader(mplsLdpHeader_t *header)
{
   printf("LPD Header : protocolVersion = %d\n   \tpduLength = %d\n   \tlsrAddress = %x\n   \tlabelSpace = %x\n",
			header->protocolVersion, header->pduLength, header->lsrAddress,
			header->labelSpace);
}
 
void printCspFlags(mplsLdpCspFlag_t * cspFlags)
{
   printf("\tCSP Flags: lad = %d, ld = %d, pvl = %d, res = %d\n", 
             cspFlags->lad,
             cspFlags->ld,
             cspFlags->pvl,
             cspFlags->res);  
}

void printCspFlagsPerByte(u_short * cspFlags)
{
   u_char *ptr;
   ptr = (u_char*)cspFlags;
   printf("\tCSP Flags: (byte 0) %x\n \t\t (byte 1) %x\n", *ptr++, *ptr);  
}

void printCspTlv(mplsLdpCspTlv_t * csp)
{
   printf("\tCSP:\n");
   printTlv(&(csp->baseTlv));
   printf("\tcsp        : protocolVersion = %d\n \t\tholdTime = %d\n \t\tmaxPduLen = %d\n \t\trcvLsrAddress = %s\n \t\trcvLsId = %d\n", 
	   csp->protocolVersion, csp->holdTime,
	   csp->maxPduLen, inet_ntoa(csp->rcvLsrAddress), csp->rcvLsId);
   printCspFlags(&(csp->flags.flags));
}

void printAspFlags(mplsLdpSPFlag_t * aspFlags)
{
   printf("\t ASP Flags: mergeType = %d, numLblRng = %d, dir = %d, res = %d\n", 
            aspFlags->mergeType, 
            aspFlags->numLblRng, 
            aspFlags->dir,
            aspFlags->res);
}

void printAspFlagsPerByte(u_int * aspFlags)
{
   u_char *ptr;
   ptr = (u_char*)aspFlags;
   printf("\tASP Flags: (byte 0) %x\n \t\t (byte 1) %x\n \t\t (byte 2) %x\n\t\t (byte 3) %x\n",
   *ptr++, *ptr++, *ptr++, *ptr);  
}


void printAtmLabel(mplsLdpAtmLblRng_t *label, int i)
{
   printf("\tATM LABEL (%d) : res1 = %d, minVci = %d, minVpi = %d, res2 = %d, maxVci = %d, maxVpi = %d\n",
	  i,
          label->flags.flags.res1, 
          label->flags.flags.minVci, 
	  label->flags.flags.minVpi, 
          label->flags.flags.res2, 
	  label->flags.flags.maxVci, 
	  label->flags.flags.maxVpi);
}

void printAspTlv(mplsLdpAspTlv_t *asp)
{
   int i = 0;

   printf("\t asp:\n");
   printTlv(&(asp->baseTlv));
   printf("\t asp labes (%d)\n", (int)(asp->flags.flags.numLblRng));
   for (i = 0; i < (int)(asp->flags.flags.numLblRng); i++)
   {
      printAtmLabel(&(asp->lblRngList[i]), i);
   }
   printAspFlags(&(asp->flags.flags));
}

void printFspFlags(mplsLdpSPFlag_t * fspFlags)
{
   printf("\t FSP Flags: mergeType = %d, numLblRng = %d, dir = %d, res = %d\n", 
	    fspFlags->mergeType,
	    fspFlags->numLblRng,
	    fspFlags->dir,
	    fspFlags->res);
}

void printFspLabel(mplsLdpFrLblRng_t *label, int i)
{
   printf("\tFR LABEL (%d) : res_max = %d, maxDlci = %d, res_min = %d, len = %d minDlci = %d\n",
	  i,
          label->flags.flags.res_max, 
          label->flags.flags.maxDlci, 
          label->flags.flags.res_min, 
          label->flags.flags.len, 
          label->flags.flags.minDlci); 
}

void printFspTlv(mplsLdpFspTlv_t *fsp)
{
   int i = 0;

   printf("\t fsp:\n");
   printTlv(&(fsp->baseTlv));
   printf("\t fsp labes (%d)\n", (int)(fsp->flags.flags.numLblRng));
   for (i = 0; i < (int)(fsp->flags.flags.numLblRng); i++)
   {
      printFspLabel(&(fsp->lblRngList[i]), i);
   }
   printFspFlags(&(fsp->flags.flags));
}

void printMsgBase(mplsLdpMsg_t * msg)
{
   printf("\tbaseMsg : uBit = %d\n \t\t  msgType = %x\n   \t\t  msgLength = %d\n   \t\t  msgId = %d\n", 
	   msg->flags.flags.uBit,
	   msg->flags.flags.msgType,
	   msg->msgLength,
	   msg->msgId);
}
void printInitMsg(mplsLdpInitMsg_t *initMsg)
{
   printf("INIT MSG ***START***:\n");
   printMsgBase(&(initMsg->baseMsg));
   if (initMsg->cspExists)
   {
      printCspTlv(&(initMsg->csp));
   }
   else
   {
      printf("\tINIT msg does NOT have CSP\n");
   }
   if (initMsg->aspExists)
   {
      printAspTlv(&(initMsg->asp));
   }
   else
   {
      printf("\tINIT msg does NOT have ASP\n");
   }
   if (initMsg->fspExists)
   {
      printFspTlv(&(initMsg->fsp));
   }
   else
   {
      printf("\tINIT msg does NOT have FSP\n");
   }
   printf("\nINIT MSG ***END***\n");
}

void printRetMsgTlv(mplsLdpRetMsgTlv_t * retMsg)
{
   printf("\t retMsgTlv:\n");
   printTlv(&(retMsg->baseTlv));
   printf("\t retMsgTlv.data is %s\n", retMsg->data);
}

void printRetPduTlv(mplsLdpRetPduTlv_t * retPdu)
{
   printf("\t retPduTlv:\n");
   printTlv(&(retPdu->baseTlv));
   printf("\t retPduTlv.data is %s\n", retPdu->data);
}

void printExStatusTlv(mplsLdpExStatusTlv_t * status)
{
   printf("\t exStatusTlv:\n");
   printTlv(&(status->baseTlv));
   printf("\t exStatus data: value = %d\n", status->value);
}

void printStatusTlv(mplsLdpStatusTlv_t * status)
{
   printf("\t statusTlv:\n");
   printTlv(&(status->baseTlv));
   printf("\t status data:   msgId = %x\n \t\t\tmsgType = %x\n", 
	   status->msgId, status->msgType);
   printf("\t status Flags:  error = %d\n \t\t\tforward = %d\n \t\t\tstatus = %d\n", 
	   status->flags.flags.error, 
	   status->flags.flags.forward, 
	   status->flags.flags.status);
}

void printNotMsg(mplsLdpNotifMsg_t * notMsg)
{
   printf("NOTIF MSG ***START***:\n");
   printMsgBase(&(notMsg->baseMsg));

   if (notMsg->statusTlvExists)
   {
      printStatusTlv(&(notMsg->status));
   }
   else
   {
      printf("\tNotif msg does not have Status TLV\n");
   }
   if (notMsg->exStatusTlvExists)
   {
      printExStatusTlv(&(notMsg->exStatus));
   }
   else
   {
      printf("\tNotif msg does not have Extended Status TLV\n");
   }
   if (notMsg->retPduTlvExists)
   {
      printRetPduTlv(&(notMsg->retPdu));
   }
   else
   {
      printf("\tNotif msg does not have Return PDU\n");
   }
   if (notMsg->retMsgTlvExists)
   {
      printRetMsgTlv(&(notMsg->retMsg));
   }
   else
   {
      printf("\tNotif msg does not have Return MSG\n");
   }
   printf("NOTIF MSG ***END***:\n");
}

void printCsnTlv(mplsLdpCsnTlv_t *csn)
{
   printf("\t csnTlv:\n");
   printTlv(&(csn->baseTlv));
   printf("\t csnTlv data: value = %d\n", csn->seqNumber);
}

void printTrAdrTlv(mplsLdpTrAdrTlv_t *trAdr)
{
   printf("\t trAdrTlv:\n");
   printTlv(&(trAdr->baseTlv));
   printf("\t trAdrTlv data: value = %08x\n", trAdr->address);
}

void printChpTlv(mplsLdpChpTlv_t *chp)
{
   printf("\t chpTlv:\n");
   printTlv(&(chp->baseTlv));
   printf("\t chpTlv data: holdTime = %d\n", chp->holdTime);
   printf("\t chpTlv Flags:  target = %d\n \t\t\trequest = %d\n \t\t\tres = %d\n", 
           chp->flags.flags.target, 
           chp->flags.flags.request,
           chp->flags.flags.res);
}

void printHelloMsg(mplsLdpHelloMsg_t * helloMsg)
{
   printf("HELLO MSG ***START***:\n");
   printMsgBase(&(helloMsg->baseMsg));

   if (helloMsg->chpTlvExists)
   {
      printChpTlv(&(helloMsg->chp));
   }
   else
   {
      printf("\tHello msg does not have Chp TLV\n");
   }
   if (helloMsg->trAdrTlvExists)
   {
      printTrAdrTlv(&(helloMsg->trAdr));
   }
   else
   {
      printf("\tHello msg does not have TrAdr TLV\n");
   }
   if (helloMsg->csnTlvExists)
   {
      printCsnTlv(&(helloMsg->csn));
   }
   else
   {
      printf("\tHello msg does not have Csn TLV\n");
   }
   printf("HELLO MSG ***END***:\n");
}

void printKeepAliveMsg(mplsLdpKeepAlMsg_t *keepAliveMsg)
{
   printf("KEEP ALIVE MSG ***START***:\n");
   printMsgBase(&(keepAliveMsg->baseMsg));
   printf("KEEP ALIVE MSG ***END***:\n");
}

void printAdrListTlv(mplsLdpAdrTlv_t *adrList)
{
   u_short    i;
   u_short    length;

   printf("\t adrListTlv:\n");
   printTlv(&(adrList->baseTlv));
   printf("\t adrListTlv data: addrFamily = %x\n", adrList->addrFamily);

   /* get the current length of the encoding for addresses */

   length = adrList->baseTlv.length - MPLS_ADDFAMFIXLEN;
   printf("\t adrListTlv addresses (with %d addresses) :\n", length/4);
   for (i = 0; i < (u_short)(length/4); i++)
   {
      if (i%4 == 0)
      {  
	 printf("\n\t\t\t");
      }
      printf("%02x  ", adrList->address[i]);
   }
   printf("\n");
}


void printAddressMsg(mplsLdpAdrMsg_t *adrMsg)
{
   if (adrMsg->baseMsg.flags.flags.msgType == MPLS_ADDR_MSGTYPE)
      printf("ADDRESS MSG ***START***:\n");
   else if (adrMsg->baseMsg.flags.flags.msgType == MPLS_ADDRWITH_MSGTYPE)
      printf("ADDRESS WITHDRAW MSG ***START***:\n");

   printMsgBase(&(adrMsg->baseMsg));

   if (adrMsg->adrListTlvExists)
   {
      printAdrListTlv(&(adrMsg->addressList));
   }
   else
   {
      printf("\tAddress msg does not have addrList Tlv\n");
   }
   if (adrMsg->baseMsg.flags.flags.msgType == MPLS_ADDR_MSGTYPE)
      printf("ADDRESS MSG ***END***:\n");
   else if (adrMsg->baseMsg.flags.flags.msgType == MPLS_ADDRWITH_MSGTYPE)
      printf("ADDRESS WITHDRAW MSG ***END***:\n");
}

void printFecListTlv(mplsLdpFecTlv_t *fecTlv)
{
   u_short    i;
   u_short    j;
   printf("\t fecTlv:\n");
   printTlv(&(fecTlv->baseTlv));
   printf("\t\tfecTlv->numberFecElements = %d\n", fecTlv->numberFecElements);
   for (i = 0; i < fecTlv->numberFecElements; i++)
   {
      printf("\t\telem %d type is %d\n", i, fecTlv->fecElemTypes[i]);
      switch (fecTlv->fecElemTypes[i])
      {
         case MPLS_PREFIX_FEC:
         case MPLS_HOSTADR_FEC:
         {
             printf("\t\tFec Element : type = %d, addFam = %x, ",
	        fecTlv->fecElArray[i].addressEl.type, 
	        fecTlv->fecElArray[i].addressEl.addressFam);
             printf("preLen = %d, address = %x\n", 
	        fecTlv->fecElArray[i].addressEl.preLen,
	        fecTlv->fecElArray[i].addressEl.address);
             break;
         }
         case MPLS_MARTINIVC_FEC:
         {
            printf("\t\tMartini VC Element : type = %d, cBit = %d, ",
               fecTlv->fecElArray[i].martiniVcEl.flags.flags.type,
               fecTlv->fecElArray[i].martiniVcEl.flags.flags.control);
            printf("VC type = 0x%x\n",
               fecTlv->fecElArray[i].martiniVcEl.flags.flags.vcType);
            printf("\t\t\tVC info len = %d group id = %d\n",
               fecTlv->fecElArray[i].martiniVcEl.flags.flags.vcInfoLen,
               fecTlv->fecElArray[i].martiniVcEl.groupId);

            if (fecTlv->fecElArray[i].martiniVcEl.flags.flags.vcInfoLen)
            {
               printf("\t\t\tVC id = %d :\n",
                  fecTlv->fecElArray[i].martiniVcEl.vcId);
               for (j=0; j<fecTlv->fecElArray[i].martiniVcEl.vcIfParamsLen; j++)
               {
                  u_char *value = (u_char*)&fecTlv->fecElArray[i].martiniVcEl.vcIfParams[j].value;
                  u_char id = fecTlv->fecElArray[i].martiniVcEl.vcIfParams[j].id;
                  u_char len = fecTlv->fecElArray[i].martiniVcEl.vcIfParams[j].len;
                  len -= MPLS_VCIFPARAM_HDR;

                  switch (id)
                  {
                     case MPLS_VCIFPARAM_MTU:
                     {
                        u_short mtu;

                        MEM_COPY (&mtu, value, len);
                        printf("\t\t\t\tMTU = %d\n", ntohs(mtu));
                        break;
                     }
                     case MPLS_VCIFPARAM_INFO:
                     {
                        char string[81];
                        MEM_COPY (string, value, len);
                        string[len] = '\0';
                        printf("\t\t\t\tIntf info = %s\n",string);
                        break; 
                     }
                     default:
                     {
                        int max = 8;
                        int k;

                        printf("\t\t\t\tType = %d:\n", id);
                        for (k = 0; k < len; k++)
                        {
                           if (!(k % max))
                           {
                              printf("\t\t\t\t\t");
                           }
                           printf("0x%02x ", value[k]);
                           if ((k % max) == (max - 1))
                           {
                              printf("\n");
                           }
                        }
                        printf("\n");
                        break;
                     }
                  }
               }
            }
         }
      }
   }
   printf("\n");
   printf("\tfecTlv.wcElemExists = %d\n", fecTlv->wcElemExists);

}

void printLblMsgIdTlv(mplsLdpLblMsgIdTlv_t * lblMsgId)
{
   printf("\t lblMsgIdTlv:\n");
   printTlv(&(lblMsgId->baseTlv));
   printf("\t LblMsgId data:  msgId = %d\n", lblMsgId->msgId);
}

void printPathVecTlv(mplsLdpPathTlv_t* pathVec)
{
   u_int i, numlsrId;

   printf("\t pathVecTlv:\n");
   printTlv(&(pathVec->baseTlv));
   printf("\t PathVec data: " );

   numlsrId = pathVec->baseTlv.length / 4;

   for (i = 0; i< numlsrId; i++)
   {
      if(i == 0)
         printf("lsrId[%d] = %x\n", i, pathVec->lsrId[i]);
      else
         printf("\t\t\tlsrId[%d] = %x\n", i, pathVec->lsrId[i]);
   }
   printf("\n");
}

void printHopTlv(mplsLdpHopTlv_t * hopCount)
{
   printf("\t hopCountTlv:\n");
   printTlv(&(hopCount->baseTlv));
   printf("\t hopCount data:  hcValue = %d\n", hopCount->hcValue);
}

void printFrLblTlv(mplsLdpFrLblTlv_t * fr)
{
   printf("\t frTlv :\n");
   printTlv(&(fr->baseTlv));
   printf("\t Fr flags: res = %d\n \t\t len = %d\n \t\tdlci = %d\n", 
	    fr->flags.flags.res, fr->flags.flags.len, fr->flags.flags.dlci);
}

void printAtmLblTlv(mplsLdpAtmLblTlv_t * atm)
{
   printf("\t atmTlv :\n");
   printTlv(&(atm->baseTlv));
   printf("\t Atm flags: res = %d\n \t\t v = %d\n \t\tvpi = %d\n", 
	    atm->flags.flags.res, atm->flags.flags.v, atm->flags.flags.vpi);
   printf("\t Atm data : vci = %d\n", atm->vci); 
}

void printGenLblTlv(mplsLdpGenLblTlv_t * genLbl)
{
   printf("\t genLblTlv:\n");
   printTlv(&(genLbl->baseTlv));
   printf("\t genLbl data: label = %d\n", genLbl->label);
}


void printLlbMapMsg(mplsLdpLblMapMsg_t *lblMapMsg)
{
   printf("LABEL MAPPING MSG ***START***:\n");
   printMsgBase(&(lblMapMsg->baseMsg));

   if (lblMapMsg->fecTlvExists)
   {
      printFecListTlv(&(lblMapMsg->fecTlv));
   }
   else
   {
      printf("\tLabel mapping msg does not have fec Tlv\n");
   }
   if (lblMapMsg->genLblTlvExists)
   {
      printGenLblTlv(&(lblMapMsg->genLblTlv));
   }
   else
   {
      printf("\tLabel mapping msg does not have gen label Tlv\n");
   }
   if (lblMapMsg->atmLblTlvExists)
   {
      printAtmLblTlv(&(lblMapMsg->atmLblTlv));
   }
   else
   {
      printf("\tLabel mapping msg does not have atm label Tlv\n");
   }
   if (lblMapMsg->frLblTlvExists)
   {
      printFrLblTlv(&(lblMapMsg->frLblTlv));
   }
   else
   {
      printf("\tLabel mapping msg does not have fr label Tlv\n");
   }
   if (lblMapMsg->hopCountTlvExists)
   {
      printHopTlv(&(lblMapMsg->hopCountTlv));
   }
   else
   {
      printf("\tLabel mapping msg does not have hop count Tlv\n");
   }
   if (lblMapMsg->pathVecTlvExists)
   {
      printPathVecTlv(&(lblMapMsg->pathVecTlv));
   }
   else
   {
      printf("\tLabel mapping msg does not have path vector Tlv\n");
   }
   if (lblMapMsg->lblMsgIdTlvExists)
   {
      printLblMsgIdTlv(&(lblMapMsg->lblMsgIdTlv));
   }
   else
   {
      printf("\tLabel mapping msg does not have label messageId Tlv\n");
   }
   if (lblMapMsg->lspidTlvExists)
   {
      printLspIdTlv(&(lblMapMsg->lspidTlv));
   }
   else
   {
      printf("\tLabel mapping msg does not have LSPID Tlv\n");
   }
   if (lblMapMsg->trafficTlvExists)
   {
      printTrafficTlv(&(lblMapMsg->trafficTlv));
   }
   else
   {
      printf("\tLabel mapping msg does not have traffic Tlv\n");
   }
   printf("LABEL MAPPING MSG ***END***:\n");
}

void printErFlags(mplsLdpErFlag_t* flags)
{
   printf("\t\tER FLAGS: l = %d, res = %d\n",
	    flags->l, flags->res);
}

void printErIPFlags(mplsLdpErIPFlag_t* flags)
{
   printf("\t\tER IP FLAGS: l = %d, res = %d, preLen = %d\n",
	    flags->l, flags->res, flags->preLen);
}


void printErHop(mplsLdpErHop_t * erHop, u_short type)
{
   int i;
   switch(type)
   {
      case MPLS_ERHOP_IPV4_TLVTYPE:
      {
	 printErIPFlags(&(erHop->erIpv4.flags.flags));
	 printf("\t\t IPv4: address = %x\n", erHop->erIpv4.address);
	 break;
      }
      case MPLS_ERHOP_IPV6_TLVTYPE:
      {
	 printErIPFlags(&(erHop->erIpv6.flags.flags));
	 printf("\t\t IPv6: address ");
	 for (i = 0; i< MPLS_IPV6ADRLENGTH; i++)
	 {
	    printf("\t\t a[%d] = %x\n", i, erHop->erIpv6.address[i]);
	 }
	 break;
      }
      case MPLS_ERHOP_AS_TLVTYPE:
      {
	 printErFlags(&(erHop->erAs.flags.flags));
	 printf("\t\t ASnumber: asNumber = %d\n", erHop->erAs.asNumber);
	 break;
      }
      case MPLS_ERHOP_LSPID_TLVTYPE:
      {
	 printErFlags(&(erHop->erLspId.flags.flags));
	 printf("\t\t LSPID: lspid = %d, routerId = %d\n",
	          erHop->erLspId.lspid, erHop->erLspId.routerId);
	 break;
      }
      default:
      {
	 printf("UNKNWON ER HOP type = %d\n", type);
      }
   }
}

void printErTlv(mplsLdpErTlv_t *erTlv)
{
   u_short i;
   printf("\t erTlv:\n");
   printTlv(&(erTlv->baseTlv));
   printf("\t erTlv has %d ErHops\n", erTlv->numberErHops);
   for (i = 0; i < erTlv->numberErHops; i++)
   {
      printf("\tTYPE[%i] = %x\n",  i, erTlv->erHopTypes[i]);
      printErHop(&(erTlv->erHopArray[i]), erTlv->erHopTypes[i]);
   }
}

void printTrafficFlags(mplsLdpTrafficFlag_t * flag)
{
   printf("\t\tTraffic flags: res = %d, F6 = %d, F5 = %d, F4 = %d, F3 = %d, F2 = %d, F1 = %d\n",
	  flag->res, flag->f6Bit, flag->f5Bit, flag->f4Bit,
	  flag->f3Bit,flag->f2Bit, flag->f1Bit);
}

void printTrafficTlv(mplsLdpTrafficTlv_t *trafficTlv)
{
   printf("\t trafficTlv:\n");
   printTlv(&(trafficTlv->baseTlv));
   printTrafficFlags(&(trafficTlv->flags.flags));
   printf("\t trafficTlv data: freq = %d, res = %d, weight = %d\n",
	   trafficTlv->freq, trafficTlv->res, trafficTlv->weight);
   printf("\t trafficTlv param: \n");
   printf("\t\t PDR = %f (%x)\n", trafficTlv->pdr.pdr,  *(u_int *)&(trafficTlv->pdr.pdr));
   printf("\t\t PBS = %f (%x)\n", trafficTlv->pbs.pbs, *(u_int *)&(trafficTlv->pbs.pbs));
   printf("\t\t CDR = %f (%x)\n", trafficTlv->cdr.cdr, *(u_int *)&(trafficTlv->cdr.cdr));
   printf("\t\t CBS = %f (%x)\n", trafficTlv->cbs.cbs, *(u_int *)&(trafficTlv->cbs.cbs));
   printf("\t\t EBS = %f (%x)\n", trafficTlv->ebs.ebs, *(u_int *)&(trafficTlv->ebs.ebs));
}

void printLlbReqMsg(mplsLdpLblReqMsg_t *lblReqMsg)
{
   printf("LABEL REQUEST MSG ***START***:\n");
   printMsgBase(&(lblReqMsg->baseMsg));

   if (lblReqMsg->fecTlvExists)
   {
      printFecListTlv(&(lblReqMsg->fecTlv));
   }
   else
   {
      printf("\tLabel request msg does not have fec Tlv\n");
   }
   if (lblReqMsg->hopCountTlvExists)
   {
      printHopTlv(&(lblReqMsg->hopCountTlv));
   }
   else
   {
      printf("\tLabel request msg does not have hop count Tlv\n");
   }
   if (lblReqMsg->pathVecTlvExists)
   {
      printPathVecTlv(&(lblReqMsg->pathVecTlv));
   }
   else
   {
      printf("\tLabel request msg does not have path vector Tlv\n");
   }
   if (lblReqMsg->lblMsgIdTlvExists)
   {
      printTlv(&(lblReqMsg->lblMsgIdTlv.baseTlv));
   }
   else
   {
      printf("\tLabel request msg does not have return msgId Tlv\n");
   }
   if (lblReqMsg->erTlvExists)
   {
      printErTlv(&(lblReqMsg->erTlv));
   }
   else
   {
      printf("\tLabel request msg does not have cr Tlv\n");
   }
   if (lblReqMsg->trafficTlvExists)
   {
      printTrafficTlv(&(lblReqMsg->trafficTlv));
   }
   else
   {
      printf("\tLabel request msg does not have traffic Tlv\n");
   }
   if (lblReqMsg->lspidTlvExists)
   {
      printLspIdTlv(&(lblReqMsg->lspidTlv));
   }
   else
   {
      printf("\tLabel request msg does not have LSPID Tlv\n");
   }
   if (lblReqMsg->pinningTlvExists)
   {
      printPinningTlv(&(lblReqMsg->pinningTlv));
   }
   else
   {
      printf("\tLabel request msg does not have Pinning Tlv\n");
   }
   if (lblReqMsg->recClassTlvExists)
   {
      printResClsTlv(&(lblReqMsg->resClassTlv));
   }
   else
   {
      printf("\tLabel request msg does not have ResClass Tlv\n");
   }
   if (lblReqMsg->preemptTlvExists)
   {
      printPreemptTlv(&(lblReqMsg->preemptTlv));
   }
   else
   {
      printf("\tLabel request msg does not have Preempt Tlv\n");
   }
   printf("LABEL REQUEST MSG ***END***:\n");
}

void printLbl_W_R_Msg(mplsLdpLbl_W_R_Msg_t * msg)
{
   if (msg->baseMsg.flags.flags.msgType == MPLS_LBLWITH_MSGTYPE)
      printf("LABEL WITHDRAW MSG ***START***:\n");
   else if (msg->baseMsg.flags.flags.msgType == MPLS_LBLREL_MSGTYPE)
      printf("LABEL RELEASE MSG ***START***:\n");
   printMsgBase(&(msg->baseMsg));

   if (msg->fecTlvExists)
   {
      printFecListTlv(&(msg->fecTlv));
   }
   else
   {
      printf("\tLabel msg does not have fec Tlv\n");
   }
   if (msg->genLblTlvExists)
   {
      printGenLblTlv(&(msg->genLblTlv));
   }
   else
   {
      printf("\tLabel msg does not have gen Tlv\n");
   }
   if (msg->atmLblTlvExists)
   {
      printAtmLblTlv(&(msg->atmLblTlv));
   }
   else
   {
      printf("\tLabel msg does not have atm Tlv\n");
   }
   if (msg->frLblTlvExists)
   {
      printFrLblTlv(&(msg->frLblTlv));
   }
   else
   {
      printf("\tLabel msg does not have fr Tlv\n");
   }
   if (msg->lspidTlvExists)
   {
      printLspIdTlv(&(msg->lspidTlv));
   }
   else
   {
      printf("\tLabel msg does not have LSPID Tlv\n");
   }
   if (msg->baseMsg.flags.flags.msgType == MPLS_LBLWITH_MSGTYPE)
      printf("LABEL WITHDRAW MSG *** END ***:\n");
   else if (msg->baseMsg.flags.flags.msgType == MPLS_LBLREL_MSGTYPE)
      printf("LABEL RELEASE MSG *** END ***:\n");
}

void printPreemptTlv(mplsLdpPreemptTlv_t *preemptTlv)
{
   printf("\t preemptTlv:\n");
   printTlv(&(preemptTlv->baseTlv));
   printf("\t preemptTlv data: setPrio = %d, holdPrio = %d, res = %d\n", 
	      preemptTlv->setPrio, preemptTlv->holdPrio, preemptTlv->res);
}

void printResClsTlv(mplsLdpResClsTlv_t *tlv)
{
   printf("\t resClsTlv:\n");
   printTlv(&(tlv->baseTlv));
   printf("\t resClsTlv data: rsCls = %x\n", tlv->rsCls);
}

void printLspIdTlv(mplsLdpLspIdTlv_t *tlv)
{
   printf("\t lspIdTlv:\n");
   printTlv(&(tlv->baseTlv));
   printf("\t lspIdTlv data: res = %d, localCrlspId = %d, routerId = %x\n",
	      tlv->res, tlv->localCrlspId, tlv->routerId);
}

void printPinningTlv(mplsLdpPinningTlv_t *tlv)
{
   printf("\t pinningTlv:\n");
   printTlv(&(tlv->baseTlv));
   printf("\t pinningTlv data: pBit = %d, res = %d\n", tlv->flags.flags.pBit, 
	      tlv->flags.flags.res);
}

void printLlbAbortMsg(mplsLdpLblAbortMsg_t *lblMsg)
{
   printf("LABEL ABORT MSG ***START***:\n");
   printMsgBase(&(lblMsg->baseMsg));

   if (lblMsg->fecTlvExists)
   {
      printFecListTlv(&(lblMsg->fecTlv));
   }
   else
   {
      printf("\tLabel abort msg does not have fec Tlv\n");
   }
   if (lblMsg->lblMsgIdTlvExists)
   {
      printLblMsgIdTlv(&(lblMsg->lblMsgIdTlv));
   }
   else
   {
      printf("\tLabel abort msg does not have label messageId Tlv\n");
   }
   printf("LABEL ABORT MSG ***END***:\n");
}


/* 
 *   Routine to convert hex string to ascii string
 */

int converHexToAscii(u_char *buffHex, int buffHexLen, u_char * buffAscii)
{
      /* convert the hexEncrypP hex string to a char sting*/
      int i = 0;
      int j = 0;
      char c, c1;
      u_char *p = buffHex;
      u_char *q = buffAscii;

      for (i = 0;  i< buffHexLen; i+=2, j++)
      {
         c = *p;
         p++;
         c1 = *p;
         p++;
         if (c >= '0' && c <= '9')
            c -= '0';
         else if (c >= 'A' && c <= 'F')
            c -= 'A' - 0xa;
         else if (c >= 'a' && c <= 'f')
            c -= 'a' - 0xa;
         else
            return 0; 
         if (c1 >= '0' && c1 <= '9')
            c1 -= '0';
         else if (c1 >= 'A' && c1 <= 'F')
            c1 -= 'A' - 0xa;
         else if (c1 >= 'a' && c1 <= 'f')
            c1 -= 'a' - 0xa;
         else
            return 0; 

	 *q++ = (c<< 4) + (c1 & 0x0f);
      }
      return j;

} /* End : converHexToAscii */


/* 
 *   Routine to convert ascii string to hex string
 */
int converAsciiToHex(u_char *buffHex, int buffAsciiLen, u_char * buffAscii)
{
   int i;
   u_char *p2 = buffHex;
   u_char *p1 = buffAscii;
   u_char buf[3];

   for (i = 0; i< buffAsciiLen; i++)
   {
      bzero(buf, 3);
      sprintf((char *)buf, "%02x", *p1++);
      memcpy(p2, buf, 2);
      p2 += strlen((char *)buf);
   }
   *p2 = '\0';

   p2 = buffHex;
   for (i = 0; i< 2*buffAsciiLen; i++)
   {
      printf("%c",*p2++); 
   }
   printf("\n");
   return i;
   
} /* End : converAsciiToHex */


/*****************************************************************************
* This section includes one example  of hex buffers which contains encoding  *
* for a pdu header and a request message.                                    *
*                                                                            *
* the hex buffer for the request message contains (where q represents the end*
* of the buffer):                                                            *
*                                                                            *
* 0001009AC00003050002040100900000000806010000010000100200011B00194059030001 *
* 042F7D308308210008000000013A1D65030800003008040008000000193A1D651708040008 * 
* 800000053A1D6503080400088000000F3A1D650D08040008000000233A1D65210810001824 * 
* 01000040e3333342c8000040e00000447a0000412000000823000480000000082200040ABC * 
* DEFF0820000407070000q                                                      *
*                                                                            *
* Make sure that when copy and paste the buffer, there are no new line chars *
* or blanks.                                                                 *
* When decoding the above buffer, the following debug output should show if  *
* the debug flag is defined and set:                                         *
*                                                                            *
*LPD Header : protocolVersion = 1                                            *
*        pduLength = 154                                                     *
*        lsrAddress = c0000305                                               *
*        labelSpace = 2                                                      *
*                                                                            *
*LABEL REQUEST MSG ***START***:                                              *
*        baseMsg : msgType = 401                                             *
*                msgLength = 144                                             *
*                msgId = 8                                                   *
*         fecTlv:                                                            *
*         Tlv:                                                               *
*         BaseTlv: type = 100                                                *
*                  length = 16                                               *
*                  uBit = 0                                                  *
*                  fBit = 0                                                  *
*                fecTlv->numberFecElements = 2                               *
*                elem 0 type is 2                                            *
*                Fec Element : type = 2, addFam = 1, preLen = 27,            *
*                              address = 194059                              *
*                elem 1 type is 3                                            *
*                Fec Element : type = 3, addFam = 1, preLen = 4,             *
*                              address = 2f7d3083                            *
*                                                                            * 
*        fecTlv.wcElemExists = 0                                             *
*        Label request msg does not have cos label Tlv                       *
*        Label request msg does not have hop count Tlv                       *
*        Label request msg does not have path vector Tlv                     *
*         Tlv:                                                               *
*         BaseTlv: type = 601                                                *
*                  length = 0                                                *
*                  uBit = 0                                                  *
*                  fBit = 0                                                  *
*         erTlv:                                                             *
*         Tlv:                                                               *
*         BaseTlv: type = 800                                                *
*                  length = 48                                               *
*                  uBit = 0                                                  *
*                  fBit = 0                                                  *
*         erTlv has 4 ErHops                                                 *
*        TYPE[0] = 804                                                       *
*                ER FLAGS: l = 0, res = 0                                    *
*                 LSPID: lspid = 25, routerId = 975004951                    *
*        TYPE[1] = 804                                                       *
*                ER FLAGS: l = 1, res = 0                                    *
*                 LSPID: lspid = 5, routerId = 975004931                     *
*        TYPE[2] = 804                                                       *
*                ER FLAGS: l = 1, res = 0                                    *
*                 LSPID: lspid = 15, routerId = 975004941                    *
*        TYPE[3] = 804                                                       *
*                ER FLAGS: l = 0, res = 0                                    *
*                 LSPID: lspid = 35, routerId = 975004961                    *
*         trafficTlv:                                                        *
*         Tlv:                                                               *
*         BaseTlv: type = 810                                                *
*                  length = 24                                               *
*                  uBit = 0                                                  *
*                  fBit = 0                                                  *
*                Traffic flags: res = 0, F6 = 1, F5 = 0, F4 = 0, F3 = 1,     * 
*                                        F2 = 0, F1 = 0                      *
*         trafficTlv data: freq = 1, res = 0, weight = 0                     *
*         trafficTlv param:                                                  *
*                 PDR = 7.1(40e33333)                                        *
*                 PBS = 100.0(42c80000)                                      *
*                 CDR = 7.0(40e00000)                                        *
*                 CBS = 1000.0(447a0000)                                     *
*                 EBS = 10.0(41200000)                                       *
*         lspIdTlv:                                                          *
*         Tlv:                                                               *
*         BaseTlv: type = 821                                                *
*                  length = 8                                                *
*                  uBit = 0                                                  *
*                  fBit = 0                                                  *
*         lspIdTlv data: res = 0, localCrlspId = 1, routerId = 3a1d6503      *
*         pinningTlv:                                                        *
*         Tlv:                                                               *
*         BaseTlv: type = 823                                                *
*                  length = 4                                                *
*                  uBit = 0                                                  *
*                  fBit = 0                                                  *
*         pinningTlv data: pBit = 1, res = 0                                 *
*         resClsTlv:                                                         *
*         Tlv:                                                               *
*         BaseTlv: type = 822                                                *
*                  length = 4                                                *
*                  uBit = 0                                                  *
*                  fBit = 0                                                  *
*         resClsTlv data: rsCls = abcdeff                                    *
*         preemptTlv:                                                        *
*         Tlv:                                                               *
*         BaseTlv: type = 820                                                *
*                  length = 4                                                *
*                  uBit = 0                                                  *
*                  fBit = 0                                                  *
*         preemptTlv data: setPrio = 7, holdPrio = 7, res = 0                *
*LABEL REQUEST MSG ***END***:                                                *
*****************************************************************************/

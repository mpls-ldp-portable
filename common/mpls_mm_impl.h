
/*
 *  Copyright (C) James R. Leu 2002
 *  jleu@mindspring.com
 *
 *  This software is covered under the LGPL, for more
 *  info check out http://www.gnu.org/copyleft/lgpl.html
 */

#ifndef _MPLS_MM_IMPL_H_
#define _MPLS_MM_IMPL_H_

#include "mpls_struct.h"

#define QUAGGA_MTYPE_FRAMEWORK
#ifdef QUAGGA_MTYPE_FRAMEWORK
#include "memory.h"

#define mpls_malloc(t, s)   XMALLOC(t, s)
#define mpls_free(t, p)   XFREE(t, p)

#else
enum
{
  MTYPE_LDP,
  MTYPE_LDP_ADDR,
  MTYPE_LDP_ADJ,
  MTYPE_LDP_ATTR,
  MTYPE_LDP_ENTITY,
  MTYPE_LDP_FEC,
  MTYPE_LDP_HOP,
  MTYPE_LDP_IF,
  MTYPE_LDP_IP,
  MTYPE_LDP_INLABEL,
  MTYPE_LDP_NEXTHOP,
  MTYPE_LDP_OUTLABEL,
  MTYPE_LDP_PEER,
  MTYPE_LDP_RESOURCE,
  MTYPE_LDP_SESSION,
  MTYPE_LDP_TUNNEL,
  MTYPE_LDP_BUF
};
/*
 * in: size
 * return: void*
 */
extern void *mpls_malloc(int type, const mpls_size_type size);

/*
 * in: mem
 */
extern void mpls_free(int type, void *mem);
#endif

#endif

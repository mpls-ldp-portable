
/*
 *  Copyright (C) James R. Leu 2002
 *  jleu@mindspring.com
 *
 *  This software is covered under the LGPL, for more
 *  info check out http://www.gnu.org/copyleft/lgpl.html
 */

#ifndef _MPLS_ASSERT_H_
#define _MPLS_ASSERT_H_

#include <assert.h>

#define NO_ASSERT_PLEASE
#ifdef NO_ASSERT_PLEASE
extern void mpls_assert(const char* func, const int line, int condition);
#define MPLS_ASSERT(x) mpls_assert(__func__, __LINE__, (int)(x))
#else
#define MPLS_ASSERT(x) assert(x)
#endif

#endif


/*
 *  Copyright (C) James R. Leu 2002
 *  jleu@mindspring.com
 *
 *  This software is covered under the LGPL, for more
 *  info check out http://www.gnu.org/copyleft/lgpl.html
 */

#include "lsr_struct.h"
#include "lsr_cfg.h"
#include "lsr_global.h"
#include "lsr_outsegment.h"
#include "lsr_insegment.h"
#include "lsr_xconnect.h"
#include "lsr_if.h"
#include "lsr_ftn.h"

#include "mpls_mm_impl.h"
#include "mpls_mpls_impl.h"
#include "mpls_ifmgr_impl.h"
#include "mpls_lock_impl.h"
#include "mpls_trace_impl.h"

mpls_cfg_handle lsr_cfg_open(mpls_instance_handle data)
{
  lsr_global *g = NULL;

  LDP_ENTER(data, "lsr_cfg_open");
  g = lsr_global_create(data);
  LDP_EXIT(data, "lsr_cfg_open");

  return (mpls_cfg_handle) g;
}

void lsr_cfg_close(mpls_cfg_handle g)
{
  LDP_ENTER((mpls_instance_handle) g->user_data, "lsr_cfg_close");
  lsr_global_delete(g);
  LDP_EXIT((mpls_instance_handle) g->user_data, "lsr_cfg_close");
}

/******************* GLOBAL **********************/

mpls_return_enum lsr_cfg_global_get(mpls_cfg_handle handle, lsr_global * g,
  uint32_t flag)
{
  lsr_global *global = (lsr_global *) handle;

  MPLS_ASSERT(global !=NULL);

  LDP_ENTER(global->user_data, "lsr_cfg_global_get");

  mpls_lock_get(global->global_lock); /* LOCK */

  if (flag & LSR_GLOBAL_CFG_ADMIN_STATE) {
    g->admin_state = global->admin_state;
  }

  mpls_lock_release(global->global_lock); /* UNLOCK */

  LDP_EXIT(global->user_data, "lsr_cfg_global_get");

  return MPLS_SUCCESS;
}

mpls_return_enum lsr_cfg_global_test(mpls_cfg_handle handle, lsr_global * g,
  uint32_t flag)
{
  lsr_global *global = (lsr_global *) handle;
  mpls_return_enum retval = MPLS_SUCCESS;

  MPLS_ASSERT(global !=NULL);

  LDP_ENTER(global->user_data, "lsr_cfg_global_test");

  mpls_lock_get(global->global_lock); /* LOCK */

  if ((global->admin_state == MPLS_ADMIN_ENABLE) &&
      (flag & LSR_GLOBAL_CFG_WHEN_DOWN))
    retval = MPLS_FAILURE;

  mpls_lock_release(global->global_lock); /* UNLOCK */

  LDP_EXIT(global->user_data, "lsr_cfg_global_test");

  return retval;
}

mpls_return_enum lsr_cfg_global_set(mpls_cfg_handle handle, lsr_global * g,
  uint32_t flag)
{
  lsr_global *global = (lsr_global *) handle;
  mpls_return_enum retval = MPLS_FAILURE;

  MPLS_ASSERT(global !=NULL);

  LDP_ENTER(global->user_data, "lsr_cfg_global_set");

  mpls_lock_get(global->global_lock); /* LOCK */

  if ((global->admin_state == MPLS_ADMIN_ENABLE) &&
      (flag & LSR_GLOBAL_CFG_WHEN_DOWN))
    goto lsr_cfg_global_set_end;

  if (flag & LSR_GLOBAL_CFG_ADMIN_STATE) {
    if ((global->admin_state == MPLS_ADMIN_ENABLE) &&
        (g->admin_state == MPLS_ADMIN_DISABLE)) {
      lsr_global_shutdown(global);
    } else if ((global->admin_state == MPLS_ADMIN_DISABLE) &&
               (g->admin_state == MPLS_ADMIN_ENABLE)) {
      lsr_global_startup(global);
    }
  }

  retval = MPLS_SUCCESS;

lsr_cfg_global_set_end:

  mpls_lock_release(global->global_lock); /* UNLOCK */

  LDP_EXIT(global->user_data, "lsr_cfg_global_set");

  return retval;
}

/******************* OUTSEGMENT **********************/

mpls_return_enum lsr_cfg_outsegment_get(mpls_cfg_handle handle, lsr_outsegment * o, uint32_t flag)
{
  lsr_global *global = (lsr_global *) handle;
  lsr_outsegment *oseg = NULL;
  mpls_return_enum retval = MPLS_FAILURE;

  MPLS_ASSERT(global !=NULL && o != NULL);

  LDP_ENTER(global->user_data, "lsr_cfg_outsegment_get");

  mpls_lock_get(global->global_lock); /* LOCK */

  if (lsr_global_find_outsegment_index(global, o->index, &oseg) != MPLS_SUCCESS)
      goto lsr_cfg_outsegment_get_end;

  if (LSR_OUTSEGMENT_CFG_INDEX & flag) {
    o->index = oseg->index;
  }
  if (LSR_OUTSEGMENT_CFG_PUSH_LABEL & flag) {
    o->info.push_label = oseg->info.push_label;
  }
  if (LSR_OUTSEGMENT_CFG_OWNER & flag) {
    o->info.owner = oseg->info.owner;
  }
  if (LSR_OUTSEGMENT_CFG_INTERFACE & flag) {
    o->if_index = oseg->iff ? oseg->iff->index : 0;
  }
  if (LSR_OUTSEGMENT_CFG_LABEL & flag) {
    memcpy(&o->info.label, &oseg->info.label, sizeof(mpls_label_struct));
  }
  if (LSR_OUTSEGMENT_CFG_NEXTHOP & flag) {
    memcpy(&o->info.nexthop, &oseg->info.nexthop, sizeof(mpls_nexthop));
  }

  retval = MPLS_SUCCESS;

lsr_cfg_outsegment_get_end:
  mpls_lock_release(global->global_lock); /* UNLOCK */

  LDP_EXIT(global->user_data, "lsr_cfg_outsegment_get");

  return retval;
}

mpls_return_enum lsr_cfg_outsegment_getnext(mpls_cfg_handle handle, lsr_outsegment * i,
  uint32_t flag)
{
  lsr_global *g = (lsr_global *) handle;
  lsr_outsegment *oseg = NULL;
  mpls_return_enum r = MPLS_FAILURE;
  mpls_bool done = MPLS_BOOL_FALSE;
  int index;

  LDP_ENTER(g->user_data, "lsr_cfg_outsegment_getnext");

  if (i->index == 0) {
    index = 1;
  } else {
    index = i->index + 1;
  }

  mpls_lock_get(g->global_lock); /* LOCK */
  while (done == MPLS_BOOL_FALSE) {
    switch ((r = lsr_global_find_outsegment_index(g, index, &oseg))) {
      case MPLS_SUCCESS:
      case MPLS_END_OF_LIST:
        done = MPLS_BOOL_TRUE;
        break;
      case MPLS_FAILURE:
        break;
      default:
        MPLS_ASSERT(0);
    }
    index++;
  }
  mpls_lock_release(g->global_lock); /* UNLOCK */

  if (r == MPLS_SUCCESS) {
    i->index = oseg->index;
    LDP_EXIT(g->user_data, "lsr_cfg_outsegment_getnext");
    return lsr_cfg_outsegment_get(g, i, flag);
  }
  LDP_EXIT(g->user_data, "lsr_cfg_outsegment_getnext");
  return r;
}

mpls_return_enum lsr_cfg_outsegment_test(mpls_cfg_handle handle, lsr_outsegment * i,
  uint32_t flag)
{
  lsr_global *global = (lsr_global *) handle;
  lsr_outsegment *oseg = NULL;
  mpls_return_enum retval = MPLS_FAILURE;

  MPLS_ASSERT(global !=NULL && i != NULL);

  LDP_ENTER(global->user_data, "lsr_cfg_outsegment_test");

  mpls_lock_get(global->global_lock); /* LOCK */

  if (!(flag & LSR_CFG_ADD)) {
    lsr_global_find_outsegment_index(global, i->index, &oseg);
  } else {
    retval = MPLS_SUCCESS;
    goto lsr_cfg_outsegment_test_end;
  }

  retval = MPLS_SUCCESS;

lsr_cfg_outsegment_test_end:
  mpls_lock_release(global->global_lock); /* UNLOCK */

  LDP_EXIT(global->user_data, "lsr_cfg_outsegment_test");

  return retval;
}

mpls_return_enum lsr_cfg_outsegment_set(mpls_cfg_handle handle, lsr_outsegment * o, uint32_t flag)
{
  lsr_global *global = (lsr_global*)handle;
  lsr_outsegment *oseg = NULL;
  mpls_return_enum retval = MPLS_FAILURE;

  MPLS_ASSERT(global !=NULL && o != NULL);

  LDP_ENTER(global->user_data, "lsr_cfg_outsegment_set");

  mpls_lock_get(global->global_lock); /* LOCK */

  if (flag & LSR_CFG_ADD) {
    if ((oseg = lsr_outsegment_create()) == NULL) {
      goto lsr_cfg_outsegment_set_end;
    }
    o->index = oseg->index;
  } else {
    lsr_global_find_outsegment_index(global, o->index, &oseg);
  }

  if (oseg == NULL)
    goto lsr_cfg_outsegment_set_end;

  if (flag & LSR_CFG_DEL) {
    _lsr_global_del_outsegment(global, oseg);
    mpls_mpls_outsegment_del(global->mpls_handle,&oseg->info);
    retval = MPLS_SUCCESS;
    goto lsr_cfg_outsegment_set_end;
  }

  if (LSR_OUTSEGMENT_CFG_PUSH_LABEL & flag) {
    oseg->info.push_label = o->info.push_label;
  }
  if (LSR_OUTSEGMENT_CFG_OWNER & flag) {
    oseg->info.owner = o->info.owner;
  }
  if (LSR_OUTSEGMENT_CFG_INTERFACE & flag) {
    lsr_if *iff = NULL;
    if (lsr_global_find_if_index(global, o->if_index, &iff) == MPLS_SUCCESS) {
      lsr_if_add_outsegment(iff,oseg);
    }
  }
  if (LSR_OUTSEGMENT_CFG_LABEL & flag) {
    memcpy(&oseg->info.label, &o->info.label, sizeof(mpls_label_struct));
  }
  if (LSR_OUTSEGMENT_CFG_NEXTHOP & flag) {
    memcpy(&oseg->info.nexthop, &o->info.nexthop, sizeof(mpls_nexthop));
  }

  retval = MPLS_SUCCESS;

  if (flag & LSR_CFG_ADD) {
    retval = mpls_mpls_outsegment_add(global->mpls_handle,&oseg->info);
    if (retval == MPLS_SUCCESS) {
      _lsr_global_add_outsegment(global, oseg);
    }
  }

lsr_cfg_outsegment_set_end:
  mpls_lock_release(global->global_lock); /* UNLOCK */

  LDP_EXIT(global->user_data, "lsr_cfg_outsegment_set");

  return retval;
}
/******************* INSEGMENT **********************/

mpls_return_enum lsr_cfg_insegment_get(mpls_cfg_handle handle, lsr_insegment * i, uint32_t flag)
{
  lsr_global *global = (lsr_global *) handle;
  lsr_insegment *iseg = NULL;
  mpls_return_enum retval = MPLS_FAILURE;

  MPLS_ASSERT(global !=NULL && i != NULL);

  LDP_ENTER(global->user_data, "lsr_cfg_insegment_get");

  mpls_lock_get(global->global_lock); /* LOCK */

  if (lsr_global_find_insegment_index(global, i->index, &iseg) != MPLS_SUCCESS)
      goto lsr_cfg_insegment_get_end;

  if (LSR_INSEGMENT_CFG_INDEX & flag) {
    i->index = iseg->index;
  }
  if (LSR_INSEGMENT_CFG_NPOP & flag) {
    i->info.npop = iseg->info.npop;
  }
  if (LSR_INSEGMENT_CFG_FAMILY & flag) {
    i->info.family = iseg->info.family;
  }
  if (LSR_INSEGMENT_CFG_LABELSPACE & flag) {
    i->info.labelspace = iseg->info.labelspace;
  }
  if (LSR_INSEGMENT_CFG_LABEL & flag) {
    memcpy(&i->info.label, &iseg->info.label, sizeof(mpls_label_struct));
  }
  if (LSR_INSEGMENT_CFG_OWNER & flag) {
    i->info.owner = iseg->info.owner;
  }

  retval = MPLS_SUCCESS;

lsr_cfg_insegment_get_end:
  mpls_lock_release(global->global_lock); /* UNLOCK */

  LDP_EXIT(global->user_data, "lsr_cfg_insegment_get");

  return retval;
}

mpls_return_enum lsr_cfg_insegment_getnext(mpls_cfg_handle handle, lsr_insegment * i,
  uint32_t flag)
{
  lsr_global *g = (lsr_global *) handle;
  lsr_insegment *iseg = NULL;
  mpls_return_enum r = MPLS_FAILURE;
  mpls_bool done = MPLS_BOOL_FALSE;
  int index;

  LDP_ENTER(g->user_data, "lsr_cfg_insegment_getnext");

  if (i->index == 0) {
    index = 1;
  } else {
    index = i->index + 1;
  }

  mpls_lock_get(g->global_lock); /* LOCK */
  while (done == MPLS_BOOL_FALSE) {
    switch ((r = lsr_global_find_insegment_index(g, index, &iseg))) {
      case MPLS_SUCCESS:
      case MPLS_END_OF_LIST:
        done = MPLS_BOOL_TRUE;
        break;
      case MPLS_FAILURE:
        break;
      default:
        MPLS_ASSERT(0);
    }
    index++;
  }
  mpls_lock_release(g->global_lock); /* UNLOCK */

  if (r == MPLS_SUCCESS) {
    i->index = iseg->index;
    LDP_EXIT(g->user_data, "lsr_cfg_insegment_getnext");
    return lsr_cfg_insegment_get(g, i, flag);
  }
  LDP_EXIT(g->user_data, "lsr_cfg_insegment_getnext");
  return r;
}

mpls_return_enum lsr_cfg_insegment_test(mpls_cfg_handle handle, lsr_insegment * i,
  uint32_t flag)
{
  lsr_global *global = (lsr_global *) handle;
  lsr_insegment *iseg = NULL;
  mpls_return_enum retval = MPLS_FAILURE;

  MPLS_ASSERT(global !=NULL && i != NULL);

  LDP_ENTER(global->user_data, "lsr_cfg_insegment_test");

  mpls_lock_get(global->global_lock); /* LOCK */

  if (!(flag & LSR_CFG_ADD)) {
    lsr_global_find_insegment_index(global, i->index, &iseg);
  } else {
    retval = MPLS_SUCCESS;
    goto lsr_cfg_insegment_test_end;
  }

  retval = MPLS_SUCCESS;

lsr_cfg_insegment_test_end:
  mpls_lock_release(global->global_lock); /* UNLOCK */

  LDP_EXIT(global->user_data, "lsr_cfg_insegment_test");

  return retval;
}

mpls_return_enum lsr_cfg_insegment_set(mpls_cfg_handle handle, lsr_insegment * i, uint32_t flag)
{
  lsr_global *global = (lsr_global*)handle;
  lsr_insegment *iseg = NULL;
  mpls_return_enum retval = MPLS_FAILURE;

  MPLS_ASSERT(global !=NULL && i != NULL);

  LDP_ENTER(global->user_data, "lsr_cfg_insegment_set");

  mpls_lock_get(global->global_lock); /* LOCK */

  if (flag & LSR_CFG_ADD) {
    if ((iseg = lsr_insegment_create()) == NULL) {
      goto lsr_cfg_insegment_set_end;
    }
    i->index = iseg->index;
  } else {
    lsr_global_find_insegment_index(global, i->index, &iseg);
  }

  if (iseg == NULL)
    goto lsr_cfg_insegment_set_end;

  if (flag & LSR_CFG_DEL) {

    mpls_mpls_insegment_del(global->mpls_handle,&iseg->info);
    _lsr_global_del_insegment(global, iseg);

    retval = MPLS_SUCCESS;
    goto lsr_cfg_insegment_set_end;
  }

  if (LSR_INSEGMENT_CFG_NPOP & flag) {
    iseg->info.npop = i->info.npop;
  }
  if (LSR_INSEGMENT_CFG_FAMILY & flag) {
    iseg->info.family = i->info.family;
  }
  if (LSR_INSEGMENT_CFG_LABELSPACE & flag) {
    iseg->info.labelspace = i->info.labelspace;
  }
  if (LSR_INSEGMENT_CFG_LABEL & flag) {
    memcpy(&iseg->info.label, &i->info.label, sizeof(mpls_label_struct));
  }
  if (LSR_INSEGMENT_CFG_OWNER & flag) {
    iseg->info.owner = i->info.owner;
  }

  retval = MPLS_SUCCESS;

  if (flag & LSR_CFG_ADD) {
    retval = mpls_mpls_insegment_add(global->mpls_handle,&iseg->info);
    if (retval == MPLS_SUCCESS) {
      memcpy(&i->info.label, &iseg->info.label, sizeof(mpls_label_struct));
      _lsr_global_add_insegment(global, iseg);
    }
  }

lsr_cfg_insegment_set_end:
  mpls_lock_release(global->global_lock); /* UNLOCK */

  LDP_EXIT(global->user_data, "lsr_cfg_insegment_set");

  return retval;
}
/******************* XCONNECT **********************/

void do_lsr_cfg_xconnect_get(lsr_xconnect * x, lsr_xconnect * xcon,
  uint32_t flag)
{
  if (LSR_XCONNECT_CFG_INDEX & flag) {
    x->index = xcon->index;
  }
  if (LSR_XCONNECT_CFG_OUTSEGMENT & flag) {
    x->outsegment_index = xcon->outsegment ? xcon->outsegment->index : 0;
  }
  if (LSR_XCONNECT_CFG_INSEGMENT & flag) {
    x->insegment_index = xcon->insegment->index;
  }
  if (LSR_XCONNECT_CFG_LSPID & flag) {
    x->info.lspid = xcon->info.lspid;
  }
  if (LSR_XCONNECT_CFG_LABELSTACK & flag) {
    x->info.stack_size = xcon->info.stack_size;
    memcpy(x->info.stack,xcon->info.stack,
      sizeof(mpls_label_struct) * xcon->info.stack_size);
  }
  if (LSR_XCONNECT_CFG_OWNER & flag) {
    x->info.owner = xcon->info.owner;
  }
}

mpls_return_enum lsr_cfg_xconnect_get(mpls_cfg_handle handle, lsr_xconnect * x, uint32_t flag)
{
  lsr_global *global = (lsr_global *) handle;
  lsr_xconnect *xcon = NULL;
  mpls_return_enum retval = MPLS_FAILURE;

  MPLS_ASSERT(global !=NULL && x != NULL);

  LDP_ENTER(global->user_data, "lsr_cfg_xconnect_get");

  mpls_lock_get(global->global_lock); /* LOCK */

  if (lsr_global_find_xconnect_index(global, x->index, &xcon) != MPLS_SUCCESS)
      goto lsr_cfg_xconnect_get_end;

  do_lsr_cfg_xconnect_get(x, xcon, flag);

  retval = MPLS_SUCCESS;

lsr_cfg_xconnect_get_end:
  mpls_lock_release(global->global_lock); /* UNLOCK */

  LDP_EXIT(global->user_data, "lsr_cfg_xconnect_get");

  return retval;
}

mpls_return_enum lsr_cfg_xconnect_get2(mpls_cfg_handle handle, lsr_xconnect *x, uint32_t flag)
{
  lsr_global *global = (lsr_global *) handle;
  lsr_xconnect *xcon = NULL;
  mpls_return_enum retval = MPLS_FAILURE;

  MPLS_ASSERT(global !=NULL && x != NULL);

  LDP_ENTER(global->user_data, "lsr_cfg_xconnect_get2");

  mpls_lock_get(global->global_lock); /* LOCK */

  if (lsr_global_find_xconnect_index2(global, x->insegment_index,
    x->outsegment_index, &xcon) != MPLS_SUCCESS)
      goto lsr_cfg_xconnect_get_end;

  do_lsr_cfg_xconnect_get(x, xcon, flag);

  retval = MPLS_SUCCESS;

lsr_cfg_xconnect_get_end:
  mpls_lock_release(global->global_lock); /* UNLOCK */

  LDP_EXIT(global->user_data, "lsr_cfg_xconnect_get2");

  return retval;
}

mpls_return_enum lsr_cfg_xconnect_getnext(mpls_cfg_handle handle, lsr_xconnect * i, uint32_t flag)
{
  lsr_global *g = (lsr_global *) handle;
  lsr_xconnect *xcon = NULL;
  mpls_return_enum r = MPLS_FAILURE;
  mpls_bool done = MPLS_BOOL_FALSE;
  int index;

  LDP_ENTER(g->user_data, "lsr_cfg_xconnect_getnext");

  if (i->index == 0) {
    index = 1;
  } else {
    index = i->index + 1;
  }

  mpls_lock_get(g->global_lock); /* LOCK */
  while (done == MPLS_BOOL_FALSE) {
    switch ((r = lsr_global_find_xconnect_index(g, index, &xcon))) {
      case MPLS_SUCCESS:
      case MPLS_END_OF_LIST:
        done = MPLS_BOOL_TRUE;
        break;
      case MPLS_FAILURE:
        break;
      default:
        MPLS_ASSERT(0);
    }
    index++;
  }
  mpls_lock_release(g->global_lock); /* UNLOCK */

  if (r == MPLS_SUCCESS) {
    i->index = xcon->index;
    LDP_EXIT(g->user_data, "lsr_cfg_xconnect_getnext");
    return lsr_cfg_xconnect_get(g, i, flag);
  }
  LDP_EXIT(g->user_data, "lsr_cfg_xconnect_getnext");
  return r;
}

mpls_return_enum lsr_cfg_xconnect_test(mpls_cfg_handle handle, lsr_xconnect * i,
  uint32_t flag)
{
  lsr_global *global = (lsr_global *) handle;
  lsr_xconnect *xcon = NULL;
  mpls_return_enum retval = MPLS_FAILURE;

  MPLS_ASSERT(global !=NULL && i != NULL);

  LDP_ENTER(global->user_data, "lsr_cfg_xconnect_test");

  mpls_lock_get(global->global_lock); /* LOCK */

  if (!(flag & LSR_CFG_ADD)) {
    lsr_global_find_xconnect_index(global, i->index, &xcon);
  } else {
    retval = MPLS_SUCCESS;
    goto lsr_cfg_xconnect_test_end;
  }

  retval = MPLS_SUCCESS;

lsr_cfg_xconnect_test_end:
  mpls_lock_release(global->global_lock); /* UNLOCK */

  LDP_EXIT(global->user_data, "lsr_cfg_xconnect_test");

  return retval;
}

static mpls_return_enum do_lsr_cfg_xconnect_set(lsr_global *global, lsr_xconnect * x, lsr_xconnect * xcon, uint32_t flag)
{
  mpls_return_enum retval = MPLS_FAILURE;

  if (flag & LSR_CFG_DEL) {

    mpls_mpls_xconnect_del(global->mpls_handle, &xcon->insegment->info,
      &xcon->outsegment->info);
    _lsr_global_del_xconnect(global, xcon);

    return MPLS_SUCCESS;
  }

  if (LSR_XCONNECT_CFG_OUTSEGMENT & flag) {
    lsr_outsegment *out = NULL;
    lsr_global_find_outsegment_index(global, x->outsegment_index, &out);
    if (!out) {
      return MPLS_FAILURE;
    }
    lsr_outsegment_add_xconnect(out,xcon);
  }
  if (LSR_XCONNECT_CFG_INSEGMENT & flag) {
    lsr_insegment *in = NULL;
    lsr_global_find_insegment_index(global, x->insegment_index, &in);
    if (!in) {
      return MPLS_FAILURE;
    }
    lsr_insegment_add_xconnect(in,xcon);
  }
  if (LSR_XCONNECT_CFG_LSPID & flag) {
    xcon->info.lspid = x->info.lspid;
  }
  if (LSR_XCONNECT_CFG_LABELSTACK & flag) {
    xcon->info.stack_size = x->info.stack_size;
    if (x->info.stack_size) {
      memcpy(xcon->info.stack, x->info.stack,
        sizeof(mpls_label_struct) * x->info.stack_size);
    }
  }
  if (LSR_XCONNECT_CFG_OWNER & flag) {
    xcon->info.owner = x->info.owner;
  }

  retval = MPLS_SUCCESS;

  if (flag & LSR_CFG_ADD) {
    retval = mpls_mpls_xconnect_add(global->mpls_handle,&xcon->insegment->info,
      &xcon->outsegment->info);
    if (retval == MPLS_SUCCESS) {
      _lsr_global_add_xconnect(global, xcon);
    }
  }
  return retval;
}

mpls_return_enum lsr_cfg_xconnect_set(mpls_cfg_handle handle, lsr_xconnect * x, uint32_t flag)
{
  lsr_global *global = (lsr_global*)handle;
  lsr_xconnect *xcon = NULL;
  mpls_return_enum retval = MPLS_FAILURE;

  MPLS_ASSERT(global !=NULL && x != NULL);

  LDP_ENTER(global->user_data, "lsr_cfg_xconnect_set");

  mpls_lock_get(global->global_lock); /* LOCK */

  if (flag & LSR_CFG_ADD) {
    if ((xcon = lsr_xconnect_create()) == NULL) {
      goto lsr_cfg_xconnect_set_end;
    }
    x->index = xcon->index;
  } else {
    lsr_global_find_xconnect_index(global, x->index, &xcon);
  }

  if (xcon == NULL)
    goto lsr_cfg_xconnect_set_end;

  retval = do_lsr_cfg_xconnect_set(global,x,xcon,flag);

lsr_cfg_xconnect_set_end:
  mpls_lock_release(global->global_lock); /* UNLOCK */

  LDP_EXIT(global->user_data, "lsr_cfg_xconnect_set");

  return retval;
}

mpls_return_enum lsr_cfg_xconnect_set2(mpls_cfg_handle handle, lsr_xconnect * x, uint32_t flag)
{
  lsr_global *global = (lsr_global*)handle;
  lsr_xconnect *xcon = NULL;
  mpls_return_enum retval = MPLS_FAILURE;

  MPLS_ASSERT(global !=NULL && x != NULL);

  LDP_ENTER(global->user_data, "lsr_cfg_xconnect_set2");

  mpls_lock_get(global->global_lock); /* LOCK */

  if (flag & LSR_CFG_ADD) {
    if ((xcon = lsr_xconnect_create()) == NULL) {
      goto lsr_cfg_xconnect_set_end2;
    }
    x->index = xcon->index;
  } else {
    lsr_global_find_xconnect_index2(global, x->insegment_index,
      x->outsegment_index, &xcon);
  }

  if (xcon == NULL)
    goto lsr_cfg_xconnect_set_end2;

  retval = do_lsr_cfg_xconnect_set(global,x,xcon,flag);

lsr_cfg_xconnect_set_end2:
  mpls_lock_release(global->global_lock); /* UNLOCK */

  LDP_EXIT(global->user_data, "lsr_cfg_xconnect_set2");

  return retval;
}

/******************* INTERFACE **********************/

mpls_return_enum lsr_cfg_if_get(mpls_cfg_handle handle, lsr_if * i, uint32_t flag)
{
  lsr_global *global = (lsr_global *) handle;
  lsr_if *iff = NULL;
  mpls_return_enum retval = MPLS_FAILURE;

  MPLS_ASSERT(global !=NULL && i != NULL);

  LDP_ENTER(global->user_data, "lsr_cfg_if_get");

  mpls_lock_get(global->global_lock); /* LOCK */

  if (flag & LSR_IF_CFG_BY_INDEX) {
    lsr_global_find_if_index(global, i->index, &iff);
  } else {
    iff = lsr_global_find_if_handle(global, i->handle);
  }
  if (!iff)
      goto lsr_cfg_if_get_end;

  if (LSR_IF_CFG_INDEX & flag) {
    i->index = iff->index;
  }
  if (LSR_IF_CFG_OPER_STATE & flag) {
    i->oper_state = iff->oper_state;
  }

  retval = MPLS_SUCCESS;

lsr_cfg_if_get_end:
  mpls_lock_release(global->global_lock); /* UNLOCK */

  LDP_EXIT(global->user_data, "lsr_cfg_if_get");

  return retval;
}

mpls_return_enum lsr_cfg_if_getnext(mpls_cfg_handle handle, lsr_if * i,
  uint32_t flag)
{
  lsr_global *g = (lsr_global *) handle;
  lsr_if *iff = NULL;
  mpls_return_enum r = MPLS_FAILURE;
  mpls_bool done = MPLS_BOOL_FALSE;
  int index;

  LDP_ENTER(g->user_data, "lsr_cfg_if_getnext");

  if (i->index == 0) {
    index = 1;
  } else {
    index = i->index + 1;
  }

  mpls_lock_get(g->global_lock); /* LOCK */
  while (done == MPLS_BOOL_FALSE) {
    switch ((r = lsr_global_find_if_index(g, index, &iff))) {
      case MPLS_SUCCESS:
      case MPLS_END_OF_LIST:
        done = MPLS_BOOL_TRUE;
        break;
      case MPLS_FAILURE:
        break;
      default:
        MPLS_ASSERT(0);
    }
    index++;
  }
  mpls_lock_release(g->global_lock); /* UNLOCK */

  if (r == MPLS_SUCCESS) {
    i->index = iff->index;
    LDP_EXIT(g->user_data, "lsr_cfg_if_getnext");
    return lsr_cfg_if_get(g, i, flag);
  }
  LDP_EXIT(g->user_data, "lsr_cfg_if_getnext");
  return r;
}

mpls_return_enum lsr_cfg_if_test(mpls_cfg_handle handle, lsr_if * i,
  uint32_t flag)
{
  lsr_global *global = (lsr_global *) handle;
  lsr_if *iff = NULL;
  mpls_return_enum retval = MPLS_FAILURE;

  MPLS_ASSERT(global !=NULL && i != NULL);

  LDP_ENTER(global->user_data, "lsr_cfg_if_test");

  mpls_lock_get(global->global_lock); /* LOCK */

  if (!(flag & LSR_CFG_ADD)) {
    lsr_global_find_if_index(global, i->index, &iff);
  } else {
    retval = MPLS_SUCCESS;
    goto lsr_cfg_if_test_end;
  }

  retval = MPLS_SUCCESS;

lsr_cfg_if_test_end:
  mpls_lock_release(global->global_lock); /* UNLOCK */

  LDP_EXIT(global->user_data, "lsr_cfg_if_test");

  return retval;
}

mpls_return_enum lsr_cfg_if_set(mpls_cfg_handle handle, lsr_if * i, uint32_t flag)
{
  lsr_global *global = (lsr_global*)handle;
  lsr_if *iff = NULL;
  mpls_return_enum retval = MPLS_FAILURE;

  MPLS_ASSERT(global !=NULL && i != NULL);

  LDP_ENTER(global->user_data, "lsr_cfg_if_set");

  mpls_lock_get(global->global_lock); /* LOCK */

  if (flag & LSR_CFG_ADD) {
    if (mpls_if_handle_verify(global->ifmgr_handle, i->handle) ==
      MPLS_BOOL_FALSE) {
      /* ADDs must supply a valid handle */
      goto lsr_cfg_if_set_end;
    }
    if ((iff = lsr_if_create()) == NULL) {
      goto lsr_cfg_if_set_end;
    }

    /* ADDs must supply the handle */
    iff->handle = i->handle;
    _lsr_global_add_if(global, iff);

    /* return the newly allocated index to the user */
    i->index = iff->index;
  } else {
    if (flag & LSR_IF_CFG_BY_INDEX) {
      lsr_global_find_if_index(global, i->index, &iff);
    } else {
      iff = lsr_global_find_if_handle(global, i->handle);
    }
  }

  if ((iff == NULL) ||
      ((flag & LSR_IF_CFG_WHEN_DOWN) && (iff->oper_state == MPLS_OPER_UP)))
    goto lsr_cfg_if_set_end;

  if (LSR_IF_CFG_OPER_STATE & flag) {
    iff->oper_state = i->oper_state;
  }

  if (flag & LSR_CFG_DEL) {
    _lsr_global_del_if(global, iff);
    retval = MPLS_SUCCESS;
    goto lsr_cfg_if_set_end;
  }

  retval = MPLS_SUCCESS;

lsr_cfg_if_set_end:
  mpls_lock_release(global->global_lock); /* UNLOCK */

  LDP_EXIT(global->user_data, "lsr_cfg_if_set");

  return retval;
}

/******************* FTN **********************/

static void do_lsr_cfg_ftn_get(lsr_ftn * f, lsr_ftn *ftn, uint32_t flag)
{
  if (LSR_FTN_CFG_INDEX & flag) {
    f->index = ftn->index;
  }
  if (LSR_FTN_CFG_OUTSEGMENT & flag) {
    f->outsegment_index = ftn->outsegment->index;
  }
  if (LSR_FTN_CFG_FEC & flag) {
    memcpy(&f->fec,&ftn->fec,sizeof(mpls_fec));
  }
}

mpls_return_enum lsr_cfg_ftn_get(mpls_cfg_handle handle, lsr_ftn * f, uint32_t flag)
{
  lsr_global *global = (lsr_global *) handle;
  lsr_ftn *ftn = NULL;
  mpls_return_enum retval = MPLS_FAILURE;

  MPLS_ASSERT(global != NULL && f != NULL);

  LDP_ENTER(global->user_data, "lsr_cfg_ftn_get");

  mpls_lock_get(global->global_lock); /* LOCK */

  if (lsr_global_find_ftn_index(global, f->index, &ftn) != MPLS_SUCCESS)
      goto lsr_cfg_ftn_get_end;

  do_lsr_cfg_ftn_get(f, ftn, flag);

  retval = MPLS_SUCCESS;

lsr_cfg_ftn_get_end:
  mpls_lock_release(global->global_lock); /* UNLOCK */

  LDP_EXIT(global->user_data, "lsr_cfg_ftn_get");

  return retval;
}

mpls_return_enum lsr_cfg_ftn_get2(mpls_cfg_handle handle, lsr_ftn * f, uint32_t flag)
{
  lsr_global *global = (lsr_global *) handle;
  lsr_ftn *ftn = NULL;
  mpls_return_enum retval = MPLS_FAILURE;

  MPLS_ASSERT(global != NULL && f != NULL);

  LDP_ENTER(global->user_data, "lsr_cfg_ftn_get2");

  mpls_lock_get(global->global_lock); /* LOCK */

  /* f->fec and f->outsegment_index are used for lookup */
  if (lsr_global_find_ftn_index2(global, f, &ftn) != MPLS_SUCCESS)
      goto lsr_cfg_ftn_get_end2;

  do_lsr_cfg_ftn_get(f, ftn, flag);

  retval = MPLS_SUCCESS;

lsr_cfg_ftn_get_end2:
  mpls_lock_release(global->global_lock); /* UNLOCK */

  LDP_EXIT(global->user_data, "lsr_cfg_ftn_get2");

  return retval;
}

mpls_return_enum lsr_cfg_ftn_getnext(mpls_cfg_handle handle, lsr_ftn * f,
  uint32_t flag)
{
  lsr_global *g = (lsr_global *) handle;
  lsr_ftn *ftn = NULL;
  mpls_return_enum r = MPLS_FAILURE;
  mpls_bool done = MPLS_BOOL_FALSE;
  int index;

  LDP_ENTER(g->user_data, "lsr_cfg_ftn_getnext");

  if (f->index == 0) {
    index = 1;
  } else {
    index = f->index + 1;
  }

  mpls_lock_get(g->global_lock); /* LOCK */
  while (done == MPLS_BOOL_FALSE) {
    switch ((r = lsr_global_find_ftn_index(g, index, &ftn))) {
      case MPLS_SUCCESS:
      case MPLS_END_OF_LIST:
        done = MPLS_BOOL_TRUE;
        break;
      case MPLS_FAILURE:
        break;
      default:
        MPLS_ASSERT(0);
    }
    index++;
  }
  mpls_lock_release(g->global_lock); /* UNLOCK */

  if (r == MPLS_SUCCESS) {
    f->index = ftn->index;
    LDP_EXIT(g->user_data, "lsr_cfg_ftn_getnext");
    return lsr_cfg_ftn_get(g, f, flag);
  }
  LDP_EXIT(g->user_data, "lsr_cfg_ftn_getnext");
  return r;
}

mpls_return_enum lsr_cfg_ftn_test(mpls_cfg_handle handle, lsr_ftn * f,
  uint32_t flag)
{
  lsr_global *global = (lsr_global *) handle;
  lsr_ftn *ftn = NULL;
  mpls_return_enum retval = MPLS_FAILURE;

  MPLS_ASSERT(global !=NULL && f != NULL);

  LDP_ENTER(global->user_data, "lsr_cfg_ftn_test");

  mpls_lock_get(global->global_lock); /* LOCK */

  if (!(flag & LSR_CFG_ADD)) {
    lsr_global_find_ftn_index(global, f->index, &ftn);
  } else {
    retval = MPLS_SUCCESS;
    goto lsr_cfg_ftn_test_end;
  }

  retval = MPLS_SUCCESS;

lsr_cfg_ftn_test_end:
  mpls_lock_release(global->global_lock); /* UNLOCK */

  LDP_EXIT(global->user_data, "lsr_cfg_ftn_test");

  return retval;
}

static mpls_return_enum do_lsr_cfg_ftn_set(lsr_global *global, lsr_ftn * f, lsr_ftn *ftn, uint32_t flag)
{
  mpls_return_enum retval = MPLS_FAILURE;

  if (flag & LSR_CFG_DEL) {

    mpls_mpls_fec2out_del(global->mpls_handle, &ftn->fec,
      &ftn->outsegment->info);
    _lsr_global_del_ftn(global, ftn);

    return MPLS_SUCCESS;
  }

  if (LSR_FTN_CFG_OUTSEGMENT & flag) {
    lsr_outsegment *out = NULL;
    lsr_global_find_outsegment_index(global, f->outsegment_index, &out);
    if (!out) {
      return MPLS_FAILURE;
    }
    lsr_outsegment_add_ftn(out,ftn);
  }
  if (LSR_FTN_CFG_FEC & flag) {
    memcpy(&ftn->fec,&f->fec,sizeof(mpls_fec));
  }

  retval = MPLS_SUCCESS;

  if (flag & LSR_CFG_ADD) {
    retval = mpls_mpls_fec2out_add(global->mpls_handle,&ftn->fec,
      &ftn->outsegment->info);
    if (retval == MPLS_SUCCESS) {
      _lsr_global_add_ftn(global, ftn);
    }
  }
  return retval;
}

mpls_return_enum lsr_cfg_ftn_set(mpls_cfg_handle handle, lsr_ftn * f, uint32_t flag)
{
  lsr_global *global = (lsr_global*)handle;
  lsr_ftn *ftn = NULL;
  mpls_return_enum retval = MPLS_FAILURE;

  MPLS_ASSERT(global !=NULL && f != NULL);

  LDP_ENTER(global->user_data, "lsr_cfg_ftn_set");

  mpls_lock_get(global->global_lock); /* LOCK */

  if (flag & LSR_CFG_ADD) {
    if ((ftn = lsr_ftn_create()) == NULL) {
      goto lsr_cfg_ftn_set_end;
    }
    f->index = ftn->index;
  } else {
    lsr_global_find_ftn_index(global, f->index, &ftn);
  }

  if (ftn == NULL)
    goto lsr_cfg_ftn_set_end;

  MPLS_REFCNT_HOLD(ftn);
  retval = do_lsr_cfg_ftn_set(global, f, ftn, flag);
  MPLS_REFCNT_RELEASE(ftn,lsr_ftn_delete);

lsr_cfg_ftn_set_end:
  mpls_lock_release(global->global_lock); /* UNLOCK */

  LDP_EXIT(global->user_data, "lsr_cfg_ftn_set");

  return retval;
}

mpls_return_enum lsr_cfg_ftn_set2(mpls_cfg_handle handle, lsr_ftn * f, uint32_t flag)
{
  lsr_global *global = (lsr_global*)handle;
  lsr_ftn *ftn = NULL;
  mpls_return_enum retval = MPLS_FAILURE;

  MPLS_ASSERT(global !=NULL && f != NULL);

  LDP_ENTER(global->user_data, "lsr_cfg_ftn_set2");

  mpls_lock_get(global->global_lock); /* LOCK */

  if (flag & LSR_CFG_ADD) {
    if ((ftn = lsr_ftn_create()) == NULL) {
      goto lsr_cfg_ftn_set_end2;
    }
    f->index = ftn->index;
  } else {
    /* f->fec and f->outsegment_index are used for lookup */
    lsr_global_find_ftn_index2(global, f, &ftn);
  }

  if (ftn == NULL)
    goto lsr_cfg_ftn_set_end2;

  retval = do_lsr_cfg_ftn_set(global, f, ftn, flag);

lsr_cfg_ftn_set_end2:
  mpls_lock_release(global->global_lock); /* UNLOCK */

  LDP_EXIT(global->user_data, "lsr_cfg_ftn_set2");

  return retval;
}


/*
 *  Copyright (C) James R. Leu 2002
 *  jleu@mindspring.com
 *
 *  This software is covered under the LGPL, for more
 *  info check out http://www.gnu.org/copyleft/lgpl.html
 */

#ifndef _LSR_INSEGMENT_H_
#define _LSR_INSEGMENT_H_

#include "lsr_struct.h"

extern lsr_insegment *lsr_insegment_create();
extern void lsr_insegment_delete(lsr_insegment * i);
extern uint32_t _lsr_insegment_get_next_index();
extern mpls_return_enum lsr_insegment_add_xconnect(lsr_insegment *in, lsr_xconnect *x);
extern mpls_return_enum lsr_insegment_del_xconnect(lsr_insegment *in, lsr_xconnect *x);

#endif

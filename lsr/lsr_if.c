
/*
 *  Copyright (C) James R. Leu 2002
 *  jleu@mindspring.com
 *
 *  This software is covered under the LGPL, for more
 *  info check out http://www.gnu.org/copyleft/lgpl.html
 */

#include "lsr_struct.h"
#include "lsr_if.h"
#include "lsr_outsegment.h"

#include "mpls_assert.h"
#include "mpls_mm_impl.h"
#include "mpls_trace_impl.h"

uint32_t _lsr_if_next_index = 1;

lsr_if *lsr_if_create()
{
  lsr_if *i = (lsr_if *) mpls_malloc(sizeof(lsr_if));

  if (i) {
    memset(i, 0, sizeof(lsr_if));
    MPLS_REFCNT_INIT(i, 0);
    MPLS_LIST_ELEM_INIT(i, _global);
    MPLS_LIST_INIT(&i->outsegment_root,lsr_outsegment); 
    i->index = _lsr_if_get_next_index();
    i->oper_state = MPLS_OPER_DOWN;
  }
  return i;
}

void lsr_if_delete(lsr_if * i)
{
  // LSR_PRINT(g->user_data,"if delete\n");
  MPLS_REFCNT_ASSERT(i, 0);
  mpls_free(i);
}

uint32_t _lsr_if_get_next_index()
{
  uint32_t retval = _lsr_if_next_index;

  _lsr_if_next_index++;
  if (retval > _lsr_if_next_index) {
    _lsr_if_next_index = 1;
  }
  return retval;
}

mpls_return_enum lsr_if_add_outsegment(lsr_if *iff, lsr_outsegment *o) {
  if (iff && o) {
    MPLS_REFCNT_HOLD(o);
    MPLS_LIST_ADD_HEAD(&iff->outsegment_root, o, _if, lsr_outsegment);
    _lsr_outsegment_add_if(o, iff);
    return MPLS_SUCCESS;
  }
  return MPLS_FAILURE;
}

mpls_return_enum lsr_if_del_outsegment(lsr_if *iff, lsr_outsegment *o) {
  if (iff && o) {
    MPLS_LIST_REMOVE(&iff->outsegment_root, o, _if);
    _lsr_outsegment_del_if(o);
    MPLS_REFCNT_RELEASE(o, lsr_outsegment_delete);
    return MPLS_SUCCESS;
  }
  return MPLS_FAILURE;
}

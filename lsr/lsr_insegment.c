
/*
 *  Copyright (C) James R. Leu 2002
 *  jleu@mindspring.com
 *
 *  This software is covered under the LGPL, for more
 *  info check out http://www.gnu.org/copyleft/lgpl.html
 */

#include "lsr_struct.h"
#include "lsr_insegment.h"
#include "lsr_xconnect.h"

#include "mpls_assert.h"
#include "mpls_mm_impl.h"
#include "mpls_trace_impl.h"

uint32_t _lsr_insegment_next_index = 1;

lsr_insegment *lsr_insegment_create()
{
  lsr_insegment *i = (lsr_insegment *) mpls_malloc(sizeof(lsr_insegment));

  if (i) {
    memset(i, 0, sizeof(lsr_insegment));
    MPLS_REFCNT_INIT(i, 0);
    MPLS_LIST_ELEM_INIT(i, _outsegment);
    MPLS_LIST_ELEM_INIT(i, _global);
    MPLS_LIST_INIT(&i->xconnect_root,lsr_xconnect);
    i->index = _lsr_insegment_get_next_index();
  }
  return i;
}

void lsr_insegment_delete(lsr_insegment * i)
{
  // LSR_PRINT(g->user_data,"if delete\n");
  MPLS_REFCNT_ASSERT(i, 0);
  mpls_free(i);
}

uint32_t _lsr_insegment_get_next_index()
{
  uint32_t retval = _lsr_insegment_next_index;

  _lsr_insegment_next_index++;
  if (retval > _lsr_insegment_next_index) {
    _lsr_insegment_next_index = 1;
  }
  return retval;
}

mpls_return_enum lsr_insegment_add_xconnect(lsr_insegment *in, lsr_xconnect *x) {
  if (in && x) {
    MPLS_REFCNT_HOLD(x);
    MPLS_LIST_ADD_HEAD(&in->xconnect_root, x, _insegment, lsr_xconnect);
    _lsr_xconnect_add_insegment(x, in);
    return MPLS_SUCCESS;
  }
  return MPLS_FAILURE;
}

mpls_return_enum lsr_insegment_del_xconnect(lsr_insegment *in, lsr_xconnect *x) {
  if (in && x) {
    MPLS_LIST_REMOVE(&in->xconnect_root, x, _insegment);
    _lsr_xconnect_del_insegment(x);
    MPLS_REFCNT_RELEASE(x, lsr_xconnect_delete);
    return MPLS_SUCCESS;
  }
  return MPLS_FAILURE;
}

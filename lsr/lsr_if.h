
/*
 *  Copyright (C) James R. Leu 2002
 *  jleu@mindspring.com
 *
 *  This software is covered under the LGPL, for more
 *  info check out http://www.gnu.org/copyleft/lgpl.html
 */

#ifndef _LSR_IF_H_
#define _LSR_IF_H_

#include "lsr_struct.h"

extern lsr_if *lsr_if_create();
extern void lsr_if_delete(lsr_if * i);
extern uint32_t _lsr_if_get_next_index();
extern mpls_return_enum lsr_if_add_outsegment(lsr_if *iff, lsr_outsegment *out);
extern mpls_return_enum lsr_if_del_outsegment(lsr_if *iff, lsr_outsegment *out);

#endif

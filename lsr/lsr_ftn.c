
/*
 *  Copyright (C) James R. Leu 2002
 *  jleu@mindspring.com
 *
 *  This software is covered under the LGPL, for more
 *  info check out http://www.gnu.org/copyleft/lgpl.html
 */

#include "lsr_struct.h"
#include "lsr_ftn.h"
#include "lsr_outsegment.h"

#include "mpls_assert.h"
#include "mpls_mm_impl.h"
#include "mpls_trace_impl.h"

uint32_t _lsr_ftn_next_index = 1;

lsr_ftn *lsr_ftn_create()
{
  lsr_ftn *i = (lsr_ftn *) mpls_malloc(sizeof(lsr_ftn));

  if (i) {
    memset(i, 0, sizeof(lsr_ftn));
    MPLS_REFCNT_INIT(i, 0);
    MPLS_LIST_ELEM_INIT(i, _outsegment);
    MPLS_LIST_ELEM_INIT(i, _global);
    i->index = _lsr_ftn_get_next_index();
  }
  return i;
}

void lsr_ftn_delete(lsr_ftn * i)
{
  // LSR_PRINT(g->user_data,"if delete\n");
  MPLS_REFCNT_ASSERT(i, 0);
  mpls_free(i);
}

uint32_t _lsr_ftn_get_next_index()
{
  uint32_t retval = _lsr_ftn_next_index;

  _lsr_ftn_next_index++;
  if (retval > _lsr_ftn_next_index) {
    _lsr_ftn_next_index = 1;
  }
  return retval;
}

mpls_return_enum _lsr_ftn_add_outsegment(lsr_ftn * x, lsr_outsegment * o)
{
  if (x && o) {
    MPLS_REFCNT_HOLD(o);
    x->outsegment = o;
    return MPLS_SUCCESS;
  }
  return MPLS_FAILURE;
}

mpls_return_enum _lsr_ftn_del_outsegment(lsr_ftn * x)
{
  if (x && x->outsegment) {
    MPLS_REFCNT_RELEASE(x->outsegment, lsr_outsegment_delete);
    x->outsegment = NULL;
    return MPLS_SUCCESS;
  }
  return MPLS_FAILURE;
}

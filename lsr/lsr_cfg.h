
/*
 *  Copyright (C) James R. Leu 2002
 *  jleu@mindspring.com
 *
 *  This software is covered under the LGPL, for more
 *  info check out http://www.gnu.org/copyleft/lgpl.html
 */

#ifndef _LSR_CFG_H_
#define _LSR_CFG_H_

#include "lsr_struct.h"

#define LSR_CFG_ADD				0x00000001
#define LSR_CFG_DEL				0x10000000

#define LSR_GLOBAL_CFG_ADMIN_STATE		0x00000002

#define LSR_GLOBAL_CFG_WHEN_DOWN		(0)

#define LSR_OUTSEGMENT_CFG_INDEX		0x00000002
#define LSR_OUTSEGMENT_CFG_PUSH_LABEL		0x00000004
#define LSR_OUTSEGMENT_CFG_OWNER		0x00000008
#define LSR_OUTSEGMENT_CFG_INTERFACE		0x00000010
#define LSR_OUTSEGMENT_CFG_LABEL		0x00000020
#define LSR_OUTSEGMENT_CFG_NEXTHOP		0x00000040
#define LSR_OUTSEGMENT_CFG_WHEN_DOWN		(LSR_CFG_DEL|\
						LSR_OUTSEGMENT_CFG_PUSH_LABEL|\
						LSR_OUTSEGMENT_CFG_OWNER|\
						LSR_OUTSEGMENT_CFG_INTERFACE|\
						LSR_OUTSEGMENT_CFG_LABEL|\
						LSR_OUTSEGMENT_CFG_NEXTHOP)

#define LSR_INSEGMENT_CFG_INDEX			0x00000002
#define LSR_INSEGMENT_CFG_NPOP			0x00000004
#define LSR_INSEGMENT_CFG_FAMILY		0x00000008
#define LSR_INSEGMENT_CFG_LABELSPACE		0x00000010
#define LSR_INSEGMENT_CFG_LABEL			0x00000020
#define LSR_INSEGMENT_CFG_OWNER			0x00000040
#define LSR_INSEGMENT_CFG_WHEN_DOWN		(LSR_CFG_DEL|\
						LSR_INSEGMENT_CFG_NPOP|\
						LSR_INSEGMENT_CFG_FAMILY|\
						LSR_INSEGMENT_CFG_LABELSPACE|\
						LSR_INSEGMENT_CFG_LABEL|\
						LSR_INSEGMENT_CFG_OWNER)

#define LSR_XCONNECT_CFG_INDEX			0x00000002
#define LSR_XCONNECT_CFG_OUTSEGMENT		0x00000004
#define LSR_XCONNECT_CFG_INSEGMENT		0x00000008
#define LSR_XCONNECT_CFG_LSPID			0x00000010
#define LSR_XCONNECT_CFG_LABELSTACK		0x00000020
#define LSR_XCONNECT_CFG_ADMIN_STATE		0x00000040
#define LSR_XCONNECT_CFG_OWNER			0x00000080
#define LSR_XCONNECT_CFG_WHEN_DOWN		(LSR_CFG_DEL|\
						LSR_XCONNECT_CFG_OUTSEGMENT|\
						LSR_XCONNECT_CFG_INSEGMENT|\
						LSR_XCONNECT_CFG_LSPID|\
						LSR_XCONNECT_CFG_LABELSTACK|\
						LSR_XCONNECT_CFG_OWNER)

#define LSR_IF_CFG_INDEX			0x00000002
#define LSR_IF_CFG_OPER_STATE			0x00000004
#define LSR_IF_CFG_NAME				0x00000008
#define LSR_IF_CFG_BY_INDEX			0x00000010
#define LSR_IF_CFG_WHEN_DOWN			(LSR_CFG_DEL|\
						LSR_IF_CFG_NAME)

#define LSR_FTN_CFG_INDEX			0x00000002
#define LSR_FTN_CFG_OUTSEGMENT			0x00000004
#define LSR_FTN_CFG_FEC				0x00000008
#define LSR_FTN_CFG_WHEN_DOWN			(LSR_CFG_DEL|\
						LSR_FTN_CFG_FEC|\
						LSR_FTN_CFG_OUTSEGMENT)

extern mpls_cfg_handle lsr_cfg_open(mpls_instance_handle data);
extern void lsr_cfg_close(mpls_cfg_handle handle);

extern mpls_return_enum lsr_cfg_global_get(mpls_cfg_handle handle, lsr_global * g, uint32_t flag);
extern mpls_return_enum lsr_cfg_global_set(mpls_cfg_handle handle, lsr_global * g, uint32_t flag);

extern mpls_return_enum lsr_cfg_outsegment_get(mpls_cfg_handle handle, lsr_outsegment * i, uint32_t flag);
extern mpls_return_enum lsr_cfg_outsegment_getnext(mpls_cfg_handle handle, lsr_outsegment * i, uint32_t flag);
extern mpls_return_enum lsr_cfg_outsegment_test(mpls_cfg_handle handle, lsr_outsegment * i, uint32_t flag);
extern mpls_return_enum lsr_cfg_outsegment_set(mpls_cfg_handle handle, lsr_outsegment * i, uint32_t flag);


extern mpls_return_enum lsr_cfg_insegment_get(mpls_cfg_handle handle, lsr_insegment * i, uint32_t flag);
extern mpls_return_enum lsr_cfg_insegment_getnext(mpls_cfg_handle handle, lsr_insegment * i, uint32_t flag);
extern mpls_return_enum lsr_cfg_insegment_test(mpls_cfg_handle handle, lsr_insegment * i, uint32_t flag);
extern mpls_return_enum lsr_cfg_insegment_set(mpls_cfg_handle handle, lsr_insegment * i, uint32_t flag);


extern mpls_return_enum lsr_cfg_xconnect_get(mpls_cfg_handle handle, lsr_xconnect * x, uint32_t flag);
extern mpls_return_enum lsr_cfg_xconnect_get2(mpls_cfg_handle handle, lsr_xconnect *x, uint32_t flag);
extern mpls_return_enum lsr_cfg_xconnect_getnext(mpls_cfg_handle handle, lsr_xconnect * x, uint32_t flag);
extern mpls_return_enum lsr_cfg_xconnect_test(mpls_cfg_handle handle, lsr_xconnect * x, uint32_t flag);
extern mpls_return_enum lsr_cfg_xconnect_set(mpls_cfg_handle handle, lsr_xconnect * x, uint32_t flag);
extern mpls_return_enum lsr_cfg_xconnect_set2(mpls_cfg_handle handle, lsr_xconnect * x, uint32_t flag);


extern mpls_return_enum lsr_cfg_if_get(mpls_cfg_handle handle, lsr_if * i, uint32_t flag);
extern mpls_return_enum lsr_cfg_if_getnext(mpls_cfg_handle handle, lsr_if * i, uint32_t flag);
extern mpls_return_enum lsr_cfg_if_test(mpls_cfg_handle handle, lsr_if * i, uint32_t flag);
extern mpls_return_enum lsr_cfg_if_set(mpls_cfg_handle handle, lsr_if * i, uint32_t flag);

extern mpls_return_enum lsr_cfg_ftn_get(mpls_cfg_handle handle, lsr_ftn * i, uint32_t flag);
extern mpls_return_enum lsr_cfg_ftn_get2(mpls_cfg_handle handle, lsr_ftn * i, uint32_t flag);
extern mpls_return_enum lsr_cfg_ftn_getnext(mpls_cfg_handle handle, lsr_ftn * i, uint32_t flag);
extern mpls_return_enum lsr_cfg_ftn_test(mpls_cfg_handle handle, lsr_ftn * i, uint32_t flag);
extern mpls_return_enum lsr_cfg_ftn_set(mpls_cfg_handle handle, lsr_ftn * i, uint32_t flag);
extern mpls_return_enum lsr_cfg_ftn_set2(mpls_cfg_handle handle, lsr_ftn * i, uint32_t flag);

#endif


/*
 *  Copyright (C) James R. Leu 2002
 *  jleu@mindspring.com
 *
 *  This software is covered under the LGPL, for more
 *  info check out http://www.gnu.org/copyleft/lgpl.html
 */

#include "lsr_struct.h"
#include "lsr_outsegment.h"
#include "lsr_xconnect.h"
#include "lsr_ftn.h"
#include "lsr_if.h"

#include "mpls_assert.h"
#include "mpls_mm_impl.h"
#include "mpls_trace_impl.h"

uint32_t _lsr_outsegment_next_index = 1;

lsr_outsegment *lsr_outsegment_create()
{
  lsr_outsegment *i = (lsr_outsegment *) mpls_malloc(sizeof(lsr_outsegment));

  if (i) {
    memset(i, 0, sizeof(lsr_outsegment));
    MPLS_REFCNT_INIT(i, 0);
    MPLS_LIST_ELEM_INIT(i, _global);
    MPLS_LIST_ELEM_INIT(i, _if);
    MPLS_LIST_INIT(&i->xconnect_root,lsr_xconnect);
    MPLS_LIST_INIT(&i->ftn_root,lsr_ftn);
    i->index = _lsr_outsegment_get_next_index();
  }
  return i;
}

void lsr_outsegment_delete(lsr_outsegment * i)
{
  // LSR_PRINT(g->user_data,"if delete\n");
  MPLS_REFCNT_ASSERT(i, 0);
  mpls_free(i);
}

uint32_t _lsr_outsegment_get_next_index()
{
  uint32_t retval = _lsr_outsegment_next_index;

  _lsr_outsegment_next_index++;
  if (retval > _lsr_outsegment_next_index) {
    _lsr_outsegment_next_index = 1;
  }
  return retval;
}

mpls_return_enum lsr_outsegment_add_xconnect(lsr_outsegment *out, lsr_xconnect *x) {
  if (out && x) {
    MPLS_REFCNT_HOLD(x);
    MPLS_LIST_ADD_HEAD(&out->xconnect_root, x, _outsegment, lsr_xconnect);
    _lsr_xconnect_add_outsegment(x, out);
    return MPLS_SUCCESS;
  }
  return MPLS_FAILURE;
}

mpls_return_enum lsr_outsegment_del_xconnect(lsr_outsegment *out, lsr_xconnect *x) {
  if (out && x) {
    MPLS_LIST_REMOVE(&out->xconnect_root, x, _outsegment);
    _lsr_xconnect_del_outsegment(x);
    MPLS_REFCNT_RELEASE(x, lsr_xconnect_delete);
    return MPLS_SUCCESS;
  }
  return MPLS_FAILURE;
}

mpls_return_enum _lsr_outsegment_add_if(lsr_outsegment * o, lsr_if * iff)
{
  if (o && iff) {
    MPLS_REFCNT_HOLD(iff);
    o->iff = iff;
    return MPLS_SUCCESS;
  }
  return MPLS_FAILURE;
}

mpls_return_enum _lsr_outsegment_del_if(lsr_outsegment * o)
{
  if (o && o->iff) {
    MPLS_REFCNT_RELEASE(o->iff, lsr_if_delete);
    o->iff = NULL;
    return MPLS_SUCCESS;
  }
  return MPLS_FAILURE;
}

mpls_return_enum lsr_outsegment_add_ftn(lsr_outsegment *out, lsr_ftn *x) {
  if (out && x) {
    MPLS_REFCNT_HOLD(x);
    MPLS_LIST_ADD_HEAD(&out->ftn_root, x, _outsegment, lsr_ftn);
    _lsr_ftn_add_outsegment(x, out);
    return MPLS_SUCCESS;
  }
  return MPLS_FAILURE;
}

mpls_return_enum lsr_outsegment_del_ftn(lsr_outsegment *out, lsr_ftn *x) {
  if (out && x) {
    MPLS_LIST_REMOVE(&out->ftn_root, x, _outsegment);
    _lsr_ftn_del_outsegment(x);
    MPLS_REFCNT_RELEASE(x, lsr_ftn_delete);
    return MPLS_SUCCESS;
  }
  return MPLS_FAILURE;
}

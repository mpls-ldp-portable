
/*
 *  Copyright (C) James R. Leu 2002
 *  jleu@mindspring.com
 *
 *  This software is covered under the LGPL, for more
 *  info check out http://www.gnu.org/copyleft/lgpl.html
 */

#ifndef _LSR_DEFAULTS_H_
#define _LSR_DEFAULTS_H_

#define LDP_RANGE_DEF_LABEL_SPACE		0
#define LDP_RANGE_DEF_TYPE			LDP_GENERIC
#define LDP_RANGE_DEF_GEN_MIN			16
#define LDP_RANGE_DEF_GEN_MAX			((2^20)-1)
#define LDP_RANGE_DEF_ATM_VPI_MIN		0
#define LDP_RANGE_DEF_ATM_VPI_MAX		255
#define LDP_RANGE_DEF_ATM_VCI_MIN		33
#define LDP_RANGE_DEF_ATM_VCI_MAX		((2^12)-1)
#define LDP_RANGE_DEF_FR_MIN			23
#define LDP_RANGE_DEF_FR_10_MAX			1023
#define LDP_RANGE_DEF_FR_24_MAX			((24^2)-1)

#endif

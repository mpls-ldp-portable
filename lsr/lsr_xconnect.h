
/*
 *  Copyright (C) James R. Leu 2002
 *  jleu@mindspring.com
 *
 *  This software is covered under the LGPL, for more
 *  info check out http://www.gnu.org/copyleft/lgpl.html
 */

#ifndef _LSR_XCONNECT_H_
#define _LSR_XCONNECT_H_

#include "lsr_struct.h"

extern lsr_xconnect *lsr_xconnect_create();
extern void lsr_xconnect_delete(lsr_xconnect * i);
extern uint32_t _lsr_xconnect_get_next_index();
extern mpls_return_enum _lsr_xconnect_add_outsegment(lsr_xconnect * x, lsr_outsegment * o);
extern mpls_return_enum _lsr_xconnect_del_outsegment(lsr_xconnect * x);
extern mpls_return_enum _lsr_xconnect_add_insegment(lsr_xconnect * x, lsr_insegment * i);
extern mpls_return_enum _lsr_xconnect_del_insegment(lsr_xconnect * x);

#endif


/*
 *  Copyright (C) James R. Leu 2002
 *  jleu@mindspring.com
 *
 *  This software is covered under the LGPL, for more
 *  info check out http://www.gnu.org/copyleft/lgpl.html
 */

#ifndef _LSR_OUTSEGMENT_H_
#define _LSR_OUTSEGMENT_H_

#include "lsr_struct.h"

extern lsr_outsegment *lsr_outsegment_create();
extern void lsr_outsegment_delete(lsr_outsegment * i);
extern uint32_t _lsr_outsegment_get_next_index();
extern mpls_return_enum lsr_outsegment_add_xconnect(lsr_outsegment *out, lsr_xconnect *x);
extern mpls_return_enum lsr_outsegment_del_xconnect(lsr_outsegment *out, lsr_xconnect *x);
extern mpls_return_enum _lsr_outsegment_add_if(lsr_outsegment * o, lsr_if * iff);
extern mpls_return_enum _lsr_outsegment_del_if(lsr_outsegment * o);
extern mpls_return_enum lsr_outsegment_add_ftn(lsr_outsegment *out, lsr_ftn *x);
extern mpls_return_enum lsr_outsegment_del_ftn(lsr_outsegment *out, lsr_ftn *x);

#endif

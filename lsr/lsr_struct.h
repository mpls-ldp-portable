/*
 *  Copyright (C) James R. Leu 2002
 *  jleu@mindspring.com
 *
 *  This software is covered under the LGPL, for more
 *  info check out http://www.gnu.org/copyleft/lgpl.html
 */

#ifndef _LSR_STRUCT_H_
#define _LSR_STRUCT_H_

#include "mpls_list.h"
#include "mpls_refcnt.h"
#include "mpls_struct.h"

MPLS_LIST_ROOT(lsr_outsegment_list, lsr_outsegment);
MPLS_LIST_ROOT(lsr_insegment_list, lsr_insegment);
MPLS_LIST_ROOT(lsr_xconnect_list, lsr_xconnect);
MPLS_LIST_ROOT(lsr_ftn_list, lsr_ftn);
MPLS_LIST_ROOT(lsr_if_list, lsr_if);

typedef enum {
  LSR_TRACE_FLAG_INSEGMENT = 0x00000001,
  LSR_TRACE_FLAG_OUTSEGMENT = 0x00000002,
  LSR_TRACE_FLAG_XCONNECT = 0x00000003,
  LSR_TRACE_FLAG_DEBUG = 0x00000004,
} lsr_trace_flags;

#include "lsr_defaults.h"
#include "mpls_handle_type.h"

typedef struct lsr_insegment {
  MPLS_REFCNT_FIELD;
  MPLS_LIST_ELEM(lsr_outsegment) _outsegment;
  MPLS_LIST_ELEM(lsr_insegment) _global;
  struct lsr_xconnect_list xconnect_root;

  uint32_t index;
  struct mpls_insegment info;

} lsr_insegment;

typedef struct lsr_outsegment {
  MPLS_REFCNT_FIELD;
  MPLS_LIST_ELEM(lsr_outsegment) _global;
  MPLS_LIST_ELEM(lsr_outsegment) _if;
  struct lsr_xconnect_list xconnect_root;
  struct lsr_ftn_list ftn_root;

  uint32_t index;
  struct mpls_outsegment info;
  struct lsr_if *iff;

  /* only valid in sets and gets */
  uint32_t if_index;
} lsr_outsegment;

typedef struct lsr_xconnect {
  MPLS_REFCNT_FIELD;
  MPLS_LIST_ELEM(lsr_xconnect) _outsegment;
  MPLS_LIST_ELEM(lsr_xconnect) _insegment;
  MPLS_LIST_ELEM(lsr_xconnect) _global;

  uint32_t index;
  struct mpls_xconnect info;
  struct lsr_outsegment *outsegment;
  struct lsr_insegment *insegment;

  /* only valid in sets and gets */
  uint32_t outsegment_index;
  uint32_t insegment_index;
} lsr_xconnect;

typedef struct lsr_ftn {
  MPLS_REFCNT_FIELD;
  MPLS_LIST_ELEM(lsr_ftn) _outsegment;
  MPLS_LIST_ELEM(lsr_ftn) _global;

  uint32_t index;
  mpls_fec fec;
  struct lsr_outsegment *outsegment;

  /* only valid in sets and gets */
  uint32_t outsegment_index;
} lsr_ftn;

typedef struct lsr_if {
  MPLS_REFCNT_FIELD;
  MPLS_LIST_ELEM(lsr_if) _global;
  struct lsr_outsegment_list outsegment_root;

  uint32_t index;
  mpls_oper_state_enum oper_state;
  mpls_if_handle handle;

} lsr_if;

typedef struct lsr_global {
  struct lsr_outsegment_list outsegment;
  struct lsr_insegment_list insegment;
  struct lsr_xconnect_list xconnect;
  struct lsr_ftn_list ftn;
  struct lsr_if_list iff;

  mpls_lock_handle global_lock;
  mpls_instance_handle user_data;

  mpls_ifmgr_handle ifmgr_handle;
  mpls_mpls_handle mpls_handle;

  mpls_admin_state_enum admin_state;
} lsr_global;

#endif

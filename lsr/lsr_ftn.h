
/*
 *  Copyright (C) James R. Leu 2002
 *  jleu@mindspring.com
 *
 *  This software is covered under the LGPL, for more
 *  info check out http://www.gnu.org/copyleft/lgpl.html
 */

#ifndef _LSR_FTN_H_
#define _LSR_FTN_H_

#include "lsr_struct.h"

extern lsr_ftn *lsr_ftn_create();
extern void lsr_ftn_delete(lsr_ftn * i);
extern uint32_t _lsr_ftn_get_next_index();
extern mpls_return_enum _lsr_ftn_add_outsegment(lsr_ftn * x, lsr_outsegment * o);
extern mpls_return_enum _lsr_ftn_del_outsegment(lsr_ftn * x);

#endif

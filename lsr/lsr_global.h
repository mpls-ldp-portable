
/*
 *  Copyright (C) James R. Leu 2002
 *  jleu@mindspring.com
 *
 *  This software is covered under the LGPL, for more
 *  info check out http://www.gnu.org/copyleft/lgpl.html
 */

#ifndef _LSR_GLOBAL_H_
#define _LSR_GLOBAL_H_

#include "lsr_struct.h"

extern lsr_global *lsr_global_create(mpls_instance_handle data);
extern mpls_return_enum lsr_global_delete(lsr_global * g);
extern mpls_return_enum lsr_global_startup(lsr_global * g);
extern mpls_return_enum lsr_global_shutdown(lsr_global * g);

extern mpls_return_enum lsr_global_find_outsegment_index(lsr_global * g,
  uint32_t index, lsr_outsegment **);
extern mpls_return_enum _lsr_global_add_outsegment(lsr_global * g,
  lsr_outsegment * i);
extern mpls_return_enum _lsr_global_del_outsegment(lsr_global * g,
  lsr_outsegment * i);

extern mpls_return_enum lsr_global_find_insegment_index(lsr_global * g,
  uint32_t index, lsr_insegment **);
extern mpls_return_enum _lsr_global_add_insegment(lsr_global * g,
  lsr_insegment * i);
extern mpls_return_enum _lsr_global_del_insegment(lsr_global * g,
  lsr_insegment * i);

extern mpls_return_enum lsr_global_find_xconnect_index(lsr_global * g,
  uint32_t index, lsr_xconnect **);
extern mpls_return_enum lsr_global_find_xconnect_index2(lsr_global * g,
  uint32_t in, uint32_t out, lsr_xconnect **);
extern mpls_return_enum _lsr_global_add_xconnect(lsr_global * g,
  lsr_xconnect * i);
extern mpls_return_enum _lsr_global_del_xconnect(lsr_global * g,
  lsr_xconnect * i);

extern mpls_return_enum lsr_global_find_if_index(lsr_global * g,
  uint32_t index, lsr_if **);
extern mpls_return_enum _lsr_global_add_if(lsr_global * g, lsr_if * i);
extern mpls_return_enum _lsr_global_del_if(lsr_global * g, lsr_if * i);
extern lsr_if * lsr_global_find_if_handle(lsr_global * g, mpls_if_handle ifhandle);

extern mpls_return_enum lsr_global_find_ftn_index(lsr_global *,
  uint32_t, lsr_ftn **);
extern mpls_return_enum lsr_global_find_ftn_index2(lsr_global *,
  lsr_ftn *, lsr_ftn **);
extern void _lsr_global_add_ftn(lsr_global *, lsr_ftn *);
extern void _lsr_global_del_ftn(lsr_global *, lsr_ftn *);

#endif

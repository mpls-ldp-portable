What is this?
-------------
quagga-mpls.diff is a quagga<->ldp-portable porting layer.

What is "quagga"?
----------------
Quagga is a set of open source routing daemons which implement the
common routing protocols (RIP, OSPF, BGP).  Each protocol runs as a seperate
dameon, and they are all syncronized via a managment daemon (zebra).  If you
have never used quagga, go to http://quagga.net/ download the source for
0.99.6, compile it and learn how to setup up a small OSPF network using
quagga.  Once you have that running then you are ready try and use this patch.

What does this patch include?
-----------------------------
It handles MPLS labels as part of the quagga nexthop and correct
handling of MPLS labels as part of a recursive nexthop.

It implements a CLI for creating static LSPs (in the zebra daemon).

It implements a porting layer for the ldp-portable library so that LDP
interact seemlessly with the rest of the quagga routing software (in the ldpd
daemon).

How do I use this patch?
-----------------------
Apply quagga-mpls.diff to the quagga source distribution. (0.99.6)
Go into the quagga/ldpd directory and modify 'create-links' to point to
your ldp-portable source distribution.  Then execute 'create-links' .
Go back to the top of the quagga source tree and configure and compile
quagga according to it's directions.  If you are not running on a mpls-linux
enabled kernel, modify quagga/zebra/Makefile.am and change mpls-linux.o to
mpls_null.o.  If you have issues compiling and you are running a
mpls-linux-1.1x kernel most likely it can't find your
linux/include/linux/mpls.h file.   Go read the file 'README.1st.really'
in the mpls-linux-1.1 distrubution.

After you have finished compling, you will have a ldpd binary that has
the same command line syntax as the rest of the quagga binaries.

'ldpd' supports LDP, zebra supports staic LSPs.

(if you are not familiar with quagga hit the '?' key and it will provide
you context sensitive help.  Also look at 'mpls' at the interface level.
There are also 'show' commands availble from the operations more.  Look at
'show ldp ?' and 'show mpls ?' for a list of commands.)

The environment I use is:

zebra -d -A 127.0.0.1 -f /etc/quagga/zebra.conf
ospfd -d -A 127.0.0.1 -f /etc/quagga/ospfd.conf
ldpd -d -A 127.0.0.1 -f /etc/quagga/ldpd.conf

Look at quagga/ldpd/ldpd.conf.sample or the sample configs I have included
below, for a basic configuration example.

I have done most of my testing with unsolicited mode, liberal retention, label
merging, ordered control.  If you have success or failures with other modes
let me know.

Please post questions to the mpls-linux mailing list:

http://sourceforge.net/mail/?group_id=15443


LDP example config files
------------------------
NOTE:  The usage of the dummy interface with a /32 address is important!

--------------------------- zebra config ------------------------------
hostname uml-2
password root
enable password root
service advanced-vty
!
mpls static 0
 label-map gen 16 pop
 label-map gen 17 swap gen 18 nexthop eth2 11.0.2.3
!
interface lo
 description Loopback 
!
interface dummy0
 ip address 192.168.0.2/32
!
interface eth0
!
interface eth1
 mpls labelspace 0
 ip address 11.0.1.2/24
!
interface eth2
 mpls labelspace 0
 ip address 11.0.2.2/24
!
interface eth3
 mpls labelspace 0
 ip address 11.0.3.2/24
!
interface teql0
!
ip route 10.0.0.0/8 gen 19 nexthop eth2 11.0.2.3
!
line vty
 exec-timeout 0 0

--------------------------- ospfd config ------------------------------

hostname uml-2-ospf
password root
enable password root
!
!
!
interface lo
!
interface teql0
!
interface eth0
!
interface eth1
!
interface eth2
!
interface eth3
!
interface dummy0
!
router ospf
 passive-interface dummy0
 network 11.0.0.0/8 area 0
 network 192.168.0.2/32 area 0
!
line vty
 exec-timeout 0 0

--------------------------- ldpd config ------------------------------

hostname uml-2-mpls
password root
enable password root
!
mpls ldp
!
interface lo
!
interface eth0
!
interface eth1
 mpls ip
!
interface eth2
 mpls ip
!
interface eth3
 mpls ip
!
interface dummy0
!
line vty
 exec-timeout 0 0

-----------------------------------------------------------------------------

Static MPLS example
-------------------

Here is a simple example of how to use these new commands to configure a
static LSP and bind a route to it:

    Ingress LER
    ----------- 
    iler# conf t
    iler(config)# ip route 10.0.0.0/24 gen 16 nexthop eth1 192.168.2.1
    iler(config)# end
    iler# show mpls forwarding
    Insegments:

    Total 0

    Outsegments:
      Interface          Label Next Hop        Owner
      eth1                  16 192.168.2.1     STATIC
    Total 1

    Cross Connects:

    Total 0

    iler# show ip route 10.0.0.0/24
    Routing entry for 10.0.0.0/24
      Known via "static", distance 1, metric 0, best
      * 192.168.2.1, via eth1 (label 16)

    iler#

    LSR
    ---
    lsr# conf t
    lsr(config)# mpls static 0
    lsr(config-ls)# label-map gen 16 swap gen 17 nexthop eth1 192.168.3.1
    lsr(config-ls)# exit
    lsr(config)# int eth0
    lsr(config-if)# mpls labelspace 0
    lsr(config-if)# end
    lsr# show mpls forwarding
    Insegments:
      Lbl Spc  Label Owner
        0         16 STATIC
    Total 1

    Outsegments:
      Interface          Label Next Hop        Owner
      eth1                  17 192.168.3.1     STATIC
    Total 1

    Cross Connects:
      Lbl Spc  In Label Out Label Interface        Next Hop        Owner
        0            16        17 eth1             192.168.3.1     STATIC
    Total 1

    lsr# show interface eth0
    Interface eth0 is up, line protocol detection is disabled
      index 8 metric 1 mtu 1500 <UP,BROADCAST,RUNNING,MULTICAST>
      HWaddr: 00:05:5d:a6:e4:33
      MPLS labelspace 0
      inet 192.168.2.1/24 broadcast 192.168.2.255
      inet6 fe80::205:5dff:fea6:e433/64
        input packets 2737, bytes 1650440, dropped 0, multicast packets 0
        input errors 0, length 0, overrun 0, CRC 0, frame 0, fifo 0, missed 0
        output packets 2862, bytes 452923, dropped 0
        output errors 0, aborted 0, carrier 0, fifo 0, heartbeat 0, window 0
        collisions 0
    lsr#

    Egress LER
    ----------

    eler# conf t
    eler(config)# int eth0
    eler(config-if)# mpls labelspace 0
    eler(config-if)# exit
    eler(config)# mpls static 0
    eler(config-ls)# label-map gen 17 pop
    eler(config-ls)# end
    eler# show mpls forwarding
    Insegments:
      Lbl Spc  Label Owner
        0         17 STATIC
    Total 1

    Outsegments:

    Total 0

    Cross Connects:

    Total 0

    eler# show int eth0
    Interface eth0 is up, line protocol detection is disabled
      index 8 metric 1 mtu 1500 <UP,BROADCAST,MULTICAST>
      HWaddr: 00:05:5d:a6:e4:33
      MPLS labelspace 0
      inet 192.168.3.1/24 broadcast 192.168.3.255
      inet6 fe80::205:5dff:fea6:e433/64
        input packets 2759, bytes 1657918, dropped 0, multicast packets 0
        input errors 0, length 0, overrun 0, CRC 0, frame 0, fifo 0, missed 0
        output packets 2881, bytes 454579, dropped 0
        output errors 0, aborted 0, carrier 0, fifo 0, heartbeat 0, window 0
        collisions 0
    eler#

------------------------------------------------------------------------------

Copyright by James R. Leu <jleu@mindspring.com> 1999-2008
